<?php

use App\Category;
use App\Page;
use App\Product;
use App\Subproduct;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Event::listen('illuminate.query', function($query)
//{
//    var_dump($query);
//});
Route::get('qbo/oauth', 'QuickBookController@qboOauth');
Route::get('qbo/success', 'QuickBookController@qboSuccess');
Route::get('qbo/disconnect', 'QuickBookController@qboDisconnect');


Route::get('/', 'HomeController@index');
Route::group(['middleware' => 'auth'], function () {

    Route::get('category/new-arrivals', 'NewarrivalsController@show');
    Route::get('category/{slug}', 'CategoryController@show');
    Route::get('product/{slug}', 'ProductController@show');
    Route::post('/cart', 'CartController@cartadd');
    Route::post('/complete', 'CheckoutController@complete');

    Route::get('/cart', 'CartController@cart');

    Route::resource('add-client',  'AddClientController');

    Route::get('/checkout', 'CheckoutController@checkout');

    Route::get('/test', 'CheckoutController@test');


    Route::get('/cartIncDec', 'CartController@cartIncDec');

    Route::get('/cartDelete', 'CartController@cartDelete');

    Route::get('/clear-cart', 'CartController@clearcart');

    #Route::get('/complete', 'CheckoutController@complete');


    Route::get('/hide', 'PricesController@hide');
    Route::get('/wholesale', 'PricesController@wholesale');
    Route::get('/rrp', 'PricesController@rrp');
// Edit company info...
    Route::get('editprofile', 'ProfileController@index');
    Route::post('editprofile', 'ProfileController@update');


});

//Route::group(['middleware' => 'auth'],  function () {
//    Route::get('/Training_room', function () {
//        return view('training_room')->with('training', \App\TrainingRoom::find(0))->with('settings', Settings::find(0));
//    });
//});

// See App\Http\Kernel that has middleware AuthAdmin.
//
// We have two levels of validation. middleware auth is the standard 'Are you logged in', used above for the training room page.
// middleware admin uses Entrust to check that the logged in user has a role of 'Admin'.  See AuthAdmin.php for details.
// ;
Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'admin']], function () {

    Route::get('makethumbnailsfromexisting', 'admin\ImagesController@makethumbnailsfromexisting');

    Route::resource('/', 'admin\DashboardController');

    Route::resource('user', 'admin\UserController');

    Route::resource('manage', 'admin\DashboardController');

    Route::resource('dashboard', 'admin\DashboardController');

    Route::get('mappoints', 'admin\MapController@mappoints');

    Route::resource('map', 'admin\MapController');

    Route::resource('settings', 'admin\SettingsController');

    Route::post('type', 'admin\UserController@type');

    Route::post('filterstock',   'admin\StockController@filterstock');
    Route::get('stock',    'admin\StockController@index');
    Route::post('savereorder', 'admin\StockController@savereorder');

    Route::get('quantity', 'admin\QuantityController@index');
    Route::post('quantitychange',   'admin\QuantityController@quantitychange');
    Route::get('quantity/{repositionid}',    'admin\QuantityController@index');

    Route::post('quantity', 'admin\QuantityController@update');

    Route::post('quantityreorder', ['as' => 'quantityreorder', 'uses' => 'admin\ReorderController@quantity']);
    Route::post('updatequantityreorder', ['as' => 'updatequantityreorder', 'uses' => 'admin\ReorderController@update']);
//    Route::get('reorder', 'admin\ReorderController@index');




    // Liam Goldstein
    // New AJAX routes.

    Route::post('/api/quantity', 'admin\QuantityController@updateAJAX');
    Route::post('/api/savereorder', 'admin\StockController@savereorderAJAX');




    Route::resource('pages', 'admin\PagesController');

    Route::resource('homepage', 'admin\HomepageController');

    Route::resource('product', 'admin\ProductController');

    Route::post('delete_order', 'admin\OrderController@destroy');
    Route::resource('orders', 'admin\OrderController');
    Route::get('order/renew/{id}', 'admin\OrderController@renew');

    Route::resource('changecustomer', 'admin\ChangecustomerController');

    Route::post('updatecustomer', ['uses' => 'admin\ChangecustomerController@updatecustomer', 'as' => 'admin.updatecustomer'] );

    Route::post('invoice/extra/{id}',       'admin\InvoiceController@extra' );
    Route::get('generate_invoice/{id}',     'admin\InvoiceController@generate_invoice');
    Route::get('email_invoice/{id}',        'admin\InvoiceController@email_invoice');
    Route::get('view_invoice/{id}',         'admin\InvoiceController@view_invoice');
    Route::get('invoice/delete/{id}',       'admin\InvoiceController@order_delete');
    Route::post('order_deleteitem',         'admin\InvoiceController@order_deleteitem');

    //Route::get('images/{id}',               'admin\ImagesController@index');
    Route::post('imagecategory',            'admin\ImagesController@selectcat');
    Route::get('imagecategory',             'admin\ImagesController@index');
    Route::post('saveimage',                'admin\ImagesController@saveimage');

    Route::get('restoreimage/{id}',              'admin\ImagesController@restoreimage');
    Route::get('makecanvasbigger/{id}',         'admin\ImagesController@makecanvasbigger');

    Route::get('images/{id}/edit',          ['uses' => 'admin\ImagesController@edit', 'as' => 'admin.images.edit']);
    Route::resource('images',               'admin\ImagesController');

    //Route::resource('orderdetail', 'admin\OrderdetailController');

    Route::get('subproduct/{capital}', 'admin\SubproductController@index');

    Route::get('subproduct/create/{id}', 'admin\SubproductController@create');

    Route::resource('subproduct', 'admin\SubproductController');

    Route::resource('subproductdelete', 'admin\SubproductController');

    Route::get('/view_reorder/{id}',         'admin\StockController@view_reorder');

    Route::get('missing',   'admin\StockController@missing');
    Route::get('missingdownload',         'admin\StockController@view_missing');
    Route::get('stockreport', 'admin\StockController@stockreport');
    Route::get('stockreportdownload', 'admin\StockController@stockreportdownload');

    Route::get('recalc', 'admin\OrderController@recalc');
});




Route::get('/contact', function () {


    //This code gets "New Arrivals" from Product & Subproduct, puts them in a multidimensional array and then sorts them by date
    $product = Product::where('category_id', '=', 7)
        ->where('live', 1)
        ->get();

    $sub = Subproduct::with('product')->where('newarrival', 1)->where('splive', 1)
        ->orderBy('created_at', 'DESC')
        ->LIMIT(26)
        ->get();

    $newarrivals = array();
    foreach ($product as $p) {
        $newarrivals[] = array("image" => $p->image, "slug" => $p->slug, "sku" => $p->sku, "date" => $p->updated_at);

    }

    foreach ($sub as $p) {
        $newarrivals[] = array("image" => $p->image, "slug" => $p->product->slug, "sku" => $p->sku, "date" => $p->updated_at);
    }

    $date = array();
    foreach ($newarrivals as $key => $row) {
        $date[$key] = $row['date'];
    }
    array_multisort($date, SORT_DESC, $newarrivals);


    return View::make('contact')->with('newarrivals', $newarrivals)->with('category', Category::with('products')->orderBy('id')->get());
});

//Route::post('enquiry/', array(
//
//    'uses' => 'App\Http\Controllers\Api\v1\ContactUsController@postEnquiryForm'));



//Route::controllers([
////    'auth' => 'Auth\AuthController',
//    'password' => 'Auth\PasswordController',
//]);


// Authentication routes...     AuthenticatesUsers.php
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');



// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');



// Single page

Route::get('{slug}', array('as' => 'page', function($slug)
{
    $page = Page::where('slug', $slug)->first();

    //This code gets "New Arrivals" from Product & Subproduct, puts them in a multidimensional array and then sorts them by date
    $product = Product::where('category_id', '=', 7)
        ->where('live', 1)
        ->get();

    $sub = Subproduct::with('product')->where('newarrival', 1)->where('splive', 1)
        ->orderBy('created_at', 'DESC')
        ->LIMIT(26)
        ->get();

    $newarrivals = array();
    foreach ($product as $p) {
        $newarrivals[] = array("image" => $p->image, "slug" => $p->slug, "sku" => $p->sku, "date" => $p->updated_at);

    }

    foreach ($sub as $p) {
        $newarrivals[] = array("image" => $p->image, "slug" => $p->product->slug, "sku" => $p->sku, "date" => $p->updated_at);
    }

    $date = array();
    foreach ($newarrivals as $key => $row) {
        $date[$key] = $row['date'];
    }
    array_multisort($date, SORT_DESC, $newarrivals);


    if ( ! $page) App::abort(404, 'Page not found');

    $category = Category::with('products')
        ->with(array('products' => function ($query) {
            $query->where('live', '=', 1);
        }))
        ->orderBy('sort_order')->get();

    return View::make('page')->with('page', $page)->with('newarrivals', $newarrivals)->with('category', $category);

}))->where('slug', '^((?!admin).)*$');

// 404 Page
//App::missing(function($exception)
//{
//    return Response::view('404', array(), 404);
//});


