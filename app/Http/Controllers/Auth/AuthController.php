<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\User;
use App\Address;
use App\Role;
use Validator;
use Illuminate\Http\Request;

use App\Mailers\WelcomeMailer as WelcomeMailer;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Session;



class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $redirectTo = '/admin/dashboard';

    protected $welcomeMailer;
    // See here http://laraveldaily.com/auth-login-with-username-instead-of-email/
    protected $username = 'username';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(WelcomeMailer $welcomeMailer)
    {
        $this->middleware('guest', ['except' => 'getLogout']);

        $this->welcomeMailer = $welcomeMailer;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'username' => 'required|max:255|unique:users',
            'address1' => 'required|max:255',
            'town' => 'required|max:255',
            'county' => 'required|max:255',
            'postcode' => 'required|max:12',
            'phone' => 'required|max:20',
            'vat' => 'max:255',
            'companyID' => 'max:255',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {

        $this->redirectTo = '/auth/logout';

        $user = User::create([
            'name2' => $data['name2'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'username' => $data['username'],
            'confirmed' => 1,
        ]);

        $address = new Address;
        $address->address1 = $data['address1'];
        $address->address2 = $data['address2'];
        $address->town = $data['town'];
        $address->county = $data['county'];
        $address->postcode = $data['postcode'];
        $address->phone = $data['phone'];
        $address->vat = $data['vat'];
        $address->companyID = $data['companyID'];
        $address->user_id = $user->id;

        $address->save();

        // Find the user just inserted.
        $insertedId = $user->id;
        $user = User::find($insertedId);

        //Find the Guest role
        $guest = Role::find(20);

        //Attach the role to the guest.
        $user->attachRole($guest);

        //Send Welcome email. Simon to confirm

        // Send Email to info@.

        $messagetext = "<br/>Thank you for registering with Simon Alexander.
        Your request has been sent to Simon and he will be in touch shortly with your login details";

        $data = array(
            'messagetext' =>$messagetext,
            'name'	=> $data['name'],
            'email' => $data['email'],
        );

        $subject = "Thank you for registering with Simon Alexander";

        // Send to Admin
        if (\App::environment('local')) {
            $result = $this->welcomeMailer->welcome(\Config::get('custom_config.admin_email_local'), $data, $subject);
        } else{
            $result = $this->welcomeMailer->welcome(\Config::get('custom_config.admin_email'), $data, $subject);
        }

        // Send to Client

        $this->welcomeMailer->welcome($data['email'], $data, $subject);


        return $user;
    }

    public function postLogin(Request $request)
    {

        $this->validate($request, [
            $this->loginUsername() => 'required', 'password' => 'required',
        ]);



        if (Auth::validate(['username' => $request->username, 'password' => $request->password, 'live' => 1])) {
//            return redirect($this->loginPath())
//                ->withInput($request->only('username', 'remember'))
//                ->withErrors('Your account is Inactive or not verified '. strtolower($request->username));
            return redirect('auth/login')
                ->withInput($request->only('email','username', 'remember'))
                ->withErrors([
                    'email' => 'Incorrect email address or password',
                ]);
        }

        $credentials = array('username' => $request->username, 'password' => $request->password);

        if (Auth::attempt($credentials, $request->has('remember'))) {

            $user = User::find(Auth::user()->id);

            $user->logins = $user->logins + 1;

            $user->save();


            return redirect()->intended($this->redirectPath());
        }

        //return redirect($this->loginPath())
        return redirect('auth/login')
            ->withInput($request->only('email', 'remember'))
            ->withErrors([
                'email' => 'Incorrect email address or password',
            ]);
    }

//    public function postLogin(Request $request)
//    {
//        $this->validate($request, [
//            $this->loginUsername() => 'required', 'password' => 'required',
//        ]);
//
//        // If the class is using the ThrottlesLogins trait, we can automatically throttle
//        // the login attempts for this application. We'll key this by the username and
//        // the IP address of the client making these requests into this application.
//        $throttles = $this->isUsingThrottlesLoginsTrait();
//
//        if ($throttles && $this->hasTooManyLoginAttempts($request)) {
//            return $this->sendLockoutResponse($request);
//        }
//
//        $credentials = $this->getCredentials($request);
//
//        if (Auth::attempt($credentials, $request->has('remember'))) {
//            $user = User::find(Auth::user()->id);
//
//            $user->logins = $user->logins + 1;
//
//            $user->save();
//            return $this->handleUserWasAuthenticated($request, $throttles);
//
//        } else {
//
//            $credentials['username'] = strtolower($credentials['username']);
//            $credentials['username'] = str_replace('.', '', $credentials['username']);
//            if (Auth::attempt($credentials, $request->has('remember'))) {
//                $user = User::find(Auth::user()->id);
//
//                $user->logins = $user->logins + 1;
//
//                $user->save();
//
//                return $this->handleUserWasAuthenticated($request, $throttles);
//            }
//        }
//
//        // If the login attempt was unsuccessful we will increment the number of attempts
//        // to login and redirect the user back to the login form. Of course, when this
//        // user surpasses their maximum number of attempts they will get locked out.
//        if ($throttles) {
//            $this->incrementLoginAttempts($request);
//        }
//
//        return redirect($this->loginPath())
//            ->withInput($request->only($this->loginUsername(), 'remember'))
//            ->withErrors([
//                $this->loginUsername() => $this->getFailedLoginMessage(),
//            ]);
//    }
//
//    protected function getCredentials(Request $request)
//    {
//        return [
//            'username' => $request->input('username'),
//            'password' => $request->input('password'),
//            'live' => 0
//        ];
//    }

    public function getLogout()
    {

        Session::flush();

        \Auth::logout();

        return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');
    }



}
