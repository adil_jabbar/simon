<?php namespace App\Http\Controllers;

use DB;
//use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

use Validator;
use App\User;
use App\Address;
use Illuminate\Support\Facades\Auth;
use App\Category;
use Krucas\Notification\Facades\Notification;

class ProfileController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Profile Controller
    |--------------------------------------------------------------------------
    |
    */

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function index () {
        $user = User::with('address')->find(Auth::user()->id);
        return view('auth.edit')->withUser($user);
    }
    public function edit($id)

    {
        $user = User::with('address')->find(Auth::user()->id);
        return view('auth.edit')->withUser($user);
    }

    public function update(Request $request)
    {
        $user = User::with('address')->find(Auth::user()->id);
        if ($request->input('username') != $user->username)
        {
            $rules = array(
                'username' => 'required|max:255|unique:users',
                'email' => 'required|email|max:255',
                'password' => 'confirmed|min:6',
//            'username' => 'required|max:255',
                'address.address1' => 'required|max:255',
                'address.town' => 'required|max:255',
                'address.county' => 'required|max:255',
                'address.postcode' => 'required|max:12',
                'address.phone' => 'required|max:20',
                'vat' => 'max:255',
                'companyID' => 'max:255',
            );
        } else {
            $rules = array(
                'email' => 'required|email|max:255',
                'password' => 'confirmed|min:6',
//            'username' => 'required|max:255',
                'address.address1' => 'required|max:255',
                'address.town' => 'required|max:255',
                'address.county' => 'required|max:255',
                'address.postcode' => 'required|max:12',
                'address.phone' => 'required|max:20',
                'vat' => 'max:255',
                'companyID' => 'max:255',
            );
        }




        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('editprofile')
                ->withErrors($validator)
                ->withInput();
        }

        $user = User::with('address')->find(Auth::user()->id);
// this 'fills' the user model with all fields of the Input that are fillable
        $user->fill(\Input::all());

        if (strlen($request->input('password') > 6)) {
            $user->password = bcrypt($request->input('password'));
        }

        $user->username = strtolower($request->input('username'));
        $user->name2 = $request->input('name2');

        $user->address->address1 = $request->input('address.address1');
        $user->address->address1 = $request->input('address.address1');
        $user->address->address2 = $request->input('address.address2');
        $user->address->address1 = $request->input('address.address1');
        $user->address->town = $request->input('address.town');
        $user->address->county = $request->input('address.county');
        $user->address->postcode = $request->input('address.postcode');
        $user->address->phone = $request->input('address.phone');
        $user->address->vat = $request->input('address.vat');
        $user->address->companyID = $request->input('address.companyID');
        $user->save(); // no validation implemented

        return view('auth.edit')->with('user', $user);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }


}
