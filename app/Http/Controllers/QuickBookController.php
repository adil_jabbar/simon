<?php

namespace App\Http\Controllers;

use App\Subproduct;
use Illuminate\Http\Request;

use App\Postage;
use App\Http\Requests;
use App\Http\Controllers\Controller;


class QuickBookController extends Controller
{

    private $IntuitAnywhere;
    private $context;
    private $realm;

    public function __construct()
    {
        if (!\QuickBooks_Utilities::initialized(env('QBO_DSN'))) {
            // Initialize creates the neccessary database schema for queueing up requests and logging
            \QuickBooks_Utilities::initialize(env('QBO_DSN'));
        }
        // $this->IntuitAnywhere = new \QuickBooks_IPP_IntuitAnywhere(
        //     env('QBO_DSN'), 
        //     env('QBO_ENCRYPTION_KEY'),
        //     env('QBO_OAUTH_CONSUMER_KEY'), 
        //     env('QBO_CONSUMER_SECRET'), 
        //     env('QBO_OAUTH_URL'), 
        //     env('QBO_SUCCESS_URL'));

        $this->IntuitAnywhere = new \QuickBooks_IPP_IntuitAnywhere(
            \QuickBooks_IPP_IntuitAnywhere::OAUTH_V2, 
            env('QBO_SANDBOX'), 
            'com.intuit.quickbooks.accounting ', 
            env('QBO_DSN'), 
            env('QBO_ENCRYPTION_KEY'), 
            env('QB_CLIENT_ID'), 
            env('QB_CLIENT_SECRET'), 
            env('QBO_OAUTH_URL'), 
            env('QBO_SUCCESS_URL'));
    }

    public function qboConnect()
    {
        // v1
        // if ( $this->IntuitAnywhere->check(env('QBO_TENANT')) && $this->IntuitAnywhere->test(env('QBO_USERNAME'), env('QBO_TENANT'))) {
        if ( $this->IntuitAnywhere->check(env('QBO_TENANT')) && $this->IntuitAnywhere->test(env('QBO_TENANT'))) {
            // Set up the IPP instance

            // v1
            // $IPP = new \QuickBooks_IPP(env('QBO_DSN'));
            $IPP = new \QuickBooks_IPP(env('QBO_DSN'), env('QBO_ENCRYPTION_KEY'));

            // Get our OAuth credentials from the database
            // v1
            // $creds = $this->IntuitAnywhere->load(env('QBO_USERNAME'), env('QBO_TENANT'));
            $creds = $this->IntuitAnywhere->load(env('QBO_TENANT'));

            // Tell the framework to load some data from the OAuth store
            // v1 
            // $IPP->authMode(
            //     \QuickBooks_IPP::AUTHMODE_OAUTH,
            //     env('QBO_USERNAME'),
            //     $creds);


            $IPP->authMode(
                \QuickBooks_IPP::AUTHMODE_OAUTHV2,
                $creds);

            if (env('QBO_SANDBOX')) {
                // Turn on sandbox mode/URLs
                $IPP->sandbox(true);
            }
            // This is our current realm
            $this->realm = $creds['qb_realm'];
            // Load the OAuth information from the database
            $this->context = $IPP->context();

            return true;
        } else {
            return false;
        }
    }

    public function qboOauth()
    {
        $oauth_state = md5(microtime(true));

        // v1
        // if ($this->IntuitAnywhere->handle(env('QBO_USERNAME'), env('QBO_TENANT'))) {
        if ($this->IntuitAnywhere->handle(env('QBO_TENANT'), $oauth_state )) {
            ; // The user has been connected, and will be redirected to QBO_SUCCESS_URL automatically.
        } else {
            // If this happens, something went wrong with the OAuth handshake
            die('Oh no, something bad happened: ' . $this->IntuitAnywhere->errorNumber() . ': ' . $this->IntuitAnywhere->errorMessage());
        }
    }

    public function qboSuccess()
    {
        return view('qbo_success');
    }

    public function qboDisconnect()
    {
        // v1 - no longer needed
        // $this->IntuitAnywhere->disconnect(env('QBO_USERNAME'), env('QBO_TENANT'), true);
        return redirect()->intended("/admin/orders");// afer disconnect redirect where you want
    }

    public function createCustomer($data)
    {

        $this->qboConnect();

        $CustomerService = new \QuickBooks_IPP_Service_Customer();

        $Customer = new \QuickBooks_IPP_Object_Customer();
        $Customer->setTitle('Mr');
//        $Customer->setGivenName('Paul');
//        $Customer->setMiddleName('A');
        $Customer->setFamilyName($data['companyName']);
        $Customer->setDisplayName($data['companyName']);

        // Terms (e.g. Net 30, etc.)
        $Customer->setSalesTermRef(3);

//        // Phone #
        $PrimaryPhone = new \QuickBooks_IPP_Object_PrimaryPhone();
        $PrimaryPhone->setFreeFormNumber($data['phone']);
        $Customer->setPrimaryPhone($PrimaryPhone);

//        // Mobile #
//        $Mobile = new \QuickBooks_IPP_Object_Mobile();
//        $Mobile->setFreeFormNumber('860-532-0089');
//        $Customer->setMobile($Mobile);

        // Fax #
//        $Fax = new \QuickBooks_IPP_Object_Fax();
//        $Fax->setFreeFormNumber('860-532-0089');
//        $Customer->setFax($Fax);

        // Bill address
        $BillAddr = new \QuickBooks_IPP_Object_BillAddr();
        $BillAddr->setLine1($data['addr1']);
        $BillAddr->setLine2($data['addr2']);
        $BillAddr->setCity($data['city']);
        $BillAddr->setCounty($data['county']);
        $BillAddr->setPostalCode($data['postcode']);
        $Customer->setBillAddr($BillAddr);

        // Email
        $PrimaryEmailAddr = new \QuickBooks_IPP_Object_PrimaryEmailAddr();
        $PrimaryEmailAddr->setAddress($data['email']);
        $Customer->setPrimaryEmailAddr($PrimaryEmailAddr);

        if ($resp = $CustomerService->add($this->context, $this->realm, $Customer)) {
            //print('Our new customer ID is: [' . $resp . '] (name "' . $Customer->getDisplayName() . '")');
            //return $resp;
            //echo $resp;exit;
            //$resp = str_replace('{','',$resp);
            //$resp = str_replace('}','',$resp);
            //$resp = abs($resp);
            return $this->getId($resp);
        } else {
            //echo 'Not Added qbo';
            //print($CustomerService->lastError($this->context));
            return ['error',$CustomerService->lastError($this->context )] ;
        }
    }

    public function addItem()
    {

        $this->qboConnect();
        $ItemService = new \QuickBooks_IPP_Service_Item();

        $Item = new \QuickBooks_IPP_Object_Item();

        $Item->setName('My Item');
        $Item->setType('Inventory');
        $Item->setIncomeAccountRef('53');

        if ($resp = $ItemService->add($this->context, $this->realm, $Item)) {
            return $this->getId($resp);
        } else {
            print($ItemService->lastError($this->context));
        }
    }

    public function addInvoice($invoiceArray, $orders, $customerRef, $agent, $data)
    {
        $this->qboConnect();

        $InvoiceService = new \QuickBooks_IPP_Service_Invoice();

        $itemArray = $orders->orderitems;

        $Invoice = new \QuickBooks_IPP_Object_Invoice();
        if($orders->is_yg == '1') {
            $Invoice->setDocNumber('WEB_YG10' . $orders->id);
        } else {
            $Invoice->setDocNumber('WEB10' . $orders->id);
        }

        if ($agent) {

            $CustomField = new \QuickBooks_IPP_Object_CustomField();
            $CustomField->setDefinitionId(1);
            $CustomField->setName('Agent');
            $CustomField->setType('StringType');
            $CustomField->setStringValue($agent);
            $Invoice->addCustomField($CustomField);
        }
        //$Invoice->setTxnDate(\Carbon::createFromFormat('Y-m-d H:i:s', \Carbon::now()))->format('Y-m-d');

//        $x = 0;
        foreach ($itemArray as $orderitem) {
//            $x++;

                $Line = new \QuickBooks_IPP_Object_Line();

                $subproduct = Subproduct::with('product')->find($orderitem->subproduct_id);

                $Line->setDetailType('SalesItemLineDetail');
                $Line->setAmount($orderitem->value);

                $addDesc = '';
                if($orderitem->is_yg == '1') {
                    $addDesc = ' (Y/G)';
                } else {
                    $addDesc = ' (R/G)';
                }
                $Line->setDescription($subproduct->product->productName . ' ' . $subproduct->sku . $addDesc);


                $SalesItemLineDetail = new \QuickBooks_IPP_Object_SalesItemLineDetail();

                $SalesItemLineDetail->setItemRef(4);
            if ($orderitem->qty > 0) {
                $SalesItemLineDetail->setUnitPrice($orderitem->value / $orderitem->qty);
            } else {
                $SalesItemLineDetail->setUnitPrice(0);
            }
                $SalesItemLineDetail->setQty($orderitem->qty);
                $SalesItemLineDetail->setTaxCodeRef(2);

                $Line->addSalesItemLineDetail($SalesItemLineDetail);
                $Invoice->addLine($Line);

        }

        $Line = new \QuickBooks_IPP_Object_Line();


        $Line->setDetailType('SalesItemLineDetail');
        $Line->setAmount($orders->postage);
        $Line->setDescription('Postage and Package');

        $SalesItemLineDetail = new \QuickBooks_IPP_Object_SalesItemLineDetail();
        $SalesItemLineDetail->setItemRef(5);
        $SalesItemLineDetail->setUnitPrice($orders->postage);
        $SalesItemLineDetail->setQty(1);
        //TaxCodeRef 2 = 20%, value of 3 equals exempt
        $SalesItemLineDetail->setTaxCodeRef(2);

        $Line->addSalesItemLineDetail($SalesItemLineDetail);
        $Invoice->addLine($Line);

        // Try adding a Billing Email Address

        $Line = new \QuickBooks_IPP_Object_Line();


//        $Line->setDetailType('BillEmail');
//        $Line->setAddress($data['email']);
//
//        $Invoice->addLine($Line);



        $Invoice->setCustomerRef($customerRef);

        \Log::info('About to send Invoice to QB for customer ref.' . $customerRef);

        if ($resp = $InvoiceService->add($this->context, $this->realm, $Invoice)) {
            return $this->getId($resp);
        } else {
            return ['error', $InvoiceService->lastError($this->context)];
            //dd($InvoiceService->lastError());
        }
    }

    public function getId($resp)
    {
        $resp = str_replace('{', '', $resp);
        $resp = str_replace('}', '', $resp);
        $resp = abs($resp);
        return $resp;
    }

}