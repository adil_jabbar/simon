<?php namespace App\Http\Controllers;

Use App\Mailers\ContactMailer as Mailer;
use App\Mailers\ThanksMailer as ThanksMailer;
use App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use League\Fractal;


class ContactUsController extends BaseController {


    protected $mailer;
    protected $thanksMailer;

    public function __construct(Mailer $mailer, ThanksMailer $thanksMailer)
    {
        $this->mailer = $mailer;
        $this->thanksMailer = $thanksMailer;
    }


    public function postEnquiryForm(Request $request) {



        //Handle form
        $validator = \Validator::make($request->all(), [
                //'company'        => 'required',
                //'id'             => 'required',
                'email' 		 => 'required|max:80|email',
                'name'	 		 => 'required|max:100|min:3',
                'comment' 		 => 'required|min:3'
            ]
        );

        if ($validator->fails()) {

            throw new \Dingo\Api\Exception\StoreResourceFailedException('Could not send comment.', $validator->errors());

        } else {
            //$email = Input::get('email');


            $data = array(
                'comment' =>$request->input('comment'),
                'name'	=> $request->input('name'),
                'email' => $request->input('email'),
            );

            // Send Email to info@.
            if (\App::environment('local')) {
                $result = $this->mailer->contact(\Config::get('custom_config.admin_email_local'), $data);
            } else{
                $result = $this->mailer->contact(\Config::get('custom_config.admin_email'), $data);
            }



            return $this->response->noContent()->setStatusCode(200);
        }
    }
}
