<?php namespace App\Http\Controllers;

use App\Product;
use App\Subproduct;
use App\Category;
use App\Homepage;
use App\Settings;

class CategoryController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */

    public function show($slug)
    {

        $categories = Category::with('products')
            ->with(array('products' => function ($query) {
                $query->where('live', '=', 1);
            }))
            ->orderBy('sort_order')->get();



//        $newarrivals = Product::with('subproduct')
//            ->whereHas('subproduct', function ($q) {
//            $q->where('newarrival', 1);
//            $q->where('splive', 1);
//            })
//            ->orWhere('category_id', '=', 7)
//            ->where('live', 1)
//        ->get();

        //This code gets "New Arrivals" from Product & Subproduct, puts them in a multidimensional array and then sorts them by date
        $product = Product::where('category_id', '=', 7)
            ->where('live', 1)
            ->get();

        $sub = Subproduct::with('product')->where('newarrival', 1)->where('splive', 1)
            ->orderBy('created_at', 'DESC')
            ->LIMIT(26)
            ->get();

        $newarrivals = array();
        foreach ($product as $p) {
            $newarrivals[] = array("image" => $p->image, "slug" => $p->slug, "sku" => $p->sku, "date" => $p->updated_at);

        }

        foreach ($sub as $p) {
            $newarrivals[] = array("image" => $p->image, "slug" => $p->product->slug, "sku" => $p->sku, "date" => $p->updated_at);
        }

        $date = array();
        foreach ($newarrivals as $key => $row) {
            $date[$key] = $row['date'];
        }
        array_multisort($date, SORT_DESC, $newarrivals);


        //$newarrivals->merge($newarrivals2);

        //dd($newarrivals);

        $cat = Category::where('slug', $slug)->with('products')->orderBy('id')->get();

        return view('category')->with('page', Homepage::find(0))->with('settings', Settings::find(0))
            ->with('category', $categories) ->with('cat', $cat)->with('newarrivals', $newarrivals);

    }

}
