<?php namespace App\Http\Controllers;

use DB;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Cart;
use App\User;
use App\Order;
use App\Orderitem;
use App\Address;
use App\Postage;
use Illuminate\Support\Facades\Auth;
use App\Subproduct;
use App\Product;
use App\Category;
use Krucas\Notification\Facades\Notification;
use Illuminate\Http\Request;

class CheckoutController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Checkout Controller
    |--------------------------------------------------------------------------
    |
    */

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function checkout(request $request)
    {
        $cart = Cart::content();

        $postage = Postage::all();
        $postamt = 0;
        $total = $this->tofloat(Cart::total());

        foreach ($postage as $p) {
            if (($total > $p->lower) and ($total < $p->upper)) {
                $postamt = $p->amt;
            }
        }

        $postageTax = $postamt * 0.2;

        //This code gets "New Arrivals" from Product & Subproduct, puts them in a multidimensional array and then sorts them by date
        $product = Product::where('category_id', '=', 7)
            ->where('live', 1)
            ->get();

        $sub = Subproduct::with('product')->where('newarrival', 1)->where('splive', 1)
            ->orderBy('created_at', 'DESC')
            ->LIMIT(26)
            ->get();

        $newarrivals = array();
        foreach ($product as $p) {
            $newarrivals[] = array("image" => $p->image, "slug" => $p->slug, "sku" => $p->sku, "date" => $p->updated_at);

        }

        foreach ($sub as $p) {
            $newarrivals[] = array("image" => $p->image, "slug" => $p->product->slug, "sku" => $p->sku, "date" => $p->updated_at);
        }

        $date = array();
        foreach ($newarrivals as $key => $row) {
            $date[$key] = $row['date'];
        }
        array_multisort($date, SORT_DESC, $newarrivals);


        if (!Auth::user()) {
            if (!Auth::agent()) {
                return view('loggedout', array('category' => Category::with('products')->orderBy('id')->get()));
            }
        }

        $user = User::with('address')->find(Auth::user()->id);

        $category = Category::with('products')
            ->with(array('products' => function ($query) {
                $query->where('live', '=', 1);
            }))
            ->orderBy('sort_order')->get();


        $total = ($postamt) + $this->tofloat(Cart::total()) + $postageTax;

        if (Auth::user()->is('admin') or Auth::user()->is('assistantAdmin') or Auth::user()->is('minAdmin')) {
            //simon or ann
            $users = User::with('roles')
                ->where('live', 0)
                ->where('agent', 0)
                ->orderBy('companyName')

                ->get();
        } else {
            if ($user->agent == 1) {
                if ($user->id == 97) {
                    //max
                    $type = 1;
                }
                if ($user->id == 103) {
                    //demelza
                    $type = 3;
                }
                $users = User::with('roles')
                    ->where('live', 0)
                    ->where('agent', 0)
                    ->orderBy('companyName')
                    ->where('type', $type)
                    ->get();
            }
        }

        $array = array();
        $array[0] = "Select";
        if (isset($users)) {
            foreach ($users as $u) {
                if ($u->roles[0]->name == 'guest') {
                    $array[$u->id] = $u->address->companyName;
                }
            }
        }

        return view('checkout', array('cart' => $cart, 'title' => 'Welcome', 'description' => '',
            'page' => 'checkout', 'category' => $category, 'user' => $user, 'newarrivals' => $newarrivals, 'postamt' => $postamt,
            'total' => $total, 'users' => $array)
            );


    }

    public function complete(Request $request)
    {
        $totalPAmount = $request->postamt;
        $cart = Cart::content();

        if (!Auth::user()) {
            return view('loggedout', array('category' => Category::with('products')->orderBy('id')->get()));
        }

        // flag up any Rose/Yellow Gold items
        $hasYellowGold = false;
        $hasRoseGold = false;

        foreach ($cart as $item) {
            $product = Subproduct::find($item->id);

            // change yellow gold flag to true if we have some items for yg.
            if(isset($item->options['qty-yg'])) {
                if((int) $item->options['qty-yg'] > 0) {
                    $hasYellowGold = true;
                }
            }

            if(isset($item->options['qty-rg'])) {
                if((int) $item->options['qty-rg'] > 0) {
                     $tmpQty = (int) $item->options['qty-rg'];

                    if ($tmpQty > $product->quantity) {

                        Notification::container('myContainer')->info('The following item is not available:');
                        Notification::container('myContainer')->info($item->name . ' quantity available ' . $item->qty);
                        Notification::container('myContainer')->info('Please adjust your quantity');
                        return redirect('cart');
                    }
                    $hasRoseGold = true;
                }
            }
        }
        $user_id = $request->input('user_id');

        $order = null;
        if($hasRoseGold) {
            // create Rose Gold Order first.
            $order = new Order;
            DB::transaction(function () use($cart, $order, $user_id, $hasRoseGold, $hasYellowGold, $totalPAmount) {

                if (Auth::user()->is('agent') or Auth::user()->is('admin') or Auth::user()->is('assistantAdmin')) {
                    $order->user_id = $user_id;
                    $order->agent_id = Auth::user()->id;
                } else {
                    $order->user_id = Auth::user()->id;
                }

                $order->status = 0;

                // store values from the cart but these will need changing.
                $order->value = $this->tofloat(Cart::total());
                $order->vat = $this->tofloat(Cart::tax());
                $order->subtotal = $this->tofloat(Cart::subtotal());

                $order->save();

                $profit = 0;

                $newCartValue = 0;
                $newCartVat = 0;
                $newCartSubtotal = 0;
                foreach ($cart as $item) {
                    $theQty = (int) $item->options['qty-rg'];
                    if($theQty !== 0) {
                        $orderitem = new Orderitem();
                        $orderitem->order_id = $order->id;
                        $orderitem->subproduct_id = $item->id;
                        $orderitem->qty = $theQty;
                        $orderitem->value = ($theQty * $item->price);
                        $orderitem->save();

                        $newCartSubtotal = $newCartSubtotal + ($theQty * $item->price);
                        $newCartVat = $newCartVat + ( ( ($theQty * $item->price) / 100 ) * $item->taxRate );

                        $product = Subproduct::find($item->id);
                        $profit = $profit + ($theQty * ($product->grossPrice - $product->into_safe));
                    }
                }

                // split postage amount if there are yellow gold items as well as rose gold items.
                if($hasYellowGold & $hasRoseGold) {
                    $totalPAmount = (int) $totalPAmount / 2;
                } 
                
                $order->postage = $this->tofloat($totalPAmount);
                $postageTax = $order->postage * 0.2;
                $order->vat = $this->tofloat($newCartVat + $postageTax);

                $newCartValue = ($newCartSubtotal + $order->vat + $totalPAmount);
                $order->value = $this->tofloat($newCartValue);

                $order->subtotal = $this->tofloat($newCartSubtotal);
                $order->profit = $profit;

                $order->save();
            });
        }

        // if has Yellow Gold then create and order for that too.
        $orderYG = null;
        if($hasYellowGold) {

            $orderYG = new Order;
            DB::transaction(function () use($cart, $orderYG, $user_id, $hasRoseGold, $hasYellowGold, $totalPAmount) {

                if (Auth::user()->is('agent') or Auth::user()->is('admin') or Auth::user()->is('assistantAdmin')) {
                    $orderYG->user_id = $user_id;
                    $orderYG->agent_id = Auth::user()->id;
                } else {
                    $orderYG->user_id = Auth::user()->id;
                }

                $orderYG->status = 0;

                // store values from the cart but these will need changing.
                $orderYG->value = $this->tofloat(Cart::total());
                $orderYG->vat = $this->tofloat(Cart::tax());
                $orderYG->subtotal = $this->tofloat(Cart::subtotal());
                $orderYG->is_yg = 1;
                $orderYG->save();

                $profit = 0;

                $newCartValue = 0;
                $newCartVat = 0;
                $newCartSubtotal = 0;
                foreach ($cart as $item) {
                    $theQty = (int) $item->options['qty-yg'];

                    if($theQty !== 0) {
                        $orderitem = new Orderitem();
                        $orderitem->order_id = $orderYG->id;
                        $orderitem->is_yg = 1;
                        $orderitem->subproduct_id = $item->id;
                        $orderitem->qty = $theQty;
                        $orderitem->value = ($theQty * $item->price);
                        $orderitem->save();

                        $newCartSubtotal = $newCartSubtotal + ($theQty * $item->price);
                        $newCartVat = $newCartVat + ( ( ($theQty * $item->price) / 100 ) * $item->taxRate );

                        $product = Subproduct::find($item->id);
                        $profit = $profit + ($theQty * ($product->grossPrice - $product->into_safe));

                        // Now we need to add to the Sales & Reorder screen for yellow gold items.
                        $sub = Subproduct::find($item->id);
                        
                        $newReorderQty = $sub->reorder_temp + ($sub->reorder_temp_yg + $theQty);
                        $sub->reorder_temp_yg = $sub->reorder_temp_yg + $theQty;
                        $sub->reorder_value = $sub->reorder_value + ($newReorderQty * $sub->fob);
                        
                        $sub->save();
                    }
                }

                // split postage amount if there are yellow gold items as well as rose gold items.
                if($hasYellowGold & $hasRoseGold) {
                    $totalPAmount = (int) $totalPAmount / 2;
                } 

                $orderYG->postage = $this->tofloat($totalPAmount);
                $postageTax = $orderYG->postage * 0.2;
                $orderYG->vat = $this->tofloat($newCartVat + $postageTax);

                $newCartValue = ($newCartSubtotal + $orderYG->vat + $totalPAmount);
                $orderYG->value = $this->tofloat($newCartValue);

                $orderYG->subtotal = $this->tofloat($newCartSubtotal);
                $orderYG->profit = $profit;

                $orderYG->save();
            });
        }
        
        Cart::destroy();

        //Send Simon an Email. Use PHP Mail for now. Do properly later!
        if (\App::environment('local')) {
            $to = 'info@pc-paul.com';
            $subject = 'New Order Received';
            $message = 'Hi Simon, You have a New Order';
            $headers = 'From: info@simonalexanderuk.com' . "\r\n" .
                'Reply-To: info@simonalexanderuk.com' . "\r\n" .
                'Cc: info@regencywebdesign.co.uk' . "\r\n" .
                'X-Mailer: PHP/' . phpversion();

            mail($to, $subject, $message, $headers);
        } else {
            $to = 'sales@simonalexanderuk.com';
            $subject = 'New Order Received';
            $message = 'Hi Simon, You have a New Order';
            $headers = 'From: info@simonalexanderuk.com' . "\r\n" .
                'Reply-To: info@simonalexanderuk.com' . "\r\n" .
                'X-Mailer: PHP/' . phpversion();

            mail($to, $subject, $message, $headers);
        }


        $newarrivals = Product::with('subproduct')
            ->whereHas('subproduct', function ($q) {
                $q->where('newarrival', 1);
                $q->where('splive', 1);
            })
            ->orWhere('category_id', '=', 7)
            ->where('live', 1)
            ->get();


        return view('ordercomplete', array('title' => 'Welcome', 'description' => '',
            'page' => 'ordercomplete', 'order' => $order, 'orderYG' => $orderYG, 'category' => Category::with('products')->orderBy('id')->get(), 'newarrivals' => $newarrivals));

    }

    function tofloat($num)
    {
        $dotPos = strrpos($num, '.');
        $commaPos = strrpos($num, ',');
        $sep = (($dotPos > $commaPos) && $dotPos) ? $dotPos :
            ((($commaPos > $dotPos) && $commaPos) ? $commaPos : false);

        if (!$sep) {
            return floatval(preg_replace("/[^0-9]/", "", $num));
        }

        return floatval(
            preg_replace("/[^0-9]/", "", substr($num, 0, $sep)) . '.' .
            preg_replace("/[^0-9]/", "", substr($num, $sep + 1, strlen($num)))
        );
    }
    function test () {

        $user = User::with('roles')
            ->where('id', 2)



            ->first();


        $user->password = Hash::make('none');
        $user->save();

    }


}
