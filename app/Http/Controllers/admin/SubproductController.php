<?php namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Product;
use App\Subproduct;
use App\Stone;
use App\Http\Requests;
use App\Http\Controllers\admin\ImagesController;

use Illuminate\Support\Facades\Auth;
use Krucas\Notification\Facades\Notification;
use Illuminate\Support\Facades\Redirect;

use Intervention\Image as Image;

use Illuminate\Http\Request;


class SubproductController extends Controller {

    public function index($id)
    {
        $product = Product::where('id', $id)->with('subproduct', 'category')->first();

        return \View::make('admin.subproduct.index')->with('product',$product) ;
    }

    public function create($id)
    {
        $stones = Stone::orderBy('stone', 'asc')->get();

        return \View::make('admin.subproduct.create')->with('categories',Category::all())->with('stones',$stones)
            ->with('product', Product::find($id));
    }
    public function store(Request $request)
    {
        $this->validate($request, [
//            'grossPrice' => 'required',
//            'into_safe' => 'required',
//            'quantity' => 'required',
            'stone_id' => 'required',
//            'image' => 'required',

        ]);

        $allowable_tags = '<br><br/><strong></strong><em></em><ul></ul><li></li>';

        $subproduct = new Subproduct;

        $product = Product::where('id', $request->input('id'))->first();


        $stone = Stone::where('id', $request->input('stone_id'))->first();

        $subproduct->sku = $product->sku . ' ' . $stone->code;
//        $subproduct->grossPrice = $request->input('grossPrice');
//        $subproduct->into_safe = $request->input('into_safe');

        $subproduct->fob = $request->input('fob');

        $subproduct->freight = round($subproduct->fob * .02, 2);
        if ($product->category->apply_bolt) {
            $subproduct->bolt = $product->category->bolt_amt;
        } else {
            $subproduct->bolt = 0;
        }
        $subproduct->hm = 1;
        $subproduct->into_safe = $subproduct->fob + $subproduct->freight + $subproduct->bolt + $subproduct->hm;
        $subproduct->grossPrice = $subproduct->into_safe * 2;

        $subproduct->quantity = $request->input('quantity');

        $subproduct->product_id = $product->id;
        $subproduct->stone_id = $request->input('stone_id');
        $subproduct->splive   = $request->input('splive');
        $subproduct->newarrival = $request->input('newarrival');
        $subproduct->image = 'currently-awaiting-image.jpg';
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $filename = str_replace('£', '', $file->getClientOriginalName()) . ' - ' . time() . '.' . $file->getClientOriginalExtension();
            $path = public_path('images/products/' . $filename);
            //http://image.intervention.io/api/resize

            \Image::make($file->getRealPath())
                ->resize(1500, 1500, function ($constraint) {
                    $constraint->aspectRatio();
                })
                ->resizeCanvas(1500, 1500, 'center', false, 'ffffff')
                ->save($path);
            $subproduct->image = $filename;
            ImagesController::makethumbnail($filename);
        } else {
            $product->image = 'currently-awaiting-image.png';
        }
//        if (!$request->input('imageAlt')) {
//            $subproduct->imageAlt = $product->productName . ' image';
//        } else {
            $subproduct->imageAlt = $request->input('imageAlt') . ' - ' . $subproduct->sku ;
        //}
        $subproduct->save();

        Notification::success('The item was saved.');

        //return Redirect::route('admin.subproduct.index');
        //dd(redirect()->route('admin.subproduct.index', ['id' => 1]));
        return redirect('admin/subproduct/'. $product->id);
    }

    public function edit($id)
    {
        $subproduct = Subproduct::where('id',$id)->first();

        return \View::make('admin.subproduct.edit')->with('categories',Category::all())->with('stones', Stone::orderBy('stone', 'asc')->get())
            ->with('product', $subproduct);
    }

    public function update($id, request $request)
    {
        $this->validate($request, [
//            'grossPrice' => 'required',
//            'into_safe'  => 'required',
//            'quantity'   => 'required',
            'stone_id'   => 'required',
        ]);

        $allowable_tags = '<br><br/><strong></strong><em></em><ul></ul><li></li>';

        $subproduct = Subproduct::find($id);

        $product = Product::where('id',$subproduct->product_id)->first();

        $stone =   Stone::where('id', $request->input('stone_id'))->first();

        $subproduct->sku              = $product->sku . ' ' . $stone->code;
//        $subproduct->grossPrice       = $request->input('grossPrice');
//        $subproduct->into_safe        = $request->input('into_safe');
//        $subproduct->quantity         = $request->input('quantity');
        $subproduct->product_id       = $product->id;
        $subproduct->stone_id         = $request->input('stone_id');
        $subproduct->splive           = $request->input('splive');
        $subproduct->eoline           = $request->input('eoline');
        $subproduct->newarrival       = $request->input('newarrival');

        $subproduct->save();
//        if ($request->hasFile('image'))
//        {
//            $file = $request->file('image');
//            $filename  = str_replace('£', '', $file->getClientOriginalName()) . ' - ' . time() . '.' . $file->getClientOriginalExtension();
//            $path = public_path('images/products/' . $filename);
//            //http://image.intervention.io/api/resize
//
//            \Image::make($file->getRealPath())
////                ->resize(500, 500, function ($constraint) {
////                    $constraint->aspectRatio();
////                })
////                ->resizeCanvas(500,500, 'center', false, 'ffffff')
//                ->save($path);
//            $subproduct->image    			 = $filename;
//
//        }
//
//        $subproduct->imageAlt       = $request->input('imageAlt');

//        $subproduct->save();

        if ($request->hasFile('image'))
        {
            $file = $request->file('image');
            //$filename  = $file->getClientOriginalName() . ' - ' . time() . '.' . $file->getClientOriginalExtension();
            $filename = str_replace('£', '', $file->getClientOriginalName()) . ' - ' . time() . '.' . $file->getClientOriginalExtension();

            $path = public_path('images/products/' . $filename);
            //http://image.intervention.io/api/resize

            \Image::make($file->getRealPath())
                ->resize(2000,null, function ($constraint) {
                    $constraint->aspectRatio();
                })
                ->resizeCanvas(2000,2000, 'center', false, 'ffffff')
                ->save($path);

            $subproduct->image    			 = $filename;

            ImagesController::makethumbnail($filename);
        }

        $subproduct->imageAlt       = $request->input('imageAlt');
        $subproduct->save();

        Notification::success('The item was saved.');

        return redirect('admin/subproduct/'. $product->id);

    }

    public function destroy($id)
    {
        $subproduct = Subproduct::find($id);
        $product = Product::find($subproduct->product_id);
        $subproduct->delete();

        Notification::success('The item was deleted.');

        return redirect('admin/subproduct/'. $product->id);
    }


}

