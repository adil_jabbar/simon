<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Settings;
use App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Krucas\Notification\Facades\Notification;
use Illuminate\Support\Facades\Redirect;

use Intervention\Image as Image;

use Illuminate\Http\Request;


class SettingsController extends Controller {



    public function show()
    {
        return \View::make('admin.settings.edit')->with('settings', Settings::find(0));
    }
    public function index()
    {
        return \View::make('admin.settings.edit')->with('settings', Settings::find(0));
    }

    public function update($id = 0, request $request)
    {
        $this->validate($request, [

            'analytics'  => 'required',
        ]);

        $settings = Settings::find($id);
        $settings->analytics    = $request->input('analytics');


        $settings->save();

        Notification::success('The settings were saved.');

        return Redirect::route('admin.manage.index');

    }




}

