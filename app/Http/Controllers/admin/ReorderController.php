<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Subproduct;
use App\Category;
use Illuminate\Support\Facades\Auth;
use Krucas\Notification\Facades\Notification;
use Illuminate\Support\Facades\Redirect;

use Intervention\Image as Image;

use Illuminate\Http\Request;


class ReorderController extends Controller
{


    public function index()
    {
        //$stock = Subproduct::with('product', 'stone', 'product.category')->orderBy('sku')->where('splive', '=', 1)->get();

        $stock = Category::with('products', 'products.subproduct', 'products.subproduct.stone')
            ->orderBy('sort_order', 'asc')
            ->with(['products' => function ($query) {
                $query->orderBy('sku', 'desc');
            }])
            ->get();

        return \View::make('admin.reorder.index')->with('stock', $stock)->with('qty', 99999)->with('category', '');
    }

    public function quantity(Request $request)
    {
        $cat = $request->input('category');

        if ($request->input('selectqty') != "") {
            $quantity = $request->input('selectqty');
        } else {
            $quantity = 9999;
        }

        if ($cat != '') {
            $stock = Category::with('products', 'products.subproduct', 'products.subproduct.stone')->where('id', '=', $cat);
        } else {
            $stock = Category::with('products', 'products.subproduct.stone');
        }

        $stock = $stock->get();

//        if ($cat == '7') {
//            $stock2 = Category::with('products', 'products.subproduct.stone')
//                ->with(['products.subproduct' => function ($query) {
//                    $query->where('newarrival', '=', 1);
//
//                }])
//                ->get();
////dd($stock2);
//            $stock->merge($stock2);
//        }


        //dd($stock);


        return \View::make('admin.reorder.index')->with('stock', $stock)->with('qty', $quantity)->with('category', $cat);
    }

    public function update(Request $request)
    {
        foreach ($request->input('id') as $key => $id) {
            $sub = Subproduct::Where('id', '=', $request->input('id')[$key])->first();
            if (isset($request->input('weight')[$key]) && $request->input('weight')[$key] != "") {

                $sub->weight = $request->input('weight')[$key];
            }
            if (isset($request->input('goldcost')[$key]) && $request->input('goldcost')[$key] != "") {

                $sub->goldcost = $request->input('goldcost')[$key];
            }
            if (isset($request->input('mm')[$key]) && $request->input('mm')[$key] != "") {

                $sub->mm = $request->input('mm')[$key];
            }
            if (isset($request->input('stonecost')[$key]) && $request->input('stonecost')[$key] != "") {

                $sub->stonecost = $request->input('stonecost')[$key];
            }
            if (isset($request->input('labourcost')[$key]) && $request->input('labourcost')[$key] != "") {

                $sub->labourcost = $request->input('labourcost')[$key];
            }
            $sub->profit20 = ($sub->goldcost + $sub->stonecost + $sub->labourcost) * .2;
            $sub->fobnew = ($sub->goldcost + $sub->stonecost + $sub->labourcost + $sub->profit20);
            if (isset($request->input('updatefob')[$key]) && $request->input('updatefob')[$key] != "") {

                $sub->fobnew = $request->input('updatefob')[$key];

            }
            $sub->save();

        }


        if (strlen($request->input('category')) == '' && $request->input('qty') == 0) {
            $stock = Category::with('products', 'products.subproduct', 'products.subproduct.stone')
                ->orderBy('sort_order', 'asc')
                ->with(['products' => function ($query) {
                    $query->orderBy('sku', 'desc');
                }])
                ->get();

            return \View::make('admin.reorder.index')->with('stock', $stock)->with('qty', 9999)->with('category', '');

        } else {

            $cat = $request->input('category');

            if ($request->input('qty') != "") {
                $quantity = $request->input('qty');
            } else {
                $quantity = 9999;
            }

            if ($cat != '') {
                $stock = Category::with('products', 'products.subproduct', 'products.subproduct.stone')->where('id', '=', $cat);
            } else {
                $stock = Category::with('products', 'products.subproduct.stone');
            }

            $stock = $stock->get();


            return \View::make('admin.reorder.index')->with('stock', $stock)->with('qty', $quantity)->with('category', $cat);
        }


        //return redirect(url('admin\quantity'));
    }


}

