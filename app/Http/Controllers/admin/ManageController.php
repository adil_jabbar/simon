<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class ManageController extends Controller {

	public function index()
	{
		//dd('hello');
		return \View::make('admin.dashboard.index');

	}
	
	public function edit($id)
	{
		//$query = "SELECT * FROM `company` WHERE  `company_name` =". $id;
		//$company = DB::select(DB::raw($query));
		return \View::make('admin.manage.edit')->with('company', Company::find($id));
	}

	public function find()
	{
		//$query = "SELECT * FROM `company` WHERE  `company_name` =". $id;
		//$company = DB::select(DB::raw($query));
		$company_name = trim(input::get("company"));
		$company = Company::where('company_name', '=', $company_name)->firstorfail();
		//log::debug('pxw!!! input data is ', array(trim(input::get('company'))) );
		//log::debug('pxw input data is ', array( $company ) );
		//var_dump($company);exit;
		return \View::make('admin.company.edit')->with('company', $company);
	}

	public function update($id)
	{
		log::debug('pxw!!! Update');
		$validation = new CompanyValidator;

		if ($validation->passes())
		{
			log::debug('pxw!!! Passed');
			$Company = Company::find($id);
			$Company->company_name   = Input::get('company_name');
			$Company->company_email    = Input::get('company_email');
			$Company->company_contact    = Input::get('company_contact');
			$Company->company_desc    = Input::get('company_desc');
			$Company->area = Sentry::getUser()->area;
			$Company->save();

			Notification::success('The Company was saved.');

			return Redirect::route('admin.manage.edit', $Company->id);
		}

		return Redirect::back()->withInput()->withErrors($validation->errors);
	}

	public function show($id)
	{
		
	}

	public function create()
	{
		
	}

	public function store()
	{
	log::debug('pxw!!! Log X');	
	}
	
}
