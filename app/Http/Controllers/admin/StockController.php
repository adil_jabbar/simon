<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Subproduct;
use App\Category;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Krucas\Notification\Facades\Notification;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;

use Intervention\Image as Image;

use Illuminate\Http\Request;


class StockController extends Controller
{


    public function index()
    {

        $date = Carbon::now();
        $month = $date->subMonth()->toDateTimeString();


        $date = Carbon::now();
        $quarter = $date->subMonths(3)->toDateTimeString();

        $date = Carbon::now();
        $year = $date->subYear()->toDateTimeString();

        $sql = "SELECT(SELECT SUM(order_items . qty)

              FROM order_items INNER JOIN orders ON order_items . order_id = orders . id WHERE order_items . subproduct_id = a . id and orders . status = 1 AND orders . created_at > '$quarter' ) as qtd,

(SELECT SUM(order_items . qty)

              FROM order_items INNER JOIN orders ON order_items . order_id = orders . id WHERE order_items . subproduct_id = a . id and orders . status = 1 AND orders . created_at > '$month' ) as mtd ,

(SELECT SUM(order_items . qty)

              FROM order_items INNER JOIN orders ON order_items . order_id = orders . id

              WHERE order_items . subproduct_id = a . id
              and orders . status = 1 AND orders . created_at > '$year' ) as ytd,

              a .*, a . sku as subproductsku, a . id as subproductId, categories .*, products .*, stones . code

FROM categories
  INNER JOIN products
    ON categories . id = products . category_id
  INNER JOIN subproducts a
    ON products . id = a . product_id
  INNER JOIN stones
    ON a . stone_id = stones . id

ORDER BY categories . sort_order, products . sku, stones.code";

        $stock = DB::select(DB::raw($sql));

        $total_reorder_value = DB::select(DB::raw('SELECT SUM(reorder_value) as total_reorder_value FROM subproducts'));

        $total_reorder_value = $total_reorder_value[0]->total_reorder_value;

//        dd($stock);

        return \View::make('admin.stock.index')->with('stock', $stock)->with('quantity', 0)->with('cat', '')->with('total_reorder_value', $total_reorder_value);
    }

    public function filterstock(Request $request)

        //This function handles the filtering of the list to show just one category or stock level

    {
        $cat = $request->input('category');

        if ($request->input('quantity') != "") {
            $quantity = $request->input('quantity');
            $whereqty = $whereqty = ' where a.quantity < ' .$quantity;
        } else {
            $quantity = 9999;
            $whereqty = '';
        }

        if ($cat != '' ) {
            $stock = Category::with('products', 'products.subproduct', 'products.subproduct.stone')->where('id', '=', $cat);
            $wherecat = ' where categories.id = ' . $cat;
            if (strlen($whereqty) > 0) {
                $whereqty .= ' and categories.id = ' . $cat;
                $wherecat ='';
            }
        } else {
            $stock = Category::with('products', 'products.subproduct.stone');
            $wherecat = '';
        }

        $date = Carbon::now();
        $month = $date->subMonth()->toDateTimeString();


        $date = Carbon::now();
        $quarter = $date->subMonths(3)->toDateTimeString();

        $date = Carbon::now();
        $year = $date->subYear()->toDateTimeString();

        $sql = "SELECT(SELECT SUM(order_items . qty)

              FROM order_items INNER JOIN orders ON order_items . order_id = orders . id WHERE order_items . subproduct_id = a . id and orders . status = 1 AND orders . created_at > '$quarter' ) as qtd,

(SELECT SUM(order_items . qty)

              FROM order_items INNER JOIN orders ON order_items . order_id = orders . id WHERE order_items . subproduct_id = a . id and orders . status = 1 AND orders . created_at > '$month' ) as mtd ,

(SELECT SUM(order_items . qty)

              FROM order_items INNER JOIN orders ON order_items . order_id = orders . id WHERE order_items . subproduct_id = a . id and orders . status = 1 AND orders . created_at > '$year' ) as ytd,

              a .*, a . sku as subproductsku, a . id as subproductId, categories .*, products .*, stones . code

FROM categories
  INNER JOIN products
    ON categories . id = products . category_id
  INNER JOIN subproducts a
    ON products . id = a . product_id
    INNER JOIN stones
    ON a . stone_id = stones . id
 $whereqty $wherecat
ORDER BY categories . sort_order, products . sku, stones.code";

        $stock = DB::select(DB::raw($sql));

        $total_reorder_value = DB::select(DB::raw('SELECT SUM(reorder_value) as total_reorder_value FROM subproducts'));

        $total_reorder_value = $total_reorder_value[0]->total_reorder_value;


        return \View::make('admin.stock.index')->with('stock', $stock)->with('quantity', $quantity)->with('cat', $cat)->with('total_reorder_value', $total_reorder_value);
    }



    public function savereorder(Request $request)
    {

//        if (file_exists(storage_path() . '/reorder/' . 'reorderform.csv')) {
//            //unlink(storage_path() . '/reorder/' . 'reorderform' . date("Y-m-d") . '.csv');
//            $fh = fopen(storage_path() . '/reorder/' . 'reorderform.csv', 'a');
//        } else {
//            $fh = fopen(storage_path() . '/reorder/' . 'reorderform.csv', 'a');
//
//            fputcsv($fh, array(
//                'Stock Code',
//                'Description',
//                'Stones',
//                'Order',
//                "\n"));
//        }

//        dd($request->input());

        foreach ($request->input('reorder') as $key => $id) {

            if ($request->input('reorder')[$key]>0) {





//                    fputcsv($fh, array(
//                        $request->input('sku')[$key],
//                        $request->input('productName')[$key],
//                        $request->input('stone')[$key],
//                        $request->input('reorder')[$key],
//                        "\n"));
//                    $prev = $request->input('productName')[$key];
//                    $count = $request->input('reorder')[$key];

                    $sub = Subproduct::where('id', $request->input('subproductId')[$key] )->first();
                    $sub->reorder_temp = $request->input('reorder')[$key];
                    $sub->reorder_value = $request->input('reorder')[$key] * $sub->fob;
                    $sub->save();

            }


        }


//        fclose($fh);

        $cat = $request->input('cat');

        if ($request->input('quantity') != "" && $request->input('quantity') > 0) {
            $quantity = $request->input('quantity');
            $whereqty = $whereqty = ' where a.quantity < ' . $quantity;
        } else {
            $quantity = 9999;
            $whereqty = '';
        }

        if ($cat != '') {
            $stock = Category::with('products', 'products.subproduct', 'products.subproduct.stone')->where('id', '=', $cat);
            $wherecat = ' where categories.id = ' . $cat;
            if (strlen($whereqty) > 0) {
                $whereqty .= ' and categories.id = ' . $cat;
                $wherecat = '';
            }
        } else {
            $stock = Category::with('products', 'products.subproduct.stone');
            $wherecat = '';
        }


        $date = Carbon::now();
        $quarter = $date->firstOfQuarter();

        $date = Carbon::now();
        $month = $date->firstOfMonth();

        $date = Carbon::now();
        $year = $date->firstOfYear();

        $sql = "SELECT(SELECT SUM(order_items . qty)

              FROM order_items INNER JOIN orders ON order_items . order_id = orders . id WHERE order_items . subproduct_id = a . id and orders . status = 1 AND orders . created_at > '$quarter' ) as qtd,

(SELECT SUM(order_items . qty)

              FROM order_items INNER JOIN orders ON order_items . order_id = orders . id WHERE order_items . subproduct_id = a . id and orders . status = 1 AND orders . created_at > '$month' ) as mtd ,

(SELECT SUM(order_items . qty)

              FROM order_items INNER JOIN orders ON order_items . order_id = orders . id WHERE order_items . subproduct_id = a . id and orders . status = 1 AND orders . created_at > '$year' ) as ytd,

              a .*, a . sku as subproductsku, a . id as subproductId, categories .*, products .*, stones . code

FROM categories
  INNER JOIN products
    ON categories . id = products . category_id
  INNER JOIN subproducts a
    ON products . id = a . product_id
    INNER JOIN stones
    ON a . stone_id = stones . id
 $whereqty $wherecat
ORDER BY categories . sort_order, products . sku";

        $stock = DB::select(DB::raw($sql));

        $total_reorder_value = DB::select(DB::raw('SELECT SUM(reorder_value) as total_reorder_value FROM subproducts'));

        $total_reorder_value = $total_reorder_value[0]->total_reorder_value;

        return \View::make('admin.stock.index')->with('stock', $stock)->with('quantity', $quantity)->with('cat', $cat)->with('total_reorder_value', $total_reorder_value);


    }

    public function view_reorder($id)
    {
        $invoice_path = storage_path() . '/reorder/';
        $file = $invoice_path . 'reorderform.csv';  // <- Replace with the path to your .pdf file

        // check if the file exists
        if (file_exists($file)) {
            rename(storage_path() . '/reorder/' . 'reorderform.csv', storage_path() . '/reorder/' . 'reorderform' . date("Y-m-d") . rand() . '.csv');
        }

        $fh = fopen(storage_path() . '/reorder/' . 'reorderform.csv', 'w');

        fputcsv($fh, array(
            'Stock Code',
            'Description',
            'Stones',
            'Order Rose Gold',
            'Order Yellow Gold',
            "\n"));

        $sub = Subproduct::with('stone', 'product')->where('reorder_temp', '>', 0)->orWhere('reorder_temp_yg', '>', 0)->get();

        foreach ($sub as $s) {

            fputcsv($fh, array(
                $s->sku,
                $s->product->productName,
                $s->stone->stone,
                $s->reorder_temp,
                $s->reorder_temp_yg,
                "\n"));
            }

            fclose($fh);
            
            $content = file_get_contents($file);
            //File has been downloaded so clear any reorder_temp values

            $sub = Subproduct::where('reorder_temp', '>', 0)->orWhere('reorder_temp_yg', '>', 0)->get();

            foreach ($sub as $s) {
                $s->reorder_temp = 0;
                $s->reorder_temp_yg = 0;
                $s->reorder_value = 0;
                $s->save();
            }

            // create a Laravel Response using the content string, an http response code of 200(OK),
            //  and an array of html headers including the pdf content type
            return \Response::make($content, 200, array('content-type' => 'text/csv',
                'Content-Disposition' => 'attachment; filename=reorder.csv'));

    }

    public function missing(Request $request)
    {

        if (file_exists(storage_path() . '/missing_images/' . 'missing_images'  . '.csv')) {
            unlink(storage_path() . '/missing_images/' . 'missing_images' .  '.csv');
        }

        $fh = fopen(storage_path() . '/missing_images/' . 'missing_images' . '.csv', 'a');

        fputcsv($fh, array(
            'Stock Code',
            'Description',
            'Stones',

            "\n"));
        $prevCat = ' ';
        $prev = ' ';
        $count = 0;
        $cattotal =0;

        $sub = Subproduct::with('product')->with('stone')->where('image', 'currently-awaiting-image.jpg')->orderBy('sku')->get();
        foreach ($sub as $s) {

                    fputcsv($fh, array(
                        $s ->product->productName ,
                        $s->sku,
                        $s->stone->stone,

                        "\n"));
            }

        fclose($fh);

        return redirect(url('\admin\product'));
    }

    public function stockreport(Request $request)
    {

        if (file_exists(storage_path() . '/stockreport/' . 'stockreport' . '.csv')) {
            unlink(storage_path() . '/stockreport/' . 'stockreport' . '.csv');
        }

        $fh = fopen(storage_path() . '/stockreport/' . 'stockreport' . '.csv', 'a');

        fputcsv($fh, array(
            'Stock Code',
            'Gross Price',
            'Quantity',
            'Product Name',

            "\n"));
        $prevCat = ' ';
        $prev = ' ';
        $count = 0;
        $cattotal = 0;

        $sub = Subproduct::with('product')->with('stone')->orderBy('sku')->get();
        foreach ($sub as $s) {

            fputcsv($fh, array(

                $s->sku,
                $s->grossPrice,
                $s->quantity,
                $s->product->productName,

                "\n"));
        }

        fclose($fh);

        return redirect(url('\admin\quantity'));
    }
    public function view_missing()
    {
        $invoice_path = storage_path() . '/missing_images/';
        $file = $invoice_path . 'missing_images.csv';  // <- Replace with the path to your .pdf file
        // check if the file exists
        if (file_exists($file)) {
            // read the file into a string
            $content = file_get_contents($file);
            // create a Laravel Response using the content string, an http response code of 200(OK),
            //  and an array of html headers including the pdf content type
            return \Response::make($content, 200, array('content-type' => 'text/csv', 'Content-Disposition' => 'attachment; filename=missing_images.csv'));
        }
    }

    public function stockreportdownload()
    {
        $invoice_path = storage_path() . '/stockreport/';
        $file = $invoice_path . 'stockreport.csv';  // <- Replace with the path to your .pdf file
        // check if the file exists
        if (file_exists($file)) {
            // read the file into a string
            $content = file_get_contents($file);
            // create a Laravel Response using the content string, an http response code of 200(OK),
            //  and an array of html headers including the pdf content type
            return \Response::make($content, 200, array('content-type' => 'text/csv', 'Content-Disposition' => 'attachment; filename=stockreport.csv'));
        }
    }


    // Liam Goldstein
    // new function save reorders using ajax.
    public function savereorderAJAX(Request $request) {
        $input = $request->all();

        $rowID = $input['rowid'];
        $fieldName = $input['name'];
        $fieldValue = $input['value'];

        $sub = Subproduct::where('id', $rowID)->first();

        // check if yellow gold or not...
        if(strpos( $fieldName, '-yg' ) !== false) {
            // is Yellow Gold
            $sub->reorder_temp_yg = $fieldValue;
            $newQty = ($sub->reorder_temp + $fieldValue);
        } else {
            // is Rose Gold
            $sub->reorder_temp = $fieldValue;
            $newQty = ($sub->reorder_temp_yg + $fieldValue);
        }

        $sub->reorder_value = ($newQty * $sub->fob);
        $sub->save();

        $total_reorder_value = DB::select(DB::raw('SELECT SUM(reorder_value) as total_reorder_value FROM subproducts'));
        $total_reorder_value = (float) $total_reorder_value[0]->total_reorder_value;

        return response()->json(['id' => $rowID, 'fieldname' => $fieldName, 'fieldval' => $fieldValue, 
            'orderval' => $sub->reorder_value,
            'totalReorderValue' => number_format($total_reorder_value,0,'.',',')]);
    }
}