<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Order;
use App\User;
use App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Krucas\Notification\Facades\Notification;
use Illuminate\Support\Facades\Redirect;

use Intervention\Image as Image;

use Illuminate\Http\Request;


class ChangecustomerController extends Controller
{

    public function edit($id)
    {
        $orders = Order::where('id', '=', $id)->with(['users' => function ($query) {
            $query->with('address');
        }])
            ->with(['orderitems' => function ($query) {
                $query->with(['subproducts' => function ($query) {
                    $query->with('product');
                }]);
            }])
            ->orderBy('created_at')->first();

        $customers = User::get();

        return \View::make('admin.order.editcompany')->with('orders', $orders)->with('customer', $customers);

    }


    public function updatecustomer(request $request){



        $order = Order::where('id', '=', $request->input('id'))->first();



        $order->user_id = $request->input('customer');

        $order->save();

        return Redirect::route('admin.orders.index');
    }


}

