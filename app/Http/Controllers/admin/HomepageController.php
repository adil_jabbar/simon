<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Homepage;
use App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Krucas\Notification\Facades\Notification;
use Illuminate\Support\Facades\Redirect;

use Intervention\Image as Image;

use Illuminate\Http\Request;


class HomepageController extends Controller {



    public function show()
	{
		return \View::make('admin.homepage.edit')->with('homepage', Homepage::find(0));
	}

	public function update($id = 0, request $request)
	{
        $this->validate($request, [
            'title1' => 'required',
            'body1'  => 'required',
            'body2'  => 'required',
            'body3'  => 'required',
            'body4'  => 'required',
            'body5'  => 'required',
        ]);
        $allowable_tags = '<br><br/><strong></strong><em></em>';
        $homepage = Homepage::find($id);

        $homepage->title1   = $request->input('title1');
        $homepage->title2   = $request->input('title2');
        $homepage->title3   = $request->input('title3');
        $homepage->title4   = $request->input('title4');
        $homepage->title5   = $request->input('title5');

        $homepage->body1    = strip_tags($request->input('body1'), $allowable_tags);
        $homepage->body2    = strip_tags($request->input('body2'), $allowable_tags);
        $homepage->body3    = strip_tags($request->input('body3'), $allowable_tags);
        $homepage->body4    = strip_tags($request->input('body4'), $allowable_tags);
        $homepage->body5    = strip_tags($request->input('body5'), $allowable_tags);

        $homepage->link1    = $request->input('link1');
        $homepage->link2    = $request->input('link2');
        $homepage->link3    = $request->input('link3');
        $homepage->link4    = $request->input('link4');
        $homepage->link5    = $request->input('link5');

        $homepage->user_id = Auth::user()->id;
        if ($request->hasFile('image'))
        {
            $file = $request->file('image');
            $filename  = $file->getClientOriginalName() . ' - ' . time() . '.' . $file->getClientOriginalExtension();
            $path = public_path('images/' . $filename);
            //http://image.intervention.io/api/resize

            \Image::make($file->getRealPath())
//                ->resize(135, 158, function ($constraint) {
//                    $constraint->aspectRatio();
//                })
//                ->resizeCanvas(135, 158, 'center', false, 'ffffff')
                ->save($path);
            $homepage->image    			 = $filename;

        }
        $homepage->image_alt    = $request->input('image_alt');
        $homepage->save();

        Notification::success('The homepage was saved.');

        return Redirect::route('admin.manage.index', $homepage->id);

	}


    

}

