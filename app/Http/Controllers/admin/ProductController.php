<?php namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Product;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Subproduct;
use Illuminate\Support\Facades\Auth;
use Krucas\Notification\Facades\Notification;
use Illuminate\Support\Facades\Redirect;

use App\Http\Controllers\admin\ImagesController;

use Intervention\Image as Image;

use Illuminate\Http\Request;


class ProductController extends Controller {



    public function index()
    {
//        $products = Product::with('subproduct', 'subproduct.stone')->with(['category' => function ($query) {
//            $query->orderBy('sort_order', 'desc');
//        }])->get();

        $products = Category::with('products', 'products.subproduct.stone')
            ->orderBy('sort_order', 'asc')
            ->with(['products' => function ($query) {
            $query->orderBy('sku', 'desc');
        }])
        ->with(['products.subproduct' => function ($query) {
            $query->orderBy('sku', 'asc');
        }])
            ->get();

        foreach ($products as $c) {
            foreach ($c->products as $p) {

                $count = Subproduct::where('splive', '=', 1)->where('product_id', $p->id)->count();

                if ($count == 0) {
                //    dd($p);
                }
            }
        }

        //dd($products[6]);
        return \View::make('admin.product.index')->with('product',$products) ;
    }

    public function show($id)
    {
        return \View::make('admin.product.show')->with('product',Product::find($id));
    }

    public function create()
    {
        $productsSelect = Product::Select('id', 'productName', 'sku')->get();

        $array = array();
        $array[0] = "Select";
        foreach ($productsSelect as $p) {
            $array[$p->id] = $p->sku . ' - ' . $p->productName;
        }

        return \View::make('admin.product.create')->with('categories',Category::all())->with('productsSelect', $array);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'productName' => 'required',
            'sku'  => 'required',
        ]);

        $allowable_tags = '<br><br/><strong></strong><em></em><ul></ul><li></li>';

        $product = new Product;
        $product->productName      = $request->input('productName');
        $product->slug             = \Slugify::slugify($request->input('productName'));
        $product->productDesc      = $request->input('productDesc');
        $product->sku              = $request->input('sku');
        $product->live             = $request->input('live');
        $product->category_id      = $request->input('category_id');
        $product->metaTitle        = $request->input('productName');
        $product->metaDescription  = $request->input('productName');
        $product->similar1         = $request->input('similar1');
        $product->similar2         = $request->input('similar2');
        $product->similar3         = $request->input('similar3');

        if ($request->hasFile('image'))
        {
            $file = $request->file('image');
            $filename  = str_replace('£', '', $file->getClientOriginalName()) . ' - ' . time() . '.' . $file->getClientOriginalExtension();
            $path = public_path('images/products/' . $filename);
            //http://image.intervention.io/api/resize

            \Image::make($file->getRealPath())
                ->resize(1500, 1500, function ($constraint) {
                    $constraint->aspectRatio();
                })
                ->resizeCanvas(1500,1500, 'center', false, 'ffffff')
                ->save($path);
            $product->image    			 = $filename;
            ImagesController::makethumbnail($filename);

        } else {
            $product->image = 'currently-awaiting-image.png';
        }
        //if (!$request->input('imageAlt')) {
            $product->imageAlt = $product->productName . ' image';
//        } else {
//            $product->imageAlt = $request->input('imageAlt');
//        }
        $product->save();

        Notification::success('The item was saved.');

        return Redirect::route('admin.product.index');

    }

    public function edit($id)
    {

        $cat = Category::all();

        foreach ($cat as $c) {
            $cat_array[$c->id] = $c->category;
        }


        $productsSelect = Product::Select('id', 'productName', 'sku')->get();

        $array = array();
        $array[0] = "Select";
        foreach ($productsSelect as $p) {
            $array[$p->id] = $p->sku . ' - ' . $p->productName;
        }
        return \View::make('admin.product.edit')->with('product', Product::find($id))->with('productsSelect', $array)
            ->with('categories',$cat_array);
    }

    public function update($id, request $request)
    {



        $rules = array(
            'productName' => 'required',
            'sku' => 'required',
        );


        $validator = Validator::make(Input::all(), $rules);

        $allowable_tags = '<br><br/><strong></strong><em></em><ul></ul><li></li>';

        $product = Product::find($id);

        if ($product->live == 0 and $request->input('live') == 1) {
        // If making a product live, make sure all subproducts are live.
            $count = Subproduct::where('splive', '=', 1)->where('product_id', $id)->count();

            if ($count == 0) {
                Notification::error('The product has no Live stones');

                return Redirect::to('admin/product/' . $id . '/edit')
                    ->withErrors($validator)
                    ->withInput();

            }
        }

        $product->productName      = $request->input('productName');
        $product->slug             = \Slugify::slugify($request->input('productName'));
        $product->productDesc      = $request->input('productDesc');
        $product->sku              = $request->input('sku');
        $product->live             = $request->input('live');
        //$product->category_id      = $request->input('category_id');
        $product->metaTitle        = $request->input('productName');
        $product->metaDescription  = $request->input('productName');
        $product->similar1         = $request->input('similar1');
        $product->similar2         = $request->input('similar2');
        $product->similar3         = $request->input('similar3');


        $product->save();
        if ($request->hasFile('image'))
        {
            $file = $request->file('image');

            $filename  = str_replace('£', '', $file->getClientOriginalName()) . ' - ' . time() . '.' . $file->getClientOriginalExtension();
            $path = public_path('images/products/' . $filename);
            //http://image.intervention.io/api/resize

            \Image::make($file->getRealPath())
                ->resize(1500,1500, function ($constraint) {
                    $constraint->aspectRatio();
                })
                ->resizeCanvas(1500,1500, 'center', false, 'ffffff')
                ->save($path);
            $product->image    			 = $filename;
            ImagesController::makethumbnail($filename);

        }

        if ($request->hasFile('image1')) {
            $file = $request->file('image1');

            $filename = $product->sku .  '-image1.' . $file->getClientOriginalExtension();
            $path = public_path('images/products/altimages/' . $filename);
            //http://image.intervention.io/api/resize

            \Image::make($file->getRealPath())
                ->resize(1500, 1500, function ($constraint) {
                    $constraint->aspectRatio();
                })
                ->resizeCanvas(1500, 1500, 'center', false, 'ffffff')
                ->save($path);
            $product->image1 = $filename;
            ImagesController::makethumbnail($filename);

        }
        if ($request->hasFile('image2')) {
            $file = $request->file('image2');

            $filename = $product->sku . '-image2.' . $file->getClientOriginalExtension();
            $path = public_path('images/products/altimages/' . $filename);
            //http://image.intervention.io/api/resize

            \Image::make($file->getRealPath())
                ->resize(1500, 1500, function ($constraint) {
                    $constraint->aspectRatio();
                })
                ->resizeCanvas(1500, 1500, 'center', false, 'ffffff')
                ->save($path);
            $product->image2 = $filename;
            ImagesController::makethumbnail($filename);

        }

        if ($request->hasFile('image3')) {
            $file = $request->file('image3');

            $filename = $product->sku . '-image3.' . $file->getClientOriginalExtension();
            $path = public_path('images/products/altimages/' . $filename);
            //http://image.intervention.io/api/resize

            \Image::make($file->getRealPath())
                ->resize(1500, 1500, function ($constraint) {
                    $constraint->aspectRatio();
                })
                ->resizeCanvas(1500, 1500, 'center', false, 'ffffff')
                ->save($path);
            $product->image3 = $filename;
            ImagesController::makethumbnail($filename);

        }

        if ($request->hasFile('image4')) {
            $file = $request->file('image4');

            $filename = $product->sku . '-image4.' . $file->getClientOriginalExtension();
            $path = public_path('images/products/altimages/' . $filename);
            //http://image.intervention.io/api/resize

            \Image::make($file->getRealPath())
                ->resize(1500, 1500, function ($constraint) {
                    $constraint->aspectRatio();
                })
                ->resizeCanvas(1500, 1500, 'center', false, 'ffffff')
                ->save($path);
            $product->image4 = $filename;
            ImagesController::makethumbnail($filename);

        }

        $product->imageAlt       = $request->input('imageAlt');
        $product->save();

        Notification::success('The item was saved.');

        return Redirect::route('admin.product.index');

    }

    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();

        Notification::success('The item was deleted.');

        return Redirect::route('admin.product.index');
    }

}

