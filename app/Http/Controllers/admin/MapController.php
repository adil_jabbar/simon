<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use App\Address;
use App\Role;

use App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Krucas\Notification\Facades\Notification;
use Illuminate\Support\Facades\Redirect;

use Intervention\Image as Image;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MapController extends Controller
{


    public function index()
    {
        //$users = Role::with('users')->where('name', '!=', 'admin')->get();
        //$users = User::with('address', 'roles')->whereHas('roles.name','=', 'admin')->get();

        $users = User::with('address')
            ->where('live', 0)
            ->get();

        // $users = User::whereHas('address', function ($query) {
        //     $query->where('lat', '!=', 0);
        // })->where('live', 0)->get();

        foreach ($users as $u) {
            if ($u->address != NULL) {
                if (strlen($u->address->postcode) > 6) {
                    if ($u->address->lat == 0) {
                        //postcode = '{{ str_replace(' ', '', $u->address->postcode) }}';

                        $url = 'https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyBRFVZKEMNf49aw7cFXEF7ygLpYUJqAvjY&address=' . str_replace(' ', '', strtoupper($u->address->postcode)) . ',+UK&sensor=false&key=AIzaSyCMRcmg27Effvcvq2dkccejqzatqb9IViY';
                        $data = json_decode(file_get_contents($url), true);

                        if (isset($data['results'][0])) {
                            $u->address->lat = $data['results'][0]['geometry']['location']['lat'];
                            $u->address->lng = $data['results'][0]['geometry']['location']['lng'];
                            $u->address->save();
                            Log::debug('Latitude added ' . $u->address->lat );
                        }
                    }

                }
            }
        }

        return \View::make('admin.map.indexnew')->with('users', $users);

    }

    public function mappoints()
    {

        $addresses = Address::where('lat', '')->get();

        foreach($addresses as $address) {
            if (strlen($address->postcode) > 4) {

                $url = 'https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyBRFVZKEMNf49aw7cFXEF7ygLpYUJqAvjY&address=' . str_replace(' ', '', strtoupper($address->postcode)) . ',+UK&sensor=false';
                $data = json_decode(file_get_contents($url), true);
                
                if (isset($data['results'][0])) {
                    $address->lat = $data['results'][0]['geometry']['location']['lat'];
                    $address->lng = $data['results'][0]['geometry']['location']['lng'];
                    $address->save();
                } else {
                    Log::debug('Google maps failed for ' . $url);
                }

            }
        }
    }
}

