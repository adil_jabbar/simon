<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Page;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Krucas\Notification\Facades\Notification;
use Illuminate\Support\Facades\Redirect;


use Illuminate\Http\Request;


class PagesController extends Controller {

	public function index()
	{
		return \View::make('admin.pages.index')->with('pages', Page::all());
	}

	public function show($id)
	{
		return \View::make('admin.pages.show')->with('page', Page::find($id));
	}

	public function create()
	{
		return \View::make('admin.pages.create');
	}

	public function store(Request $request)
	{


        $this->validate($request, [
            'title' => 'required',
            'body'  => 'required',
        ]);

        $page = new Page;
        $page->title   = $request->input('title');
        $page->slug    = \Slugify::slugify($request->input('title'));
        $page->body    = $request->input('body');
        $page->metatitle    = $request->input('metatitle');
        $page->meta    = $request->input('meta');
        $page->user_id = Auth::user()->id;
        $page->save();

        Notification::success('The page was saved.');

        return Redirect::route('admin.pages.index', $page->id);

	}

	public function edit($id)
	{
		return \View::make('admin.pages.edit')->with('page', Page::find($id));
	}

	public function update($id, Request $request)
	{
        $this->validate($request, [
            'title' => 'required',
            'body'  => 'required',
        ]);



        $page = Page::find($id);
        $page->title   = $request->input('title');
        $page->slug    = \Slugify::slugify($request->input('title'));
        $page->body    = $request->input('body');
        $page->metatitle    = $request->input('metatitle');
        $page->meta    = $request->input('meta');
        //$page->user_id = Auth::user()->id;
        $page->save();

        Notification::success('The page was saved.');

        return Redirect::route('admin.pages.index', $page->id);

	}

	public function destroy($id)
	{
		$page = Page::find($id);
		$page->delete();

		Notification::success('The page was deleted.');

		return Redirect::route('admin.pages.index');
	}

	
}
