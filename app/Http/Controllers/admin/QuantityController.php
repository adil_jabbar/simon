<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Subproduct;
use App\Category;

use Illuminate\Support\Facades\Auth;
use Krucas\Notification\Facades\Notification;
use Illuminate\Support\Facades\Redirect;

use Intervention\Image as Image;

use Illuminate\Http\Request;


class QuantityController extends Controller
{

    public function index($scroll = 1)
    {
        //$stock = Subproduct::with('product', 'stone', 'product.category')->orderBy('sku')->where('splive', '=', 1)->get();

        $stock = Category::with('products',  'products.subproduct.stone')
            ->orderBy('sort_order', 'asc')
            ->with(['products.subproduct' => function ($query) {
                $query->orderBy('sku', 'asc');
            }])
            ->get();


        return \View::make('admin.quantity.index')->with('stock', $stock)->with('qty', 99999)->with('category', '')->with('middle', $scroll);
    }

    public function quantitychange(Request $request, $scroll = 1)
    {
        $cat = $request->input('category');

        if ($request->input('selectqty') != "") {
            $quantity = $request->input('selectqty');
        } else {
            $quantity = 9999;
        }

        if ($cat != '') {
            $stock = Category::with('products', 'products.subproduct.stone')
                ->with(['products.subproduct' => function ($query) {
                    $query->orderBy('sku', 'asc');
                }])
                ->where('id', '=', $cat);
        } else {
            $stock = Category::with('products', 'products.subproduct.stone')
                ->with(['products.subproduct' => function ($query) {
                    $query->orderBy('sku', 'asc');
                }]);
        }

        $stock = $stock->get();

//        if ($cat == '7') {
//            $stock2 = Category::with('products', 'products.subproduct.stone')
//                ->with(['products.subproduct' => function ($query) {
//                    $query->where('newarrival', '=', 1);
//
//                }])
//                ->get();
////dd($stock2);
//            $stock->merge($stock2);
//        }





        return \View::make('admin.quantity.index')->with('stock', $stock)->with('qty', $quantity)->with('category', $cat)->with('middle', $scroll);
       // return Redirect::to('admin/quantity/' . $scroll)->with('stock', $stock)->with('qty', $quantity)->with('category', $cat);
        //return Redirect::to('admin/quantity/')->with('stock', $stock)->with('qty', $quantity)->with('category', $cat)->with('middle', $scroll);
    }

    public function update(Request $request)
    {

        $returnid = '';
        //dd($request->input('max'));

        foreach ($request->input('fob') as $key => $id) {

            $sub = Subproduct::with('product', 'product.category')->Where('id', '=', $request->input('id')[$key])->first();
            $sub->simon = 0;
            $sub->max = 0;
            $sub->demelza = 0;


            if (isset($request->input('quantity')[$key]) && $request->input('quantity')[$key] != "") {
                //dd($sub);
                $sub->quantity = $request->input('quantity')[$key];
                $returnid = $sub->id;
            }

            if (isset($request->input('fob')[$key]) && $request->input('fob')[$key] != "") {
                $sub->fob = $request->input('fob')[$key];
                $returnid = $sub->id;
            }

            $sub->freight = round($sub->fob * .02, 2);
            if ($sub->product->category->apply_bolt) {
                $sub->bolt = $sub->product->category->bolt_amt;
            } else {
                $sub->bolt = 0;
            }
            $sub->hm = 1;
            $sub->into_safe = $sub->fob + $sub->freight + $sub->bolt + $sub->hm;
            //$sub->grossPrice = round($sub->into_safe * 2);
            $sub->save();

            if (isset($request->input('grossPrice')[$key]) && $request->input('grossPrice')[$key] != "") {
                $sub->grossPrice = $request->input('grossPrice')[$key];
                $sub->save();
                $returnid = $sub->id;
            }
        }

        if ($request->input('max') != NULL) {
            foreach ($request->input('max') as $key => $id) {
                $sub = Subproduct::with('product', 'product.category')->Where('id', '=', $key)->first();
                $sub->max = $id;
                $sub->save();
            }
        }

        if ($request->input('simon') != NULL) {
            foreach ($request->input('simon') as $key => $id) {
                $sub = Subproduct::with('product', 'product.category')->Where('id', '=', $key)->first();
                $sub->simon = $id;
                $sub->save();
            }
        }

        if ($request->input('demelza') != NULL) {
            foreach ($request->input('demelza') as $key => $id) {
                $sub = Subproduct::with('product', 'product.category')->Where('id', '=', $key)->first();
                $sub->demelza = $id;
                $sub->save();
            }
        }
        if (strlen($request->input('category')) == '' && $request->input('qty') == 0) {
            $stock = Category::with('products',  'products.subproduct.stone')
                ->orderBy('sort_order', 'asc')
                ->with(['products.subproduct' => function ($query) {
                    $query->orderBy('sku', 'asc');
                }])
                ->get();

            return \View::make('admin.quantity.index')->with('stock', $stock)->with('qty', 9999)->with('category', '')->with('middle', $returnid);
            //return Redirect::to('admin/quantity/' . $returnid)->with('stock', $stock)->with('qty', 9999)->with('category', '');

        } else {

            $cat = $request->input('category');

            if ($request->input('qty') != "") {
                $quantity = $request->input('qty');
            } else {
                $quantity = 9999;
            }

            if ($cat != '') {
                $stock = Category::with('products',  'products.subproduct.stone')
                    ->with(['products.subproduct' => function ($query) {
                        $query->orderBy('sku', 'asc');
                    }])
                    ->where('id', '=', $cat);
            } else {
                $stock = Category::with('products', 'products.subproduct.stone')
                    ->with(['products.subproduct' => function ($query) {
                        $query->orderBy('sku', 'asc');
                    }]);
            }

            $stock = $stock->get();

            return \View::make('admin.quantity.index')->with('stock', $stock)->with('qty', $quantity)->with('category', $cat)->with('middle', $returnid);
            //return Redirect::to('admin/quantity/' . $returnid)->with('stock', $stock)->with('qty', 9999)->with('category', '');
            //return Redirect::to('admin/quantity/' . $returnid)->with('stock', $stock)->with('qty', $quantity)->with('category', $cat);
        }


        //return redirect(url('admin\quantity'));
    }

    // Liam Goldstein
    // new function to update product quantities using ajax.
    public function updateAJAX(Request $request) {
        $input = $request->all();

        $rowID = $input['rowid'];
        $fieldName = $input['name'];
        $fieldValue = $input['value'];

        if ($fieldName === 'simon[' . $rowID . ']') {
            if($fieldValue === '1') {
                $fieldValue = 1;
            } elseif ($fieldValue === '0') {
                $fieldValue = 0;
            }

            $sub = Subproduct::with('product', 'product.category')->Where('id', '=', $rowID)->first();
            $sub->simon = $fieldValue;
            $sub->save();

            return response()->json(['id' => $rowID, 'fieldname' => $fieldName, 'fieldval' => $fieldValue]);

        } else if($fieldName === 'max[' . $rowID . ']') {
            if($fieldValue === '1') {
                $fieldValue = 1;
            } elseif ($fieldValue === '0') {
                $fieldValue = 0;
            }

            $sub = Subproduct::with('product', 'product.category')->Where('id', '=', $rowID)->first();
            $sub->max = $fieldValue;
            $sub->save();

            return response()->json(['id' => $rowID, 'fieldname' => $fieldName, 'fieldval' => $fieldValue]);

        } else if($fieldName === 'demelza[' . $rowID . ']') {
            if($fieldValue === '1') {
                $fieldValue = 1;
            } elseif ($fieldValue === '0') {
                $fieldValue = 0;
            }

            $sub = Subproduct::with('product', 'product.category')->Where('id', '=', $rowID)->first();
            $sub->demelza = $fieldValue;
            $sub->save();

            return response()->json(['id' => $rowID, 'fieldname' => $fieldName, 'fieldval' => $fieldValue]);

        } elseif ($fieldName === 'grossPrice[' . $rowID . ']') {
            $sub = Subproduct::with('product', 'product.category')->Where('id', '=', $rowID)->first();

            $sub->grossPrice = $fieldValue;
            $sub->save();

            return response()->json(['id' => $rowID, 'fieldname' => $fieldName, 'fieldval' => $fieldValue]);

        } elseif ($fieldName === 'quantity[' . $rowID . ']') {
            $sub = Subproduct::with('product', 'product.category')->Where('id', '=', $rowID)->first();

            $sub->quantity = intval($fieldValue);
            $sub->save();

            return response()->json(['id' => $rowID, 'fieldname' => $fieldName, 'fieldval' => $fieldValue]);

        } elseif($fieldName === 'fob[' . $rowID . ']') {
            $sub = Subproduct::with('product', 'product.category')->Where('id', '=', $rowID)->first();

            $sub->fob = $fieldValue;

            $sub->freight = round($sub->fob * .02, 2);
            if ($sub->product->category->apply_bolt) {
                $sub->bolt = $sub->product->category->bolt_amt;
            } else {
                $sub->bolt = 0;
            }
            $sub->hm = 1;
            $sub->into_safe = $sub->fob + $sub->freight + $sub->bolt + $sub->hm;
            $sub->save();

            return response()->json(['id' => $rowID, 'fieldname' => $fieldName, 'fieldval' => $fieldValue, 
                'newFrt' => $sub->freight, 
                'newBolt' => $sub->bolt, 
                'newHm' => $sub->hm, 
                'newIs' => $sub->into_safe, 
                'newIsp' => $sub->into_safe * 2]);
        }
    }
}