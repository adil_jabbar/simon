<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Order;
use Illuminate\Support\Facades\DB;
use App\Subproduct;
use Carbon\Carbon;

class DashboardController extends Controller {

    public function index()
    {

        $date = Carbon::now();
        $firstOfQuarter = $date->firstOfQuarter();

        $date = Carbon::now();
        $firstOfMonth = $date->firstOfMonth();

        $date = Carbon::now();
        $firstOfYear = $date->firstOfYear();

//        dd($firstOfYear);

        $days = Order::selectraw('SUM(orders.value) AS value, SUM(orders.profit) AS profit, SUM(orders.vat) AS vat')
            ->whereRaw('(status = 1 or status = 2)')
            ->where('created_at', '>', $firstOfMonth)
            ->first();

        $valuemtd = $days['value'];
        $profitmtd = $days->profit;
        $vatmtd = $days->vat;

//        $date = Carbon::now()->subDays(90);
        $days = Order::selectraw('SUM(orders.value) AS value, SUM(orders.profit) AS profit, SUM(orders.vat) AS vat')
            ->whereRaw('(status = 1 or status = 2)')
            ->where('created_at', '>', $firstOfQuarter)
            ->first();

        $valueqtd = $days['value'];
        $profitqtd = $days->profit;
        $vatqtd = $days->vat;

//        $date = Carbon::now()->subDays(365);
        $days = Order::selectraw('SUM(orders.value) AS value, SUM(orders.profit) AS profit, SUM(orders.vat) AS vat')
            ->whereRaw('(status = 1 or status = 2)')
            ->where('created_at', '>', $firstOfYear)
            ->first();

        $valueytd = $days['value'];
        $profitytd = $days->profit;
        $vatytd = $days->vat;

        $stock = Subproduct::where('quantity', '>', 0)->get();

        $qty = 0;$safe=0;$sell=0;
        foreach ($stock as $s) {
            $qty = $s->quantity + $qty;
            $safe = $safe + ($s->quantity * $s->into_safe);
            $sell = $sell + ($s->quantity * $s->grossPrice);
        }

        $max = Subproduct::select(DB::raw('sum(into_safe) as total'))->where('max', '=', 1)->first();

        $simon = Subproduct::select(DB::raw('sum(into_safe) as total'))->where('simon', '=', 1)->first();

        $demelza = Subproduct::select(DB::raw('sum(into_safe) as total'))->where('demelza', '=', 1)->first();
        //dd($max->total);

        return \View::make('admin.dashboard.index')
            ->with('firstOfMonth', $firstOfMonth)
            ->with('firstOfQuater', $firstOfQuarter)
            ->with('firstOfYear', $firstOfYear)
            ->with('valuemtd', $valuemtd)
            ->with('profitmtd', $profitmtd)
            ->with('vatmtd', $vatmtd)
            ->with('valueqtd', $valueqtd)
            ->with('profitqtd', $profitqtd)
            ->with('vatqtd', $vatqtd)
            ->with('valueytd', $valueytd)
            ->with('profitytd', $profitytd)
            ->with('vatytd', $vatytd)
            ->with('pieces', $qty)
            ->with('safe', $safe)
            ->with('sell', $sell)
            ->with('max', $max)
            ->with('demelza', $demelza)
            ->with('simon', $simon);

    }

    public function edit($id)
    {
        //$query = "SELECT * FROM `company` WHERE  `company_name` =". $id;
        //$company = DB::select(DB::raw($query));
        return \View::make('admin.manage.edit')->with('company', Company::find($id));
    }

    public function find()
    {
        //$query = "SELECT * FROM `company` WHERE  `company_name` =". $id;
        //$company = DB::select(DB::raw($query));
        $company_name = trim(input::get("company"));
        $company = Company::where('company_name', '=', $company_name)->firstorfail();
        //log::debug('pxw!!! input data is ', array(trim(input::get('company'))) );
        //log::debug('pxw input data is ', array( $company ) );
        //var_dump($company);exit;
        return \View::make('admin.company.edit')->with('company', $company);
    }

    public function update($id)
    {
        log::debug('pxw!!! Update');
        $validation = new CompanyValidator;

        if ($validation->passes())
        {
            log::debug('pxw!!! Passed');
            $Company = Company::find($id);
            $Company->company_name   = Input::get('company_name');
            $Company->company_email    = Input::get('company_email');
            $Company->company_contact    = Input::get('company_contact');
            $Company->company_desc    = Input::get('company_desc');
            $Company->area = Sentry::getUser()->area;
            $Company->save();

            Notification::success('The Company was saved.');

            return Redirect::route('admin.manage.edit', $Company->id);
        }

        return Redirect::back()->withInput()->withErrors($validation->errors);
    }

    public function show($id)
    {

    }

    public function create()
    {

    }

    public function store()
    {
        log::debug('pxw!!! Log X');
    }

}
