<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use App\Address;
use App\Role;

use App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Krucas\Notification\Facades\Notification;
use Illuminate\Support\Facades\Redirect;

use Intervention\Image as Image;

use App\Mailers\WelcomeMailer as WelcomeMailer;

use Illuminate\Http\Request;


class UserController extends Controller
{


    protected $welcomeMailer;

    public function __construct(WelcomeMailer $welcomeMailer)
    {
        $this->welcomeMailer = $welcomeMailer;
    }

    public function index()
    {

        $users = Role::with(['users' => function ($query) {
            $query->orderBy('username', 'asc');
            $query->where('live', 0);
        }])->whereName('guest')
            ->first();

        return \View::make('admin.user.index')->with('users', $users->users);

    }

    public function type(Request $request)
    {
        $type = $request->input('type');
        $users = Role::with(['users' => function ($query) use($type) {

            $query->where('type', $type);
            $query->orderBy('username', 'asc');
        }])->whereName('guest')

            ->first();

        return \View::make('admin.user.index')->with('users', $users->users);

    }
    public function show($id)
    {
        return \View::make('admin.user.show')->with('user', User::find($id));
    }

    public function create()
    {
        return \View::make('admin.user.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'username' => 'max:255|unique:users',
            'email' => 'email|max:255',
            'password' => 'confirmed|min:6',
            'companyName' => 'required|max:255',
            'address1' => 'max:255',
            'town' => 'max:255',
            'county' => 'max:255',
            'postcode' => 'max:12',
            'phone' => 'max:20',
            'vat' => 'max:255',
            'companyID' => 'max:255',


        ]);

        $user = new User;
        $user->email = $request->input('email');
        //$user->username = $request->input('companyName');
        $user->username = strtolower($request->input('username'));
        $user->name_role = '';
        $user->name2 = $request->input('name2');
        $user->name2_role = $request->input('name2_role');
        $user->name3 = $request->input('name3');
        $user->name3_role = $request->input('name3_role');
        $user->notes = $request->input('notes');
        $user->type = $request->input('type');
        $user->agent = $request->input('agent');
        if (strlen($request->input('password')) > 0) {
            $user->password = bcrypt($request->input('password'));
        } else {
            $user->password = bcrypt('rosegold');
        }
        $user->companyName = $request->input('companyName');
        $user->save();

        $address = new Address;
        $address->companyName = $request->input('companyName');
        $address->address1 = $request->input('address1');
        $address->address2 = $request->input('address2');
        $address->town = $request->input('town');
        $address->county = $request->input('county');
        $address->postcode = $request->input('postcode');
        $address->phone = $request->input('phone');
        $address->vat = $request->input('vat');
        $address->companyID = $request->input('companyID');
        $address->user_id = $user->id;

        $address->save();


        if (strlen($address->postcode) > 4) {

            $url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' . str_replace(' ', '', strtoupper($address->postcode)) . ',+UK&sensor=false';
            $data = json_decode(file_get_contents($url), true);
            if (isset($data['results'][0])) {
                $address->lat = $data['results'][0]['geometry']['location']['lat'];
                $address->lng = $data['results'][0]['geometry']['location']['lng'];
                $address->save();
            } else {
                Log::debug('Google maps failed for ' . $url);
            }

        }
//        $role = new role_user;
//        $role->user_id = $user->id;
//        $role->role_id = 20;
//        $role->save();

        // Find the user just inserted.
        $insertedId = $user->id;
        $user = User::find($insertedId);

        //Find the Guest role
        if ($user->agent == 0) {
            $guest = Role::find(20);
        } else {
            $guest = Role::find(19);
        }
        //Attach the role to the guest.
        $user->attachRole($guest);

        Notification::success('The User was saved.');

        return Redirect::route('admin.user.index');

    }

    public function edit($id)
    {
        $user = User::with('address')->find($id);
        return \View::make('admin.user.edit')->with('user', $user);
    }

    public function update($id, request $request)
    {
        $this->validate($request, [
            'name' => 'max:255',
            'companyName' => 'required|max:255',
            'address1' => 'max:255',
            'town' => 'max:255',
            'county' => 'max:255',
            'postcode' => 'max:12',
            'phone' => 'max:20',
            'vat' => 'max:255',
            'companyID' => 'max:255',
        ]);


        $user = User::find($id);
        $emailsent = '';
        if ($user->confirmed != $request->input('confirmed')) {
            if ($user->confirmed == 1) {
                //The user has just been confirmed, so sent them a welcome email.

                $messagetext = 'Welcome to Simon Alexander Jewellery. Your registration is now authorised and you can now login with
                    your company name as your username '   . $user->username .   ' and the password you entered.';

                $data = array(
                    'messagetext' =>$messagetext,
                    'name'	=> $user->name,
                    'email' => $user->email,
                );
                $subject = "Welcome to Simon Alexander";

                // Send to client
                if (\App::environment('local')) {
                    $result = $this->welcomeMailer->welcome($user->email, $data, $subject);
                } else{
                    $result = $this->welcomeMailer->welcome($user->email, $data, $subject);
                }
                $emailsent = " Confirmation of membership email sent";
            }
        }

        $user->email = $request->input('email');
        $user->username = strtolower($request->input('username'));
       // $user->username = $request->input('companyName');
        $user->name_role = '';
        $user->confirmed = $request->input('confirmed');

        $user->name2 = $request->input('name2');
        $user->name2_role = $request->input('name2_role');
        $user->name3 = $request->input('name3');
        $user->name3_role = $request->input('name3_role');
        $user->notes = $request->input('notes');
        $user->type = $request->input('type');

        if ($request->input('password')) {
            $user->password = bcrypt($request->input('password'));
        }
        $user->companyName = $request->input('companyName');
        $user->save();

        $address = Address::where('user_id', '=', $user->id)->first();
        $address->companyName = $request->input('companyName');
        $address->address1 = $request->input('address1');
        $address->address2 = $request->input('address2');
        $address->town = $request->input('town');
        $address->county = $request->input('county');

        if ($address->postcode != $request->input('postcode')) {
            if (strlen($address->postcode) > 6) {
                $url = 'https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyBRFVZKEMNf49aw7cFXEF7ygLpYUJqAvjY&address=' . str_replace(' ', '', strtoupper($address->postcode)) . ',+UK&sensor=false';
                $data = json_decode(file_get_contents($url), true);
                if (isset($data['results'][0])) {
                    $address->lat = $data['results'][0]['geometry']['location']['lat'];
                    $address->lng = $data['results'][0]['geometry']['location']['lng'];
                    $address->save();
                }
            }
        }

        $address->postcode = $request->input('postcode');
        $address->phone = $request->input('phone');
        $address->vat = $request->input('vat');
        $address->companyID = $request->input('companyID');
        $address->user_id = $user->id;



        $address->save();
        $users = User::with('address')->get();

        foreach ($users as $u) {
            $u->companyName = $u->address->companyName;
            $u->save();
        }




        Notification::success('The User was saved. ' . $emailsent);

        return Redirect::route('admin.user.index');

    }

    public function destroy($id)
    {

        $user = User::find($id);

        //$address = Address::where('user_id', '=', $user->id);
        //$address->delete();

        $user->live = 1;
        $user->save();

        Notification::success('The User was deleted.');

        return Redirect::route('admin.user.index');
    }

}

