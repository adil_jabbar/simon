<?php namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Product;
use App\Postage;
use App\Order;
use App\Http\Requests;

use App\Subproduct;
use Illuminate\Support\Facades\Auth;
use Krucas\Notification\Facades\Notification;
use Illuminate\Support\Facades\Redirect;


use Intervention\Image as Image;

use Illuminate\Http\Request;


class OrderController extends Controller
{


    public function index()
    {
        $orders = Order::with('users', 'users.address')
            ->has('users')
            ->with('agent', 'users.address')
            //ignore deleted orders. Deleted must have been cancelled first
            ->where('status', '!=', 4)
            ->orderBy('status', 'ASC')->orderBy('id', 'DESC')->get();
        //dd($orders);
        return \View::make('admin.order.index')->with('orders', $orders);
    }

    public function show($id)
    {
        return \View::make('admin.product.show')->with('product', Product::find($id));
    }

    public function create()
    {
        return \View::make('admin.product.create')->with('categories', Category::all());
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'productName' => 'required',
            'sku' => 'required',

        ]);

        $allowable_tags = '<br><br/><strong></strong><em></em><ul></ul><li></li>';

        $product = new Product;
        $product->productName = $request->input('productName');
        $product->slug = \Slugify::slugify($request->input('productName'));
        $product->productDesc = $request->input('productDesc');
        $product->sku = $request->input('sku');
        $product->live = $request->input('live');
        $product->category_id = $request->input('category_id');
        $product->metaTitle = $request->input('productName');
        $product->metaDescription = $request->input('productName');

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $filename = $file->getClientOriginalName() . ' - ' . time() . '.' . $file->getClientOriginalExtension();
            $path = public_path('images/products/' . $filename);
            //http://image.intervention.io/api/resize

            \Image::make($file->getRealPath())
//                ->resize(500, 500, function ($constraint) {
//                    $constraint->aspectRatio();
//                })
//                ->resizeCanvas(500,500, 'center', false, 'ffffff')
                ->save($path);
            $product->image = $filename;

        }
        //if (!$request->input('imageAlt')) {
        $product->imageAlt = $product->productName . ' image';
//        } else {
//            $product->imageAlt = $request->input('imageAlt');
//        }
        $product->save();

        Notification::success('The item was saved.');

        return Redirect::route('admin.product.index');

    }

    public function edit($id)
    {
        $orders = Order::where('id', '=', $id)->with(['users' => function ($query) {
            $query->with('address');
        }])
            ->with(['orderitems' => function ($query) {
                $query->where('deleted', 0);
                $query->with(['subproducts' => function ($query) {
                    $query->with('product');
                }]);
            }])
            ->orderBy('created_at')->first();

        //$productsSelect = Subproduct::Select('id', 'sku')->orderBy('sku')->where('quantity', '>', 0)->get();
        // Simon wants to be able to add items even when no stock exists.
        $productsSelect = Subproduct::Select('id', 'sku')->orderBy('sku')->get();


        $array = array();
        $array[0] = "Select";
        foreach ($productsSelect as $p) {
            $array[$p->id] = $p->sku;
        }

        return \View::make('admin.order.edit')->with('orders', $orders)->with('productsSelect', $array);

    }

    public function update($id, request $request)
    {
        $this->validate($request, [
            'productName' => 'required',
            'productDesc' => 'required',
        ]);

        $allowable_tags = '<br><br/><strong></strong><em></em><ul></ul><li></li>';

        $product = Product::find($id);
        $product->productName = $request->input('productName');
        $product->slug = \Slugify::slugify($request->input('productName'));
        $product->productDesc = $request->input('productDesc');
        $product->sku = $request->input('sku');
        $product->live = $request->input('live');
        $product->category_id = $request->input('category_id');
        $product->metaTitle = $request->input('productName');
        $product->metaDescription = $request->input('productName');


        $product->save();
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $filename = $file->getClientOriginalName() . ' - ' . time() . '.' . $file->getClientOriginalExtension();
            $path = public_path('images/products/' . $filename);
            //http://image.intervention.io/api/resize

            \Image::make($file->getRealPath())
//                ->resize(500,500, function ($constraint) {
//                    $constraint->aspectRatio();
//                })
//                ->resizeCanvas(500,500, 'center', false, 'ffffff')
                ->save($path);
            $product->image = $filename;

        }
        $product->imageAlt = $request->input('imageAlt');
        $product->save();

        Notification::success('The item was saved.');

        return Redirect::route('admin.product.index');

    }

    public function updatecustomer($id, request $request){

        $order = Order::where('id', $id)->first();


        $order->user_id = $request->input['customer'];

        $order->save();


    }

    public function renew($id)
    {

        $order = Order::with('orderitems')->where('id', $id)->first();
        $order->status = 0;

        $order->save();

        if($order->is_yg == '1') {
            // Now we need to add to the Sales & Reorder screen for yellow gold items.
            foreach ($order->orderitems as $orderitem) {
                $sub = Subproduct::where('id', $orderitem->subproduct_id)->first();

                $newReorderQty = $sub->reorder_temp + ($sub->reorder_temp_yg + $orderitem->qty);
                $sub->reorder_temp_yg = $sub->reorder_temp_yg + $orderitem->qty;

                $sub->reorder_value = ($newReorderQty * $sub->fob);
                $sub->save();
            }
        }

        return Redirect::route('admin.orders.index');

    }

    public function destroy(Request $request)
    {
        $order = Order::where('id', $request->del_id)->first();

        if ($order) {
            // Status 4 = order deleted
            $order->status = 4;
            if ($order->save()) {
                if ($order->save()) {
                    return response(['msg' => 'Successfull'], 200)// 200 Status Code: Standard response for successful HTTP request
                    ->header('Content-Type', 'application/json');
                } else {
                    return response(['msg' => 'failed'], 401)
                   ->header('Content-Type', 'application/json');
                }
            }
        }

        return Redirect::route('admin.order.index');
    }

    /* ONE OFF FUNCTION USED TO RECALCULATE ORDER VALUE, VAT, POSTAGE AND PROFIT */
    /* CAN BE RE-USED IF NEEDED BY RE-ENABLING THE ROUTE. */
    public function recalc()
    {
        $orders = Order::with(['orderitems' => function ($query) {
                $query->where('deleted', 0);
                $query->with(['subproducts' => function ($query) {
                    $query->with('product');
                }]);
            }])
            ->where('status', '!=', 4)
            ->get();

        foreach($orders as $order) {
            $updatedAt = $order->updated_at;
            $orderID = $order->id;

            $items = $order->orderitems;

            $profit = 0;
            $value = 0;
            $postage = 0;

            foreach($items as $theItem) {

                $product = Subproduct::Select('id', 'grossPrice', 'into_safe', 'sku')->where('id', '=', $theItem->subproduct_id)->get();
                $profit = $profit + ( $theItem->qty * ( $product[0]['grossPrice'] - $product[0]['into_safe'] ) ) ;

                $value = $value + $theItem->qty * $product[0]['grossPrice'];

            }
                
            $order->profit = $profit;
            $order->subtotal = $value;
            $order->vat = $value * 0.2;

            $postalValue = $value + $order->vat;

            $postage = Postage::all();
            $postamt = 0;
          
            foreach ($postage as $p) {
                if (($postalValue > $p->lower) and ($postalValue < $p->upper)) {
                    $postamt = $p->amt;
                }
            }

            $order->postage = $postamt;
            $postageTax = $postamt * 0.2;
            $order->vat = $order->vat + $postageTax;

            $order->value = $value + $postamt + $order->vat;
            $order->updated_at = $updatedAt;

            $order->save();
        }

        return \View::make('admin.order.recalc');
    }

}

