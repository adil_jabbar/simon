<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Order;
use App\User;
use App\Orderitem;
use App\Emailtext;
use App\Subproduct;
use App\Postage;
use App\System;
use App\Http\Requests;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Krucas\Notification\Facades\Notification;
use Illuminate\Support\Facades\Redirect;

use Intervention\Image as Image;
use App\Http\Controllers\QuickBookController;

use Illuminate\Http\Request;
use Dompdf\Dompdf;
use App\Mailers\InvoiceMailer as Mailer;

class InvoiceController extends Controller
{
    protected $mailer;

    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function extra($id, request $request)
    {
        
        if($request->input('extraitem') === '' || $request->input('extraitem') === '0') {
            // not amount /product entered.

            Notification::error('Please select an extra product from the dropdown.');

            return Redirect::to('admin/orders/' . $id . '/edit');
        }

        $order = Order::where('id', $id)->first();

        // Added by Paul, don't do anything but have columns in db
        // $order->notes = $request->input('notes');
        // $order->extraAmount = $request->input('extraAmount');

        $sub = Subproduct::where('id', $request->input('extraitem'))->first();

        // Check that we have the quantity requested.
        // 12/05/2017 Simon doesn't want to stop this, if qty is going to go below zero, set it to zero.

       // if ($request->input('qty') > $sub->quantity) {
       //     Notification::error('Quantity unavailable. Max quantity is ' . $sub->quantity);

       //     return Redirect::to('admin/orders/' . $id . '/edit');
       // }

        $orderitem = new Orderitem();

        $orderitem->order_id = $request->input('id');
        $orderitem->is_yg = $request->input('is_yg');
        $orderitem->subproduct_id = $request->input('extraitem');
        $orderitem->qty = $request->input('qty');
        $orderitem->value = $orderitem->qty * $sub->grossPrice;
        $orderitem->added = 1;
        $orderitem->save();

        // calculate the new value of the order.

        $newItemValue = $orderitem->value;
        $newItemVat = (float) $newItemValue * (config('cart.tax') / 100);
        $order->vat = $order->vat + $newItemVat;
        $order->subtotal = $order->subtotal + $newItemValue;

        // Re-calculate postage
        $postage = Postage::all();
        $postamt = 0;

        $postalValue = ($order->subtotal + $newItemValue) + $order->vat;

        foreach ($postage as $p) {
            if (($postalValue > $p->lower) and ($postalValue < $p->upper)) {
                $postamt = $p->amt;
            }
        }
        $order->postage = $postamt;

        // Recalculate profit
        $profit = $order->profit;
        $profit = $profit + ($orderitem->qty * ($sub->grossPrice - $sub->into_safe));
        $order->profit = $profit;

        $order->value = $order->subtotal + $order->vat + $order->postage;

        $order->save();

        if($request->input('is_yg') == '1') {
            // Now we need to add to the Sales & Reorder screen for yellow gold items.
        
            $newReorderQty = $sub->reorder_temp + ($sub->reorder_temp_yg + $request->input('qty'));
            $sub->reorder_temp_yg = $sub->reorder_temp_yg + $request->input('qty');

            $sub->reorder_value = ($newReorderQty * $sub->fob);
            $sub->save();
        }

        Notification::success('Extra item added');

        return Redirect::to('admin/orders/' . $id . '/edit');
    }


    public function email_invoice($id)
    {
        $orders = Order::with('users', 'users.address')->where('orders.id', '=', $id)
            ->first();


        $invoice_path = storage_path() . '/Invoices/';
        $file = $invoice_path . '/Invoice_' . $id . '.pdf';  // <- Replace with the path to your .pdf file
        // check if the file exists
        if (file_exists($file)) {
            // Get the text for sending with the review
            $emailtext = Emailtext::find(6);

            $data = [
                'salutation' => $orders->users->name,
                'messagetext' => $emailtext->email_text
            ];


            $result = $this->mailer->invoice($orders->users->email, $data, $emailtext->email_subject, $file);

            if ($result) {
                $orders->invoiceSent = true;
                $orders->status = 2;
                $orders->save();
            }
            if (getenv('APP_ENV') == 'local') {
                $result = $this->mailer->invoice(\Config::get('custom_config.admin_email_local'), $data, $emailtext->email_subject, $file);
            } else {
                $result = $this->mailer->invoice(\Config::get('custom_config.admin_email'), $data, $emailtext->email_subject, $file);
            }
        }

        Notification::success('The invoice was sent.');

        return Redirect::to('admin/orders/' . $orders->id . '/edit');
    }

    public function order_delete($id)
    {
        $orders = Order::with('orderitems', 'orderitems.subproducts', 'users', 'users.address')->where('orders.id', '=', $id)
            ->first();

        //Quantity only updated
        if ($orders->status != 0) {
            foreach ($orders->orderitems as $orderitem) {
                $orderitem->subproducts->quantity = $orderitem->subproducts->quantity + $orderitem->qty;
                $orderitem->subproducts->save();

                if($orders->is_yg == '1') {
                    // Now we need to decrease the Sales & Reorder screen for yellow gold items.
                    $subproduct = Subproduct::where('id', $orderitem->subproduct_id)->first();
                
                    $newReorderQty = $subproduct->reorder_temp + ($subproduct->reorder_temp_yg - $orderitem->qty);
                    $subproduct->reorder_temp_yg = $subproduct->reorder_temp_yg - $orderitem->qty;

                    $subproduct->reorder_value = ($newReorderQty * $subproduct->fob);
                    $subproduct->save();
                }
            }
        }

        // Now we need to decrease the Sales & Reorder screen for yellow gold items.
        if($orders->is_yg == '1') {
            foreach ($orders->orderitems as $orderitem) {
                $subproduct = Subproduct::where('id', $orderitem->subproduct_id)->first();
            
                $newReorderQty = $subproduct->reorder_temp + ($subproduct->reorder_temp_yg - $orderitem->qty);
                $subproduct->reorder_temp_yg = $subproduct->reorder_temp_yg - $orderitem->qty;

                $subproduct->reorder_value = ($newReorderQty * $subproduct->fob);
                $subproduct->save();
            }
        }

        $orders->status = 3;
        $orders->save();

        Notification::success('The order was cancelled.');

        return Redirect::to('admin/orders/' . $orders->id . '/edit');
    }

    public function order_deleteitem(Request $request)
    {
        $is_yg = false;
        if($request->is_yg == '1') {
            $is_yg = true;
        }

        $orderID = $request->input('order_id');
        $order = Order::where('id', $orderID)->first();

        $id = $request->input('del_id');

        $orderitem = Orderitem::with('order')->where('id', $id)->first();

        \Log::info('This is some useful information.' . $id);

        $subproduct = Subproduct::where('id', $orderitem->subproduct_id)->first();
        if ($orderitem->order->status > 0 & $is_yg == false) {
            // add to stock quantity if Rose Gold only.
            $subproduct->quantity = $subproduct->quantity + $orderitem->qty;
            $subproduct->save();
        }

        $orderitem->deleted = 1;

        // calculate the new value of the order.
        $delItemValue = $orderitem->value;
        $delItemVat = (float) $delItemValue * (config('cart.tax') / 100);
        $order->vat = $order->vat - $delItemVat;
        $order->subtotal = $order->subtotal - $delItemValue;

        // Re-calculate postage
        $postage = Postage::all();
        $postamt = 0;

        $postalValue = ($order->subtotal - $delItemValue) + $order->vat;

        foreach ($postage as $p) {
            if (($postalValue > $p->lower) and ($postalValue < $p->upper)) {
                $postamt = $p->amt;
            }
        }
        $order->postage = $postamt;

        // Recalculate profit
        $profit = $order->profit;
        $profit = $profit - ($orderitem->qty * ($subproduct->grossPrice - $subproduct->into_safe));
        $order->profit = $profit;

        $order->value = $order->subtotal + $order->vat + $order->postage;

        $order->save();

        if($is_yg) {
            // Now we need to decrease the Sales & Reorder screen for yellow gold items.
        
            $newReorderQty = $subproduct->reorder_temp + ($subproduct->reorder_temp_yg - $orderitem->qty);
            $subproduct->reorder_temp_yg = $subproduct->reorder_temp_yg - $orderitem->qty;

            $subproduct->reorder_value = ($newReorderQty * $subproduct->fob);
            $subproduct->save();
        }

        \Log::info('Order item removed.' . $orderitem->id);

        if ($orderitem->save()) {
            return response(['msg' => 'Successfull', 'order_value' => number_format((float) $order->value, 2), 'order_vat' => number_format((float) $order->vat, 2), 'order_postage' => number_format((float) $order->postage, 2)], 200) // 200 Status Code: Standard response for successful HTTP request
            ->header('Content-Type', 'application/json');
        } else {
            return response(['msg' => 'failed'], 401)
                ->header('Content-Type', 'application/json');
        }
    }


    public function view_invoice($id)
    {

        $invoice_path = storage_path() . '/Invoices/';
        $file = $invoice_path . '/Invoice_' . $id . '.pdf';  // <- Replace with the path to your .pdf file
        // check if the file exists
        if (file_exists($file)) {
            // read the file into a string
            $content = file_get_contents($file);
            // create a Laravel Response using the content string, an http response code of 200(OK),
            //  and an array of html headers including the pdf content type
            return \Response::make($content, 200, array('content-type' => 'application/pdf'));
        }
    }


    public function generate_invoice($id)
    {

        $orders = Order::with('orderitems')->where('id', $id)
            ->first();


        $qbo_obj = new QuickBookController;

        //First create the customer if they don't exist in QB
        $user = User::with('address')->find($orders->user_id);
        $qbid = $user->qbid;

        $data = array();
        $data['name'] = $user->username;
        $data['name2'] = $user->name2;
        $data['companyName'] = $user->address->companyName;
        $data['addr1'] = $user->address->address1;
        $data['addr2'] = $user->address->address2;
        $data['city'] = $user->address->town;
        $data['county'] = $user->address->county;
        $data['postcode'] = $user->address->postcode;
        $data['email'] = $user->email;
        $data['phone'] = $user->phone;
        $data['vat'] = $user->vat;

        if ($user->qbid == 0) {
            $qb = $qbo_obj->createCustomer($data);

            if (!is_array($qb)) {
                $user->qbid = $qb;
                $user->save();
                $qbid = $user->qbid;
                \Log::info('QB Customer added.' . $qb);
            } else {
                Notification::error($qb[1]);
            }
        }
        //Now send the data to QB.

        $invoiceArray = array();
        $invoiceArray['docNumber'] = $orders->id;
        $invoiceArray['date'] = Carbon::createFromFormat('Y-m-d H:i:s', $orders->created_at)->format('Y-m-d');

        $agent = null;
        if ($orders->agent_id != 0) {
            $user = User::find($orders->agent_id);
            $agent = $user->username;
        }

        \Log::info('QB About to add Invoice. Agent ID is ' . $orders->agent_id);
        \Log::info('QB About to add Invoice. QB ID is ' . $qbid);

        $qb = $qbo_obj->addInvoice($invoiceArray, $orders, $qbid, $agent, $data);
        if (!is_array($qb)) {

            // only do the below for Rose Gold items.
            if($orders->is_yg == '0') {
                // Decrease the quantity's bought
                foreach ($orders->orderitems as $orderitem) {

                    $subproduct = Subproduct::where('id', $orderitem->subproduct_id)->first();
                    $subproduct->quantity = $subproduct->quantity - $orderitem->qty;
                    //As of email 12/5/2017, Simon wants to be able to add items manually that don't exist, therefore
                    //if quantity below zero, set to zero.
                    if ($subproduct->quantity < 0) {
                    $subproduct->quantity = 0;
                    }
                    $subproduct->save();
                }
            }

            // Set order status to Invoice Generated.
            $orders->status = 1;
            $orders->save();
            Notification::success('The invoice was generated.');
        } else {
            Notification::error($qb[1]);
        }

        //Notification::success('The invoice was generated.');

        return Redirect::to('admin/orders/' . $orders->id . '/edit');
    }


    public function generate_invoice_old($id)
    {

//        $orders = Order::find($id)
//            ->with(['users' => function ($query) {
//            $query->with('address');
//            }])
//            ->with(['orderitems' => function ($query) {
//                $query->with(['subproducts' => function ($query) {
//                    $query->with('stone');
//                    $query->with(['product' => function ($query) {
//                        $query->with(['category' => function ($query) {
//                            $query->orderBy('category');
//                        }]);
//                    }]);
//                }]);
//            }])
//
//            ->orderBy('subproducts.product.category.category')
//            ->first();

        //dd($orders);

//        $orders = SELECT
//        *
//        FROM orders
//  INNER JOIN order_items
//    ON orders.id = order_items.order_id
//  INNER JOIN subproducts
//    ON order_items.subproduct_id = subproducts.id
//  INNER JOIN products
//    ON subproducts.product_id = products.id
// INNER JOIN categories
//    ON products.category_id = categories.id
//GROUP BY categories.category
//ORDER BY categories.slug


        $orders = \DB::table('orders')
            ->select(\DB::raw('*, orders.value as totalvalue'))
            ->join('order_items', 'orders.id', '=', 'order_items.order_id')
            ->join('subproducts', 'order_items.subproduct_id', '=', 'subproducts.id')
            ->join('products', 'subproducts.product_id', '=', 'products.id')
            ->join('stones', 'subproducts.stone_id', '=', 'stones.id')
            ->join('categories', 'products.category_id', '=', 'categories.id')
            ->join('users', 'orders.user_id', '=', 'users.id')
            ->join('addresses', 'addresses.user_id', '=', 'users.id')
            ->orderBy('categories.slug')
            ->where('orders.id', '=', $id)
            ->get();


//        $address = \DB::table('users')
//            ->join('addresses', 'addresses.user_id', '=', 'users.id')
//            ->where('users.id', '=', $orders[0]->user_id)
//            ->get();

//        dd($orders);

        if ($orders[0]->invoiceNo === 0) {
            $inv = System::find(0)->first();
            $no = $inv->invoiceNo + 1;
            $ord = Order::where('id', '=', $id)->first();
            $ord->invoiceNo = $no;
            $ord->status = 1;
            $ord->save();
            $inv->invoiceNo = $no;
            $inv->save();

            // Update the quantity for the product.
            foreach ($orders as $orderitem) {
                $subproduct = Subproduct::where('id', $orderitem->subproduct_id)->first();
                $subproduct->quantity = $subproduct->quantity - $orderitem->qty;
                $subproduct->save();
            }

        } else {
            $no = $orders[0]->invoiceNo;
        }

        $html = '

<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>Invoice</title>
</head>

<body style="width:800px;font-size:.7em">

<div style="width:700px;margin-right:100px;"> <img src="./assets/images/invoice_header.jpg" alt="img1" width="100%" />

 </div>

<div style="clear:both;height: 20px"></div>

<table width="800px" border="0" cellspacing="0" cellpadding="0">
  <tr style="height:100px">
    <td width="300px">&nbsp;</td>
    <td><div style="border:1px solid #ccc;padding:10px;float:left;text-align:center;
			width:100px;font-weight:bold;font-size:20px;">INVOICE</div></td>
    <td>&nbsp;</td>
  </tr>
</table>
<br/><br/><br/>
<table>
  <tr>
    <td width="250px">&nbsp;</td>
    <td><span style="margin-top: 20px"> VAT Registration No: 921 3082 59 <br/>
      </span></td>
    <td>&nbsp;</td>
  </tr>
  </table>

<table>
  <tr>
    <td width="250px">&nbsp;</td>
    <td><span style="font-weight:bold">Invoice number: ' . $no . '</span></td>
    <td>&nbsp;</td>
  </tr>
  </table>

<table>
   <tr>
    <td width="250px">&nbsp;</td>
    <td>Date of invoice: ' . date('jS F Y') . '</td>
    <td>&nbsp;</td>
  </tr>
</table>
<br/>
<table width="800px" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>&nbsp;</td>
    <td>' . $orders[0]->companyName . '</td>
    <td>&nbsp;</td>
  </tr>
  <br/>
  <tr>
    <td>&nbsp;</td>
    <td>' . $orders[0]->address1 . '</td>
    <td>&nbsp;</td>
  </tr>

  <br/>';

        if (trim(strlen($orders[0]->address2)) > 0) {
            $html .= '<tr>
            <td>&nbsp;</td>
            <td>' . $orders[0]->address2 . '</td>
            <td>&nbsp;</td>
          </tr>
          <br/>';
        }


        $html .= '

 <br/>
  <tr>
    <td>&nbsp;</td>
    <td>' . $orders[0]->town . '</td>
    <td>&nbsp;</td>
  </tr>
 <br/>
  <tr>
    <td>&nbsp;</td>
    <td>' . $orders[0]->county . '</td>
    <td>&nbsp;</td>
  </tr>
 <br/>
  <tr>
    <td>&nbsp;</td>
    <td>' . $orders[0]->postcode . '</td>
    <td>&nbsp;</td>
  </tr>

  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>

  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><span style="font-weight:bold">Attention: </span>:' . $orders[0]->username . '</td>

  </tr>
</table>

<table width="800px" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="800px">Your order details are as follows: </td>
  </tr>
</table>
<br/>
<table width="700px" border="0" cellspacing="0" cellpadding="0">';
        $html .= '<tr>
					<td style="width:90px">Category</td>
					<td style="width:60px">Code</td>
					<td style="width:200px">Product Name</td>
					<td style="width:180px">Stone</td>
					<td style="width:100px">Qty</td>
					<td style="min-width:150px;text-align: right">Cost</td>
				</tr>';
        $x = 0;
        $prev = '';
        $total = 0;
        foreach ($orders as $orderitem) {

            if ($orderitem->category != $prev) {
                $html .= '<tr style="height:40px;font-family: Arial;font-weight: 900">
					<td>' . $orderitem->category . '</td>
				</tr>';
                $prev = $orderitem->category;
            }

            if ($x == 0) {
                $html .= '<tr>
                    <td style="width:90px">&nbsp;</td>
					<td style="width:60px">' . $orderitem->sku . '</td>
					<td style="width:200px">' . $orderitem->productName . '</td>
					<td style="width:180px">' . $orderitem->stone . '</td>
					<td style="width:100px">' . $orderitem->qty . '</td>

					<td style="min-width:150px;text-align: right">£' . number_format($orderitem->value, 2, '.', '') . '</td>
				</tr>';
                $x = 1;
            } else {
                $html .= '<tr style="background-color: #fdeeee">
                    <td style="width:90px">&nbsp;</td>
					<td style="width:60px">' . $orderitem->sku . '</td>
					<td style="width:200px">' . $orderitem->productName . '</td>
					<td style="width:180px">' . $orderitem->stone . '</td>
					<td style="width:100px">' . $orderitem->qty . '</td>

					<td style="min-width:150px;text-align: right">£' . number_format($orderitem->value, 2, '.', '') . '</td>
				</tr>';
                $x = 0;
            }
        }

        $order = Order::where('id', $id)->first();
        if (strlen($order->notes) > 3) {
            $html .= '<tr>
					<td colspan="4">' . $order->notes . '</td>

					<td style="width:50px">Adjustments</td>
					<td style="min-width:150px;text-align: right">£' . number_format($order->extraAmount, 2, '.', '') . '</td>
				</tr>';

        }


        $html .= '<tr>
					<td style="width:900px"></td>
					<td style="width:60px"></td>
					<td style="width:250px"></td>
					<td style="width:180px"></td>
					<td style="width:50px">Sub Total</td>
					<td style="min-width:150px;text-align: right">£' . number_format(round($order->subtotal + $order->extraAmount, 2), 2, '.', '') . '</td>
				</tr>';


        $html .= '<tr>
					<td style="width:900px"></td>
					<td style="width:60px"></td>
					<td style="width:250px"></td>
					<td style="width:180px"></td>
					<td style="width:50px">VAT</td>
					<td style="min-width:150px;text-align: right">£' . number_format(round($order->vat + ($order->extraAmount * 0.2), 2), 2, '.', '') . '</td>
				</tr>';


        $postage = Postage::all();
        $postamt = 0;
        $total = $order->value + ($order->extraAmount * 1.2);

        foreach ($postage as $p) {
            if (($total > $p->lower) and ($total < $p->upper)) {
                $postamt = $p->amt;
            }
        }

        $html .= '<tr>
					<td style="width:900px"></td>
					<td style="width:60px"></td>
					<td style="width:250px"></td>
					<td style="width:180px"></td>
					<td style="width:50px">Postage</td>
					<td style="min-width:150px;text-align: right">£' . number_format(round($postamt, 2), 2, '.', '') . '</td>
				</tr>';


        $html .= '<tr>
					<td style="width:900px"></td>
					<td style="width:60px"></td>
					<td style="width:250px"></td>
					<td style="width:180px"></td>
					<td style="width:50px">Total</td>
					<td style="min-width:150px;text-align: right">£' . number_format(round($order->value + $postamt + ($order->extraAmount * 1.2), 2), 2, '.', '') . '</td>
				</tr>';


        $html .= '</table>



<div style="width:700px; position: fixed;height:100px;margin-right:100px;
   bottom: 0px;"> <img src="./assets/images/invoice_footer.jpg" alt="img1" width="100%" />

 </div>
</body>
</html>


';

        $pdf = new Dompdf();
        $pdf->loadHTML($html);


        $invoice_path = storage_path() . '/Invoices/';

        if (!\File::exists($invoice_path)) {
            // path does not exist
            \File::makeDirectory($invoice_path);
        }

        \File::delete($invoice_path . '/Invoice_' . $orders[0]->order_id . '.pdf');
        //unlink(storage_path() . '/Invoices/Invoice_' . $listing->id    . '.pdf');

        $pdf->render();

        //$pdf->save($invoice_path . '/Invoice_' . $orders->id . '.pdf');


        $output = $pdf->output();
        file_put_contents($invoice_path . '/Invoice_' . $orders[0]->order_id . '.pdf', $output);

        // Now reduce the qty of stock


        Notification::success('The invoice was generated.');

        return Redirect::to('admin/orders/' . $orders[0]->order_id . '/edit');
    }

}

