<?php

namespace App\Http\Controllers\admin;


use Carbon\Carbon as Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Product;
use App\Subproduct;
use App\Category;
use App\Stone;

class ImagesController extends Controller
{
    //
    public function index()
    {
        $products = NULL;

        return \View::make('admin.image.index')->with('products', $products);
    }

    public function selectcat(Request $request)
    {

        $cat = $request->input('category');

        $products = Product::with('subproduct')->where('category_id', $cat)->get();

        return \View::make('admin.image.index')->with('products', $products);


    }


    public function edit($id)
    {
        $subproduct = Subproduct::with('product')->where('id', $id)->first();

        return \View::make('admin.image.edit')->with('categories', Category::all())->with('stones', Stone::orderBy('stone', 'asc')->get())
            ->with('product', $subproduct);
    }

    public function saveimage(Request $request)
    {

        $id = $request->input('id');

        $subproduct = Subproduct::where('id', $id)->first();

        $data = $request->input('avatar_data');
//        \Log::info($request->input('avatar_data'));
//        \Log::info($data['height']);


        if ($subproduct) {

            $filename = $subproduct->image;
            $folderPath = "/images/products";

            if (empty($subproduct->imagebeforecropping)) {
                $path = public_path('images/products/' . $filename);

                $dest = public_path('images/products/beforecropping/' . $filename);
                IF (!File::copy($path, $dest)) {
                    return response()->json(['success' => false, 'data' => 'Backup failed']);
                }
                $subproduct->imagebeforecropping = $filename;
                $subproduct->save();
            }

            $file = Storage::get($filename);


            //Now rename the file to something nice.

            $filename = str_replace('/', '-', str_replace(' ', '-', $subproduct->sku)) . '-' . time() . '.jpg';
            $path = public_path('images/products/' . $filename);

            // http://image.intervention.io/
            if ($file) {
                \Image::make($file)->crop(
                    intval($data['height']),
                    intval($data['width']),
                    intval($data['x']),
                    intval($data['y'])
                )
                    ->resize(1500, 1500)
                    ->resizeCanvas(1500, 1500, 'center', false, 'ffffff')
                    ->save($path);

                $subproduct->image = $filename;

                $subproduct->save();

                ImagesController::makethumbnail($filename);

                return response()->json(['success' => true, 'data' => 'All good', 'id' => $id]);

//                return \Redirect::route('admin.images.edit', array('id' => $id));
//                return redirect()->route('admin.images.edit', $id);
            } else {
                return response()->json(['success' => false, 'data' => 'File not found']);
            }

        }

        return response()->json(['success' => false, 'data' => 'Subprduct not found']);


    }



    public function restoreimage($id)
    {


        $subproduct = Subproduct::where('id', $id)->first();

        $path = public_path('images/products/beforecropping/' . $subproduct->imagebeforecropping);

        $dest = public_path('images/products/' . $subproduct->imagebeforecropping);
        if (!File::copy($path, $dest)) {
            return \Redirect::route('admin.images.edit', array('id' => $id));
        }
        $subproduct->image = $subproduct->imagebeforecropping;
        $subproduct->save();

        return \Redirect::route('admin.images.edit', array('id' => $id));
    }

    public function makecanvasbigger($id)
    {


        $subproduct = Subproduct::where('id', $id)->first();

        $filename = $subproduct->image;

        if (empty($subproduct->imagebeforecropping)) {
            $path = public_path('images/products/' . $filename);

            $dest = public_path('images/products/beforecropping/' . $filename);
            IF (!File::copy($path, $dest)) {
                return \Redirect::route('admin.images.edit', array('id' => $id));
            }
            $subproduct->imagebeforecropping = $filename;
            $subproduct->save();
        }


        $path = public_path('images/products/' . $filename);

        $file = Storage::get($filename);

        list($width, $height, $type, $attr) = getimagesize($path);

        $x = $width + 500;
        $y = $height + 500;
        //Now rename the file to something nice.

        // http://image.intervention.io/
        if ($file) {
            \Image::make($file)->resizeCanvas($x, $y, 'center', false, 'ffffff')
                ->save($path);

            return \Redirect::route('admin.images.edit', array('id' => $id));
        }
    }

    public function makethumbnailsfromexisting () {

        $product = Product::get();

        foreach ($product as $p) {
            $path = $p->image;
            self::makethumbnail($path);
        }

        $product = Subproduct::get();

        foreach ($product as $p) {
            $path = str_replace('£', '',$p->image);

            self::makethumbnail($path);
        }
    }

    public static function makethumbnail ($filename) {

//        dd(public_path('images/products/' . $filename));
        if (file_exists(public_path('images/products/' . $filename))) {
            $file = Storage::get($filename);


            //Now rename the file to something nice.


            // http://image.intervention.io/
            if ($file) {

                $path = public_path('images/products/thumbnails/' . $filename);

                \Image::make($file)
                    ->resize(100, 100)
                    ->resizeCanvas(100, 100, 'center', false, 'ffffff')
                    ->save($path);

            }
        }

    }


}
