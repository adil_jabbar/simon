<?php namespace App\Http\Controllers;

use App\Product;
use App\Subproduct;
use App\Category;
use App\Homepage;
use App\Settings;

class ProductController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */

    public function show($slug)
    {


        //This code gets "New Arrivals" from Product & Subproduct, puts them in a multidimensional array and then sorts them by date
        $product = Product::where('category_id', '=', 7)
            ->where('live', 1)
            ->get();

        $sub = Subproduct::with('product')->where('newarrival', 1)->where('splive', 1)
            ->orderBy('created_at', 'DESC')
            ->LIMIT(26)
            ->get();

        $newarrivals = array();
        foreach ($product as $p) {
            $newarrivals[] = array("image" => $p->image, "slug" => $p->slug, "sku" => $p->sku, "date" => $p->updated_at);

        }

        foreach ($sub as $p) {
            $newarrivals[] = array("image" => $p->image, "slug" => $p->product->slug, "sku" => $p->sku, "date" => $p->updated_at);
        }

        $date = array();
        foreach ($newarrivals as $key => $row) {
            $date[$key] = $row['date'];
        }
        array_multisort($date, SORT_DESC, $newarrivals);

        $product = Product::where('slug', $slug)
            ->with(array('subproduct' => function ($query) {
                $query->where('splive', '=', 1);
                $query->orderBy('sku', 'asc');
            }))
            ->first();

        $instock = false;

        foreach ($product->subproduct as $s) {
            if ($s->quantity > 0) {
                $instock = true;
            }
        }

        $category = Category::with('products')
            ->with(array('products' => function ($query) {
                $query->where('live', '=', 1);
            }))
            ->orderBy('sort_order')->get();

        $p1 = null;$p2=null;$p3=null;

        if ($product->similar1 != 0)
            $p1 = Product::with('subproduct')->find($product->similar1);


        if ($product->similar2 != 0)
            $p2 = Product::with('subproduct')->find($product->similar2);

        if ($product->similar3 != 0)
            $p3 = Product::with('subproduct')->find($product->similar3);

        $itemcode = null;
        if (isset($_GET['sku'])) {
            $item = Subproduct::where('sku', $_GET['sku'])->first();
            if ($item) {
                $itemcode = $item->id;
            }
        }


        return view('product', compact('itemcode'))->with('instock', $instock)->with('newarrivals', $newarrivals)
            ->with('product', $product)->with('category', $category)
            ->with('p1', $p1)->with('p2', $p2)->with('p3', $p3)
            ;

    }

}
