<?php namespace App\Http\Controllers;

use DB;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;

use App\User;
use App\Order;
use App\Orderitem;
use App\Address;
use Illuminate\Support\Facades\Auth;
use App\Subproduct;
use App\Product;
use App\Category;
use Krucas\Notification\Facades\Notification;

class PricesController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Checkout Controller
    |--------------------------------------------------------------------------
    |
    */

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function hide()
    {

        if (!Auth::user()) {
            return view('loggedout', array('category' => Category::with('products')->orderBy('id')->get()));
        }

        $user = User::find(Auth::user()->id);

        $user->prices = 1;

        $user->save();

        return back();

    }


    public function wholesale()
    {

        if (!Auth::user()) {
            return view('loggedout', array('category' => Category::with('products')->orderBy('id')->get()));
        }

        $user = User::find(Auth::user()->id);

        $user->prices = 0;

        $user->save();

        return back();

    }

    public function rrp()
    {

        if (!Auth::user()) {
            return view('loggedout', array('category' => Category::with('products')->orderBy('id')->get()));
        }

        $user = User::find(Auth::user()->id);

        $user->prices = 2;

        $user->save();

        return back();

    }



}
