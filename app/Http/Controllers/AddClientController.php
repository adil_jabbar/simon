<?php namespace App\Http\Controllers;


//use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Subproduct;
use App\Product;
use App\User;
use App\Role;
use App\Address;
use App\Category;
use Krucas\Notification\Facades\Notification;
class AddClientController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Home Controller
    |--------------------------------------------------------------------------
    |
    */

     /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    function index()
    {
        $category = Category::with('products')
            ->with(array('products' => function ($query) {
                $query->where('live', '=', 1);
            }))
            ->orderBy('sort_order')->get();

        $product = Product::where('category_id', '=', 7)
            ->where('live', 1)
            ->get();

        $sub = Subproduct::with('product')->where('newarrival', 1)->where('splive', 1)
            ->orderBy('created_at', 'DESC')
            ->LIMIT(26)
            ->get();

        $newarrivals = array();
        foreach ($product as $p) {
            $newarrivals[] = array("image" => $p->image, "slug" => $p->slug, "sku" => $p->sku, "date" => $p->updated_at);

        }

        foreach ($sub as $p) {
            $newarrivals[] = array("image" => $p->image, "slug" => $p->product->slug, "sku" => $p->sku, "date" => $p->updated_at);
        }

        $date = array();
        foreach ($newarrivals as $key => $row) {
            $date[$key] = $row['date'];
        }
        array_multisort($date, SORT_DESC, $newarrivals);


        return \View::make('addclient')->with('category', $category)->with('newarrivals', $newarrivals);
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'username' => 'required|max:255|unique:users',
            'email' => 'required|email|max:255',
            'password' => 'confirmed|min:6',
            'name'     => 'required',
            'companyName' => 'required|max:255',
            'address1' => 'max:255',
            'town' => 'max:255',
            'county' => 'max:255',
            'postcode' => 'max:12',
            'phone' => 'max:20',
            'vat' => 'max:255',
            'companyID' => 'max:255',

        ]);

        $user = new User;
        $user->email = $request->input('email');
        //$user->username = $request->input('companyName');
        $user->username = strtolower($request->input('username'));
        $user->name_role = '';
        $user->name2 = $request->input('name');
        $user->name2_role = $request->input('name_role');
        $user->notes = ' ';

        $type = 0;
        if (Auth::user()->username == "demelza") {
            $type = 3;
        }
        if (Auth::user()->username == 'simonagent') {
            $type = 0;
        }
        if (Auth::user()->username == 'max') {
            $type = 1;
        }
        if (Auth::user()->username == 'target') {
            $type = 2;
        }

        $user->type = $type;
        $user->agent = 0;

        if (strlen($request->input('password')) > 0) {
            $user->password = bcrypt($request->input('password'));
        } else {
            $user->password = bcrypt('rosegold');
        }
        $user->companyName = $request->input('companyName');
        $user->save();


        $address = new Address;
        $address->companyName = $request->input('companyName');
        $address->address1 = $request->input('address1');
        $address->address2 = $request->input('address2');
        $address->town = $request->input('town');
        $address->county = $request->input('county');
        $address->postcode = $request->input('postcode');
        $address->phone = $request->input('phone');
        $address->vat = $request->input('vat');
        $address->companyID = $request->input('companyID');
        $address->user_id = $user->id;

        if (strlen($address->postcode) > 4) {

            $url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' . str_replace(' ', '', strtoupper($address->postcode)) . ',+UK&sensor=false';
            $data = json_decode(file_get_contents($url), true);
            if (isset($data['results'][0])) {
                $address->lat = $data['results'][0]['geometry']['location']['lat'];
                $address->lng = $data['results'][0]['geometry']['location']['lng'];
                $address->save();
            } else {
                Log::debug('Google maps failed for ' . strtoupper($address->postcode));
            }

        }

        $address->save();

        // Find the user just inserted.
        $insertedId = $user->id;
        $user = User::find($insertedId);


        $guest = Role::find(20);

        //Attach the role to the guest.
        $user->attachRole($guest);

        Notification::success('The User was saved.');

        return Redirect::route('add-client.index');

    }

}
