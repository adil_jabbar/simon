<?php namespace App\Http\Controllers;


//use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use Illuminate\Http\Request;

use Cart;
use App\Subproduct;
use App\Product;
use App\Postage;
use App\Category;
use Krucas\Notification\Facades\Notification;
class CartController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Home Controller
    |--------------------------------------------------------------------------
    |
    */

     /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function cart() {

        $cart = Cart::content();

        $postage = Postage::all();
        $postamt = 0;
        $total = $this->tofloat(Cart::total());

        foreach ($postage as $p) {
            if (($total > $p->lower) and ($total < $p->upper)) {
                $postamt = $p->amt;
            }
        }

        //This code gets "New Arrivals" from Product & Subproduct, puts them in a multidimensional array and then sorts them by date
        $product = Product::where('category_id', '=', 7)
            ->where('live', 1)
            ->get();

        $sub = Subproduct::with('product')->where('newarrival', 1)->where('splive', 1)
            ->orderBy('created_at', 'DESC')
            ->LIMIT(26)
            ->get();

        $newarrivals = array();
        foreach ($product as $p) {
            $newarrivals[] = array("image" => $p->image, "slug" => $p->slug, "sku" => $p->sku, "date" => $p->updated_at);

        }

        foreach ($sub as $p) {
            $newarrivals[] = array("image" => $p->image, "slug" => $p->product->slug, "sku" => $p->sku, "date" => $p->updated_at);
        }

        $date = array();
        foreach ($newarrivals as $key => $row) {
            $date[$key] = $row['date'];
        }
        array_multisort($date, SORT_DESC, $newarrivals);

        $category = Category::with('products')
            ->with(array('products' => function ($query) {
                $query->where('live', '=', 1);
            }))
            ->orderBy('sort_order')->get();


        $postageTax = $postamt * 0.2;
        $tax = $this->tofloat(Cart::tax() + $postageTax);

        $total = ($postamt) + $total + $postageTax;
        $subtotal = $this->tofloat(Cart::subtotal());

        return view('cart', array('cart' => $cart, 'title' => 'Welcome', 'description' => '',
            'page' => 'home',
            'category' => $category,
            'newarrivals' => $newarrivals,
            'postamt' => $postamt,
            'total' => $total,
            'subtotal' => $subtotal,
            'tax' => $tax));
    }

    public function cartAdd(Request $request) {

        $rules = [
            'product_id' => 'required',
            'qty'  => 'required',
            'qty-yg'  => 'required',
        ];

        if( !empty($request->get('qty')) ) {
            $rules['qty-yg'] = ''; 
        }

        if( !empty($request->get('qty-yg')) ) {
            $rules['qty'] = ''; 
        }

        $this->validate($request, $rules);

        $product_id = $request->product_id;

        $subproduct = Subproduct::find($product_id);

        if ($request->isMethod('post')) {

            // process rose gold

            $successMsg = '';
            $total = 0;

            if( !empty( $request->get('qty') ) ) {
                $successMsg .= $request->qty . ' ' . $subproduct->product->productName . ' - ' . $subproduct->stone->stone . ' (Rose Gold) added to basket.<br/>';

                $total = ($total + (int) $request->get('qty'));
            }

            // process yellow gold
            if( !empty($request->get('qty-yg') ) ) {
                $successMsg .= $request->qty . ' ' . $subproduct->product->productName . ' - ' . $subproduct->stone->stone . ' (Yellow Gold) added to basket.<br/>';

                $total = ($total + (int) $request->get('qty-yg'));
            }

            $cartItem = Cart::add(array(
                'id' => $product_id, 
                'sku' => $subproduct->sku, 
                'name' => $subproduct->product->productName . ' - ' . $subproduct->stone->stone,
                'qty' => $total, 
                'price' => $subproduct->grossPrice, 
                'options' => ['qty' => $subproduct->sku, 'qty-rg' => $request->get('qty'), 'qty-yg' => $request->get('qty-yg'), 'eoline' => $subproduct->eoline])
            );

            Cart::associate($cartItem->rowId, 'App\Subproduct');
        }

        Notification::success($successMsg);

        return redirect(url('product/'. $subproduct->product->slug));

    }

    public function cartIncDec(Request $request) {


    //increment the quantity
    if ($request->rowId && $request->increment== 1) {

        $rowId = $request->rowId;
        $item = Cart::get($rowId);

        if(isset($request->gold) && $request->gold === 'yg') {
            $item->options['qty-yg'] = ((int) $item->options['qty-yg'] + 1);
        } else {
            $item->options['qty-rg'] = ((int) $item->options['qty-rg'] + 1);
        }

        Cart::update($rowId, $item->qty + 1);
    }

    //decrease the quantity
    if ($request->rowId && $request->decrease == 1) {
        $rowId = $request->rowId;
        $item = Cart::get($rowId);

        if(isset($request->gold) && $request->gold === 'yg') {
            $item->options['qty-yg'] = ((int) $item->options['qty-yg'] - 1);
        } else {
            $item->options['qty-rg'] = ((int) $item->options['qty-rg'] - 1);
        }

        Cart::update($rowId, $item->qty - 1);
    }

    $cart = Cart::content();

    return redirect(url('cart'));
}


    public function cartDelete(Request $request) {

        //increment the quantity
        if ($request->rowId && $request->delete == 1) {

            $rowId = $request->rowId;
            $item = Cart::get($rowId);

            Cart::remove($rowId);
        }

        $cart = Cart::content();

        return redirect(url('cart'));
    }
    public function clearcart() {

        $cart = Cart::destroy();

        return redirect(url('cart'));
    }

    function tofloat($num)
    {
        $dotPos = strrpos($num, '.');
        $commaPos = strrpos($num, ',');
        $sep = (($dotPos > $commaPos) && $dotPos) ? $dotPos :
            ((($commaPos > $dotPos) && $commaPos) ? $commaPos : false);

        if (!$sep) {
            return floatval(preg_replace("/[^0-9]/", "", $num));
        }

        return floatval(
            preg_replace("/[^0-9]/", "", substr($num, 0, $sep)) . '.' .
            preg_replace("/[^0-9]/", "", substr($num, $sep + 1, strlen($num)))
        );
    }


}
