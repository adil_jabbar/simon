<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class AuthAdmin {

    protected $auth;

    public function __construct(Guard $auth) {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next) {


        if ($this->auth->guest()) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('auth/login');
            }
            // if they aren't admin, redirect to homepage
        } else if(! $request->user()->hasRole('admin') && !$request->user()->hasRole('assistantAdmin') && !$request->user()->hasRole('minAdmin')) {
            return redirect()->guest('/');
        }
        return $next($request);
    }

}