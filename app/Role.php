<?php namespace App;
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 14/07/2015
 * Time: 10:55
 */



use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    public function users()
    {
        return $this->belongsToMany('App\User');
    }
}