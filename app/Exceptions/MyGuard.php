<?php namespace App\Extensions;

use Illuminate\Auth\Guard;

class MyGuard extends Guard
{
    public function agent()
    {
        return !$this->check();
    }
}