<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $dates = ['created_at', 'updated_at'];
    public function users()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
    public function orderitems()
    {
        return $this->hasMany('App\Orderitem');
    }

    public function agent()
    {
        return $this->hasOne('App\User', 'id', 'agent_id');
    }

}
