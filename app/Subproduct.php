<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subproduct extends Model
{
    //
    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id', 'id');
    }
    public function stone()
    {
        return $this->hasOne('App\Stone', 'id', 'stone_id');
    }
}
