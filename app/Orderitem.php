<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orderitem extends Model

{
    protected $table = 'order_items';
    protected $dates = ['created_at', 'updated_at'];

    public function subproducts()
    {
        return $this->belongsTo('App\Subproduct', 'subproduct_id', 'id');
    }
    public function order()
    {
        return $this->belongsTo('App\Order');
    }

}
