<?php namespace App\Mailers;
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 11/02/2015
 * Time: 14:49
 */
Use Mail;


abstract class Mailer {


    public function sendTo($user, $subject, $view, $data = [], $attach_file = null )
    {

        if ($attach_file === null) {

            $result = Mail::send($view, $data, function ($message) use ($user, $subject, $view, $data) {

                if (\App::environment('local')) {
                    $message->to($user)
                        ->from($data['email'],$data['name'])
                        ->subject($subject);

                } else {
                    $message->to($user)
                        ->from($data['email'],$data['name'])
                        ->subject($subject);

                }

            });


        } else {


            $result = Mail::send($view, $data, function ($message) use ($user, $subject,$attach_file, $view) {

                if (\App::environment('local')) {
                    $message->to($user)
                        ->attach($attach_file)
                        ->subject($subject);
                } else {
                    $message->to($user)
                        ->attach($attach_file)
                        ->subject($subject);
                }

            });


        }

        return $result;

    }
}
