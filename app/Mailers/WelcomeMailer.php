<?php namespace App\Mailers;
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 11/02/2015
 * Time: 15:13
 */
Class WelcomeMailer extends Mailer{

    public function welcome($user, $data, $subject)
    {

        $view = 'emails.welcome';

        //$subject = 'Thank You';
        return $this->sendTo($user, $subject, $view, $data);
    }
}