<?php namespace App\Mailers;
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 11/02/2015
 * Time: 15:13
 */
Class ContactMailer extends Mailer{

    public function contact($user, $data)
    {

        $view = 'emails.contact';

        //$subject = $data['propertyname'];
        $subject = "Website contact";
        return $this->sendTo($user, $subject, $view, $data);
    }
}