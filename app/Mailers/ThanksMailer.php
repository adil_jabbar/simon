<?php namespace App\Mailers;
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 11/02/2015
 * Time: 15:13
 */
Class ThanksMailer extends Mailer{

    public function thanks($user, $data, $subject)
    {

        $view = 'emails.thanks';

        //$subject = 'Thank You';
        return $this->sendTo($user, $subject, $view, $data);
    }
}