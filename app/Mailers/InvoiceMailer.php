<?php namespace App\Mailers;
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 11/02/2015
 * Time: 15:13
 */


Class InvoiceMailer extends Mailer{

    public function invoice($user, $data, $subject, $file)
    {

        $view = 'emails.invoice';

        return $this->sendTo($user, $subject, $view, $data, $file);
    }
}