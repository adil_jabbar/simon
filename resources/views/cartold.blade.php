@extends('layouts.master')

@section('content')


    <div class="container-fluid container-inset wrapper">

        <div class="row">

            <div class="col-md-12 col-xs-12 col-sm-12" >

                {!! Notification::container('myContainer')->all() !!}

                <section id="cart_items">
                    <div class="container" style="width:auto;">

                        <div class="table-responsive cart_info">
                            @if(count($cart))
                                <table class="table table-condensed">
                                    <thead>
                                    <tr class="cart_menu">
                                        <td class="image">Item</td>
                                        <td class="description"></td>
                                        <td class="price">Price</td>
                                        <td class="quantity">Quantity</td>
                                        <td class="total">Total</td>
                                        <td></td>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($cart as $item)
                                        <tr>
                                            <td class="cart_product">
                                                <img width="80" height="80"
                                                     src="/images/products/{{ $item->model->image }}"
                                                     alt="{{ $item->model->imageAlt }}">
                                            </td>
                                            <td class="cart_description">
                                                <h4><a href="">{{$item->name}}</a></h4>
                                                {{--<p>Web ID: {{$item->id}}</p>--}}
                                            </td>
                                            <td class="cart_price">
                                                <p>£{{$item->price}}</p>
                                            </td>
                                            <td class="cart_quantity">
                                                <div class="cart_quantity_button">
                                                    <a class="cart_quantity_up"
                                                       href="{{url("cartIncDec?rowId=$item->rowId&increment=1")}}">
                                                        + </a>
                                                    <input class="cart_quantity_input" type="text" name="quantity"
                                                           value="{{$item->qty}}" autocomplete="off" size="2">
                                                    <a class="cart_quantity_down"
                                                       href="{{url("cartIncDec?rowId=$item->rowId&decrease=1")}}">
                                                        - </a>
                                                </div>
                                            </td>
                                            <td class="cart_total">
                                                <p class="cart_total_price">£{{$item->subtotal}}</p>
                                            </td>
                                            <td class="cart_delete"><p>
                                                    <a class="cart_quantity_delete"
                                                       href="{{url("cartDelete?rowId=$item->rowId&delete=1")}}">X</a>
                                                </p>
                                            </td>
                                        </tr>
                                    @endforeach
                                    @else
                                        <p>You have no items in the shopping cart</p>
                                    @endif
                                    </tbody>
                                </table>
                        </div>
                    </div>
                </section> <!--/#cart_items-->

                <section id="do_action">
                    <div class="container">

                        <div class="rowx">

                            <div class="col-md-5 col-md-offset-6 col-xs-12 col-sm-12">

                                    <div id="carttotal">

                                            <div class="col-xs-9 col-md-5 col-md-offset-5 col-sm-2 col-sm-offset-6">Basket total:</div><div class="col-xs-3 col-md-2 col-sm-2"
                                                                                                          style="text-align: right"> <span>£{{Cart::subtotal()}}</span></div>
                                            <div class="col-xs-9 col-md-5 col-md-offset-5 col-sm-2 col-sm-offset-6">Post & Packing:</div><div class="col-xs-3 col-md-2 col-sm-2"
                                                                                                            style="text-align: right"> <span>Free</span></div>
                                            <div class="col-xs-9 col-md-5 col-md-offset-5 col-sm-2 col-sm-offset-6">VAT:</div><div class="col-xs-3 col-md-2 col-sm-2"
                                                                                                 style="text-align: right"> <span>£{{Cart::tax()}}</span></div>
                                            <div class="col-xs-9 col-md-5 col-md-offset-5 col-sm-2 col-sm-offset-6">Total:</div><div class="col-xs-3 col-md-2 col-sm-2"
                                                                                                   style="text-align: right"> <span>£{{Cart::total()}}</span></div>



                                    </div>


                                    {{--<ul id="carttotal">--}}
                                        {{--<li>Cart Sub Total (inc VAT): <span>£{{Cart::total()}}</span></li>--}}

                                        {{--<li>Delivery: <stong>Free</stong></li>--}}
                                        {{--<li>Total: <span>£{{Cart::total()}}</span></li>--}}
                                    {{--</ul>--}}
                                    <div style="margin-top:10px" class="col-xs-12 col-lg-6 col-lg-offset-5 col-md-8 col-md-offset-3 col-sm-4 col-sm-offset-6" >
                                        <a class="btn btn-default update" href="{{url('clear-cart')}}">Clear Cart</a>
                                        <a class="btn btn-default green" href="{{url('checkout')}}">Check Out</a>
                                    </div>

                            </div>

                        </div>
                    </div>
                </section><!--/#do_action-->


            </div>


        </div>
        <div class="push"></div>

    </div>


@stop