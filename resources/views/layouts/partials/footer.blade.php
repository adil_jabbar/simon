<footer class="footer">
    <div class="row">
        <div class=" col-xs-12">
            <div class="col-sm-4 col-xs-12 footer-numbers">
                <a href="tel:01395578373">01395 578373</a>
            </div>
            <div class="col-sm-4  col-xs-12">
                <img src="/images/sa_logo.png"/>
            </div>
            <div class="col-sm-4 col-xs-12 footer-numbers">
                <a href="mailto:sales@simonalexanderuk.com">sales@simonalexanderuk.com</a>
            </div>
        </div>

        <div class=" col-xs-12">
            <span style="color:lightblue;margin-top:10px;margin-bottom:10px">Copyright © 2016 - <?php echo date("Y"); ?>. All rights reserved</span>
        </div>
    </div>
</footer>

{{--<script>--}}
{{--$("#footer").hover(function () {--}}
{{--$(this).children('.slide').slideToggle("slow");--}}
{{--});--}}`
{{--</script>--}}


<script src="/js/jquery.slicknav.min.js"></script>

{{--<script src="/js/jquery.easing.js"></script><!-- jQuery Easing effects -->--}}
{{--<script src="/js/megamenu_plugins.js"></script><!-- Mega Menu Plugins (scroller, form, hoverIntent) -->--}}
{{--<script src="/js/megamenu.js"></script><!-- Mega Menu Script -->--}}
<script>


    $(function () {
        $('.mobilemenu').slicknav();
    });

    $(document).ready(function ($) {
//        $('.megamenu').megaMenuReloaded({
//            menu_speed_show: 300, // Time (in milliseconds) to show a drop down
//            menu_speed_hide: 200, // Time (in milliseconds) to hide a drop down
//            menu_speed_delay: 200, // Time (in milliseconds) before showing a drop down
//            menu_effect: 'open_close_slide', // Drop down effect, choose between 'hover_fade', 'hover_slide', 'click_fade', 'click_slide', 'open_close_fade', 'open_close_slide'
//            menu_easing: 'jswing', // Easing Effect : 'easeInQuad', 'easeInElastic', etc.
//            menu_click_outside: 1, // Clicks outside the drop down close it (1 = true, 0 = false)
//            menu_show_onload: 0, // Drop down to show on page load (type the number of the drop down, 0 for none)
//            menubar_trigger: 1, // Show the menu trigger (button to show / hide the menu bar), only for the fixed version of the menu (1 = show, 0 = hide)
//            menubar_hide: 0, // Hides the menu bar on load (1 = hide, 0 = show)
//            menu_responsive: 1, // Enables mobile-specific script
//            menu_carousel: 0, // Enable / disable carousel
//            menu_carousel_groups: 0 // Number of groups of elements in the carousel
//        });
//        $('#megamenu_form').ajaxForm({target: '#alert'});

        $('#prices').hover(function () {
            $('.img_list').css('display', 'inline');
        });

        $('.notprices').hover(function () {
            $('.img_list').css('display', 'none');
        });

//        $(window).resize(function () {
//            var footerHeight = $('.footer').outerHeight();
//            var stickFooterPush = $('.push').height(footerHeight);
//
//            $('.wrapper').css({'marginBottom': '-' + footerHeight + 'px'});
//        });
//
//        $(window).resize();
    });


</script>