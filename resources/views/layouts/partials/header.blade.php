<header id="header">
    <nav id="main-nav" class="navbar navbar-default " role="navigation">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    {{--<a class="navbar-brand img-responsive" href="/"></a>--}}
                    <a href="/">
                        <div class="abc"></div>
                    </a>
                    {{--{{ Auth::user() }}--}}
                    {{--@if(Auth::check())--}}
                    {{--<a href="{{ url('auth/logout') }}">Logout</a>--}}
                    {{--@else--}}
                    {{--<a href="{{ url('auth/login') }}">Login</a>--}}
                    {{--@endif--}}


                </div>

                <div class="col-md-12">

                    <div class="megamenu_wrapper megamenu_container">
                        <ul class="megamenu" style="padding-top:0px;padding-bottom:0px">

                            <li class="megamenu_button"><span>Menu</span></li>

                            {{--<li style="float:left"><span class="drop">☰</span><!-- Begin Item -->--}}
                            <li style="float:left;width:30px;"><span class="drop"><img
                                            src="/assets/images/menuicon.jpg"/></span>


                                <div class="megamenu_fullwidth" id="submenu"><!-- Begin Item Container -->


                                    <div class="megamenu_tabs"><!-- Begin Tabs Container -->

                                        <ul style="width:6%;text-align: left;padding-left: 0px;"
                                            class="megamenu_tabs_nav">
                                            <li class="addli"><a id="prices" href="#section1" class="current">PRICES</a>
                                            </li>
                                        </ul>

                                        <ul id="addui">
                                            <li class="addli notprices"><a class="adda" href="/payment">Payment</a></li>
                                            <li class="addli notprices"><a class="adda"
                                                                           href="/delivery-charges">Postage</a></li>
                                            {{--<li class="addli notprices"><a class="adda" href="/stones">Stones</a></li>--}}
                                            {{--<li class="addli notprices"><a class="adda" href="/about">About Us</a></li>--}}
                                            <li class="addli notprices"><a class="adda" href="/contact">Contact</a></li>
                                            <li class="addli notprices">
                                                @if(Auth::check())
                                                    <a class="adda" href="{{ url('/editprofile') }}">Edit Profile</a>


                                                @endif</li>
                                            @if(!Auth::user()->is('guest'))
                                                @if(!Auth::user()->is('minAdmin'))
                                                <li class="addli notprices">

                                                    <a class="adda" href="{{ url('/add-client') }}">Add Client</a>


                                                </li>
                                                @endif
                                            @endif

                                            @if(Auth::user()->is('admin') || Auth::user()->is('assistantAdmin') || Auth::user()->is('minAdmin'))
                                                <li class="addli notprices"><a class="adda" href="{{ url('/admin') }}">Admin
                                                        Area</a></li>
                                            @endif
                                            <li class="addli notprices">
                                                @if(Auth::check())

                                                    <a class="adda" href="{{ url('auth/logout') }}">Logout</a>
                                                @else
                                                    <a class="adda" href="{{ url('auth/login') }}">Login</a>
                                                @endif</li>
                                        </ul>

                                        <div class="clear"></div>


                                        <div class="megamenu_tabs_panels"><!-- Begin Panels Container -->


                                            <div id="section1"><!-- Begin Section 1 -->


                                                <ul class="img_list" style="display: none">

                                                    <li><a href="/hide">Hide</a></li>
                                                    <li><a href="/wholesale">Wholesale</a></li>
                                                    <li><a href="/rrp">RRP</a></li>


                                                </ul>


                                            </div><!-- End Section 1 -->


                                        </div><!-- End Panels Container -->


                                    </div><!-- End Tabs Container -->


                                </div><!-- End Item Container -->


                            </li><!-- End Item -->
                            @foreach($category as $c)
                                @if ($c->sort_order > 0)
                                    <li><span class="drop"><a href="/category/{{$c->slug}}">{{$c->category}}</a></span>

                                        <div class="megamenu_fullwidth">

                                            @foreach($c->products as $p)
                                                @if (count($p->subproduct) > 0)

                                                    @if (strlen($p->image)>2)

                                                        <a href="/product/{{ $p->slug }}">
                                                            <figure><img
                                                                        src="/images/products/thumbnails/{{ $p->image }}"
                                                                        width="100"
                                                                        height="100"/>
                                                                <figcaption>{{ $p->sku }}</figcaption>
                                                            </figure>
                                                        </a>


                                                    @endif

                                                @endif
                                            @endforeach


                                        </div>
                                    </li>
                                @endif
                            @endforeach


                            <li>
                                @if (Cart::count())
                                    <a id="cartIcon" href="{{ url('/cart') }}"><img
                                                src="/assets/images/cart-clipart-shopping-cart-md.png" width="25"
                                                height="25"/> </a>
                                @endif
                            </li>


                            @foreach($category as $c)
                                @if ($c->sort_order == 0)
                                    <li><span class="drop"><a href="/category/{{$c->slug}}">{{$c->category}}</a></span>

                                        <div class="megamenu_fullwidth">
                                            <?php $x = 0; ?>
                                            @foreach ($newarrivals as $key => $row)
                                                @if ($x < 26)
                                                    <a href="/product/{{ $row['slug'] }}?sku={{ $row['sku'] }}">
                                                        <figure><img
                                                                    src="/images/products/thumbnails/{{ $row['image'] }}"
                                                                    width="100"
                                                                    height="100"/>
                                                            <figcaption>{{ $row['sku'] }}</figcaption>
                                                        </figure>
                                                    </a>
                                                    <?php $x++; ?>
                                                @endif
                                            @endforeach

                                            {{--@foreach($newarrivals as $p)--}}

                                            {{--@if ($p->category_id == 7)--}}
                                            {{--@if (count($p->subproduct) > 0)--}}
                                            {{--@if (strlen($p->image)>2)--}}
                                            {{--<a href="/product/{{ $p->slug }}">--}}
                                            {{--<figure><img--}}
                                            {{--src="/images/products/{{ $p->image }}"--}}
                                            {{--width="100"--}}
                                            {{--height="100"/>--}}
                                            {{--<figcaption>{{ $p->sku }}</figcaption>--}}
                                            {{--</figure>--}}
                                            {{--</a>--}}
                                            {{--@endif--}}
                                            {{--@endif--}}
                                            {{--@else--}}
                                            {{--@foreach($p->subproduct as $s)--}}

                                            {{--@if (file_exists('images/products/'. $p->subproduct->image ))--}}
                                            {{--@if ($s->newarrival == 1 and $s->splive == 1)--}}
                                            {{--<a href="/product/{{ $p->slug }}">--}}
                                            {{--<figure><img--}}
                                            {{--src="/images/products/{{ $s->image }}"--}}
                                            {{--width="100"--}}
                                            {{--height="100"/>--}}
                                            {{--<figcaption>{{ $s->sku }}</figcaption>--}}
                                            {{--</figure>--}}
                                            {{--</a>--}}
                                            {{--@endif--}}
                                            {{--@endforeach--}}
                                            {{--@endif--}}
                                            {{--@endforeach--}}

                                        </div>
                                    </li>
                                @endif
                            @endforeach


                        </ul>

                    </div>


                    {{--New mobile menu--}}


                    <div>
                        <ul class="mobilemenu">
                            <li>☰
                                <ul>
                                    <li>Prices
                                        <ul>
                                            <li><a href="/hide">Hide</a></li>
                                            <li><a href="/wholesale">Wholesale</a></li>
                                            <li><a href="/rrp">RRP</a></li>
                                        </ul>

                                    </li>

                                    <li><a href="/payment">Payment</a></li>
                                    <li><a href="/delivery-charges">Postage</a></li>
                                    {{--<li><a href="/stones">Stones</a></li>--}}
                                    {{--<li><a href="/about">About Us</a></li>--}}
                                    <li><a href="/contact">Contact</a></li>
                                    @if(!Auth::user()->is('guest'))
                                        <li>

                                            <a href="{{ url('/add-client') }}">Add Client</a>


                                        </li>
                                    @endif

                                     @if(Auth::user()->is('admin') || Auth::user()->is('assistantAdmin') )
                                        <li><a href="{{ url('/admin') }}">Admin
                                                Area</a></li>
                                    @endif
                                    <li>
                                        @if(Auth::check())
                                            <a href="{{ url('auth/logout') }}">Logout</a>
                                        @else
                                            <a href="{{ url('auth/login') }}">Login</a>
                                        @endif
                                    </li>

                                </ul>
                            </li>


                            @foreach($category as $c)
                                @if ($c->sort_order > 0)
                                    <li><span class="drop"><a href="/category/{{$c->slug}}">{{$c->category}}</a></span>

                                    </li>
                                @endif
                            @endforeach



                            @foreach($category as $c)
                                @if ($c->sort_order == 0)
                                    <li><span class="drop"><a href="/category/{{$c->slug}}">{{$c->category}}</a></span>

                                    </li>
                                @endif
                            @endforeach
                            <li>
                                @if (Cart::count())
                                    <a id="cartIcon" href="{{ url('/cart') }}"><img
                                                src="/assets/images/cart-clipart-shopping-cart-md.png"
                                                width="25"
                                                height="25"/> </a>
                                @endif
                            </li>
                        </ul>
                    </div>


                </div>
            </div>
        </div>
    </nav>
</header>