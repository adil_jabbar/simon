@extends('layouts.master')

@section('content')


    <div class="container-fluid container-inset wrapper">


        <div class="row">

            <div class="col-md-12 col-xs-12 col-sm-12">
                <br/>
                <h2>Delivery Address:</h2>
                <br/>
                @if (Auth::user()->is('agent') or Auth::user()->is('admin')or Auth::user()->is('assistantAdmin')or Auth::user()->is('minAdmin'))

                    <p>N/A - Please Select Customer</p>
                @else
                <p>{{ $user->address->companyName }}<br/>
                    {{ $user->address->address1 }}<br/>
                    @if ($user->address->address2){{ $user->address->address2 }}<br/>@endif
                    {{ $user->address->town }}<br/>{{ $user->address->county }}<br/>
                    {{ $user->address->postcode }}</p>
                @endif
                <h2>Your Order</h2>


                <div class="table-responsive">
                    @if(count($cart))
                        <table class="table">
                            <thead>
                            <tr>
                                <td class="col-xs-1">Item</td>
                                <td class="col-sm-5 col-xs-3"><span class="mobilehide">Description</span>
                                    <span class="mobileshow">Code</span></td>
                                <td class="col-sm-1 col-xs-2">Price</td>
                                <td class="col-xs-1">Qty</td>
                                <td class="col-sm-1 col-xs-2">Total</td>

                            </tr>
                            </thead>
                            <tbody>

                            @foreach($cart as $item)
                                <tr>
                                    <td class="cart_product">

                                        <img width="80" height="80"
                                             src="/images/products/{{ $item->model->image }}"
                                             alt="{{ $item->model->imageAlt }}">
                                    </td>
                                    <td class="cart_description">
                                        <span class="mobilehide">
                                            {!! $item->name !!}
                                            <br/>
                                            <?php
                                                $sep = '';
                                                if ($item->options['qty-rg'] != 0) {
                                                    ?>
                                                        <?=$item->options['qty-rg'];?> x Rose Gold
                                                    <?php
                                                    $sep = '&nbsp;&nbsp;|&nbsp;&nbsp;';
                                                } 
                                                if ($item->options['qty-yg'] != 0) {
                                                    ?>
                                                        <?=$sep . $item->options['qty-yg'];?>  x Yellow Gold
                                                    <?php
                                                }
                                            ?>
                                        </span>
                                        <span class="mobileshow">{{$item->model->sku}}</span>
                                    </td>

                                    <td class="cart_price">
                                        £{{$item->price}}
                                    </td>
                                    <td class="cart_quantity">
                                        {{$item->qty}}
                                    </td>

                                    <td class="cart_total">
                                        £{{$item->subtotal}}.00
                                    </td>

                                </tr>

                            @endforeach
                            <tr>
                                <td colspan="3" style="white-space: normal;text-align: right;margin-right:10px">Total
                                    number of items & price including postage & VAT &nbsp;&nbsp;&nbsp;&nbsp;</td>

                                <td class="cart_total">{{Cart::count()}}</td>
                                <td class="cart_total">£{{  number_format($total, 2, '.', '')  }}</td>
                            </tr>
                            @else
                                <p>You have no items in the shopping cart</p>
                            @endif
                            </tbody>
                        </table>
                </div>

                <div class="row">
                    <div class="col-md-12 col-xs-12 col-sm-12">

                        {!! Form::open( array('method' => 'post', 'url' => '/complete')) !!}

                        <div class="row">

                            <div class="col-md-3 col-md-offset-9 col-xs-12 col-sm-12">

                                @if (Auth::user()->is('agent') or Auth::user()->is('admin') or Auth::user()->is('assistantAdmin')or Auth::user()->is('minAdmin'))
                                    {!! Form::label('Select Customer') !!}
                                    {!! Form::select('user_id', $users, null) !!}

                                    <div id="add-client">
                                        <a href="/add-client" class="btn btn-large">Add Client</a>

                                    </div>

                                @endif


                            </div>
                        </div>

                        <div class="row">

                            <div class="col-md-6 col-md-offset-6 col-xs-12 col-sm-12">


                                <div style="margin-top:10px;text-align: right"
                                     class="col-xs-12 col-lg-7 col-lg-offset-5 col-md-8 col-md-offset-4 col-sm-6 col-sm-offset-6">
                                    <a class="btn btn-default update" href="{{url('clear-cart')}}">Clear Cart</a>




                                    {!! Form::submit('Complete Order', array('class' => 'btn btn-success btn-save btn-large')) !!}
                                    <input type="hidden" name="postamt" value=<?=$postamt;?> />
                                    {!! Form::close()  !!}
                                    {{--<a class="btn btn-default green" href="{{url('complete')}}">Check Out</a>--}}


                                </div>

                            </div>

                        </div>

                    </div>
                </div>

                <div class="push"></div>

            </div>

        </div>
    </div>


@stop