<header id="header">
    <nav id="main-nav" class="navbar navbar-default " role="navigation">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <a class="navbar-brand" href="/"></a>
                    {{--@if(Auth::check())--}}
                    {{--<a href="{{ url('auth/logout') }}">Logout</a>--}}
                    {{--@else--}}
                    {{--<a href="{{ url('auth/login') }}">Login</a>--}}
                    {{--@endif--}}


                </div>
                <div class="col-md-12">

                    <div class="megamenu_wrapper megamenu_container">
                        <ul class="megamenu">
                            <li>
                                @if(Auth::check())
                                    <a href="{{ url('auth/logout') }}">Logout</a>
                                @else
                                    <a href="{{ url('auth/login') }}">Login</a>
                                @endif
                            </li>
                            @foreach($category as $c)
                                <li><span class="drop"><a href="/category/{{$c->slug}}">{{$c->category}}</a></span>

                                    <div class="megamenu_fullwidth">

                                        @foreach($c->products as $p)
                                            @if (count($p->subproduct) > 0)

                                                @if ($p->image)
                                                    {{--<div style="display:inline">--}}
                                                    {{--<div style="float:left">--}}
                                                    {{--<a href="/product/{{ $p->slug }}"><img--}}
                                                    {{--src="/images/products/resized/{{ $p->image }}"--}}
                                                    {{--width="100"--}}
                                                    {{--height="100"/></a>--}}

                                                    {{--</div>--}}
                                                    {{--<div style="float:left;text-align: center;width:100px">--}}
                                                    {{--<a href="/product/{{ $p->slug }}"> {{ $p->sku }}</a>--}}
                                                    {{--</div>--}}
                                                    {{--</div>--}}
                                                    <a href="/product/{{ $p->slug }}">
                                                        <figure><img
                                                                    src="/images/products/resized/{{ $p->image }}"
                                                                    width="100"
                                                                    height="100"/><figcaption>{{ $p->sku }}</figcaption>
                                                        </figure></a>


                                                @endif

                                            @endif
                                        @endforeach


                                    </div>
                                </li>
                            @endforeach


                            {{--<li><span class="drop">Shepherd Hook Earrings</span>--}}

                            {{--<div class="megamenu_fullwidth">--}}



                            {{--</div></li>--}}
                            {{--<li><span class="drop">Stud Earrings</span>--}}

                            {{--<div class="megamenu_fullwidth">--}}

                            {{--DROP DOWN CONTENT--}}

                            {{--</div></li>--}}
                            {{--<li><span class="drop">Braclets</span>--}}

                            {{--<div class="megamenu_fullwidth">--}}

                            {{--DROP DOWN CONTENT--}}

                            {{--</div></li>--}}
                            {{--<li><span class="drop">Necklaces</span>--}}

                            {{--<div class="megamenu_fullwidth">--}}

                            {{--DROP DOWN CONTENT--}}

                            {{--</div></li>--}}
                            {{--<li><span class="drop">Pendants</span>--}}

                            {{--<div class="megamenu_fullwidth">--}}

                            {{--DROP DOWN CONTENT--}}

                            {{--</div></li>--}}
                            {{--<li><span class="drop">Rings</span>--}}

                            {{--<div class="megamenu_fullwidth">--}}

                            {{--DROP DOWN CONTENT--}}

                            {{--</div></li>--}}
                            <li>
                                @if (Cart::count())
                                    <a id="cartIcon" href="{{ url('/cart') }}"><img
                                                src="/assets/images/cart-clipart-shopping-cart-md.png" width="25" height="25"/> </a>
                                @endif
                            </li>
                        </ul>
                    </div>


                </div>
            </div>
        </div>
    </nav>
</header>