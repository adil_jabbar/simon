@extends('layouts.master')

@section('content')


    <div class="container-fluid container-inset wrapper">
        <div class="row">

            <div class="col-md-12 col-xs-12 col-sm-12">
                @if ($order !== null)
                    <h1>Your order ref {{ $order->id }} for your Rose Gold items is now complete.</h1>
                @endif

                @if ($orderYG !== null)
                    <h1>Your order ref {{ $orderYG->id }} for your Yellow Gold items is now complete.</h1>
                @endif

                @if ($order !== null or $orderYG !== null)
                    <p>You will be sent an email confirmation shortly with your invoice.</p>
                @endif
            </div>
        </div>

        <div class="push"></div>
        <div class="push"></div>

    </div>


@stop