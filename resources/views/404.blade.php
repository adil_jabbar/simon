@extends('layout.main')

@section('content')


    <div id="main-wrapper">
        <div class="container">
            <div class="row 200%">

                <div class="10u important(collapse)" id="maincontent">
                    <div id="content">

                        <!-- Content -->
                        <article>
                            <h2>404 Error</h2>
                            <p>The requested page was not found.</p>
                        </article>


                    </div>
                </div>
            </div>
        </div>
    </div>

@stop