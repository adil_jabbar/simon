@extends('app')

@section('title', 'Products')

@section('content')
	@include('admin._partials.leftWrapper')

	<div id="rightWrapper">
		{{--<div id="header"><a id="fullPage" href="#">|||</a></div>--}}
	<h2>Display continualdevelopment</h2>

	<hr>

	<h3>{{ $continualdevelopment->title }}</h3>
	<h5>@{{ $continualdevelopment->created_at }}</h5>
	{{ $continualdevelopment->body }}

	@if ($continualdevelopment->image)
		<hr>
		<figure><img src="{{ Image::resize($continualdevelopment->image, 800, 600) }}" alt=""></figure>
	@endif
@stop
