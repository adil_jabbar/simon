@extends('app')

@section('title', 'Products')

@section('content')
	@include('admin._partials.leftWrapper')

	<div id="rightWrapper">
		{{--<div id="header"><a id="fullPage" href="#">|||</a></div>--}}

	<hr>

	{!!  Notification::showAll()  !!}

	<table class="table table-striped">
		<thead>
			<tr>
				<th>Order#</th>
				<th>Gold</th>
				<th>Customer</th>
				<th>Company</th>
				<th>Agent</th>
				<th>Value</th>
				<th>Status</th>
				<th>Date</th>

				<th><i class="icon-cog"></i></th>
			</tr>
		</thead>
		<style>table form {width:250px;margin:0;padding:0;display: inline}
			   table form input {width:100px;margin:0;padding:0}</style>
		<tbody>
			@foreach ($orders as $order)

				<tr>
					<td style="width:50px">{{ $order->id }}</td>
					<td style="width:50px">
						@if ($order->is_yg == '1')
							<p>Y/G</p>
						@else
							<p>R/G</p>
						@endif
					</td>
					<td style="width:70px">{{ $order->users->username }}</td>

					<td style="width:200px"><a href="/admin/changecustomer/{{ $order->id }}/edit">{{ $order->users->address->companyName }}</a>
					</td>
					<td style="width:100px">@if ($order->agent) {{ $order->agent->address->companyName }}@endif</td>


					<td style="min-width:150px">£{{ $order->value }}</td>

					@if ($order->status == 0)
						<td style="min-width:150px">New</td>
					@endif
					@if ($order->status == 1)
						<td style="min-width:150px">Invoice Generated</td>
					@endif
					@if ($order->status == 2)
						<td style="min-width:150px">Invoice Sent</td>
					@endif
					@if ($order->status == 3)
						<td style="min-width:150px">Order Cancelled</td>
					@endif

					<td style="min-width:150px;">{{ $order->created_at->format('d M Y') }}</td>

					<td>
						<a style="max-width:30px;" href="/admin/orders/{{ $order->id }}/edit" class="button">Edit</a>
					</td>

					<td>
						@if ($order->status == 3)
							<a href="#" id="<?php echo $order->id ?>" class="trash">
								Delete
							</a>
						@endif
					</td>

				</tr>
			@endforeach
		</tbody>
	</table>
		<br/><br/><br/>
		<meta name="csrf-token"
			  content="{{ csrf_token() }}"/>
		<script>

			$(function () {
				$('.trash').click(function () {

					var del_id = $(this).attr('id');

					var ele = $(this).parent().parent();

					var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

					$.ajax({
						url: '/admin/delete_order',
						type: 'POST',
						data: {_token: CSRF_TOKEN, del_id: del_id},
						dataType: 'JSON',

						success: function (NULL, NULL, jqXHR) {
							if (jqXHR.status === 200) {
								ele.fadeOut().remove();

							}
						},
						error: function (NULL, NULL, jqXHR) {
							alert('Orderitem failed to delete');
						}

					});
				})
			});
		</script>

@stop
