@extends('app')

@section('title', 'Products')

@section('content')
    @include('admin._partials.leftWrapper')

    <div id="rightWrapper">
        {{--<div id="header"><a id="fullPage" href="#">|||</a></div>--}}

        <h2>Order Number {{ $orders->id }}</h2>

        <h3>Current Delivery Address:</h3>

        <p>{{ $orders->users->address->companyName }}<br/>
            {{ $orders->users->address->address1 }}<br/>
            @if ($orders->users->address->address2){{ $orders->users->address->address2 }}<br/>@endif
            {{ $orders->users->address->town }}<br/>{{ $orders->users->address->county }}<br/>
            {{ $orders->users->address->postcode }}</p>
        <p><span style="font-weight: bold">Client Email Address:</span> {{ $orders->users->email }}</p>

        @include('admin._partials.notifications')

        {{--{!!  Notification::showAll()  !!}--}}


        {!! Form::open(['route' => 'admin.updatecustomer', 'method' => 'post']) !!}
        <br/><br/>
        <table>
            <tr>
                <td width="260"><strong>Select New Customer</strong></td>

            </tr>
            <tr>
                <td><select id="form1_customer" name="customer" class="autosubmit">
                        <option value="">Please Select</option>

                        @foreach($customer as $cust)
                            <option value="{{ $cust->id}}"> {{ $cust->username}} </option>
                        @endforeach
                    </select>
                    <input type="hidden" name="id" value="{{ $orders->id }}"/>
                </td>

            </tr>
        </table>


        <input type="submit" value="Submit">
        {!! Form::close() !!}
        <br/><br/><br/><br/><br/>
    </div>

@stop
