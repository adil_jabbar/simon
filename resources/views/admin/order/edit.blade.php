@extends('app')

@section('title', 'Products')

@section('content')
    @include('admin._partials.leftWrapper')

    <div id="rightWrapper">

        {{--<div id="header"><a id="fullPage" href="#">|||</a></div>--}}

        <h2>Order Number {{ $orders->id }}</h2>

        <h3>Delivery Address:</h3>

        <p>{{ $orders->users->address->companyName }}<br/>
            {{ $orders->users->address->address1 }}<br/>
          @if ($orders->users->address->address2){{ $orders->users->address->address2 }}<br/>@endif
            {{ $orders->users->address->town }}<br/>{{ $orders->users->address->county }}<br/>
            {{ $orders->users->address->postcode }}</p>
        <p><span style="font-weight: bold">Client Email Address:</span> {{ $orders->users->email }}</p>

        @include('admin._partials.notifications')

        {{--{!!  Notification::showAll()  !!}--}}

        <div class="alert alert-success alert-item-remove" style="display:none;">
            Item removed from order.
        </div>

        <table class="table table-striped">
            <thead>
            <tr>
                <th>Product Code</th>
                <th style="text-align:center;">Gold</th>
                <th style="text-align:center;">Quantity</th>

                <th>Total Value</th>

                <th>Date</th>

                <th><i class="icon-cog"></i></th>
            </tr>
            </thead>

            <style>table form {
                    width: 250px;
                    margin: 0;
                    padding: 0;
                    display: inline
                }

                table form input {
                    width: 100px;
                    margin: 0;
                    padding: 0
                }</style>
            <tbody>

            @foreach ($orders->orderitems as $orderitem)

                <tr>
                    <td style="width:150px">{{ $orderitem->subproducts->sku }}</td>
                    <td style="width:150px;text-align:center;">
                        @if ($orderitem->is_yg == '1')
                            <p>Y/G</p>
                        @else
                            <p>R/G</p>
                        @endif
                    </td>
                    <td style="width:170px;text-align:center;">{{ $orderitem->qty }}</td>
                    <td style="min-width:150px">£{{ $orderitem->value }}</td>


                    <td style="min-width:150px;">{{ $orderitem->created_at->format('d M Y') }}</td>
                    {{--<td><a href="#" class="button">Edit</a></td>--}}

                    <td>
                        @if ($orders->status != 3)
                            <a href="#" id="<?php echo $orderitem->id ?>" class="trash">
                            Del
                        </a>
                        @endif
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>
        
        <p>Total: £<span class="total-order-value"><?=$orders->value;?></span> &nbsp;&nbsp;|&nbsp;&nbsp;
            Sub Total: £<span class="total-order-value"><?=$orders->subtotal;?></span> &nbsp;&nbsp;|&nbsp;&nbsp;
            VAT: £<span class="total-order-vat"><?=$orders->vat;?></span> &nbsp;&nbsp;|&nbsp;&nbsp; 
            Postage: £<span class="total-order-postage"><?=$orders->postage;?></span>  
        </p> 

        @if ($orders->status == 3)
        <a href="/admin/order/renew/{{ $orders->id }}" id="<?php echo $orderitem->id ?>"><button class="btn btn-large btn-success">Make New Again</button>
        @endif
            <script type="text/javascript" src="https://appcenter.intuit.com/Content/IA/intuit.ipp.anywhere.js"></script>
        <script type="text/javascript">
            intuit.ipp.anywhere.setup({
                menuProxy: '<?php print(env('QBO_MENU_URL')); ?>',
                grantUrl: '<?php print(env('QBO_OAUTH_URL')); ?>'
            });
        </script>


        <?php
            $qbo_obj = new \App\Http\Controllers\QuickBookController();
            $qbo_connect = $qbo_obj->qboConnect();
            //$qbo_connect = null;
        ?>

        @if ($orders->status != 3)
            {{--@if (!$orders->invoiceNo)--}}

            @if($qbo_connect)
            <a class="btn btn-small btn-info" href="{{ URL::to('admin/generate_invoice/' . $orders->id ) }}">Update
                Quickbooks</a>
            @endif
            {{--@endif--}}

            {{--@if (\File::exists(storage_path() . '/Invoices/Invoice_' . $orders->id    . '.pdf'))--}}
            {{--<a class="btn btn-small btn-success" href="{{ URL::to('admin/view_invoice/' . $orders->id ) }}">View</a>--}}

            {{--@if (trim(strlen($orders->users->email)) > 0 && $orders->invoiceSent == 0)--}}
            {{--<a target="_blank" class="btn btn-small btn-primary"--}}
            {{--href="{{ URL::to('admin/email_invoice/' . $orders->id ) }}">Email Invoice</a>--}}
            {{--@else--}}
            {{--<a target="_blank" class="btn btn-small btn-primary"--}}
            {{--href="{{ URL::to('admin/email_invoice/' . $orders->id ) }}">Resend Invoice</a>--}}
            {{--@endif--}}
            {{--@endif--}}

            {!! Form::open(array('url' => 'admin/invoice/delete/' . $orders->id, 'class' => 'pull-right','method' => 'get'))  !!}
            {!! Form::hidden('_method', 'DELETE')  !!}
            {!! Form::submit('Cancel Order', array('class' => 'btn btn-warning confirmdelete'))  !!}
            {!! Form::close()  !!}

        @endif

        <br/><br/>

        @if ($orders->status != 3)
            @if(!$qbo_connect)
                <ipp:connectToIntuit></ipp:connectToIntuit>
            @else
                <a href="{{url('qbo/disconnect')}}" title="">Disconnect</a>
            @endif
        @endif


        <div style="clear:both"></div>

        <style> .extraitem {
                width: 200px
            }</style>

        {{--{!! Form::model($orders, (array('url' => 'admin/invoice/extra/' . $orders->id, 'method' => 'post'))  !!}--}}

        @if ($orders->status != 3)
            {!! Form::model($orders, array('method' => 'post', 'url' => 'admin/invoice/extra/' . $orders->id)) !!}
                <br/><br/>
                <div class="row">
                    <div class="col-md-4">
                        <div class="control-group">
                            @if ($orders->is_yg == '1') 
                                {!! Form::label('extraitem', 'Extra Yellow Gold Item') !!}
                            @else
                                {!! Form::label('extraitem', 'Extra Rose Gold Item') !!}
                            @endif
                            
                            <div class="form-group">
                                {!! Form::select('extraitem', $productsSelect, null, ['class' => 'form-control extraitem']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="control-group">
                            {!! Form::label('qty', 'Quantity') !!}
                            <div class="form-group">
                                {!! Form::number('qty', '1', ['min'=>1]) !!}
                            </div>
                        </div>
                    </div>
                </div>

                <div style="clear: both"></div>
            {!! Form::hidden('id',  $orders->id )  !!}
            {!! Form::hidden('is_yg',  $orders->is_yg )  !!}
            @if ($orders->is_yg == '1') 
                {!! Form::submit('Add Yellow Gold Item', array('class' => 'btn btn-success btn-save btn-large')) !!}
            @else
                {!! Form::submit('Add Rose Gold Item', array('class' => 'btn btn-success btn-save btn-large')) !!}
            @endif

            {!! Form::close()  !!}
        @endif
    </div>

    <meta name="csrf-token"
          content="{{ csrf_token() }}"/>
    <script>

        $(function () {
            $('.alert').delay(2000).fadeOut();

            $('.trash').click(function () {

                var del_id = $(this).attr('id');
                var ele = $(this).parent().parent();

                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

                $.ajax({
                    url: '/admin/order_deleteitem',
                    type: 'POST',
                    data: {_token: CSRF_TOKEN, del_id: del_id, order_id: {{ $orders->id }}, is_yg: {{ $orders->is_yg }}},
                    dataType: 'JSON',

                    success: function (NULL, NULL, jqXHR) {
                        if (jqXHR.status === 200) {
                            ele.fadeOut().remove();

                            $('span.total-order-value').html(jqXHR.responseJSON.order_value);
                            if(jqXHR.responseJSON.order_postage != 0) {
                                $('span.total-order-postage').html(jqXHR.responseJSON.order_postage);
                                $('span.total-order-vat').html(jqXHR.responseJSON.order_vat - jqXHR.responseJSON.order_postage);
                            } else {
                                $('span.total-order-vat').html(jqXHR.responseJSON.order_vat);
                            }

                            $('.alert-item-remove').fadeIn().delay(2000).fadeOut();
                            
                        }
                    },
                    error: function (NULL, NULL, jqXHR) {
                        alert('Orderitem failed to delete');
                    }

                });
            })
        });
    </script>
@stop
