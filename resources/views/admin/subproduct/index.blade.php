@extends('app')

@section('title', 'Sub Products')

@section('content')
	@include('admin._partials.leftWrapper')

	<div id="rightWrapper">
		{{--<div id="header"><a id="fullPage" href="#">|||</a></div>--}}

	<h1>
		<a href="/admin/subproduct/create/{{ $product->id }}" class="btn btn-success"><i class="icon-plus-sign"></i> Add new product option</a>


	</h1>

		<h3>{{ $product->productName }} &nbsp; <img src="{{ '/images/products/' . $product->image }}" width="50" height="50"/> </h3>

	<hr>

	{!! Notification::showAll() !!}

	<table class="table table-striped">
		<thead>
			<tr>
				<th>Code</th>
				<th></th>

				<th>Quantity</th>
				<th>Price</th>
				<th>Nett Price</th>

				<th><i class="icon-cog"></i></th>
			</tr>
		</thead>
		<tbody>

			@foreach ($product->subproduct as $subproduct)
				<tr>

					<td style="padding-top:25px;">{{ $subproduct->sku }}</td>
					<td><img width="50" height="50" src="{{ '/images/products/thumbnails/' . $subproduct->image }}"></td>

					<td style="padding-top:25px;">{{ $subproduct->quantity }}</td>
					<td style="padding-top:25px;">{{ $subproduct->grossPrice }}</td>
					<td style="padding-top:25px;">{{ $subproduct->into_safe }}</td>

					<td style="padding-top:25px;">
						<a href="{{ URL::route('admin.subproduct.edit', $subproduct->id) }}" class="btn btn-success btn-mini pull-left">Edit</a>

						{{--{!! Form::open(array('route' => array('admin.subproduct.destroy', $subproduct->id), 'method' => 'delete', 'data-confirm' => 'Are you sure?')) !!}--}}
							{{--<button type="submit" href="{{ URL::route('admin.subproduct.destroy', $subproduct->id) }}" class="btn btn-danger btn-mini">Delete</button>--}}
						{{--{!! Form::close() !!}--}}


						{!!  Form::open(array('url' => 'admin/subproduct/' . $subproduct->id, 'class' => 'pull-right')) !!}
						{!! Form::hidden('_method', 'DELETE') !!}
						{!! Form::submit('Delete', array('class' => 'btn btn-warning confirmdelete')) !!}
						{!! Form::close() !!}

					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	<br/><br/><br/><br/>
@stop
