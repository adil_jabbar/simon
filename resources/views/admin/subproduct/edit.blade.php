@extends('app')

@section('title', 'Products')

@section('content')
	@include('admin._partials.leftWrapper')

	<div id="rightWrapper">
		{{--<div id="header"><a id="fullPage" href="#">|||</a></div>--}}

	<h2>Edit Product {{ $product->sku }}</h2>

	@include('admin._partials.notifications')

	{!! Form::model($product, array('method' => 'put', 'route' => array('admin.subproduct.update', $product->id), 'files' => true)) !!}

		<div class="form-group">
			{!!  Form::label('stone_id', 'Stone')  !!}
			<select name="stone_id" id="stone_id" required="required">
				<option value="">Select Stone...</option>
				@foreach($stones as $stone)
					@if ($product->stone_id == $stone->id)
						<option value="{{ $stone->id }}" selected>{{ $stone->stone }}</option>
					@else
						<option value="{{  $stone->id }}">{{ $stone->stone }}</option>
					@endif
					{{--<option value="{{ $stone->id}}"> {{ $stone->stone}} </option>--}}
				@endforeach
			</select>
		</div>

		<div class="control-group">
			{!! Form::label('splive', 'Live?') !!}
			<div style="width:90px" class="form-group">
				{!!Form::select('splive', array('0' => 'No', '1' => 'Yes'), null, array('class' => 'form-control')) !!}
			</div>
		</div>
        <div class="control-group">
            {!! Form::label('eoline', 'End of Line?') !!}
            <div style="width:90px" class="form-group">
                {!!Form::select('eoline', array('0' => 'No', '1' => 'Yes'), null, array('class' => 'form-control')) !!}
            </div>
        </div>
		<div class="control-group">
			{!! Form::label('newarrival', 'New Arrival') !!}
			<div style="width:90px" class="form-group">
				{!!Form::select('newarrival', array('0' => 'No', '1' => 'Yes'), null, array('class' => 'form-control')) !!}
			</div>
		</div>

		{{--<div class="control-group">--}}
			{{--<div class="col-md-8">--}}
				{{--<div class="col-md-4">--}}
					{{--{!! Form::label('grossPrice', 'Gross Price') !!}--}}
					{{--<div class="controls">--}}
						{{--{!! Form::text('grossPrice', null, array('class' => 'shorterfield')) !!}--}}
					{{--</div>--}}
				{{--</div>--}}
				{{--<div class="col-md-6">--}}
					{{--{!! Form::label('into_safe', 'Nett Price') !!}--}}
					{{--<div class="controls">--}}
						{{--{!! Form::text('into_safe', null, array('class' => 'shorterfield')) !!}--}}
					{{--</div>--}}
				{{--</div>--}}
				{{--<div class="col-md-6">--}}
					{{--{!! Form::label('quantity', 'Quantity') !!}--}}
					{{--<div class="controls">--}}
						{{--{!! Form::text('quantity', null, array('class' => 'shorterfield')) !!}--}}
					{{--</div>--}}
				{{--</div>--}}
			{{--</div>--}}
		{{--</div>--}}

		<div class="clearfix"></div>

		<div class="control-group">
			{!! Form::label('image', 'Image') !!}

			<div class="fileupload fileupload-new" data-provides="fileupload">
				<div class="fileupload-preview thumbnail" style="width: 200px; height: 150px;">
					@if (!$product->image)
						<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image">
					@else
						<img src="/images/products/{{ $product->image }}">
					@endif
				</div>
				<div>
                <span class="btn btn-file"><span class="fileupload-new">Select image</span><span
							class="fileupload-exists">Change</span>{!! Form::file('image') !!}</span>
					<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
				</div>
			</div>
		</div>

		<div class="control-group">
			{!! Form::label('imageAlt', 'Image Alt Text') !!}
			<div class="controls">
				{!! Form::text('imageAlt') !!}
			</div>
		</div>



		<div class="form-actions">
			{!! Form::submit('Save', array('class' => 'btn btn-success btn-save btn-large')) !!}
			<a href="{{ URL::route('admin.product.index') }}" class="btn btn-large">Cancel</a>
		</div>
		<br/><br/><br/><br/><br/>
	{!! Form::close() !!}

@stop
