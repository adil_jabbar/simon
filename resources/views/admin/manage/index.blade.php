@extends('admin._layouts.default')

@section('main')

	<h1>
		Management area
	</h1>
	<br/><br/>



    {{--<p id="selection"></p>--}}
    <div style="width:50%;float:left">

        <h3>Website settings</h3>
        <p><a href="{{ \URL::to('admin/settings/edit') }}">Settings</a></p>

        <p><a href="{{ \URL::to('admin/user') }}">Manage Users</a></p>

         <h3>Pages</h3>
         <p><a href="{{ \URL::to('admin/homepage/edit') }}">Manage Homepage</a></p>

         <p><a href="{{ \URL::to('admin/level2gym/edit') }}">Level 2 Gym</a></p>

         <p><a href="{{ \URL::to('admin/personaltrainer/edit') }}">Personal Trainer</a></p>

         <p><a href="{{ \URL::to('admin/continualdevelopment') }}">Continual Development</a></p>

         <p><a href="{{ \URL::to('admin/coursedates/edit') }}">Course Dates & Venues</a></p>

        <p><a href="{{ \URL::route('admin.workshops.index') }}">Workshops for Course Dates and Venues</a></p>

         <p><a href="{{ \URL::to('admin/trainingroom/edit') }}">Training Room</a></p>

         <p><a href="{{ \URL::to('admin/news/edit') }}">News page</a></p>

         <p><a href="{{ \URL::route('admin.specials.index') }}">Special Offers for News Page</a></p>

         {{--<p><a href="{{ \URL::to('admin/listings/book/2') }}">Manage Surrey</a></p>--}}

         {{--<p><a href="{{ \URL::to('admin/listings/book/3') }}">Manage Reading</a></p>--}}

         {{--<p><a href="{{ \URL::to('admin/listings/book/4') }}">Manage Maidenhead</a></p>--}}
    {{--</div>--}}

    {{--<div style="width:50%;float:left">--}}
        {{--<h3>Manage Invoices</h3>--}}
        {{--<p><a href="{{ \URL::to('admin/invoices/book/1') }}">Wokingham</a></p>--}}

        {{--<p><a href="{{ \URL::to('admin/invoices/book/2') }}">Surrey</a></p>--}}

        {{--<p><a href="{{ \URL::to('admin/invoices/book/3') }}">Reading</a></p>--}}

        {{--<p><a href="{{ \URL::to('admin/invoices/book/4') }}">Maidenhead</a></p>--}}
    {{--</div>--}}

    <div style="clear:both"></div>


    {{--@if (Sentry::getUser()->isSuperUser())--}}
        {{--<h3>Email Texts</h3>--}}

        {{--<p><a href="{{ URL::to('admin/emailtexts') }}">Manage Emails</a></p>--}}
    {{--@endif--}}


    
@stop
