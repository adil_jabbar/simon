@extends('app')

@section('title', 'Dashboard')

@section('content')

    @include('admin._partials.leftWrapper')
    <style>.col-xs-3 {
            text-align: right;
        }

        .row {
            width: 900px;
            margin-bottom: 10px
        }</style>
    <div id="rightWrapper">
        {{--<div id="header"><a id="fullPage" href="#">|||</a></div>--}}
        <div id="contentWrapper">




            <div class="container-fluid container-inset wrapper">

                @if (Auth::user()->is('admin'))

                <div class="row" style="margin-bottom:20px">
                    <h1>Sales</h1>
                    <div class="col-md-12 col-xs-12 col-sm-12">

                        <div class="col-xs-3">
                        </div>

                        <div class="col-xs-3">
                            MTD ({{ $firstOfMonth->format('d/m/Y') }})

                        </div>

                        <div class="col-xs-3">
                            QTD ( {{ $firstOfQuater->format('d/m/Y') }} )

                        </div>

                        <div class="col-xs-3">

                            YTD ( {{ $firstOfYear->format('d/m/Y') }} )
                        </div>

                    </div>

                </div>

                <div class="row">
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <div class="col-xs-3">
                            TURNOVER
                        </div>

                        <div class="col-xs-3">
                            £{{ number_format($valuemtd, 2) }}

                        </div>

                        <div class="col-xs-3">
                            £{{ number_format($valueqtd, 2) }}

                        </div>

                        <div class="col-xs-3">

                            £{{ number_format($valueytd, 2) }}
                        </div>

                    </div>

                </div>


                <div class="row">
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <div class="col-xs-3">
                            PROFIT
                        </div>

                        <div class="col-xs-3">
                            £{{ number_format($profitmtd, 2) }}

                        </div>

                        <div class="col-xs-3">
                            £{{ number_format($profitqtd, 2) }}

                        </div>

                        <div class="col-xs-3">

                            £{{ number_format($profitytd, 2) }}
                        </div>

                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <div class="col-xs-3">
                            VAT
                        </div>

                        <div class="col-xs-3">
                            £{{ number_format($vatmtd, 2) }}

                        </div>

                        <div class="col-xs-3">
                            £{{ number_format($vatqtd, 2) }}

                        </div>

                        <div class="col-xs-3">

                            £{{ number_format($vatytd, 2) }}
                        </div>

                    </div>

                </div>




                <div class="row">
                    <h1 style="margin-top:50px">Stock</h1>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <div class="col-xs-3">
                            PIECES
                        </div>

                        <div class="col-xs-3">
                            SAFE

                        </div>

                        <div class="col-xs-3">
                            SELL

                        </div>


                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <div class="col-xs-3">
                            {{ $pieces }}
                        </div>

                        <div class="col-xs-3">
                            £{{ number_format($safe, 2) }}

                        </div>

                        <div class="col-xs-3">
                            £{{ number_format($sell, 2) }}

                        </div>


                    </div>

                </div>

                <div class="row">
                    <h1 style="margin-top:50px">Simon</h1>
                    <div class="col-md-12 col-xs-12 col-sm-12">


                        <div class="col-xs-3">
                           INTO SAFE

                        </div>



                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12 col-xs-12 col-sm-12">

                        <div class="col-xs-3">
                            £{{ number_format($simon->total, 2) }}

                        </div>


                    </div>

                </div>
                <div class="row">
                    <h1 style="margin-top:50px">Max</h1>
                    <div class="col-md-12 col-xs-12 col-sm-12">


                        <div class="col-xs-3">
                            INTO SAFE

                        </div>


                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12 col-xs-12 col-sm-12">

                        <div class="col-xs-3">
                            £{{ number_format($max->total, 2) }}

                        </div>



                    </div>

                </div>

                <div class="row">
                    <h1 style="margin-top:50px">Demelza</h1>
                    <div class="col-md-12 col-xs-12 col-sm-12">


                        <div class="col-xs-3">
                            INTO SAFE

                        </div>


                    </div>

                </div>
                <div class="row" style="margin-bottom:100px;">
                    <div class="col-md-12 col-xs-12 col-sm-12">

                        <div class="col-xs-3">
                            £{{ number_format($demelza->total, 2) }}

                        </div>


                    </div>

                </div>
            </div>
            @endif
        </div>

@endsection