@extends('app')

@section('title', 'Users')

@section('content')
	@include('admin._partials.leftWrapper')

	<div id="rightWrapper">
		{{--<div id="header"><a id="fullPage" href="#">|||</a></div>--}}

	<div id="contentWrapper">


	<h1>
		Pages <a href="{{ URL::route('admin.pages.create') }}" class="btn btn-success"><i class="icon-plus-sign"></i> Add new page</a>
	</h1>

	<hr>

	{!!  Notification::showAll() !!}

		<table class="table table-striped">
		<thead>
			<tr>
				<th>#</th>
				<th>Title</th>
				<th>When</th>
				<th><i class="icon-cog"></i></th>
			</tr>
		</thead>
		<tbody>
			@foreach ($pages as $page)
				<tr>
					<td>{{ $page->id }}</td>
					<td>{{ $page->title }}</td>
					<td>{{ $page->created_at }}</td>
					<td>
						<a href="{{ URL::route('admin.pages.edit', $page->id) }}" class="btn btn-success btn-mini pull-left">Edit</a>

						{!! Form::open(array('route' => array('admin.pages.destroy', $page->id), 'method' => 'delete', 'data-confirm' => 'Are you sure?')) !!}
							<button type="submit" href="{{ URL::route('admin.pages.destroy', $page->id) }}" class="btn btn-danger btn-mini">Delete</button>
							<!--   // PXW The above delete button could also use the method link_to_route
							{!!	 link_to_route('admin.pages.destroy', 'Edit', array($page->id), array('class' => 'btn btn-danger btn-mini')) !!} -->
						{!! Form::close() !!}
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>

	</div>

@endsection
