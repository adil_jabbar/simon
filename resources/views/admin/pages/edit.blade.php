@extends('app')

@section('title', 'Products')

@section('content')
	@include('admin._partials.leftWrapper')

	<div id="rightWrapper">
	<h2>Edit page</h2>

	@include('admin._partials.notifications')

	{!! Form::model($page, array('method' => 'put', 'route' => array('admin.pages.update', $page->id))) !!}

		<div class="control-group">
			{!! Form::label('title', 'Title') !!}
			<div class="controls">
				{!! Form::text('title') !!}
			</div>
		</div>

		<div class="control-group">
			{!! Form::label('body', 'Content') !!}
			<div class="controls">
				{!! Form::textarea('body') !!}
				<script> 
					var roxyFileman = '../../../fileman/index.html'; 

					$(function(){
   						CKEDITOR.replace( 'body',{filebrowserBrowseUrl:roxyFileman, 
                                filebrowserUploadUrl:roxyFileman,
                                filebrowserImageBrowseUrl:roxyFileman+'?type=image',
                                filebrowserImageUploadUrl:roxyFileman+'?type=image'}); 
					});
 				</script>
			</div>
		</div>

		<div class="control-group">
			{!! Form::label('title', 'Meta Title') !!}
			<div class="controls">
				{!! Form::text('metatitle') !!}
			</div>
		</div>

		<div class="control-group">
			{!! Form::label('title', 'Meta description') !!}
			<div >
				{!! Form::text('meta') !!}
			</div>
		</div>


		<div class="form-actions">
			{!! Form::submit('Save', array('class' => 'btn btn-success btn-save btn-large')) !!}
			<a href="{{ URL::route('admin.pages.index') }}" class="btn btn-large">Cancel</a>
		</div>

	{!! Form::close() !!}

</div>
	@endsection

