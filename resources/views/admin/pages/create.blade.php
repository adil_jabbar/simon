@extends('app')

@section('title', 'Pages')

@section('content')
    @include('admin._partials.leftWrapper')

    <div id="rightWrapper">

        <h2>Create new page</h2>

        @include('admin._partials.notifications')

        {!! Form::open(array('route' => 'admin.pages.store')) !!}

        <div class="control-group">
            {!! Form::label('title', 'Title') !!}
            <div class="controls">
                {!! Form::text('title') !!}
            </div>
        </div>

        <div class="control-group">
            {!! Form::label('body', 'Content') !!}
            <div class="controls">
                {!! Form::textarea('body') !!}
                <script>
                    var roxyFileman = '../../../fileman/index.html';

                    $(function () {
                        CKEDITOR.replace('body', {
                            filebrowserBrowseUrl: roxyFileman,
                            filebrowserUploadUrl: roxyFileman,
                            filebrowserImageBrowseUrl: roxyFileman + '?type=image',
                            filebrowserImageUploadUrl: roxyFileman + '?type=image'
                        });
                    });
                </script>
            </div>
        </div>


        <div class="control-group">
            {!! Form::label('title', 'Meta Title') !!}
            <div class="controls">
                {!! Form::text('metatitle') !!}
            </div>
        </div>

        <div class="control-group">
            {!! Form::label('title', 'Meta description') !!}
            <div class="controls">
                {!! Form::text('meta') !!}
            </div>
        </div>


        <div class="form-actions">
            {!! Form::submit('Save', array('class' => 'btn btn-success btn-save btn-large')) !!}
            <a href="{{ URL::route('admin.pages.index') }}" class="btn btn-large">Cancel</a>
        </div>

        {!! Form::close() !!}

    </div>
@endsection
