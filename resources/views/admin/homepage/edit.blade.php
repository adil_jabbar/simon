@extends('admin._layouts.default')
@section('main')


	<h2>Edit Homepage</h2>

	@include('admin._partials.notifications')

	{!! Form::model($homepage, array('method' => 'put', 'route' => array('admin.homepage.update', $homepage->id), 'files' => true)) !!}


        <div class="control-group">
            {!! Form::label('title1', 'Homepage Box 1') !!}
            <div class="controls">
                {!! Form::text('title1') !!}
            </div>
        </div>

		<div class="control-group">
			{!! Form::label('body1', 'Box Text 1') !!}
			<div class="controls">
				{!! Form::textarea('body1', null, ['size' => '120x3']) !!}
				<script>
					var roxyFileman = '../../../fileman/index.html';

					$(function(){
   						CKEDITOR.replace( 'body1',{filebrowserBrowseUrl:roxyFileman,
                                height:100,
                                filebrowserUploadUrl:roxyFileman,
                                filebrowserImageBrowseUrl:roxyFileman+'?type=image',
                                filebrowserImageUploadUrl:roxyFileman+'?type=image'});
					});
 				</script>
			</div>
		</div>

        <div class="control-group">
            {!! Form::label('link1', 'Read more link URL') !!}
            <div class="controls">
                {!! Form::text('link1', null) !!}
            </div>
        </div>

    <div class="control-group">
        {!! Form::label('title2', 'Homepage Box 2') !!}
        <div class="controls">
            {!! Form::text('title2') !!}
        </div>
    </div>

    <div class="control-group">
        {!! Form::label('body2', 'Box Text 2') !!}
        <div class="controls">
            {!! Form::textarea('body2', null, ['size' => '120x3']) !!}
            <script>
                var roxyFileman = '../../../fileman/index.html';

                $(function(){
                    CKEDITOR.replace( 'body2',{filebrowserBrowseUrl:roxyFileman,
                        height:100,
                        filebrowserUploadUrl:roxyFileman,
                        filebrowserImageBrowseUrl:roxyFileman+'?type=image',
                        filebrowserImageUploadUrl:roxyFileman+'?type=image'});
                });
            </script>
        </div>
    </div>

    <div class="control-group">
        {!! Form::label('link2', 'Read more link URL') !!}
        <div class="controls">
            {!! Form::text('link2', null) !!}
        </div>
    </div>


    <div class="control-group">
        {!! Form::label('title3', 'Homepage Box 3') !!}
        <div class="controls">
            {!! Form::text('title3') !!}
        </div>
    </div>

    <div class="control-group">
        {!! Form::label('body3', 'Continual Development') !!}
        <div class="controls">
            {!! Form::textarea('body3', null, ['size' => '120x3']) !!}
            <script>
                var roxyFileman = '../../../fileman/index.html';

                $(function(){
                    CKEDITOR.replace( 'body3',{filebrowserBrowseUrl:roxyFileman,
                        height:100,
                        filebrowserUploadUrl:roxyFileman,
                        filebrowserImageBrowseUrl:roxyFileman+'?type=image',
                        filebrowserImageUploadUrl:roxyFileman+'?type=image'});
                });
            </script>
        </div>
    </div>

    <div class="control-group">
        {!! Form::label('link3', 'Read more link URL') !!}
        <div class="controls">
            {!! Form::text('link3', null) !!}
        </div>
    </div>

    <div class="control-group">
        {!! Form::label('title4', 'Homepage Box 4') !!}
        <div class="controls">
            {!! Form::text('title4') !!}
        </div>
    </div>

    <div class="control-group">
        {!! Form::label('body4', 'Box 4') !!}
        <div class="controls">
            {!! Form::textarea('body4', null, ['size' => '120x3']) !!}
            <script>
                var roxyFileman = '../../../fileman/index.html';

                $(function(){
                    CKEDITOR.replace( 'body4',{filebrowserBrowseUrl:roxyFileman,
                        height:100,
                        filebrowserUploadUrl:roxyFileman,
                        filebrowserImageBrowseUrl:roxyFileman+'?type=image',
                        filebrowserImageUploadUrl:roxyFileman+'?type=image'});
                });
            </script>
        </div>
    </div>

    <div class="control-group">
        {!! Form::label('link4', 'Read more link URL') !!}
        <div class="controls">
            {!! Form::text('link4', null) !!}
        </div>
    </div>


    {{--<div class="control-group">--}}
        {{--{!! Form::label('title5', 'Title') !!}--}}
        {{--<div class="controls">--}}
            {{--{!! Form::text('title5') !!}--}}
        {{--</div>--}}
    {{--</div>--}}

    <div class="control-group">
        {!! Form::label('body5', 'Special Offer') !!}
        <div class="controls">
            {!! Form::textarea('body5', null, ['size' => '120x3']) !!}
            <script>
                var roxyFileman = '../../../fileman/index.html';

                $(function(){
                    CKEDITOR.replace( 'body5',{filebrowserBrowseUrl:roxyFileman,
                        height:100,
                        filebrowserUploadUrl:roxyFileman,
                        filebrowserImageBrowseUrl:roxyFileman+'?type=image',
                        filebrowserImageUploadUrl:roxyFileman+'?type=image'});
                });
            </script>
        </div>
    </div>

    <div class="control-group">
        {!! Form::label('link5', 'Read more link URL') !!}
        <div class="controls">
            {!! Form::text('link5', null) !!}
        </div>
    </div>

		<div class="control-group">
			{!! Form::label('image', 'Image') !!}

			<div class="fileupload fileupload-new" data-provides="fileupload">
				<div class="fileupload-preview thumbnail" style="width: 200px; height: 150px;">
					@if ($homepage->image)
						<a href="<?php echo $homepage->image; ?>"><img src="/images/<?php echo $homepage->image; ?>" alt=""></a>
					@else
						<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image">
					@endif
				</div>
				<div>
					<span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span>{!! Form::file('image') !!}</span>
					<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
				</div>
			</div>
		</div>

        <div class="control-group">
            {!! Form::label('image_alt', 'Image Alt Text') !!}
            <div class="controls">
                {!! Form::text('image_alt') !!}
            </div>
        </div>

		<div class="form-actions">
			{!! Form::submit('Save', array('class' => 'btn btn-success btn-save btn-large')) !!}
			<a href="{{ URL::route('admin.homepage.index') }}" class="btn btn-large">Cancel</a>
		</div>

	{!! Form::close() !!}

@stop
