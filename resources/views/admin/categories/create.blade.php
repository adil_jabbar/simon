@extends('admin._layouts.default')

@section('main')

<style>input {width:500px;}</style>
    @include('admin._partials.notifications')

<div class="subnav">
	<ul>
		<li><a class="btn btn-default btn-xs" href="{{ URL::to('admin/categories') }}">View All Categories</a></li>

	</ul>
</div>
<h1>Create a Category</h1>

<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all() )}}

{{ Form::open(array('url' => 'admin/categories', 'files' => true)) }}

	<div class="form-group">
		{{ Form::label('title', 'Category') }}
		{{ Form::text('title', null, array('class' => 'form-control', 'required')) }}
	</div>

	<div class="form-group">
    {{ Form::label('book', 'Book') }}
    <select name="id" id="id" required="required" >
        <option value="">Select a book...</option>
        @foreach($books as $book)
            <option value="{{ $book->id}}"> {{ $book->Title}} </option>
        @endforeach
      </select>
    </div>


	{{ Form::submit('Create the Category', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}

</div>
@stop