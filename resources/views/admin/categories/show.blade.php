@extends('admin._layouts.default')

@section('main')

    
    @include('admin._partials.notifications')

<div class="subnav">
	<ul>
		<li><a class="btn btn-default btn-xs" href="{{ URL::to('admin/banners') }}">View All Bannerss</a></li>
		<li><a class="btn btn-default btn-xs" href="{{ URL::to('admin/banners/create') }}">Create a Banner</a></li>
	</ul>
</div>
<h1>Showing {{ $banners->name }}</h1>

	<div class="jumbotron text-center">
		<h2>{{ $banners->name }}</h2>
		<p>
			<strong>Email:</strong> {{ $banners->banner_alttext }}<br>
			<strong>Banners View:</strong> {{ $banners->banner_position }}
		</p>
	</div>

</div>
@stop