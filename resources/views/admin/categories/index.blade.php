@extends('admin._layouts.default')

@section('main')
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
    
    @include('admin._partials.notifications')

    <div class="subnav">
	<ul>
		<li><a class="btn btn-default btn-xs" href="{{ URL::to('admin/categories/create') }}">Create a Category</a></li>
	</ul>
	</div>

<h1>All the Categories</h1>

<!-- will be used to show any messages -->
@if (Session::has('message'))
	<div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Category</th>
			<th>Book</th>
			<th class="col-sm-4" style="width:160px !important;min-width:160px;">Actions</th>
		</tr>
	</thead>
	<tbody>

	@foreach($categories as $key => $value)
		<tr>
			<td>{{ $value->title }}</td>
			<td width="180">{{ $value->Title }}</td>

			<!-- we will also add show, edit, and delete buttons -->
			<td>

				<!-- delete the category (uses the destroy method DESTROY /category/{id} -->
				<!-- we will add this later since its a little more complicated than the first two buttons -->
				
				@if (Sentry::getUser()->isSuperUser())

				{{ Form::open(array('url' => 'admin/categories/' . $value->id, 'class' => 'pull-right')) }}
					{{ Form::hidden('_method', 'DELETE') }}
					{{ Form::submit('Delete', array('class' => 'btn btn-warning')) }}
				{{ Form::close() }}
				@endif
				
				<!-- edit this category (uses the edit method found at GET /category/{id}/edit -->
				<a class="btn btn-small btn-info" href="{{ URL::to('admin/categories/' . $value->id . '/edit') }}">Edit</a>

			</td>
		</tr>
	@endforeach
	</tbody>
</table>
	{{ $categories->links() }}
</div>
@stop