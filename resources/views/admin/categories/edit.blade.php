@extends('admin._layouts.default')

@section('main')

    
    @include('admin._partials.notifications')

<div class="subnav">
	<ul>
        <li><a class="btn btn-default btn-xs" href="{{ URL::to('admin/categories') }}">View All Categories</a></li>
	</ul>
</div>
<h1>Edit {{ $categories->title }} in {{$book->Title}} book</h1>

<!-- if there are creation errors, they will show here -->



{{ Form::model($categories, array('route' => array('admin.categories.update', $categories->id), 'method' => 'PUT', 'files' => true)) }}

    <div class="form-group">
        {{ Form::label('title', 'Category') }}
        {{ Form::text('title', null, array('class' => 'form-control', 'required', 'style' => 'Width:500px')) }}
    </div>

    <div class="form-group">
        {{ Form::label('book', 'Book') }}

        {{Form::select('bookid', $books, null, array('class' => 'form-control')) }}

    </div>

	{{ Form::submit('Edit the Category!', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}

    <table class="table table-condensed table-striped table-bordered">
        <thead>
        <tr>
            <th class="col-sm-1" style="width:4%">ID</th>
            <th class="col-sm-4" style="width:40%">Company</th>
            <th class="col-sm-4" style="width:20%">Book</th>
            <th class="col-sm-2" style="width:20%">Listing</th>

            <th class="col-sm-4" style="width:280px !important;min-width:280px;">Actions</th>
        </tr>
        </thead>
        <tbody>

        @foreach($listing as $key => $value)
            <tr>

                <td>{{ $value->listingbusinessid }}</td>
                <td @if ($value->listings_deleted)
                    style="color:red"
                        @endif
                        >{{ $value->listingname }}</td>

                <td>{{ $value->Title }}</td>
                <td><div style="width: 400px; word-wrap: break-word"> {{ Str::limit(strip_tags($value->listingtext), 40) }}</div></td>

                <!-- we will also add show, edit, and delete buttons -->
                <td>

                    <!-- delete the company (uses the destroy method DESTROY /company/{id} -->
                    <!-- we will add this later since its a little more complicated than the first two buttons -->
                    {{ Form::open(array('url' => 'admin/listings/' . $value->id, 'class' => 'pull-right')) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-warning')) }}
                    {{ Form::close() }}

                    <!-- show the company (uses the show method found at GET /company/{id} -->
                    <a class="btn btn-small btn-success" href="{{ URL::to('admin/listings/' . $value->id) }}">Show</a>

                    <!-- show the company (uses the show method found at GET /company/{id} -->
                    <a class="btn btn-small btn-success" href="{{ URL::to('admin/reviews/list/' . $value->id) }}">Reviews</a>

                    <!-- edit this company (uses the edit method found at GET /company/{id}/edit -->
                    <a class="btn btn-small btn-info" href="{{ URL::to('admin/listings/' . $value->id . '/edit') }}">Edit</a>

                </td>
            </tr>
        @endforeach

        </tbody>
    </table>
@stop
