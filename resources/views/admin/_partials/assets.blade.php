<link href="{{ URL::asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ URL::asset('assets/css/bootstrap-responsive.min.css') }}" rel="stylesheet">
<link href="//netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">
<link href="{{ URL::asset('assets/css/main.css') }}" rel="stylesheet">
<link href="{{ URL::asset('assets/css/adminmain.css') }}" rel="stylesheet">
{{--<link href="//cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet">--}}


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
{{--<script src="//code.jquery.com/jquery-1.9.1.min.js"></script>--}}
<script src="{{ URL::asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/script.js?v=2') }}"></script>
{{--<script src="//cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>--}}
<script type="text/javascript" src="https://appcenter.intuit.com/Content/IA/intuit.ipp.anywhere.js"></script>
<script type="text/javascript">
    intuit.ipp.anywhere.setup({
        menuProxy: '<?php print(env('QBO_MENU_URL')); ?>',
        grantUrl: '<?php print(env('QBO_OAUTH_URL')); ?>'
    });
</script>




