<div id="leftWrapper">
    <div id="listView" class="list">

        @if (Auth::user()->hasRole('admin'))
        <li><a class="dashboard" href="{{ URL::to('admin/dashboard') }}">Home</a></li>
        @endif

        <li><a class="product" href="{{ URL::to('admin/product') }}">Products</a></li>
        <li><a class="orders" href="{{ URL::to('admin/orders') }}">Orders</a></li>
        @if (Auth::user()->hasRole('admin') or Auth::user()->hasRole('assistantAdmin'))
            <li><a id="filterstock" class="stock" href="{{ URL::to('admin/stock') }}">Sales & Reorder</a></li>
            <li><a class="quantity" href="{{ URL::to('admin/quantity') }}">Prices & Quantity</a></li>
            <li><a class="user" href="{{ URL::to('admin/user') }}">Customers</a></li>
            <li><a class="map" href="{{ URL::to('admin/map') }}">Customer Map</a></li>
            <li><a class="pages" href="{{ URL::to('admin/pages') }}">Pages</a></li>
            <li><a class="settings" href="{{ URL::to('admin/settings') }}">Settings</a></li>
        @endif
        {{--<li><a class="reorder" href="{{ URL::to('admin/reorder') }}">Reorder</a></li>--}}
        <li><a class="images" href="{{ URL::to('admin/images') }}">Images</a></li>
    </div>
</div>

<script>


    $('.list li').removeClass('list-item-active');
    setNavigation();

    function setNavigation() {
        var path = window.location.pathname;
        path = path.replace(/\/$/, "");
        path = decodeURIComponent(path);

        $("#listView li a").each(function () {
            var href = $(this).attr('class');
            if (path.indexOf(href) >= 0) {
                $(this).closest('li').addClass('list-item-active');
            }
            if (path.indexOf("filterstock") >= 0 && href == 'stock') {
                $(this).closest('li').addClass("list-item-active");
            }
            if (path.indexOf("savereorder") >= 0 && href == 'stock') {
                $(this).closest('li').addClass("list-item-active");
            }
        });

    }


</script>

