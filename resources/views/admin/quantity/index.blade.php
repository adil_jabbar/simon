@extends('app2')

@section('title', 'Update Sales & Reorder Quantity')

@section('content')
    @include('admin._partials.leftWrapper')

    <div id="rightWrapper">
        {{--<div id="header"><a id="fullPage" href="#">|||</a></div>--}}

        <div id="stickydiv">
            <h1>Update Stock Quantity
                <a style="float:right;margin-left:10px" href="/admin/stockreportdownload/" class="btn btn-success"><i
                            class="icon-plus-sign"></i> Download Stock Report</a> 
                <a style="float:right" href="/admin/stockreport/" class="btn btn-success"><i class="icon-plus-sign"></i>
                    Create Stock Report</a>
            </h1>

            {!! Form::open(array('action' => 'admin\QuantityController@quantitychange')) !!}


            <table>
                <tr>
                    <td width="260"><strong>Show Category</strong></td>
                    <td><strong>Show Stock</strong></td>
                </tr>
                <tr>
                    <td><select id="form1_category" name="category" class="autosubmit">
                            <option value="">Please Select</option>
                            <option value="1">Sheppherd Hook Earrings</option>
                            <option value="2">Stud Earrings</option>
                            <option value="3">Braclets</option>
                            <option value="4">Necklaces</option>
                            <option value="5">Pendants</option>
                            <option value="6">Rings</option>
                            {{--<option value="7">New Arrivals</option>--}}
                        </select></td>
                    <td><select id="form1_quantity" name="selectqty" class="autosubmit">
                            <option value="">Please Select</option>
                            <option value="0">Zero quantity</option>
                            <option value="3">Quantity less than 3</option>
                            <option value="5">Quantity less than 5</option>
                            <option value="10">Quantity less than 10</option>
                            <option value="15">Quantity less than 15</option>
                            <option value="20">Quantity less than 20</option>
                            <option value="9999">Show all</option>
                        </select></td>
                </tr>
            </table>

            <input type="submit" value="Submit">
            {!! Form::close() !!}


            {!! Notification::showAll() !!}

            <style>
                #stickydiv {
                    height: 210px !important;
                }

                table#quantity {
                    margin-top: 215px !important;
                }

                .shorterfield {
                    width: 60px !important;
                }

                table#quantity td {
                    padding-bottom: 10px !important;
                }
            </style>
            <table class="table table-striped" style="table-layout: fixed;width:900px">
                <thead>
                    <tr>
                        <th style="text-align: left;width:200px;">Code</th>
                        <th style="width:40px;">S</th>
                        <th style="width:40px;">M</th>
                        <th style="width:40px;">D</th>
                        <th style="width:60px;">FOB</th>
                        <th style="width:80px;text-align:center">UPDATE<br/> FOB</th>
                        <th style="width:60px;">FRT</th>
                        <th style="width:60px;">BLT</th>
                        <th style="width:60px;">H/M</th>
                        <th style="width:60px;">I/S</th>
                        <th style="width:60px;">100%</th>
                        <th style="width:60px;">SELL</th>
                        <th style="width:80px;text-align:center">UPDATE<br/> SELL</th>
                        <th style="width:60px;">STOCK</th>
                        <th style="width:70px;text-align: center">UPDATE STOCK</th>
                    </tr>
                </thead>
            </table>
        </div>

        <meta name="csrf-token" content="{{ csrf_token() }}">
        <table class="table table-striped" id="quantity" style="table-layout: fixed;width:900px;">
            <tbody>
            <?php $prev = ''; ?>

            @foreach ($stock as $c)
                @foreach ($c->products as $p)

                    @foreach ($p->subproduct as $subproduct)

                        @if (isset($qty) && ($qty >= $subproduct->quantity) or (!isset($qty)))

                            @if ($p['productName'] != $prev)
                                <tr>
                                    <td style="text-align:left;padding-top:10px;padding-bottom:3px;font-weight:bold;width:200px;">
                                        {{ $p['productName'] }}
                                    </td>
                                    <td style="width:40px;"></td>
                                    <td style="width:40px;"></td>
                                    <td style="width:40px;"></td>
                                    <td style="width:60px;"></td>
                                    <td style="text-align: left;width:80px;"></td>
                                    <td style="width:60px;"></td>
                                    <td style="width:60px;"></td>
                                    <td style="width:60px;"></td>
                                    <td style="width:60px;"></td>
                                    <td style="width:60px;padding-top:10px;padding-bottom:3px;text-align: center"></td>
                                    <td style="width:60px;"></td>
                                    <td style="padding-top:-10px;width:80px"></td>
                                    <td style="padding-top:10px;padding-bottom:3px;color:red;text-align: center;width:60px"></td>
                                    <td style="padding-top:-10px;width:70px"></td>
                                </tr>
                                <?php $prev = $p['productName']; ?>
                            @endif

                            <tr id="prod-id-{{ $subproduct->id }}" data-prop="{{ $subproduct->id }}" class="sub-product-row">
                                <td style="text-align: left;width:200px;">
                                    {{ $subproduct->sku }}
                                    <a name="{{ $subproduct->id }}" id="{{ $subproduct->id }}"></a>
                                </td>
                                <td style="width:40px;">
                                    <input type="checkbox" name="simon[{{ $subproduct->id }}]" @if ($subproduct->simon == 1) checked @endif value="1" />
                                </td>
                                <td style="width:40px;">
                                    <input type="checkbox" name="max[{{ $subproduct->id }}]" @if ($subproduct->max == 1) checked @endif value="1" />
                                </td>
                                <td style="width:40px;">
                                    <input type="checkbox" name="demelza[{{ $subproduct->id }}]" @if ($subproduct->demelza == 1) checked @endif value="1 "/>
                                </td>
                                <td style="width:60px;">
                                    <span class="fob-{{ $subproduct->id }}">{{ $subproduct->fob }}</span>
                                </td>
                                <td style="padding-top:-10px;width:80px">
                                    {!! Form::text('fob['.$subproduct->id.']', null, array('class' => 'shorterfield')) !!}
                                </td>
                                <td style="width:60px;">
                                    <span class="frt-{{ $subproduct->id }}">{{ $subproduct->freight }}</span>
                                </td>
                                <td style="width:60px;">
                                    <span class="bolt-{{ $subproduct->id }}">{{ $subproduct->bolt }}</span>
                                </td>
                                <td style="width:60px;">
                                    <span class="hm-{{ $subproduct->id }}">{{ $subproduct->hm }}</span>
                                </td>
                                <td style="width:60px;">
                                    <span class="is-{{ $subproduct->id }}">{{ $subproduct->into_safe }}</span>
                                </td>
                                <td style="width:60px;padding-top:10px;padding-bottom:3px;text-align: center">
                                    <span class="isp-{{ $subproduct->id }}">{{ $subproduct->into_safe * 2 }}</span>
                                </td>
                                <td style="width:60px;">
                                    <span class="sell-{{ $subproduct->id }}">{{ round($subproduct->grossPrice, 0) }}</span>
                                </td>
                                <td style="padding-top:-10px;width:80px">
                                    {!! Form::text('grossPrice['.$subproduct->id.']', null, array('class' => 'shorterfield')) !!}
                                </td>
                                {{--<td><img width="50" height="50" src="{{ '/images/products/' . $subproduct->image }}"></td>--}}
                                @if ($subproduct->quantity > 5)
                                    <td style="padding-top:10px;padding-bottom:3px;text-align: center;width:60px">
                                        <span class="qty-{{ $subproduct->id }}">{{ $subproduct->quantity }}</span>
                                    </td>
                                @else
                                    <td style="padding-top:10px;padding-bottom:3px;color:red;text-align: center;width:60px">
                                        <span class="qty-{{ $subproduct->id }}">{{ $subproduct->quantity }}</span>
                                    </td>
                                @endif
                                <td style="padding-top:-10px;width:70px">
                                    {!! Form::text('quantity['.$subproduct->id.']', null, array('class' => 'shorterfield')) !!}
                                    <input type="hidden" name="id[]" value="{{ $subproduct->id }}"/>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                @endforeach
            @endforeach
            <tr>

            </tr>


            </tbody>
        </table>
        <input type="hidden" id="category" name="category" value="{{ $category }}"/>
        <input type="hidden" id="qty" name="qty" value="{{ $qty }}"/>
    </div>
    <style>

        .up-border {
            border-bottom: 1px solid green;  
        }

        tr.fob-updated span[class^="fob-"], 
        tr.fob-updated span[class^="frt-"], 
        tr.fob-updated span[class^="bolt-"],
        tr.fob-updated span[class^="hm-"], 
        tr.fob-updated span[class^="is-"],
        tr.fob-updated span[class^="isp-"],
        tr.qty-updated span[class^="qty-"],
        tr.gross-updated span[class^="sell-"] {
            border-bottom: 1px solid green;
            color: green;
        }

        .normal-text {
            color: initial;
        }

        .red-text {
            color: red;
        }
    </style>
    <script src="//code.jquery.com/jquery-1.9.1.min.js"></script>
    <script type='text/javascript'>
        $(  document).ready(function(){
            @if (isset($middle) && $middle > 1)
                document.getElementById('{{ $middle }}').scrollIntoView(false);
            @endif
            
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // process checkboxes
            $('.sub-product-row input[type="checkbox"]').on('change', function(){
                
                $attName = $(this).attr('name');
                $trID = $(this).closest('tr').attr('data-prop');
                
                if($(this).is(':checked')) {
                    $attVal = 1;
                } else {
                    $attVal = 0;
                }

                $.ajax({
                    type:'POST',
                    url:'/admin/api/quantity',
                    dataType: 'json',
                    data: {
                        rowid: $trID,
                        name: $attName,
                        value: $attVal
                    },
                    success: function(data){
                        $('#prod-id-' + data.id + ' input[name="' + data.fieldname + '"]').closest('td').addClass('up-border');

                        clearField($trID, $attName);
                    },
                    error: function(){
                        alert('An error occurred. Please refresh the page and try again.');
                    }
                });
            });


            $('.sub-product-row input[name^=fob]').keyup(delay(function (e) {
                $attName = $(this).attr('name');
                $trID = $(this).closest('tr').attr('data-prop');
                $attVal = $(this).val();

                var keycode = (window.event) ? event.keyCode : e.keyCode;

                if($attVal !== '' & keycode !== 9) {
                    $.ajax({
                        type:'POST',
                        url:'/admin/api/quantity',
                        dataType: 'json',
                        data: {
                            rowid: $trID,
                            name: $attName,
                            value: $attVal
                        },
                        success: function(data) {
                            $fobIinputSel = $('#prod-id-' + data.id + ' input[name="' + data.fieldname + '"]');
                            $fobIinputSel.closest('tr').addClass('fob-updated');

                            // update fob
                            $fobSpan = $('#prod-id-' + data.id + ' span.fob-' + data.id);
                            $fobSpan.html('').html(data.fieldval);;

                            // update frt
                            $frtSpan = $('#prod-id-' + data.id + ' span.frt-' + data.id);
                            $frtSpan.html('').html(data.newFrt);;

                            // update bolt
                            $boltSpan = $('#prod-id-' + data.id + ' span.bolt-' + data.id);
                            $boltSpan.html('').html(data.newBlt);;

                            // update hm
                            $hmSpan = $('#prod-id-' + data.id + ' span.hm-' + data.id);
                            $hmSpan.html('').html(data.newHm);;

                            // update is
                            $isSpan = $('#prod-id-' + data.id + ' span.is-' + data.id);
                            $isSpan.html('').html(data.newIs);;

                            // update percentage
                            $ispSpan = $('#prod-id-' + data.id + ' span.isp-' + data.id);
                            $ispSpan.html('').html(data.newIsp);
                            
                            clearField($trID, $attName, 'fob-updated');
                        },
                        error: function(){
                            alert('An error occurred. Please refresh the page and try again.');
                        }
                     });
                }
            }, 800));


            $('.sub-product-row input[name^=quantity]').keyup(delay(function (e) {
                $attName = $(this).attr('name');
                $trID = $(this).closest('tr').attr('data-prop');
                $attVal = $(this).val();

                var keycode = (window.event) ? event.keyCode : e.keyCode;

                if($attVal !== '' & keycode !== 9) {
                    $.ajax({
                        type:'POST',
                        url:'/admin/api/quantity',
                        dataType: 'json',
                        data: {
                            rowid: $trID,
                            name: $attName,
                            value: $attVal
                        },
                        success: function(data) {
                            $qtyInputSel = $('#prod-id-' + data.id + ' input[name="' + data.fieldname + '"]');
                            $qtyInputSel.closest('tr').addClass('qty-updated');

                            $qtySpan = $('#prod-id-' + data.id + ' span.qty-' + data.id);
                            $qtySpan.html('').html(data.fieldval);
                            
                            clearField($trID, $attName, 'qty-updated');

                            if(data.fieldval > 5) {
                                $qtySpan.removeClass('red-text').addClass('normal-text');
                            } else {
                                $qtySpan.removeClass('normal-text').addClass('red-text');
                            }
                        },
                        error: function(){
                            alert('An error occurred. Please refresh the page and try again.');
                        }
                    });
                }
            }, 800));

 
            $('.sub-product-row input[name^=grossPrice]').keyup(delay(function (e) {
                $attName = $(this).attr('name');
                $trID = $(this).closest('tr').attr('data-prop');
                $attVal = $(this).val();

                var keycode = (window.event) ? event.keyCode : e.keyCode;

                if($attVal !== '' & keycode !== 9) {
                    $.ajax({
                        type:'POST',
                        url:'/admin/api/quantity',
                        dataType: 'json',
                        data: {
                            rowid: $trID,
                            name: $attName,
                            value: $attVal
                        },
                        success: function(data) {
                            $grossInputSel = $('#prod-id-' + data.id + ' input[name="' + data.fieldname + '"]');
                            $grossInputSel.closest('tr').addClass('gross-updated');

                            $sellSpan = $('#prod-id-' + data.id + ' span.sell-' + data.id);
                            $sellSpan.html('').html(Math.round(data.fieldval));
                            
                            clearField($trID, $attName, 'gross-updated');
                        },
                        error: function(){
                            alert('An error occurred. Please refresh the page and try again.');
                        }
                    });
                }
            }, 800));
        });

        function clearField(id, fieldname, className='') {
            setTimeout(
                function() {
                    $clearInp = $('#prod-id-' + id + ' input[name="' + fieldname + '"]');
                    $clearInp.val('');
                    
                    $clearInp.closest('td').removeClass('up-border');
                    if(className !== '') {
                        $clearInp.closest('tr').removeClass(className);
                    }
            }, 2000);
        }
        function delay(callback, ms) {
            var timer = 0;
            return function() {
                var context = this, args = arguments;
                clearTimeout(timer);
                timer = setTimeout(function () {
                    callback.apply(context, args);
                }, ms || 0);
            };
        }

    </script>

@stop
