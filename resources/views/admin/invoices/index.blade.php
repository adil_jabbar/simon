<!DOCTYPE html>
<html>
<head>
	<title>Performance CMS</title>
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">

<nav class="navbar navbar-inverse">
	<div class="navbar-header">
		<a class="navbar-brand" href="{{ URL::to('admin') }}">Back to Admin</a>
	</div>

</nav>

<h1>Invoices for {{ $book->Title }}
    <span style="float:right">
        <a title="This will take a minute!" class="btn btn-small btn-success"
           href="{{ URL::to('admin/renewal/' . $book->id) }}">Update Renewal Prices</a>

         <a title="This will take a minute!" class="btn btn-small btn-success"
            href="{{ URL::to('admin/generate_renewals/' . $book->id) }}">Generate All Invoices</a>
    </span></h1>

<h3> {{ $listing->count() }} listings found in {{ $book->Title }}</h3>
<!-- will be used to show any messages -->
@if (Session::has('message'))
	<div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

<div class="container-fluid">       
    <div class="row-fluid">
<table class="table table-condensed table-striped table-bordered">
	<thead>
		<tr>

			<th class="col-sm-4" style="text-align:center;width:40px !important;min-width:40px;">ID</th>
			<th class="col-sm-2" style="text-align:center;width:240px !important;min-width:240px;">Listing</th>
            <th class="col-sm-2" style="text-align:center;width:250px !important;min-width:250px;">Category</th>
            <th class="col-sm-2" style="text-align:center;width:60px !important;min-width:60px;">Original Price</th>
            <th class="col-sm-2" style="text-align:center;width:60px !important;min-width:60px;">Renewal Price</th>
            <th class="col-sm-2" style="text-align:center;width:60px !important;min-width:60px;">Override Amt</th>
			<th class="col-sm-4" style="text-align:center;width:350px !important;min-width:350px;">Actions</th>
		</tr>
	</thead>
	<tbody>

	@foreach($listing as $key => $value)

		<tr>

			<td align="center">{{ $value->listingbusinessid }}</td>
			<td><a style="color:green;text-decoration: underline" href="{{ URL::to('admin/override/' . $value->id . '/edit' ) }}">{{ $value->listingname }}</a></td>

			
			{{--<td><div style="width: 250px; word-wrap: break-word"> {{ Str::limit(strip_tags($value->listingtext), 60) }}</div></td>--}}
            <td> {{ $value->title }}</td>
            <td align="center"> {{ $value->renewal_price }}</td>
            <td align="center"
                    @if (($value->calculated_renewal_total - $value->renewal_price) > 10 &&  $value->renewal_price !=0  )
                        {{ ' style="color:red"' }}
                    @endif
                    @if (($value->calculated_renewal_total - $value->renewal_price) < -10  )
                         {{ ' style="color:green;font-weight:bold"' }}
                    @endif


                    > {{ $value->calculated_renewal_total }}</td>
            <td align="center"> {{ $value->override_amount }}</td>
            <td>

                @if (!$value->listings_deleted)
                <a class="btn btn-small btn-info" href="{{ URL::to('admin/generate_invoice/' . $value->id ) }}">Generate Invoice</a>
                @endif
                @if (File::exists(storage_path() . '/Invoices/' . Date('Y') . '/Invoice_' . $value->id    . '.pdf'))
                    <a class="btn btn-small btn-success" href="{{ URL::to('admin/view_invoice/' . $value->id ) }}">View</a>

                    @if (trim(strlen($value->email1)) > 0)
                       <a target="_blank" class="btn btn-small btn-primary" href="{{ URL::to('admin/email_invoice/' . $value->id ) }}">Email Invoice</a>
                    @endif
                @endif

			</td>
		</tr>
	@endforeach
    <tr><td colspan="3" style="text-align: right">Total value of renewals </td><td>{{ $book->prev_total; }}</td><td align="right">{{ $book->renewal_total; }}</td></tr>
    </tbody>
</table>
        {{ $listing->links() }}
    </div></div></div>
</body>
</html>