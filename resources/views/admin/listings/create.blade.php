<!-- app/views/job/create.blade.php -->

<!DOCTYPE html>
<html>
<head>
	<title>Performance CMS</title>
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
	<script src="{{ URL::asset('assets/js/script.js') }}"></script>
	<script src="{{ URL::asset('ckeditor/ckeditor.js?v=1') }}"></script>
	<link rel="stylesheet" href="{{ URL::asset('assets/css/adminmain.css') }}"></link>

</head>

<body>
<div class="container">

<nav class="navbar navbar-inverse">
	<div class="navbar-header">
		<a class="navbar-brand" href="{{ URL::to('admin') }}">Back to Admin</a>
	</div>
	<ul class="nav navbar-nav">
		<li><a href="{{ URL::to('admin/listings/book/' . $book->id) }}">View All Listings</a></li>
		<li><a href="{{ URL::to('admin/listings/create/' . $book->id) }}">Create a Listing</a></li>
	</ul>
</nav>

    @include('admin._partials.notifications')


    <h1>Create a listing in {{ $book->Title }} book</h1>

<!-- if there are creation errors, they will show here -->


    <div style="width:57%;float:left">

{{ Form::open(array('url' => 'admin/listings')) }}

	<div class="form-group">
    		{{ Form::label('listingname', 'Company Name') }}
    		{{ Form::textarea('listingname', null, array('class' => 'form-control')) }}
    	</div>

        <div class="form-group">
            {{ Form::label('category_id', 'Listing Category') }}<br/>
            {{ Form::select('category_id', $categories) }}
        </div>
    	{{--<div class="form-group">--}}
            {{--{{ Form::label('listingbusinessid', 'Business ID') }}--}}
            {{--{{ Form::text('listingbusinessid', null, array('class' => 'form-control')) }}--}}
        {{--</div>--}}

        <input value="{{ $book->id }}" name="listingbookid" id="listingbookid" type="hidden">

        {{ Form::label('listings_deleted', 'Listing Live?') }}
        <div style="width:80px" class="form-group">

    	{{Form::select('listings_deleted', array( '0' => 'Yes', '1' => 'No'), null, array('class' => 'form-control')) }}


        </div>

        {{--<div class="form-mainitem">--}}
            {{--{{ Form::label('listingbusinessid', 'Business ID') }}--}}
            {{--{{ Form::text('listingbusinessid', null, array('class' => 'form-control', 'style' => "width:80px")) }}--}}
        {{--</div>--}}

        <div class="form-mainitem">
            {{ Form::label('listing_year', 'Year 1st Listed') }}
            {{ Form::text('listing_year', null, array('class' => 'form-control', 'style' => "width:80px")) }}
        </div>

         <div class="form-group ">
                <div class="form-mainitem">
                        {{ Form::label('listingaddress', 'Address 1') }}
                        {{ Form::text('listingaddress', null, array('class' => 'form-control')) }}
                </div>
                {{--<div class="form-lineitem">--}}
                        {{--{{ Form::label('listingaddressline', 'Line') }}--}}
                        {{--{{ Form::text('listingaddressline', null, array('class' => 'form-control')) }}--}}
                {{--</div>--}}
        </div>


        <div class="form-group ">
                <div class="form-mainitem">
                        {{ Form::label('address1', 'Address 2') }}
                        {{ Form::text('address1', null, array('class' => 'form-control')) }}
                </div>
                {{--<div class="form-lineitem">--}}
                        {{--{{ Form::label('address1line', 'Line') }}--}}
                        {{--{{ Form::text('address1line', null, array('class' => 'form-control')) }}--}}
                {{--</div>--}}
        </div>

        <div style="clear:both"></div>
    	<div class="form-group">
    		{{ Form::label('listingtext', 'Listing') }}
    		{{ Form::textarea('listingtext', null, array('class' => 'form-control')) }}
    	</div>
        <div class="form-group">
            {{ Form::label('specialoffer', 'Special Offer') }}
            {{ Form::textarea('specialoffer', null, array('class' => 'form-control')) }}
        </div>
        {{ Form::submit('Create Listing!', array('class' => 'btn btn-primary')) }}
        <br/><br/>
        <div class="form-group ">
            <div class="form-mainitem">
                    {{ Form::label('email1', 'Email 1') }}
                    {{ Form::text('email1', null, array('class' => 'form-control')) }}
            </div>
            {{--<div class="form-lineitem">--}}
                    {{--{{ Form::label('email1line', 'Line') }}--}}
                    {{--{{ Form::text('email1line', null, array('class' => 'form-control')) }}--}}
            {{--</div>--}}
        </div>
        <div class="form-group ">
                <div class="form-mainitem">
                        {{ Form::label('email2', 'Email 2') }}
                        {{ Form::text('email2', null, array('class' => 'form-control')) }}
                </div>
                {{--<div class="form-lineitem">--}}
                        {{--{{ Form::label('emai21line', 'Line') }}--}}
                        {{--{{ Form::text('email2line', null, array('class' => 'form-control')) }}--}}
                {{--</div>--}}
        </div>

        <div class="form-group ">
                <div class="form-mainitem">
                        {{ Form::label('webaddress1', 'Web address 1') }}
                        {{ Form::text('webaddress1', null, array('class' => 'form-control')) }}
                </div>
                {{--<div class="form-lineitem">--}}
                        {{--{{ Form::label('webaddress1line', 'Line') }}--}}
                        {{--{{ Form::text('webaddress1line', null, array('class' => 'form-control')) }}--}}
                {{--</div>--}}
        </div>

         <div class="form-group ">
                    <div class="form-mainitem">
                            {{ Form::label('webaddress2', 'Web address 2') }}
                            {{ Form::text('webaddress2', null, array('class' => 'form-control')) }}
                    </div>
                    {{--<div class="form-lineitem">--}}
                            {{--{{ Form::label('webaddress2line', 'Line') }}--}}
                            {{--{{ Form::text('webaddress2line', null, array('class' => 'form-control')) }}--}}
                    {{--</div>--}}
            </div>
            <div class="form-group ">
                    <div class="form-mainitem">
                            {{ Form::label('phone1', 'Phone 1') }}
                            {{ Form::text('phone1', null, array('class' => 'form-control')) }}
                    </div>
                    {{--<div class="form-lineitem">--}}
                            {{--{{ Form::label('phone1line', 'Line') }}--}}
                            {{--{{ Form::text('phone1line', null, array('class' => 'form-control')) }}--}}
                    {{--</div>--}}
                    <div class="form-type">
                            {{ Form::label('phone1type', 'Type') }}
                            {{Form::select('phone1type', array('L' => 'Landline', 'M' => 'Mobile', 'F' => 'Fax' ,
                            'U' => 'Unknown', 'X' => 'Phone/Fax', 'O' => 'Office', 'R' => 'Local rate', 'Z' => 'Freephone'),
                             null, array('class' => 'form-control')) }}
                    </div>
            </div>

            <div class="form-group ">
                    <div class="form-mainitem">
                            {{ Form::label('phone2', 'Phone 2') }}
                            {{ Form::text('phone2', null, array('class' => 'form-control')) }}
                    </div>
                    {{--<div class="form-lineitem">--}}
                            {{--{{ Form::label('phone2line', 'Line') }}--}}
                            {{--{{ Form::text('phone2line', null, array('class' => 'form-control')) }}--}}
                    {{--</div>--}}
                     <div class="form-type">
                        {{ Form::label('phone2type', 'Type') }}
                        {{Form::select('phone2type', array('L' => 'Landline', 'M' => 'Mobile', 'F' => 'Fax', 'U' => 'Unknown'
                        , 'X' => 'Phone/Fax', 'O' => 'Office', 'R' => 'Local rate', 'Z' => 'Freephone'),
                         null, array('class' => 'form-control')) }}
                    </div>
            </div>

            <div class="form-group ">
                    <div class="form-mainitem">
                            {{ Form::label('phone3', 'Phone 3') }}
                            {{ Form::text('phone3', null, array('class' => 'form-control')) }}
                    </div>
                    {{--<div class="form-lineitem">--}}
                            {{--{{ Form::label('phone3line', 'Line') }}--}}
                            {{--{{ Form::text('phone3line', null, array('class' => 'form-control')) }}--}}
                    {{--</div>--}}
                     <div class="form-type">
                        {{ Form::label('phone3type', 'Type') }}
                        {{Form::select('phone3type', array('L' => 'Landline', 'M' => 'Mobile', 'F' => 'Fax',
                         'U' => 'Unknown', 'X' => 'Phone/Fax', 'O' => 'Office', 'R' => 'Local rate', 'Z' => 'Freephone'),
                         null, array('class' => 'form-control')) }}
                    </div>
            </div>
            <div class="form-group ">
                    <div class="form-mainitem">
                            {{ Form::label('phone4', 'Phone 4') }}
                            {{ Form::text('phone4', null, array('class' => 'form-control')) }}
                    </div>
                    {{--<div class="form-lineitem">--}}
                            {{--{{ Form::label('phone4line', 'Line') }}--}}
                            {{--{{ Form::text('phone4line', null, array('class' => 'form-control')) }}--}}
                    {{--</div>--}}
                     <div class="form-type">
                         {{ Form::label('phone4type', 'Type') }}
                         {{Form::select('phone4type', array('L' => 'Landline', 'M' => 'Mobile', 'F' => 'Fax', 'U' => 'Unknown',
                         'X' => 'Phone/Fax', 'R' => 'Local rate', 'O' => 'Office', 'Z' => 'Freephone'),
                          null, array('class' => 'form-control')) }}
                     </div>
            </div>
         <div class="form-group ">
                <div class="form-mainitem">
                        {{ Form::label('address2', 'Address 3') }}
                        {{ Form::text('address2', null, array('class' => 'form-control')) }}
                </div>
                {{--<div class="form-lineitem">--}}
                        {{--{{ Form::label('address2line', 'Line') }}--}}
                        {{--{{ Form::text('address2line', null, array('class' => 'form-control')) }}--}}
                {{--</div>--}}
        </div>
         <div class="form-group ">
                    <div class="form-mainitem">
                            {{ Form::label('address3', 'Address 4') }}
                            {{ Form::text('address3', null, array('class' => 'form-control')) }}
                    </div>
                    {{--<div class="form-lineitem">--}}
                            {{--{{ Form::label('address3line', 'Line') }}--}}
                            {{--{{ Form::text('address3line', null, array('class' => 'form-control')) }}--}}
                    {{--</div>--}}
            </div>
             <div class="form-group ">
                        <div class="form-mainitem">
                                {{ Form::label('address4', 'Address 5') }}
                                {{ Form::text('address4', null, array('class' => 'form-control')) }}
                        </div>
                        {{--<div class="form-lineitem">--}}
                                {{--{{ Form::label('address4line', 'Line') }}--}}
                                {{--{{ Form::text('address4line', null, array('class' => 'form-control')) }}--}}
                        {{--</div>--}}
                </div>
                <div class="form-group ">
                        <div class="form-mainitem">
                                {{ Form::label('address5', 'Address 6') }}
                                {{ Form::text('address5', null, array('class' => 'form-control')) }}
                        </div>
                        {{--<div class="form-lineitem">--}}
                                {{--{{ Form::label('address5line', 'Line') }}--}}
                                {{--{{ Form::text('address5line', null, array('class' => 'form-control')) }}--}}
                        {{--</div>--}}
                </div>
    	<script type="text/javascript">
    		CKEDITOR.replace('listingtext');
            CKEDITOR.replace('listingname');
            CKEDITOR.replace('specialoffer');
    	</script>


    	<div style="clear:both"></div>
	{{ Form::submit('Create the Listing!', array('class' => 'btn btn-primary')) }}


    </div>
    <style>.toplines {font-weight: bold;margin-bottom:0px;"}
        h3 {font-style: italic;border-bottom: 2px solid #000000;margin-bottom:10px}
    </style>
    <div style="width:40%;float:left;margin-left:3%">

    <h2>Business Renewal Details</h2>
    <div class="form-mainitem">
        {{ Form::label('contact_name', 'Contact Name') }}
        {{ Form::text('contact_name', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-mainitem">
        {{ Form::label('contact_salutation', 'Contact Salutation') }}
        {{ Form::text('contact_salutation', null, array('class' => 'form-control', 'style' => "width:60%")) }}
    </div>

    <div class="form-mainitem">
        {{ Form::label('address_line1', 'Address Line 1') }}
        {{ Form::text('address_line1', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-mainitem">
        {{ Form::label('address_line2', 'Address Line 2') }}
        {{ Form::text('address_line2', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-mainitem">
        {{ Form::label('address_line3', 'Address Line 3') }}
        {{ Form::text('address_line3', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-mainitem">
        {{ Form::label('address_line4', 'Address Line 4') }}
        {{ Form::text('address_line4', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-mainitem">
        {{ Form::label('renewal_price', 'Renewal Price') }}
        {{ Form::text('renewal_price', null, array('class' => 'form-control', 'style' => "width:30%")) }}
    </div>

    <div class="form-mainitem">
        {{ Form::label('renewal_vat', 'Renewal VAT') }}
        {{ Form::text('renewal_vat', null, array('class' => 'form-control', 'style' => "width:30%")) }}
    </div>

    <div class="form-mainitem">
        {{ Form::label('renewal_total', 'Renewal Total') }}
        {{ Form::text('renewal_total', null, array('class' => 'form-control', 'style' => "width:30%")) }}
    </div>

    <div class="form-mainitem">
        {{ Form::submit('Create Listing!', array('class' => 'btn btn-primary')) }}
    </div>

    {{ Form::close() }}

    </div>

</div>
</body>
</html>