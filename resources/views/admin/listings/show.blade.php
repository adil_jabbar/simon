<!-- app/views/company/show.blade.php -->

<!DOCTYPE html>
<html>
<head>
	<title>Performance CMS</title>
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
	<script src="{{ URL::asset('assets/js/script.js?v=2') }}"></script>
	<script src="{{ URL::asset('ckeditor/ckeditor.js') }}"></script>
	<link rel="stylesheet" href="{{ URL::asset('assets/css/adminmain.css') }}"></link>

</head>
<body>
<div class="container">

<nav class="navbar navbar-inverse">
	<div class="navbar-header">
		<a class="navbar-brand" href="{{ URL::to('admin') }}">Back to Admin</a>
	</div>
	<ul class="nav navbar-nav">
		<li><a href="{{ URL::to('admin/listings/book/' . $book->id) }}">View All Listings</a></li>
		<li><a href="{{ URL::to('admin/listings/create/' . $book->id) }}">Create a Listing</a></li>
	</ul>
</nav>

<h1>Showing {{ trim(strip_tags($listing->listingname)) }}</h1>

<style>.toplines {font-weight: bold;margin-bottom:0px;"}
h3 {font-style: italic;border-bottom: 2px solid #000000;margin-bottom:10px}
</style>
<div style="width:40%;float:left;margin-left:3%">
<h2>{{ $book->Title }}</h2>
@if(isset($category))
<h3>{{ $category->title }}</h3>
@else
<h3>NO CATEGORY FOUND!</h3>
@endif
    <p class="toplines">{{ $listing->listingbusinessid }} {{ '&nbsp;' }} {{ strip_tags($listing->listingname, '<strong><em>') }}
        @if($listing->listingaddressline == 0)
            <span style="font-weight: normal;font-size:0.9em"> {{ $listing->listingaddress }}</span>
        @endif
        @if($listing->phone1line == 1 or $listing->phone1line == 0)
            {{ '&nbsp;' }}
            {{ $listing->phone1 }}
        @endif
        @if($listing->phone2line == 1 or $listing->phone2line == 0)
            {{ '&nbsp;' }}
            {{ $listing->phone2 }}
        @endif
    </p>
    <p class="toplines">
        @if($listing->listingaddressline == 2)
            {{ $listing->listingaddress }}
            {{ '&nbsp;' }}
        @endif
        @if($listing->email1line == 2)
            {{ $listing->email1 }}
            {{ '&nbsp;' }}
        @endif
        @if($listing->webaddress1line == 2)
            {{ $listing->webaddress1 }}
            {{ '&nbsp;' }}
        @endif
        @if($listing->phone1line == 2)
            {{ $listing->phone1 }}
            {{ '&nbsp;' }}
        @endif
        @if($listing->phone2line == 2)
            {{ $listing->phone2 }}
            {{ '&nbsp;' }}
        @endif
        @if($listing->phone3line == 2)
            {{ $listing->phone3 }}
            {{ '&nbsp;' }}
        @endif
        @if($listing->phone4line == 2)
            {{ $listing->phone4 }}
            {{ '&nbsp;' }}
        @endif
    </p>
    {{--// Line 3--}}
    <p class="toplines">
        @if($listing->listingaddressline == 3)
            {{ $listing->listingaddress }}
            {{ '&nbsp;' }}
        @else
            @if($listing->address1line == 3)
                {{ $listing->address1 }}
                {{ '&nbsp;' }}
            @endif
        @endif
        @if($listing->email1line == 3)
            {{ $listing->email1 }}
            {{ '&nbsp;' }}
        @else
            @if($listing->email2line == 3)
                {{ $listing->email2 }}
                {{ '&nbsp;' }}
            @endif
        @endif
        @if($listing->webaddress1line == 3)
            {{ $listing->webaddress1 }}
            {{ '&nbsp;' }}
        @else
            @if($listing->webaddress2line == 3)
                {{ $listing->webaddress2 }}
                {{ '&nbsp;' }}
            @endif
        @endif
        @if($listing->phone1line == 3)
            {{ $listing->phone1 }}
            {{ '&nbsp;' }}
        @endif
        @if($listing->phone2line == 3)
            {{ $listing->phone2 }}
            {{ '&nbsp;' }}
        @endif
        @if($listing->phone3line == 3)
            {{ $listing->phone3 }}
            {{ '&nbsp;' }}
        @endif
        @if($listing->phone4line == 3)
            {{ $listing->phone4 }}
            {{ '&nbsp;' }}
        @endif

    </p>

    {{--// Line 4--}}
    <p class="toplines">
        @if($listing->listingaddressline == 4)
            {{ $listing->listingaddress }}
            {{ '&nbsp;' }}
        @else
            @if($listing->address1line == 4)
                {{ $listing->address1 }}
                {{ '&nbsp;' }}
            @endif
            @if($listing->address2line == 4)
                {{ $listing->address2 }}
                {{ '&nbsp;' }}
            @endif
            @if($listing->address3line == 4)
                {{ $listing->address3 }}
                {{ '&nbsp;' }}
            @endif
        @endif
        @if($listing->email1line == 4)
            {{ $listing->email1 }}
            {{ '&nbsp;' }}
        @else
            @if($listing->email2line == 4)
                {{ $listing->email2 }}
                {{ '&nbsp;' }}
            @endif
        @endif
        @if($listing->webaddress1line == 4)
            {{ $listing->webaddress1 }}
            {{ '&nbsp;' }}
        @else
            @if($listing->webaddress2line == 4)
                {{ $listing->webaddress2 }}
                {{ '&nbsp;' }}
            @endif
        @endif
        @if($listing->phone1line == 4)
            {{ $listing->phone1 }}
            {{ '&nbsp;' }}
        @endif
        @if($listing->phone2line == 4)
            {{ $listing->phone2 }}
            {{ '&nbsp;' }}
        @endif
        @if($listing->phone3line == 4)
            {{ $listing->phone3 }}
            {{ '&nbsp;' }}
        @endif
        @if($listing->phone4line == 4)
            {{ $listing->phone4 }}
            {{ '&nbsp;' }}
        @endif
    </p>


    {{--// Line 5--}}
    <p class="toplines">
        @if($listing->listingaddressline == 5)
            {{ $listing->listingaddress }}
            {{ '&nbsp;' }}
        @else
            @if($listing->address1line == 5)
                {{ $listing->address1 }}
                {{ '&nbsp;' }}
            @endif
            @if($listing->address2line == 5)
                {{ $listing->address2 }}
                {{ '&nbsp;' }}
            @endif
            @if($listing->address3line == 5)
                {{ $listing->address3 }}
                {{ '&nbsp;' }}
            @endif
            @if($listing->address4line == 5)
                {{ $listing->address4 }}
                {{ '&nbsp;' }}
            @endif
            @if($listing->address5line == 5)
                {{ $listing->address5 }}
                {{ '&nbsp;' }}
            @endif
            @if($listing->address6line == 5)
                {{ $listing->address6 }}
                {{ '&nbsp;' }}
            @endif
        @endif
        @if($listing->email1line == 5)
            {{ $listing->email1 }}
            {{ '&nbsp;' }}
        @else
            @if($listing->email2line == 5)
                {{ $listing->email2 }}
                {{ '&nbsp;' }}
            @endif
        @endif
        @if($listing->webaddress1line == 5)
            {{ $listing->webaddress1 }}
            {{ '&nbsp;' }}
        @else
            @if($listing->webaddress2line == 5)
                {{ $listing->webaddress2 }}
                {{ '&nbsp;' }}
            @endif
        @endif
        @if($listing->phone1line == 5)
            {{ $listing->phone1 }}
            {{ '&nbsp;' }}
        @endif
        @if($listing->phone2line == 5)
            {{ $listing->phone2 }}
            {{ '&nbsp;' }}
        @endif
        @if($listing->phone3line == 5)
            {{ $listing->phone3 }}
            {{ '&nbsp;' }}
        @endif
        @if($listing->phone4line == 5)
            {{ $listing->phone4 }}
            {{ '&nbsp;' }}
        @endif
    </p>

    {{--// Line 6--}}
    <p class="toplines">
        @if($listing->listingaddressline == 6)
            {{ $listing->listingaddress }}
            {{ '&nbsp;' }}
        @else
            @if($listing->address1line == 6)
                {{ $listing->address1 }}
                {{ '&nbsp;' }}
            @endif
            @if($listing->address2line == 6)
                {{ $listing->address2 }}
                {{ '&nbsp;' }}
            @endif
            @if($listing->address3line == 6)
                {{ $listing->address3 }}
                {{ '&nbsp;' }}
            @endif
            @if($listing->address4line == 6)
                {{ $listing->address4 }}
                {{ '&nbsp;' }}
            @endif
            @if($listing->address5line == 6)
                {{ $listing->address5 }}
                {{ '&nbsp;' }}
            @endif
            @if($listing->address6line == 6)
                {{ $listing->address6 }}
                {{ '&nbsp;' }}
            @endif
        @endif
        @if($listing->email1line == 6)
            {{ $listing->email1 }}
            {{ '&nbsp;' }}
        @else
            @if($listing->email2line == 6)
                {{ $listing->email2 }}
                {{ '&nbsp;' }}
            @endif
        @endif
        @if($listing->webaddress1line == 6)
            {{ $listing->webaddress1 }}
            {{ '&nbsp;' }}
        @else
            @if($listing->webaddress2line == 6)
                {{ $listing->webaddress2 }}
                {{ '&nbsp;' }}
            @endif
        @endif
        @if($listing->phone1line == 6)
            {{ $listing->phone1 }}
            {{ '&nbsp;' }}
        @endif
        @if($listing->phone2line == 6)
            {{ $listing->phone2 }}
            {{ '&nbsp;' }}
        @endif
        @if($listing->phone3line == 6)
            {{ $listing->phone3 }}
            {{ '&nbsp;' }}
        @endif
        @if($listing->phone4line == 6)
            {{ $listing->phone4 }}
            {{ '&nbsp;' }}
        @endif
    </p>


    {{--// Line 7--}}
    <p class="toplines">
        @if($listing->listingaddressline == 7)
            {{ $listing->listingaddress }}
            {{ '&nbsp;' }}
        @else
            @if($listing->address1line == 7)
                {{ $listing->address1 }}
                {{ '&nbsp;' }}
            @endif
            @if($listing->address2line == 7)
                {{ $listing->address2 }}
                {{ '&nbsp;' }}
            @endif
            @if($listing->address3line == 7)
                {{ $listing->address3 }}
                {{ '&nbsp;' }}
            @endif
            @if($listing->address4line == 7)
                {{ $listing->address4 }}
                {{ '&nbsp;' }}
            @endif
            @if($listing->address5line == 7)
                {{ $listing->address5 }}
                {{ '&nbsp;' }}
            @endif
            @if($listing->address6line == 7)
                {{ $listing->address6 }}
                {{ '&nbsp;' }}
            @endif
        @endif
        @if($listing->email1line == 7)
            {{ $listing->email1 }}
            {{ '&nbsp;' }}
        @else
            @if($listing->email2line == 7)
                {{ $listing->email2 }}
                {{ '&nbsp;' }}
            @endif
        @endif
        @if($listing->webaddress1line == 7)
            {{ $listing->webaddress1 }}
            {{ '&nbsp;' }}
        @else
            @if($listing->webaddress2line == 7)
                {{ $listing->webaddress2 }}
                {{ '&nbsp;' }}
            @endif
        @endif
        @if($listing->phone1line == 7)
            {{ $listing->phone1 }}
            {{ '&nbsp;' }}
        @endif
        @if($listing->phone2line == 7)
            {{ $listing->phone2 }}
            {{ '&nbsp;' }}
        @endif
        @if($listing->phone3line == 7)
            {{ $listing->phone3 }}
            {{ '&nbsp;' }}
        @endif
        @if($listing->phone4line == 7)
            {{ $listing->phone4 }}
            {{ '&nbsp;' }}
        @endif
    </p>
    {{--// Line 8--}}
    <p class="toplines">
        @if($listing->listingaddressline == 8)
            {{ $listing->listingaddress }}
            {{ '&nbsp;' }}
        @else
            @if($listing->address1line == 8)
                {{ $listing->address1 }}
                {{ '&nbsp;' }}
            @endif
            @if($listing->address2line == 8)
                {{ $listing->address2 }}
                {{ '&nbsp;' }}
            @endif
            @if($listing->address3line == 8)
                {{ $listing->address3 }}
                {{ '&nbsp;' }}
            @endif
            @if($listing->address4line == 8)
                {{ $listing->address4 }}
                {{ '&nbsp;' }}
            @endif
            @if($listing->address5line == 8)
                {{ $listing->address5 }}
                {{ '&nbsp;' }}
            @endif
            @if($listing->address6line == 8)
                {{ $listing->address6 }}
                {{ '&nbsp;' }}
            @endif
        @endif
        @if($listing->email1line ==8)
            {{ $listing->email1 }}
            {{ '&nbsp;' }}
        @else
            @if($listing->email2line == 8)
                {{ $listing->email2 }}
                {{ '&nbsp;' }}
            @endif
        @endif
        @if($listing->webaddress1line == 8)
            {{ $listing->webaddress1 }}
            {{ '&nbsp;' }}
        @else
            @if($listing->webaddress2line == 8)
                {{ $listing->webaddress2 }}
                {{ '&nbsp;' }}
            @endif
        @endif
        @if($listing->phone1line == 8)
            {{ $listing->phone1 }}
            {{ '&nbsp;' }}
        @endif
        @if($listing->phone2line == 8)
            {{ $listing->phone2 }}
            {{ '&nbsp;' }}
        @endif
        @if($listing->phone3line == 8)
            {{ $listing->phone3 }}
            {{ '&nbsp;' }}
        @endif
        @if($listing->phone4line == 8)
            {{ $listing->phone4 }}
            {{ '&nbsp;' }}
        @endif
    </p>
<p>{{ $listing->listingtext }}</p>
</div>
</body>
</html>