<!DOCTYPE html>
<html>
<head>
	<title>Performance CMS</title>
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
    <script src="//code.jquery.com/jquery-1.9.1.min.js"></script>
</head>
<body>
<div class="container">

<nav class="navbar navbar-inverse">
	<div class="navbar-header">
		<a class="navbar-brand" href="{{ URL::to('admin') }}">Back to Admin</a>
	</div>
	<ul class="nav navbar-nav">
		<li><a href="{{ URL::to('admin/listings/book/' . $book->id) }}">View All Listings</a></li>
		<li><a href="{{ URL::to('admin/listings/create/' . $book->id) }}">Create a Listing</a></li>
	</ul>
</nav>
    {{--//        The book creation renumbers all of the business ids. In order for the business id numbers in the book to match the invoices, this process--}}
    {{--//        should not be repeated after the book has been produced already by Wendy, otherwise new listings or deleted listings will mess up the--}}
    {{--//        numbering.--}}
    <script>$(function(){
            $(document).on('click', 'a.confirm', function(){
                return confirm("WARNING: Creating the book re-numbers the business ID numbers. Only do this if the book needs to be reproduced, normally once a year. Are you sure you want to?");        });
        });</script>
<h1>All the Listings
    <span style="float:right">
        {{--<a title="This will take a minute!" class="btn btn-small btn-success" href="{{ URL::to('admin/renewal/' . $book->id) }}">Renewal Amt</a>--}}
        {{--<a title="This will take a minute!" class="btn btn-small btn-success" href="{{ URL::to('admin/reorder/' . $book->id) }}">Reorder</a>--}}
        <a title="This will take a minute!" class="btn btn-small btn-success confirm" href="{{ URL::to('admin/create_word_book/' . $book->id) }}">Recreate Word Book</a>
        <a title="Open book" target="_blank" href="{{ '/' . strtolower($book->Title . '.docx') }}" class="btn btn-small btn-success">Download Book</a>
    </span></h1>

<h3> {{ $listing->count() }} listings found in {{ $book->Title }}</h3>
<!-- will be used to show any messages -->
@if (Session::has('message'))
	<div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

<div class="container-fluid">       
    <div class="row-fluid">
<table class="table table-condensed table-striped table-bordered">
	<thead>
		<tr>
            <th class="col-sm-1" style="text-align:center;width:40px !important;min-width:40px;">ID</th>
			<th class="col-sm-4" style="text-align:center;width:220px !important;min-width:220px;">Company</th>
			<th class="col-sm-2" style="text-align:center;width:290px !important;min-width:290px;">Listing</th>
			<th class="col-sm-2" style="text-align:center;width:40px !important;min-width:40px;">Number of reviews</th>
            {{--<th class="col-sm-2" style="text-align:center;width:40px !important;min-width:40px;">Renewal Old </th>--}}
            {{--<th class="col-sm-2" style="text-align:center;width:40px !important;min-width:40px;">Renewal New</th>--}}

			<th class="col-sm-4" style="text-align:center;width:530px !important;min-width:530px;">Actions</th>
		</tr>
	</thead>
	<tbody>

	@foreach($listing as $key => $value)

		<tr>

			<td>{{ $value->listingbusinessid }}</td>
			<td @if ($value->listings_deleted)
					style="color:red"
				@endif
					>{{ $value->listingname }}</td>

			
			{{--<td><div style="width: 250px; word-wrap: break-word"> {{ Str::limit(strip_tags($value->listingtext), 60) }}</div></td>--}}
            <td> {{ $value->title }}</td>
			<td align="center"> {{ $value->reviewcount }}</td>
            {{--<td> {{ $value->renewal_price }}</td>--}}

            {{--<td> {{ $value->calculated_renewal_total }}</td>--}}
			<!-- we will also add show, edit, and delete buttons -->
			<td>

				<!-- delete the company (uses the destroy method DESTROY /company/{id} -->
				<!-- we will add this later since its a little more complicated than the first two buttons -->
				{{ Form::open(array('url' => 'admin/listings/' . $value->id, 'class' => 'pull-right')) }}
					{{ Form::hidden('_method', 'DELETE') }}
					{{ Form::submit('Delete', array('class' => 'btn btn-warning')) }}
				{{ Form::close() }}


                    <a style="font-size:10px;background-color:#d2322d;" class="btn btn-small btn-success" href="{{ URL::to('admin/listings/copy/' . $value->id . '/'. 1) }}">Copy to<br/>Wokingham</a>
                    <a style="font-size:10px;background-color:#d2322d;" class="btn btn-small btn-success" href="{{ URL::to('admin/listings/copy/' . $value->id . '/'. 2) }}">Copy to<br/>Surrey</a>
                    <a style="font-size:10px;background-color:#d2322d;" class="btn btn-small btn-success" href="{{ URL::to('admin/listings/copy/' . $value->id . '/'. 3) }}">Copy to<br/>Reading</a>
                    <a style="font-size:10px;background-color:#d2322d;" class="btn btn-small btn-success" href="{{ URL::to('admin/listings/copy/' . $value->id . '/'. 4) }}">Copy to<br/>Maidenhead</a>
                <!-- show the company (uses the show method found at GET /company/{id} -->
				{{--<a class="btn btn-small btn-success" href="{{ URL::to('admin/listings/' . $value->id) }}">Show</a>--}}

				<!-- show the company (uses the show method found at GET /company/{id} -->
				<a class="btn btn-small btn-success" href="{{ URL::to('admin/reviews/list/' . $value->id) }}">Reviews</a>

				<!-- edit this company (uses the edit method found at GET /company/{id}/edit -->
				<a class="btn btn-small btn-info" href="{{ URL::to('admin/listings/' . $value->id . '/edit') }}">Edit</a>

			</td>
		</tr>
	@endforeach
    {{--<tr><td colspan="4" style="text-align: right">Total value of renewals </td><td>{{ $book->prev_total; }}</td><td>{{ $book->renewal_total; }}</td></tr>--}}
	</tbody>
</table>
{{ $listing->links() }}
</div></div></div>
{
</body>
</html>