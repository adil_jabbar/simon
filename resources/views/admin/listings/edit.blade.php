<!-- app/views/listings/edit.blade.php -->

<!DOCTYPE html>
<html>
<head>
	<title>Performance CMS</title>
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ URL::asset('assets/css/adminmain.css') }}"></link>

    <script src="//code.jquery.com/jquery-1.9.1.min.js"></script>
	<script src="{{ URL::asset('ckeditor/ckeditor.js') }}"></script>
    <script src="{{ URL::asset('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/script.js?v=2') }}"></script>

<style>
    .caption {
        text-align: right;
        color: #fff;
        background-color: #bfbfbf;
        padding: 1px 2px 0 2px;
        border-bottom: 1px solid #f1f1f1;
        border-left: 0px solid #f1f1f1;
        border-top: 1px solid #f1f1f1;
        font-weight: bold;
    }
    #h1profile_span {
        color: #000;
        font-size: 1.0em;
    }
    .specialoffer {
        padding: 10px;
        padding-bottom:0px;
        border: 1px solid #aaa;
        margin: 15px 0 0 0;
        float: left;
        clear: left;
        font-weight: bold;
    }
</style>
</head>
<body>
<div class="container">

<nav class="navbar navbar-inverse">
	<div class="navbar-header">
		<a class="navbar-brand" href="{{ URL::to('admin') }}">Back to Admin</a>
	</div>
	<ul class="nav navbar-nav">
		<li><a href="{{ URL::to('admin/listings/book/' . $book->id) }}">View All Listings</a></li>
		<li><a href="{{ URL::to('admin/listings/create/' . $book->id) }}">Create a Listing</a></li>
	</ul>
</nav>

<h1>Edit {{ strip_tags($listing->listingname) }} in book {{ $book->Title }}</h1>
<div style="width:57%;float:left">
<!-- if there are creation errors, they will show here -->
    <h3 style="color:red">{{  HTML::ul($errors->all())  }}</h3>

{{ Form::model($listing, array('route' => array('admin.listings.update',
$listing->id), 'method' => 'PUT','files' => true)) }}

	<div class="form-group">
		{{ Form::label('listingname', 'Company Name') }}
		{{ Form::textarea('listingname', null, array('class' => 'form-control')) }}
	</div>
    <script type="text/javascript">
        CKEDITOR.config.height = 50;
        CKEDITOR.replace('listingname');

    </script>


    <input value="{{ $listing->listingbookid }}" name="listingbookid" id="listingbookid" type="hidden">
    <div style="clear:both"></div>
    {{ Form::submit('Edit the Listing!', array('class' => 'btn btn-primary')) }}
    <div style="clear:both;height:10px"></div>
    {{ Form::label('listings_deleted', 'Listing Live?') }}
    <div style="width:80px" class="form-group">

	{{Form::select('listings_deleted', array('0' => 'Yes', '1' => 'No'), null, array('class' => 'form-control')) }}


    </div>


    <div class="form-mainitem">
        {{ Form::label('listing_year', 'Year 1st Listed') }}
        {{ Form::text('listing_year', null, array('class' => 'form-control', 'style' => "width:80px")) }}
    </div>


    <div class="form-group" style="clear:both">
        {{ Form::label('category_id', 'Listing Category') }}<br/>
        {{ Form::select('category_id', $categories, $listing->category_id, array('class' => 'form-control', 'style' => "width:390px")) }}
    </div>

    <div class="form-group ">
            <div class="form-mainitem">
                    {{ Form::label('listingaddress', 'Address 1') }}
                    {{ Form::text('listingaddress', null, array('class' => 'form-control')) }}
            </div>
            {{--<div class="form-lineitem">--}}
                    {{--{{ Form::label('listingaddressline', 'Line') }}--}}
                    {{--{{ Form::text('listingaddressline', null, array('class' => 'form-control')) }}--}}
            {{--</div>--}}
    </div>
    <div style="clear:both"></div>
    {{ Form::submit('Edit the Listing!', array('class' => 'btn btn-primary')) }}


    <div style="clear:both"></div><br/>

    <div class="form-group">
		{{ Form::label('listingtext', 'Listing') }}
		{{ Form::textarea('listingtext', null, array('class' => 'form-control')) }}
	</div>

    <div class="form-group">
        {{ Form::label('specialoffer', 'Special Offer') }}
        {{ Form::textarea('specialoffer', null, array('class' => 'form-control')) }}
    </div>
    {{ Form::submit('Edit the Listing!', array('class' => 'btn btn-primary')) }}
    <br/><br/>





    <div class="form-group ">
        <div class="form-mainitem">
                {{ Form::label('email1', 'Email 1') }}
                {{ Form::text('email1', null, array('class' => 'form-control')) }}
        </div>
        {{--<div class="form-lineitem">--}}
                {{--{{ Form::label('email1line', 'Line') }}--}}
                {{--{{ Form::text('email1line', null, array('class' => 'form-control')) }}--}}
        {{--</div>--}}
    </div>


    <div class="form-group ">
            <div class="form-mainitem">
                    {{ Form::label('webaddress1', 'Web address 1') }}
                    {{ Form::text('webaddress1', null, array('class' => 'form-control')) }}
            </div>
            {{--<div class="form-lineitem">--}}
                    {{--{{ Form::label('webaddress1line', 'Line') }}--}}
                    {{--{{ Form::text('webaddress1line', null, array('class' => 'form-control')) }}--}}
            {{--</div>--}}
    </div>


        <div class="form-group ">
                <div class="form-mainitem">
                        {{ Form::label('phone1', 'Phone 1') }}
                        {{ Form::text('phone1', null, array('class' => 'form-control')) }}
                </div>
                {{--<div class="form-lineitem">--}}
                        {{--{{ Form::label('phone1line', 'Line') }}--}}
                        {{--{{ Form::text('phone1line', null, array('class' => 'form-control')) }}--}}
                {{--</div>--}}
                <div class="form-type">
                        {{ Form::label('phone1type', 'Type') }}
                        {{Form::select('phone1type', array('L' => 'Landline', 'M' => 'Mobile', 'F' => 'Fax' ,
                        'U' => 'Unknown', 'X' => 'Phone/Fax', 'O' => 'Office', 'R' => 'Local rate', 'Z' => 'Freephone'),
                         null, array('class' => 'form-control')) }}
                </div>
        </div>

        <div class="form-group ">
                <div class="form-mainitem">
                        {{ Form::label('phone2', 'Phone 2') }}
                        {{ Form::text('phone2', null, array('class' => 'form-control')) }}
                </div>
                {{--<div class="form-lineitem">--}}
                        {{--{{ Form::label('phone2line', 'Line') }}--}}
                        {{--{{ Form::text('phone2line', null, array('class' => 'form-control')) }}--}}
                {{--</div>--}}
                 <div class="form-type">
                    {{ Form::label('phone2type', 'Type') }}
                    {{Form::select('phone2type', array('L' => 'Landline', 'M' => 'Mobile', 'F' => 'Fax',
                    'U' => 'Unknown', 'X' => 'Phone/Fax', 'O' => 'Office', 'R' => 'Local rate', 'Z' => 'Freephone'),
                     null, array('class' => 'form-control')) }}
                </div>
        </div>
		<div style="clear:both"></div>
    <br/>
			{{ Form::submit('Edit the Listing!', array('class' => 'btn btn-primary')) }}
    <br/>        <br/>

    <div class="form-group ">
        <div class="form-mainitem">
            {{ Form::label('address1', 'Address 2') }}
            {{ Form::text('address1', null, array('class' => 'form-control')) }}
        </div>
        {{--<div class="form-lineitem">--}}
            {{--{{ Form::label('address1line', 'Line') }}--}}
            {{--{{ Form::text('address1line', null, array('class' => 'form-control')) }}--}}
        {{--</div>--}}
    </div>

        <div class="form-group ">
                <div class="form-mainitem">
                        {{ Form::label('phone3', 'Phone 3') }}
                        {{ Form::text('phone3', null, array('class' => 'form-control')) }}
                </div>
                {{--<div class="form-lineitem">--}}
                        {{--{{ Form::label('phone3line', 'Line') }}--}}
                        {{--{{ Form::text('phone3line', null, array('class' => 'form-control')) }}--}}
                {{--</div>--}}
                 <div class="form-type">
                    {{ Form::label('phone3type', 'Type') }}
                    {{Form::select('phone3type', array('L' => 'Landline', 'M' => 'Mobile', 'F' => 'Fax',
                    'U' => 'Unknown', 'X' => 'Phone/Fax', 'O' => 'Office',  'R' => 'Local rate', 'Z' => 'Freephone'),
                     null, array('class' => 'form-control')) }}
                </div>
        </div>
        <div class="form-group ">
                <div class="form-mainitem">
                        {{ Form::label('phone4', 'Phone 4') }}
                        {{ Form::text('phone4', null, array('class' => 'form-control')) }}
                </div>
                {{--<div class="form-lineitem">--}}
                        {{--{{ Form::label('phone4line', 'Line') }}--}}
                        {{--{{ Form::text('phone4line', null, array('class' => 'form-control')) }}--}}
                {{--</div>--}}
                 <div class="form-type">
                     {{ Form::label('phone4type', 'Type') }}
                     {{Form::select('phone4type', array('L' => 'Landline', 'M' => 'Mobile', 'F' => 'Fax', 'U' => 'Unknown',
                     'X' => 'Phone/Fax', 'O' => 'Office',  'R' => 'Local rate', 'Z' => 'Freephone'),
                      null, array('class' => 'form-control')) }}
                 </div>
        </div>

    <div class="form-group ">
        <div class="form-mainitem">
            {{ Form::label('email2', 'Email 2') }}
            {{ Form::text('email2', null, array('class' => 'form-control')) }}
        </div>
        {{--<div class="form-lineitem">--}}
            {{--{{ Form::label('emai21line', 'Line') }}--}}
            {{--{{ Form::text('email2line', null, array('class' => 'form-control')) }}--}}
        {{--</div>--}}
    </div>


    <div class="form-group ">
        <div class="form-mainitem">
            {{ Form::label('webaddress2', 'Web address 2') }}
            {{ Form::text('webaddress2', null, array('class' => 'form-control')) }}
        </div>
        {{--<div class="form-lineitem">--}}
            {{--{{ Form::label('webaddress2line', 'Line') }}--}}
            {{--{{ Form::text('webaddress2line', null, array('class' => 'form-control')) }}--}}
        {{--</div>--}}
    </div>


     <div class="form-group ">
            <div class="form-mainitem">
                    {{ Form::label('address2', 'Address 3') }}
                    {{ Form::text('address2', null, array('class' => 'form-control')) }}
            </div>
            {{--<div class="form-lineitem">--}}
                    {{--{{ Form::label('address2line', 'Line') }}--}}
                    {{--{{ Form::text('address2line', null, array('class' => 'form-control')) }}--}}
            {{--</div>--}}
    </div>
     <div class="form-group ">
                <div class="form-mainitem">
                        {{ Form::label('address3', 'Address 4') }}
                        {{ Form::text('address3', null, array('class' => 'form-control')) }}
                </div>
                {{--<div class="form-lineitem">--}}
                        {{--{{ Form::label('address3line', 'Line') }}--}}
                        {{--{{ Form::text('address3line', null, array('class' => 'form-control')) }}--}}
                {{--</div>--}}
        </div>
         <div class="form-group ">
                    <div class="form-mainitem">
                            {{ Form::label('address4', 'Address 5') }}
                            {{ Form::text('address4', null, array('class' => 'form-control')) }}
                    </div>
                    {{--<div class="form-lineitem">--}}
                            {{--{{ Form::label('address4line', 'Line') }}--}}
                            {{--{{ Form::text('address4line', null, array('class' => 'form-control')) }}--}}
                    {{--</div>--}}
            </div>
            <div class="form-group ">
                    <div class="form-mainitem">
                            {{ Form::label('address5', 'Address 6') }}
                            {{ Form::text('address5', null, array('class' => 'form-control')) }}
                    </div>
                    {{--<div class="form-lineitem">--}}
                            {{--{{ Form::label('address5line', 'Line') }}--}}
                            {{--{{ Form::text('address5line', null, array('class' => 'form-control')) }}--}}
                    {{--</div>--}}
            </div>
	<script type="text/javascript">
        CKEDITOR.replace('listingtext');
        CKEDITOR.config.height = 200;
        CKEDITOR.replace('specialoffer');

    </script>

	{{--@foreach($listingaddress as $key => $value)--}}
	{{--<div class="form-group">--}}
		{{--{{ Form::label('Phone["'.$key.'"]', 'Listing Phone') }}--}}
		{{--<input type="text" id="phone" name="phone['{{$key}}']" size="20"--}}
		{{--{{ (Input::old('phone["' .$key . '"]') ? ' value="' . e(Input::old('phone["' .$key . '"]')) . '"' : '$value') }}>--}}

	{{--</div>--}}

	{{--<div class="form-group">--}}
            {{--{{ Form::label("address[".$key."]", 'Other Address['.$key.']') }}--}}

           {{--{{ (Input::old('address["' .$key . '"]') ? ' value="' . e(Input::old('address["' .$key . '"]')) . '"' : '$value') }}--}}
    {{--</div>--}}
	{{--@endforeach--}}
	<div style="clear:both"></div>
	{{ Form::submit('Edit the Listing!', array('class' => 'btn btn-primary')) }}


</div>
<style>.toplines {font-weight: bold;margin-bottom:0px;"}
h3 {font-style: italic;border-bottom: 2px solid #000000;margin-bottom:10px}
</style>
<div style="width:40%;float:left;margin-left:3%">
<h2>{{ $book->Title }}</h2>
@if(isset($category))
<h3 style="font-size:19px">{{ $category->title }}</h3>
@else
<h3 style="font-size:19px">NO CATEGORY FOUND!</h3>
@endif

{{--<p class="toplines">{{ $listing->listingbusinessid }} {{ '&nbsp;' }} {{ strip_tags($listing->listingname, '<strong><em>') }}--}}
{{--@if($listing->listingaddressline == 0)--}}
    {{--<span style="font-weight: normal;font-size:0.9em"> {{ $listing->listingaddress }}</span>--}}
{{--@endif--}}
{{--@if($listing->phone1line == 1 or $listing->phone1line == 0)--}}
{{--{{ '&nbsp;' }}--}}
{{--{{ $listing->phone1 }}--}}
{{--@endif--}}
{{--@if($listing->phone2line == 1 or $listing->phone2line == 0)--}}
    {{--{{ '&nbsp;' }}--}}
    {{--{{ $listing->phone2 }}--}}
{{--@endif--}}
{{--</p>--}}
{{--<p class="toplines">--}}
{{--@if($listing->listingaddressline == 2)--}}
{{--{{ $listing->listingaddress }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@endif--}}
{{--@if($listing->email1line == 2)--}}
{{--{{ $listing->email1 }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@endif--}}
{{--@if($listing->webaddress1line == 2)--}}
{{--{{ $listing->webaddress1 }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@endif--}}
{{--@if($listing->phone1line == 2)--}}
{{--{{ $listing->phone1 }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@endif--}}
{{--@if($listing->phone2line == 2)--}}
{{--{{ $listing->phone2 }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@endif--}}
{{--@if($listing->phone3line == 2)--}}
{{--{{ $listing->phone3 }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@endif--}}
{{--@if($listing->phone4line == 2)--}}
{{--{{ $listing->phone4 }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@endif--}}
{{--</p>--}}
{{--// Line 3--}}
{{--<p class="toplines">--}}
{{--@if($listing->listingaddressline == 3)--}}
{{--{{ $listing->listingaddress }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@else--}}
    {{--@if($listing->address1line == 3)--}}
    {{--{{ $listing->address1 }}--}}
    {{--{{ '&nbsp;' }}--}}
    {{--@endif--}}
{{--@endif--}}
{{--@if($listing->email1line == 3)--}}
{{--{{ $listing->email1 }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@else--}}
    {{--@if($listing->email2line == 3)--}}
    {{--{{ $listing->email2 }}--}}
    {{--{{ '&nbsp;' }}--}}
    {{--@endif--}}
{{--@endif--}}
{{--@if($listing->webaddress1line == 3)--}}
{{--{{ $listing->webaddress1 }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@else--}}
    {{--@if($listing->webaddress2line == 3)--}}
    {{--{{ $listing->webaddress2 }}--}}
    {{--{{ '&nbsp;' }}--}}
    {{--@endif--}}
{{--@endif--}}
{{--@if($listing->phone1line == 3)--}}
{{--{{ $listing->phone1 }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@endif--}}
{{--@if($listing->phone2line == 3)--}}
{{--{{ $listing->phone2 }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@endif--}}
{{--@if($listing->phone3line == 3)--}}
{{--{{ $listing->phone3 }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@endif--}}
{{--@if($listing->phone4line == 3)--}}
{{--{{ $listing->phone4 }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@endif--}}

{{--</p>--}}

{{--// Line 4--}}
{{--<p class="toplines">--}}
{{--@if($listing->listingaddressline == 4)--}}
{{--{{ $listing->listingaddress }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@else--}}
    {{--@if($listing->address1line == 4)--}}
    {{--{{ $listing->address1 }}--}}
    {{--{{ '&nbsp;' }}--}}
    {{--@endif--}}
    {{--@if($listing->address2line == 4)--}}
        {{--{{ $listing->address2 }}--}}
        {{--{{ '&nbsp;' }}--}}
    {{--@endif--}}
    {{--@if($listing->address3line == 4)--}}
            {{--{{ $listing->address3 }}--}}
            {{--{{ '&nbsp;' }}--}}
    {{--@endif--}}
{{--@endif--}}
{{--@if($listing->email1line == 4)--}}
{{--{{ $listing->email1 }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@else--}}
    {{--@if($listing->email2line == 4)--}}
    {{--{{ $listing->email2 }}--}}
    {{--{{ '&nbsp;' }}--}}
    {{--@endif--}}
{{--@endif--}}
{{--@if($listing->webaddress1line == 4)--}}
{{--{{ $listing->webaddress1 }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@else--}}
    {{--@if($listing->webaddress2line == 4)--}}
    {{--{{ $listing->webaddress2 }}--}}
    {{--{{ '&nbsp;' }}--}}
    {{--@endif--}}
{{--@endif--}}
{{--@if($listing->phone1line == 4)--}}
{{--{{ $listing->phone1 }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@endif--}}
{{--@if($listing->phone2line == 4)--}}
{{--{{ $listing->phone2 }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@endif--}}
{{--@if($listing->phone3line == 4)--}}
{{--{{ $listing->phone3 }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@endif--}}
{{--@if($listing->phone4line == 4)--}}
{{--{{ $listing->phone4 }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@endif--}}
{{--</p>--}}


{{--// Line 5--}}
{{--<p class="toplines">--}}
{{--@if($listing->listingaddressline == 5)--}}
{{--{{ $listing->listingaddress }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@else--}}
    {{--@if($listing->address1line == 5)--}}
    {{--{{ $listing->address1 }}--}}
    {{--{{ '&nbsp;' }}--}}
    {{--@endif--}}
    {{--@if($listing->address2line == 5)--}}
        {{--{{ $listing->address2 }}--}}
        {{--{{ '&nbsp;' }}--}}
    {{--@endif--}}
    {{--@if($listing->address3line == 5)--}}
        {{--{{ $listing->address3 }}--}}
        {{--{{ '&nbsp;' }}--}}
    {{--@endif--}}
    {{--@if($listing->address4line == 5)--}}
        {{--{{ $listing->address4 }}--}}
        {{--{{ '&nbsp;' }}--}}
    {{--@endif--}}
    {{--@if($listing->address5line == 5)--}}
        {{--{{ $listing->address5 }}--}}
        {{--{{ '&nbsp;' }}--}}
    {{--@endif--}}
    {{--@if($listing->address6line == 5)--}}
        {{--{{ $listing->address6 }}--}}
        {{--{{ '&nbsp;' }}--}}
    {{--@endif--}}
{{--@endif--}}
{{--@if($listing->email1line == 5)--}}
{{--{{ $listing->email1 }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@else--}}
    {{--@if($listing->email2line == 5)--}}
    {{--{{ $listing->email2 }}--}}
    {{--{{ '&nbsp;' }}--}}
    {{--@endif--}}
{{--@endif--}}
{{--@if($listing->webaddress1line == 5)--}}
{{--{{ $listing->webaddress1 }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@else--}}
    {{--@if($listing->webaddress2line == 5)--}}
    {{--{{ $listing->webaddress2 }}--}}
    {{--{{ '&nbsp;' }}--}}
    {{--@endif--}}
{{--@endif--}}
{{--@if($listing->phone1line == 5)--}}
{{--{{ $listing->phone1 }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@endif--}}
{{--@if($listing->phone2line == 5)--}}
{{--{{ $listing->phone2 }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@endif--}}
{{--@if($listing->phone3line == 5)--}}
{{--{{ $listing->phone3 }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@endif--}}
{{--@if($listing->phone4line == 5)--}}
{{--{{ $listing->phone4 }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@endif--}}
{{--</p>--}}

{{--// Line 6--}}
{{--<p class="toplines">--}}
{{--@if($listing->listingaddressline == 6)--}}
{{--{{ $listing->listingaddress }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@else--}}
    {{--@if($listing->address1line == 6)--}}
    {{--{{ $listing->address1 }}--}}
    {{--{{ '&nbsp;' }}--}}
    {{--@endif--}}
    {{--@if($listing->address2line == 6)--}}
        {{--{{ $listing->address2 }}--}}
        {{--{{ '&nbsp;' }}--}}
    {{--@endif--}}
    {{--@if($listing->address3line == 6)--}}
        {{--{{ $listing->address3 }}--}}
        {{--{{ '&nbsp;' }}--}}
    {{--@endif--}}
    {{--@if($listing->address4line == 6)--}}
        {{--{{ $listing->address4 }}--}}
        {{--{{ '&nbsp;' }}--}}
    {{--@endif--}}
    {{--@if($listing->address5line == 6)--}}
        {{--{{ $listing->address5 }}--}}
        {{--{{ '&nbsp;' }}--}}
    {{--@endif--}}
    {{--@if($listing->address6line == 6)--}}
        {{--{{ $listing->address6 }}--}}
        {{--{{ '&nbsp;' }}--}}
    {{--@endif--}}
{{--@endif--}}
{{--@if($listing->email1line == 6)--}}
{{--{{ $listing->email1 }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@else--}}
    {{--@if($listing->email2line == 6)--}}
    {{--{{ $listing->email2 }}--}}
    {{--{{ '&nbsp;' }}--}}
    {{--@endif--}}
{{--@endif--}}
{{--@if($listing->webaddress1line == 6)--}}
{{--{{ $listing->webaddress1 }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@else--}}
    {{--@if($listing->webaddress2line == 6)--}}
    {{--{{ $listing->webaddress2 }}--}}
    {{--{{ '&nbsp;' }}--}}
    {{--@endif--}}
{{--@endif--}}
{{--@if($listing->phone1line == 6)--}}
{{--{{ $listing->phone1 }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@endif--}}
{{--@if($listing->phone2line == 6)--}}
{{--{{ $listing->phone2 }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@endif--}}
{{--@if($listing->phone3line == 6)--}}
{{--{{ $listing->phone3 }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@endif--}}
{{--@if($listing->phone4line == 6)--}}
{{--{{ $listing->phone4 }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@endif--}}
{{--</p>--}}


{{--// Line 7--}}
{{--<p class="toplines">--}}
{{--@if($listing->listingaddressline == 7)--}}
{{--{{ $listing->listingaddress }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@else--}}
    {{--@if($listing->address1line == 7)--}}
    {{--{{ $listing->address1 }}--}}
    {{--{{ '&nbsp;' }}--}}
    {{--@endif--}}
    {{--@if($listing->address2line == 7)--}}
        {{--{{ $listing->address2 }}--}}
        {{--{{ '&nbsp;' }}--}}
    {{--@endif--}}
    {{--@if($listing->address3line == 7)--}}
        {{--{{ $listing->address3 }}--}}
        {{--{{ '&nbsp;' }}--}}
    {{--@endif--}}
    {{--@if($listing->address4line == 7)--}}
        {{--{{ $listing->address4 }}--}}
        {{--{{ '&nbsp;' }}--}}
    {{--@endif--}}
    {{--@if($listing->address5line == 7)--}}
        {{--{{ $listing->address5 }}--}}
        {{--{{ '&nbsp;' }}--}}
    {{--@endif--}}
    {{--@if($listing->address6line == 7)--}}
        {{--{{ $listing->address6 }}--}}
        {{--{{ '&nbsp;' }}--}}
    {{--@endif--}}
{{--@endif--}}
{{--@if($listing->email1line == 7)--}}
{{--{{ $listing->email1 }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@else--}}
    {{--@if($listing->email2line == 7)--}}
    {{--{{ $listing->email2 }}--}}
    {{--{{ '&nbsp;' }}--}}
    {{--@endif--}}
{{--@endif--}}
{{--@if($listing->webaddress1line == 7)--}}
{{--{{ $listing->webaddress1 }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@else--}}
    {{--@if($listing->webaddress2line == 7)--}}
    {{--{{ $listing->webaddress2 }}--}}
    {{--{{ '&nbsp;' }}--}}
    {{--@endif--}}
{{--@endif--}}
{{--@if($listing->phone1line == 7)--}}
{{--{{ $listing->phone1 }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@endif--}}
{{--@if($listing->phone2line == 7)--}}
{{--{{ $listing->phone2 }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@endif--}}
{{--@if($listing->phone3line == 7)--}}
{{--{{ $listing->phone3 }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@endif--}}
{{--@if($listing->phone4line == 7)--}}
{{--{{ $listing->phone4 }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@endif--}}
{{--</p>--}}
{{--// Line 8--}}
{{--<p class="toplines">--}}
{{--@if($listing->listingaddressline == 8)--}}
{{--{{ $listing->listingaddress }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@else--}}
    {{--@if($listing->address1line == 8)--}}
    {{--{{ $listing->address1 }}--}}
    {{--{{ '&nbsp;' }}--}}
    {{--@endif--}}
    {{--@if($listing->address2line == 8)--}}
        {{--{{ $listing->address2 }}--}}
        {{--{{ '&nbsp;' }}--}}
    {{--@endif--}}
    {{--@if($listing->address3line == 8)--}}
        {{--{{ $listing->address3 }}--}}
        {{--{{ '&nbsp;' }}--}}
    {{--@endif--}}
    {{--@if($listing->address4line == 8)--}}
        {{--{{ $listing->address4 }}--}}
        {{--{{ '&nbsp;' }}--}}
    {{--@endif--}}
    {{--@if($listing->address5line == 8)--}}
        {{--{{ $listing->address5 }}--}}
        {{--{{ '&nbsp;' }}--}}
    {{--@endif--}}
    {{--@if($listing->address6line == 8)--}}
        {{--{{ $listing->address6 }}--}}
        {{--{{ '&nbsp;' }}--}}
    {{--@endif--}}
{{--@endif--}}
{{--@if($listing->email1line ==8)--}}
{{--{{ $listing->email1 }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@else--}}
    {{--@if($listing->email2line == 8)--}}
    {{--{{ $listing->email2 }}--}}
    {{--{{ '&nbsp;' }}--}}
    {{--@endif--}}
{{--@endif--}}
{{--@if($listing->webaddress1line == 8)--}}
{{--{{ $listing->webaddress1 }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@else--}}
    {{--@if($listing->webaddress2line == 8)--}}
    {{--{{ $listing->webaddress2 }}--}}
    {{--{{ '&nbsp;' }}--}}
    {{--@endif--}}
{{--@endif--}}
{{--@if($listing->phone1line == 8)--}}
{{--{{ $listing->phone1 }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@endif--}}
{{--@if($listing->phone2line == 8)--}}
{{--{{ $listing->phone2 }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@endif--}}
{{--@if($listing->phone3line == 8)--}}
{{--{{ $listing->phone3 }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@endif--}}
{{--@if($listing->phone4line == 8)--}}
{{--{{ $listing->phone4 }}--}}
{{--{{ '&nbsp;' }}--}}
{{--@endif--}}
{{--</p>--}}
{{--<p>{{ $listing->listingtext }}</p>--}}
    {{--<p>{{ $listing->specialoffer }}</p>--}}

    <article class="listing-summary" style="font-size:.9em">


        <span class="listingname" style="font-weight: bold;color: #000;font-size: 1.3em;"> {{ strip_tags($listing->listingname) }}

        </span>

        @if(strlen(trim($listing->listingaddress)) > 0 )
            <div class="adr">
                @if(strlen($listing->listingaddress) > 0)
                    {{ $listing->listingaddress }}
                @endif

            </div>
        @endif
        @if(strlen(trim($listing->address1)) > 0 )
            <div class="adr">
                {{ $listing->address1 }}
            </div>
        @endif

        <div class="url">
            @if(strlen($listing->webaddress1) > 0)
                <a rel=nofollow title="Visit {{strip_tags($listing->listingname)}} website" href="http://{{ $listing->webaddress1 }}">{{ $listing->webaddress1 }}</a>
            @endif
            @if(strlen($listing->webaddress2) > 0)
                <span style="margin-left:10px"><a rel=nofollow title="Visit {{strip_tags($listing->listingname)}} website" href="http://{{ $listing->webaddress2}}">{{ $listing->webaddress2 }}</a></span>
            @endif
        </div>

        <div class="listingtext"> {{ $listing->listingtext }}</div>


        <div class="fieldRow">
            @if(strlen($listing->phone1) > 0)

                @if ($listing->phone1type == 'F')
                    <span class="caption">Fax:</span>
                @elseif ($listing->phone1type == 'X')
                    <span class="caption">Phone/Fax:</span>
                @elseif ($listing->phone1type == 'O')
                    <span class="caption">Office:</span>
                @elseif ($listing->phone1type == 'Z')
                    <span class="caption">Freephone:</span>
                @elseif ($listing->phone1type == 'M')
                    <span class="caption">Mobile:</span>
                @else
                    <span class="caption">Tel:</span>
                @endif

                <span class="tel">{{ $listing->phone1 }}</span>

            @endif
            @if(strlen($listing->phone2) > 0)

                @if ($listing->phone2type == 'F')
                    <span class="caption">Fax:</span>
                @elseif ($listing->phone2type == 'X')
                    <span class="caption">Phone/Fax:</span>
                @elseif ($listing->phone2type == 'O')
                    <span class="caption">Office:</span>
                @elseif ($listing->phone2type == 'Z')
                    <span class="caption">Freephone:</span>
                @elseif ($listing->phone2type == 'M')
                    <span class="caption">Mobile:</span>
                @else
                    <span class="caption">Tel:</span>
                @endif

                <span class="tel">{{ $listing->phone2 }}</span>
            @endif
            @if(strlen($listing->phone3) > 0)

                @if ($listing->phone3type == 'F')
                    <span class="caption">Fax:</span>
                @elseif ($listing->phone3type == 'X')
                    <span class="caption">Phone/Fax:</span>
                @elseif ($listing->phone3type == 'O')
                    <span class="caption">Office:</span>
                @elseif ($listing->phone3type == 'Z')
                    <span class="caption">Freephone:</span>
                @elseif ($listing->phone3type == 'M')
                    <span class="caption">Mobile:</span>
                @else
                    <span class="caption">Tel:</span>
                @endif

                <span class="tel">{{ $listing->phone3 }}</span>

            @endif
            @if(strlen($listing->phone4) > 0)

                @if ($listing->phone4type == 'F')
                    <span class="caption">Fax:</span>
                @elseif ($listing->phone4type == 'X')
                    <span class="caption">Phone/Fax:</span>
                @elseif ($listing->phone4type == 'O')
                    <span class="caption">Office:</span>
                @elseif ($listing->phone4type == 'Z')
                    <span class="caption">Freephone:</span>
                @elseif ($listing->phone4type == 'M')
                    <span class="caption">Mobile:</span>
                @else
                    <span class="caption">Tel:</span>
                @endif

                <span class="tel">{{ $listing->phone4 }}</span>
            @endif

            @if(strlen($listing->email1) > 0)
                <span class="caption">Email:</span> <a href="mailto:{{ $listing->email1 }}"><span class="email">{{ $listing->email1 }}</span></a>
            @endif
            @if(strlen($listing->email2) > 0)
                <span class="caption">Email:</span> <a href="mailto:{{ $listing->email2 }}"><span class="email">{{ $listing->email2 }}</span></a>
            @endif

        </div>
        @if (strlen($listing->specialoffer) > 0)
            <div class="specialoffer">
                {{ $listing->specialoffer }}
            </div>
        @endif

    </article>
<div style="clear:both"></div>

    <p>-------------------------------------------------------------------------------------------</p>
<h2>Business Renewal Details</h2>
<div class="form-mainitem">
   {{ Form::label('contact_name', 'Contact Name') }}
   {{ Form::text('contact_name', null, array('class' => 'form-control')) }}
</div>

<div class="form-mainitem">
    {{ Form::label('contact_salutation', 'Contact Salutation') }}
    {{ Form::text('contact_salutation', null, array('class' => 'form-control', 'style' => "width:60%")) }}
</div>

<div class="form-mainitem">
    {{ Form::label('address_line1', 'Address Line 1') }}
    {{ Form::text('address_line1', null, array('class' => 'form-control')) }}
</div>

<div class="form-mainitem">
    {{ Form::label('address_line2', 'Address Line 2') }}
    {{ Form::text('address_line2', null, array('class' => 'form-control')) }}
</div>

<div class="form-mainitem">
    {{ Form::label('address_line3', 'Address Line 3') }}
    {{ Form::text('address_line3', null, array('class' => 'form-control')) }}
</div>

<div class="form-mainitem">
    {{ Form::label('address_line4', 'Address Line 4') }}
    {{ Form::text('address_line4', null, array('class' => 'form-control')) }}
</div>

    <div class="form-mainitem">
    {{ Form::label('renewal_price', 'Renewal Price') }}
    {{ Form::text('renewal_price', null, array('class' => 'form-control', 'style' => "width:30%")) }}
</div>

<div class="form-mainitem">
    {{ Form::label('renewal_vat', 'Renewal VAT') }}
    {{ Form::text('renewal_vat', null, array('class' => 'form-control', 'style' => "width:30%")) }}
</div>

<div class="form-mainitem">
    {{ Form::label('renewal_total', 'Renewal Total') }}
    {{ Form::text('renewal_total', null, array('class' => 'form-control', 'style' => "width:30%")) }}
</div>

<div class="form-mainitem">
    {{ Form::submit('Edit the Listing!', array('class' => 'btn btn-primary')) }}
</div>




    <div style="clear:both;"></div>

    <h3>Main image</h3>
    <div class="fileupload fileupload-new" data-provides="fileupload">
        <div class="fileupload-preview thumbnail" style="width: 195px;">
            @if (strlen($listing->image1) > 0)
                <a href=""><img style="max-width: 175px;" src="{{ '/uploads/'  .$listing->image1; }}" alt=""></a>
            @else
                <img src="http://www.placehold.it/250x200/EFEFEF/AAAAAA&amp;text=no+image">
            @endif
        </div>
        <div>
            <span class="btn btn-file">
                <span class="fileupload-new">Select image</span>
                <span class="fileupload-exists">Change</span>
                <input type="file" name="image" id="image" />
            </span>
            <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
        </div>
    </div>

    <div class="form-mainitem">
        {{ Form::label('Alt Text', 'Alt Text') }}
        {{ Form::text('image1alt', null, array('class' => 'form-control', 'style' => "width:100%")) }}
    </div>

    <div style="clear:both;"></div>

    <h3>Small image 1</h3>
    <div class="fileupload fileupload-new" data-provides="fileupload">
        <div class="fileupload-preview thumbnail" style="width: 120px;">
            @if (strlen($listing->image2) > 0)
                <a href="{{ $listing->image2;}}"><img style="max-width: 120px;" src="{{ '/uploads/' .$listing->image2; }}" alt=""></a>
            @else
                <img src="http://www.placehold.it/100x100/EFEFEF/AAAAAA&amp;text=no+image">
            @endif
        </div>
        <div>
            <span class="btn btn-file">
                <span class="fileupload-new">Select image</span>
                <span class="fileupload-exists">Change</span>
                <input type="file" name="image2" id="image2" />
            </span>
            <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
        </div>
    </div>

    <div class="form-mainitem">
        {{ Form::label('Alt Text', 'Alt Text') }}
        {{ Form::text('image2alt', null, array('class' => 'form-control', 'style' => "width:100%")) }}
    </div>

    <div style="clear:both;"></div>

    <h3>Small image 2</h3>
    <div class="fileupload fileupload-new" data-provides="fileupload">
        <div class="fileupload-preview thumbnail" style="width: 120px;">
            @if (strlen($listing->image3) > 0)
                <a href="{{ $listing->image3; }}"><img style="max-width: 120px;" src="{{ '/uploads/' .$listing->image3; }}" alt=""></a>
            @else
                <img src="http://www.placehold.it/100x100/EFEFEF/AAAAAA&amp;text=no+image">
            @endif
        </div>
        <div>
            <span class="btn btn-file">
                <span class="fileupload-new">Select image</span>
                <span class="fileupload-exists">Change</span>
                <input type="file" name="image3" id="image3" />
            </span>
            <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
        </div>
    </div>

    <div class="form-mainitem">
        {{ Form::label('Alt Text', 'Alt Text') }}
        {{ Form::text('image3alt', null, array('class' => 'form-control', 'style' => "width:100%")) }}
    </div>

    <div style="clear:both;"></div>

    <h3>Small image 3</h3>
    <div class="fileupload fileupload-new" data-provides="fileupload">
        <div class="fileupload-preview thumbnail" style="width: 120px;">
            @if (strlen($listing->image4) > 0)
                <a href="{{ $listing->image4; }}"><img style="max-width: 120px;" src="{{ '/uploads/' .$listing->image4; }}" alt=""></a>
            @else
                <img src="http://www.placehold.it/100x100/EFEFEF/AAAAAA&amp;text=no+image">
            @endif
        </div>
        <div>
            <span class="btn btn-file">
                <span class="fileupload-new">Select image</span>
                <span class="fileupload-exists">Change</span>
                <input type="file" name="image4" id="image4" />
            </span>
            <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
        </div>
    </div>

    <div class="form-mainitem">
        {{ Form::label('Alt Text', 'Alt Text') }}
        {{ Form::text('image4alt', null, array('class' => 'form-control', 'style' => "width:100%")) }}
    </div>


    <div class="form-mainitem">
        {{ Form::label('Awards, certificates, etc', 'Awards, certificates, etc') }}
        {{ Form::textarea('awards', null, array('class' => 'form-control', 'style' => "width:150%")) }}
    </div>
    <div style="clear:both;"></div>
    {{ Form::submit('Edit the Listing!', array('class' => 'btn btn-primary')) }}
    {{ Form::close() }}



</div>


</div>
</body>
</html>