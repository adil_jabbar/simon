@extends('app')

@section('title', 'Sub Products')

@section('content')

    @include('admin._partials.leftWrapper')


    <div id="rightWrapper">
        {{--<div id="header"><a id="fullPage" href="#">|||</a></div>--}}

        <h1>
            Images


        </h1>

        {!! Form::open(array('action' => 'admin\ImagesController@selectcat')) !!}

        <table>
            <tr>
                <td width="260"><strong>Show Category</strong></td>

            </tr>
            <tr>
                <td><select id="form1_category" name="category" class="autosubmit">
                        <option value="">Please Select</option>
                        <option value="1">Sheppherd Hook Earrings</option>
                        <option value="2">Stud Earrings</option>
                        <option value="3">Braclets</option>
                        <option value="4">Necklaces</option>
                        <option value="5">Pendants</option>
                        <option value="6">Rings</option>
                        {{--<option value="7">New Arrivals</option>--}}
                    </select></td>
            </tr>
        </table>


        <input type="submit" value="Submit">
        {!! Form::close() !!}


        <hr>

        {!! Notification::showAll() !!}

        <table class="table table-striped">
            <thead>
            <tr>
                <th>Code</th>
                <th></th>

                <th>Size</th>

                <th><i class="icon-cog"></i></th>
            </tr>
            </thead>
            <tbody>
            @if ($products)
                @foreach ($products as $product)
                    @foreach ($product->subproduct as $subproduct)
                        <?php if (file_exists(public_path('/images/products/' . $subproduct->image)) && $subproduct->image != 'currently-awaiting-image.jpg') {
                        //                        $size = getimagesize(public_path('/images/products/' . $subproduct->image));
                        //                        if ($size[3] != 'width="1500" height="1500"') {
                        ?>
                        <tr>

                            <td style="padding-top:25px;">{{ $subproduct->sku }}</td>
                            <td><img width="50" height="50" src="{{ '/images/products/thumbnails/' . $subproduct->image }}"></td>

                            <td style="padding-top:25px;"><?php if (file_exists(public_path('/images/products/' . $subproduct->image))) {
                                    $size = getimagesize(public_path('/images/products/' . $subproduct->image));

                                    echo $size[3];

                                    if ($size[0] < 1500 || $size[1] < 1500) {
                                        echo ' (Recommend getting a larger image)';
                                    }

                                } ?>
                            </td>


                            <td style="padding-top:25px;">
                                <a href="{{ URL::route('admin.images.edit', $subproduct->id) }}"
                                   class="btn btn-success btn-mini pull-left">Edit</a>


                            </td>
                        </tr>
                        <?php
                        //                    }

                        } ?>
                    @endforeach
                @endforeach
            @else
                <h2>Please select a category</h2>
            @endif
            </tbody>
        </table>
        <br/><br/><br/><br/>
@stop
