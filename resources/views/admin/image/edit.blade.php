@extends('app')

@section('title', 'Products')

@section('content')
    <link href="/css/cropper.css" rel="stylesheet">
    <script src="/js/cropper.min.js"></script>

    @include('admin._partials.leftWrapper')




    <div id="rightWrapper">
        {{--<div id="header"><a id="fullPage" href="#">|||</a></div>--}}

        <h2>Edit Product {{ $product->sku }}</h2>

        <div id="notif"></div>

        @include('admin._partials.notifications')


        <style>/* Limit image width to avoid overflow the container */
            img {
                max-width: 100%; /* This rule is very important, please do not ignore this! */
            }
        </style>

        <div class="control-group col-md-6">

            <div>
                <input type="hidden" id="id" value="{{ $product->id }}">
                <img style="max-height: 500px" src="/images/products/{{ $product->image }}?{{ rand() }}" id="image">
            </div>

            <button class="save btn btn-info btn-large">Save</button>
            <a href="/admin/restoreimage/{{$product->id}}">
                <button class="btn btn-info btn-large">Restore Image</button>
            </a>
            <a href="/admin/makecanvasbigger/{{$product->id}}">
                <button class="btn btn-info btn-large">Resize Canvas</button>
            </a>

        </div>


        <div class="control-group col-md-3">
            <input type="hidden" id="id" value="{{ $product->id }}">
            <h3 style="text-align: center">Main Product Image</h3>
            <img src="/images/products/{{ $product->product->image }}" id="image">
        </div>

        <div class="control-group col-md-3">
            <h3 style="text-align: center">Original Image</h3>
            <img src="/images/products/{{ $product->image }}?{{ rand() }}" id="original">

            <br/><br/><br/><br/><br/>
            <div class="docs-data">
                <div class="input-group">
                    <label class="input-group-addon" for="dataX">X</label>
                    <input class="form-control" id="dataX" type="text" placeholder="x">
                    <span class="input-group-addon">px</span>
                </div>
                <div class="input-group">
                    <label class="input-group-addon" for="dataY">Y</label>
                    <input class="form-control" id="dataY" type="text" placeholder="y">
                    <span class="input-group-addon">px</span>
                </div>
                <div class="input-group">
                    <label class="input-group-addon" for="dataWidth">Width</label>
                    <input class="form-control" id="dataWidth" type="text" placeholder="width">
                    <span class="input-group-addon">px</span>
                </div>
                <div class="input-group">
                    <label class="input-group-addon" for="dataHeight">Height</label>
                    <input class="form-control" id="dataHeight" type="text" placeholder="height">
                    <span class="input-group-addon">px</span>
                </div>
                <!-- <div class="input-group">
                  <label class="input-group-addon" for="dataRotate">Rotate</label>
                  <input class="form-control" id="dataRotate" type="text" placeholder="rotate">
                  <span class="input-group-addon">deg</span>
                </div> -->
            </div>
        </div>

        <script>



            var image = document.getElementById('image');

            var width = image.clientWidth;
            var height = image.clientHeight;

            var cropper = new Cropper(image, {
                        aspectRatio: 1,
                        viewMode: 0,
                        data: {
                            x: 0,
                            y: 0,
                            width: width,
                            height: height
                        },
                        crop: function (e) {
                            console.log(e.detail.x);
                            console.log(e.detail.y);
                            console.log(e.detail.width);
                            console.log(e.detail.height);
//                            console.log(e.detail.rotate);
//                            console.log(e.detail.scaleX);
//                            console.log(e.detail.scaleY);
                            $("#dataX").val(e.detail.x);
                            $("#dataY").val(e.detail.y);
                            $("#dataWidth").val(e.detail.width);
                            $("#dataHeight").val(e.detail.height);
                        }
                    }
            );




        </script>
        <meta name="csrf-token"
              content="{{ csrf_token() }}"/>
        <script>

            $(function () {

                $('.save').click(function (e) {
                    e.preventDefault();

                    var imageData = cropper.getData(true);
//						alert(JSON.stringify(imageData));
//
//						alert(imageData['x']);

                    var id = $('#id').val();

                    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

                    $.ajax({
                                url: '/admin/saveimage',
                                type: 'POST',
                                data: {_token: CSRF_TOKEN, avatar_data: imageData, id: id},
                                dataType: 'JSON'
                            })
                            .done(function (response) {
                                if (response.success) {

                                    window.setTimeout(function () {
                                        $('#notif').replaceWith('<div class="alert alert-success text-center">The image was saved.</div>');
                                    }, 2000);

                                    window.location.href = "/admin/images/" + response.id + "/edit";
//                                    alert(response.data)

                                } else {
                                    $('#notif').before('<div class="alert alert-danger text-center">There was an error, please try again.</div>');
                                    window.setTimeout(function () {
                                        $('.alert-danger').fadeOut();
                                    }, 2000);
//                                    alert(response.data)
                                }
                            });

                })
            });


        </script>


@stop
