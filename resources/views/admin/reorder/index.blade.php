@extends('app2')

@section('title', 'Reorder ')

@section('content')
    @include('admin._partials.leftWrapper')

    <div id="rightWrapper">
        {{--<div id="header"><a id="fullPage" href="#">|||</a></div>--}}

        <div id="stickydiv">
            <h1>Update Stock Quantity</h1>

            {!! Form::open(['url'=>'admin/quantityreorder']) !!}


            <table>
                <tr>
                    <td width="260"><strong>Show Category</strong></td>
                    <td><strong>Show Stock</strong></td>
                </tr>
                <tr>
                    <td><select id="form1_category" name="category" class="autosubmit">
                            <option value="">Please Select</option>
                            <option value="1">Sheppherd Hook Earrings</option>
                            <option value="2">Stud Earrings</option>
                            <option value="3">Braclets</option>
                            <option value="4">Necklaces</option>
                            <option value="5">Pendants</option>
                            <option value="6">Rings</option>
                            {{--<option value="7">New Arrivals</option>--}}
                        </select></td>
                    <td><select id="form1_quantity" name="selectqty" class="autosubmit">
                            <option value="">Please Select</option>
                            <option value="0">Zero quantity</option>
                            <option value="3">Quantity less than 3</option>
                            <option value="5">Quantity less than 5</option>
                            <option value="10">Quantity less than 10</option>
                            <option value="15">Quantity less than 15</option>
                            <option value="20">Quantity less than 20</option>
                            <option value="9999">Show all</option>
                        </select></td>
                </tr>
            </table>

            <input type="submit" value="Submit">
            {!! Form::close() !!}

            {!! Notification::showAll() !!}

            {!! Form::open(['url'=>'admin/updatequantityreorder']) !!}
            <div class="form-actions">
                {!! Form::submit('Save', array('class' => 'btn btn-success btn-save btn-large')) !!}


            </div>

            <table class="table table-striped" style="table-layout: fixed;width:900px">
                <thead>
                <tr>
                    <th style="text-align: left;width:210px;">Code</th>
                    <th style="width:90px;text-align: center">Gold Weight</th>
                    <th style="width:90px;text-align: center">Gold Cost</th>
                    <th style="width:90px;text-align: center">Stones MM Size</th>
                    <th style="width:90px;text-align: center">Stones Cost</th>
                    <th style="width:90px;text-align: center">Labour Cost</th>
                    <th style="width:50px;text-align: center">Profit 20%</th>
                    <th style="width:50px;text-align: center">New FOB</th>
                    <th style="width:90px;text-align: center">Update FOB</th>

                    <th style="width:50px;text-align: center">FOB</th>

                </tr>

                </thead>

            </table>

        </div>


        <table class="table table-striped" id="quantity" style="table-layout: fixed;width:900px;">


            <tbody>


            <?php $prev = ''; ?>
            @foreach ($stock as $c)
                @foreach ($c->products as $p)
                    @foreach ($p->subproduct as $subproduct)

                        @if (isset($qty) && ($qty >= $subproduct->quantity ) or (!isset($qty)))

                            @if ($p['productName'] != $prev)
                                <tr>
                                    <td style="text-align:left;padding-top:10px;padding-bottom:3px;font-weight:bold;width:210px;">{{ $p['productName'] }}</td>
                                    <td style="width:90px;"></td>
                                    <td style="text-align: left;width:90px;"></td>

                                    <td style="width:90px;"></td>
                                    <td style="width:90px;"></td>
                                    <td style="width:90px;"></td>
                                    <td style="width:50px;"></td>

                                    <td style="width:50px;"></td>

                                    <td style="width:90px;"></td>

                                    <td style="padding-top:-10px;width:50px">


                                    </td>

                                </tr>
                                <?php $prev = $p['productName']; ?>
                            @endif

                            <tr>
                                <td style="text-align: left;width:210px;">{{ $subproduct->sku }}</td>
                                <td style="text-align: left;width:90px;">{!! Form::text('weight[]', null, array('class' => 'shorterfield')) !!}</td>
                                <td style="width:90px;">{!! Form::text('goldcost[]', null, array('class' => 'shorterfield')) !!}</td>
                                <td style="padding-top:-10px;width:90px">
                                    {!! Form::text('mm[]', null, array('class' => 'shorterfield')) !!}
                                </td>

                                <td style="width:90px;">{!! Form::text('stonecost[]', null, array('class' => 'shorterfield')) !!}</td>
                                <td style="width:90px;">{!! Form::text('laboutcost[]', null, array('class' => 'shorterfield')) !!}</td>
                                <td style="width:50px;text-align: right">{{ $subproduct->profit20 }}</td>

                                <td style="width:50px;text-align: right">{{ $subproduct->fobnew }}</td>
                                <td style="width:90px;">{!! Form::text('updatefob[]', null, array('class' => 'shorterfield')) !!}</td>

                                <td style="width:50px;text-align: right">{{ $subproduct->fob }}</td>

                                    <input type="hidden" name="id[]" value="{{ $subproduct->id }}"/>



                            </tr>
                        @endif
                    @endforeach
                @endforeach
            @endforeach
            <tr>

            </tr>


            </tbody>
        </table>
        <input type="hidden" id="category" name="category" value="{{ $category }}" />
        <input type="hidden" id="qty" name="qty" value="{{ $qty }}"/>

        <div class="form-actions">
            {!! Form::submit('Save', array('class' => 'btn btn-success btn-save btn-large')) !!}
            <a href="{{ URL::route('admin.product.index') }}" class="btn btn-large">Cancel</a>

        </div>
    </div>
    </form>
    <script type="text/javascript">
        //            $('.autosubmit').on('change', function () {
        //                //alert('Option Changed');
        //                $(this).parent('form').submit();
        //            });
    </script>

@stop
