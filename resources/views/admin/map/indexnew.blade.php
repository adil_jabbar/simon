@extends('app')

@section('title', 'Customers')

@section('content')
    @include('admin._partials.leftWrapper')

    <div id="rightWrapper">
        {{--<div id="header"><a id="fullPage" href="#">|||</a></div>--}}
        <div id="contentWrapper">
           <div style="height: 85%" id="map-canvas"></div>
        </div>

        <script>
            function initMap() {
                // This function and page not being refreshed within Laravel for some reason. See if the cache view is updated with this.

                var map = new google.maps.Map(document.getElementById('map-canvas'), {
                    center: {lat: -1.458774, lng: 53.375499},
                    zoom: 6,
                    scaleControl: true,
                    scrollwheel: false
                });

                var bounds = new google.maps.LatLngBounds();
                var markers = [];
                var infowindows = [];
                var properties = [];

                @foreach($users as $u)
                  @if($u->address != NULL)
                     @if($u->address->lat != 0)
                         properties.push(
                            {
                                position: {lat: {{ $u->address->lat }}, lng: {{ $u->address->lng }} },
                                text: '<h4>{{ $u->address->companyName }}</h4><strong>Name:</strong> {{ $u->name2 }}<br/><strong>Phone:</strong> {{ $u->address->phone }}<br/><strong>Postcode:</strong> {{ $u->address->postcode }}<br/><strong>Logins:</strong> {{ $u->logins }}',
                                img: '/assets/images/icons/distance-icon-small{{ $u->type }}.png',
                                imghover: '/assets/images/icons/distance-icon{{ $u->type }}.png'
                            }
                         );
                    @endif
                  @endif
                @endforeach

                for (i = 0; i < properties.length; i++) {

                    markers[i] = new google.maps.Marker({
                        position: properties[i].position,
                        map: map,
                        icon: properties[i].img
                    });

                    infowindows[i] = new google.maps.InfoWindow({
                        content: properties[i].text
                    });

                    google.maps.event.addListener(markers[i], 'click', (function (marker, i) {
                        return function () {
                            for(j = 0; j < infowindows.length; j++) {
                                infowindows[j].close();
                                markers[j].setIcon(properties[j].img);
                            }
                            infowindows[i].open(map, markers[i]);
                            markers[i].setIcon(properties[i].imghover);
                        }
                    })(markers[i], i));

                    bounds.extend(markers[i].getPosition());

                }

                map.setCenter(bounds.getCenter());
                //map.fitBounds(bounds);

            }

            function getPosition(postcode, callback) {
                var geocoder = new google.maps.Geocoder();

                geocoder.geocode({'address': postcode}, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        callback({
                            latt: results[0].geometry.location.lat(),
                            long: results[0].geometry.location.lng()
                        });
                    }
                });
            }
        </script>

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBRFVZKEMNf49aw7cFXEF7ygLpYUJqAvjY&callback=initMap" async defer></script>

@endsection
