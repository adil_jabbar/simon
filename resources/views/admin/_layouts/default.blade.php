<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Simon Alexander</title>

	@include('admin._partials.assets')
	<script src="{{ URL::asset('ckeditor/ckeditor.js') }}"></script>
    <style>
        textarea {width:auto;height: auto}
    </style>
</head>
<body>
<div class="container">
	@include('admin._partials.header')

	@yield('main')


</div>
</body>
</html>
