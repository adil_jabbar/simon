@extends('app')

@section('title', 'Users')

@section('content')
    @include('admin._partials.leftWrapper')

    <div id="rightWrapper">
        {{--<div id="header"><a id="fullPage" href="#">|||</a></div>--}}
    <div id="contentWrapper">


    <h2>Edit Settings</h2>

    @include('admin._partials.notifications')

    {!! Form::model($settings, array('method' => 'put', 'route' => array('admin.settings.update', $settings->id), 'files' => true)) !!}


    <div class="control-group">
        {!! Form::label('body1', 'Website Analytics Code') !!}
        <div class="controls">
            {!! Form::textarea('analytics', null, ['size' => '120x3']) !!}

        </div>
    </div>



    <div class="form-actions">
        {!! Form::submit('Save', array('class' => 'btn btn-success btn-save btn-large')) !!}
        <a href="{{ URL::route('admin.settings.index') }}" class="btn btn-large">Cancel</a>
    </div>

    {!! Form::close() !!}
</div>

@endsection
