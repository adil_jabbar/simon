@extends('app2')

@section('title', 'Stock levels')

@section('content')
    @include('admin._partials.leftWrapper')

    <div id="rightWrapper">

        <style>

            input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {
                margin-bottom: 0px;
            }
            
            .form-actions {
                width: 420px;
                float:left;
            }
            
            #total {
                width: 300px;
                float:left;
                margin-top:60px;
                margin-left:0px;
            }
            
            table#quantity {
                margin-top: 325px !important;
            }

            #stickydiv {
                height: 305px !important;
            }
            table#quantity td {
                padding-bottom: 10px !important;
            }

            .btn.download {
                float: right;
            }

            .fixed_header {
                position: fixed;
                width: calc(100% - 290px);
                background-color: #FFF;
            }

            .fixed_header table {
                margin-bottom: 0px;
            }

            .table-data {
                margin-top: 350px;
            }

            .table-data label {
                display: inline-block;
                margin-right: 10px;
                margin-left: 10px;
            }

            .table-data td label:first-child {
                margin-left: 0px;
            }

            .table-data td label:last-child {
                margin-right: 0px;
            }

            .table-data input.shorterfield {
                width: 60px;
            }

            table.form-filter td {
                border: 0px;
            }
        </style>


        <div class="fixed_header">
            <table class="table table-striped tablesorter" width="100%">
                <tr>
                    <td colspan="9">
                        <h1>Stock levels</h1>
                        {!! Form::open(array('action' => 'admin\StockController@filterstock')) !!}

                            <table class="form-filter">
                                <tr>
                                    <td width="260"><strong>Show Category</strong></td>
                                    <td><strong>Show Stock</strong></td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="form1_category" name="category" class="autosubmit">
                                            <option value="">Please Select</option>
                                            <option value="1">Sheppherd Hook Earrings</option>
                                            <option value="2">Stud Earrings</option>
                                            <option value="3">Braclets</option>
                                            <option value="4">Necklaces</option>
                                            <option value="5">Pendants</option>
                                            <option value="6">Rings</option>
                                            {{--<option value="7">New Arrivals</option>--}}
                                        </select>
                                    </td>
                                    <td>
                                        <select id="form1_quantity" name="quantity" class="autosubmit">
                                            <option value="">Please Select</option>
                                            <option value="0">Zero quantity</option>
                                            <option value="3">Quantity less than 3</option>
                                            <option value="5">Quantity less than 5</option>
                                            <option value="10">Quantity less than 10</option>
                                            <option value="15">Quantity less than 15</option>
                                            <option value="20">Quantity less than 20</option>
                                            <option value="9999">Show all</option>
                                        </select>
                                    </td>
                                </tr>
                            </table>


                            <input type="submit" value="Submit">
                        {!! Form::close() !!}

                        <br/> 
                        <a class="btn btn-small btn-success download @if  ($total_reorder_value === '0') disabled @endif" 
                            href="{{ URL::to('admin/view_reorder/reorderform.csv') }}" 
                            target="_blank" style="margin-right: 20px;">Download Reorder</a>

                        {!! Notification::showAll() !!}
                        <div id="total"> Total Reorder Value: <span style="font-weight: bold" class="total_reorder_value"> £{{ number_format($total_reorder_value,0,'.',',') }}</span></div>

                        <meta name="csrf-token" content="{{ csrf_token() }}">
                    </td>
                </tr>
                <tr>
                    <th style="text-align:left;width: 320px;">Code</th>
                    <th style="text-align:center;width:60px;">PROFIT</th>
                    <th style="text-align:center;width:60px;">MTD</th>
                    <th style="text-align:center;width:60px;">QTD</th>
                    <th style="text-align:center;width:60px;">YTD</th>
                    <th style="text-align:center;width:60px;">STOCK</th>
                    <th style="text-align:center;width:70px;">FOB</th>
                    <th style="text-align:left;">REORDER QUANTITY</th>
                    <th style="text-align:center;width:100px;">FOB VALUE</th>
                </tr>
            </table>
        </div>

        <table class="table table-striped tablesorter table-data">
            <tbody>

                <?php $prev = ''; ?>
                @foreach ( $stock as $subproduct)

                    @if ($subproduct->productName != $prev)
                        <tr>
                            <td colspan="9" style="text-align:left;padding-top:10px;padding-bottom:3px;font-weight:bold">{{ $subproduct->productName }}</td>
                        </tr>
                        <?php $prev = $subproduct->productName; ?>
                    @endif

                    <tr id="prod-id-{{ $subproduct->subproductId }}" data-prop="{{ $subproduct->subproductId }}" class="sub-product-row">

                        <td style="width:320px;text-align:left;padding-top:10px;padding-bottom:3px">
                            {{ $subproduct->subproductsku }}
                        </td>
                        
                        <td style="width:80px;padding-top:10px;padding-bottom:3px;text-align: center;">
                            {{ (($subproduct->grossPrice - ($subproduct->into_safe)) * $subproduct->ytd) }}
                        </td>
                        
                        <td style="width:60px;padding-top:10px;padding-bottom:3px;text-align: center;">
                            @if ($subproduct->mtd != NULL)  
                                {{ $subproduct->mtd }} 
                            @else
                                0  
                            @endif
                        </td>
                        
                        <td style="width:60px;padding-top:10px;padding-bottom:3px;text-align: center;">
                            @if ($subproduct->qtd != NULL)  
                                {{ $subproduct->qtd }} 
                            @else
                                0  
                            @endif
                        </td>
                        
                        <td style="width:60px;padding-top:10px;padding-bottom:3px;text-align: center;">
                            @if ($subproduct->ytd != NULL)  
                                {{ $subproduct->ytd }} 
                            @else
                                0  
                            @endif
                        </td>
                        
                        {{--<td><img width="50" height="50" src="{{ '/images/products/' . $subproduct->image }}"></td>--}}
                        
                        @if ($subproduct->quantity > 5)
                            <td style="width:76px;padding-top:10px;padding-bottom:3px;text-align: center;">
                                {{ $subproduct->quantity }}
                            </td>
                        @else
                            <td style="width:76px;padding-top:10px;padding-bottom:3px;color:red;text-align: center;">
                                {{ $subproduct->quantity }}
                            </td>
                        @endif

                        <td style="width:60px;padding-top:10px;padding-bottom:3px;color:red;text-align: center;">
                            {{ $subproduct->fob }}
                        </td>

                        <td style="text-align: left;white-space:nowrap;">
                            @if ($subproduct->reorder_temp > 0)
                                <label>R/G</label> <input class="shorterfield" name="reorder[{{ $subproduct->subproductId }}]" type="text" value="{{ $subproduct->reorder_temp }}">
                            @else
                                <label>R/G</label> <input class="shorterfield" name="reorder[{{ $subproduct->subproductId }}]" type="text">
                            @endif

                            @if ($subproduct->reorder_temp_yg > 0)
                                <label>Y/G</label> <input class="shorterfield" name="reorder-yg-[{{ $subproduct->subproductId }}]" type="text" value="{{ $subproduct->reorder_temp_yg }}">
                            @else
                                <label>Y/G</label> <input class="shorterfield" name="reorder-yg-[{{ $subproduct->subproductId }}]" type="text">
                            @endif

                            <input type="hidden" name="id[{{ $subproduct->subproductId }}]" value="{{ $subproduct->id }}"/>
                            <input type="hidden" name="subproductId[{{ $subproduct->subproductId }}]" value="{{ $subproduct->subproductId }}"/>
                            <input type="hidden" name="productName[{{ $subproduct->subproductId }}]" value="{{ $subproduct->productName}}"/>
                            <input type="hidden" name="sku[{{ $subproduct->subproductId }}]" value="{{ $subproduct->sku }}"/>
                            <input type="hidden" name="stone[{{ $subproduct->subproductId }}]" value="{{ $subproduct->code }}"/>
                            <input type="hidden" name="category[{{ $subproduct->subproductId }}]" value="{{ $subproduct->category }}"/>
                            <input type="hidden" id="category" name="cat" value="{{ $cat }}"/>
                            <input type="hidden" id="quantity" name="quantity" value="{{ $quantity }}"/>

                        </td>

                        @if ($subproduct->reorder_value > 0)
                            <td style="width:110px;padding-top:10px;padding-bottom:3px;color:red;text-align: center;" class="orderval">
                                {{ $subproduct->reorder_value }}
                            </td>
                        @else
                            <td style="width:110px;padding-top:10px;padding-bottom:3px;color:red;text-align: center;" class="orderval">
                                &nbsp;
                            </td>
                        @endif

                    </tr>
                    {{--@endif--}}
                    {{--@endforeach--}}
                    {{--@endforeach--}}
                @endforeach

            </tbody>
        </table>
    </form>
    <script type="text/javascript">
        $(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.sub-product-row input[type="text"]').keyup(delay(function (e) {
                $attName = $(this).attr('name');
                $trID = $(this).closest('tr').attr('data-prop');
                $attVal = $(this).val();

                $.ajax({
                    type:'POST',
                    url:'/admin/api/savereorder',
                    dataType: 'json',
                    data: {
                        rowid: $trID,
                        name: $attName,
                        value: $attVal
                    },
                    success: function(data){
                        $('#prod-id-' + data.id + ' input[name="' + data.fieldname + '"]').closest('td').css('border-bottom', '1px solid green');

                        $('#prod-id-' + data.id + ' .orderval').html('');
                        $('#prod-id-' + data.id + ' .orderval').html(data.orderval);
                        $('#prod-id-' + data.id + ' .orderval').css('border-bottom', '1px solid green');

                        if(data.totalReorderValue !== '' & data.totalReorderValue !== '0') {
                            $('.total_reorder_value').html('');
                            $('.total_reorder_value').html('&pound;' + data.totalReorderValue);
                            $('.btn.download').removeClass('disabled');
                        } else {
                            $('.total_reorder_value').html('');
                            $('.total_reorder_value').html('&pound; 0');
                            $('.btn.download').addClass('disabled');
                        }

                        setTimeout(
                            function() {
                                $('#prod-id-' + data.id + ' input[name="' + data.fieldname + '"]').closest('td').css('border-bottom', 'initial');

                                $('#prod-id-' + data.id + ' .orderval').css('border-bottom', 'initial');
                        }, 3000);
                    },
                    error: function(){
                        alert('An error occurred. Please refresh the page and try again.');
                    }
                });

            }, 800));

            function delay(callback, ms) {
                var timer = 0;
                return function() {
                    var context = this, args = arguments;
                    clearTimeout(timer);
                    timer = setTimeout(function () {
                        callback.apply(context, args);
                    }, ms || 0);
                };
            }

            $('a.btn.download').on('click', function (e) {
                setTimeout(
                    function() {
                        location.reload();
                }, 200);
            });
        });
    </script>

@stop
