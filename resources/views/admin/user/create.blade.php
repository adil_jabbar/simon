@extends('app')

@section('title', 'Customers')

@section('content')

    <style>
        .form-group {
            margin-bottom: 0;
        }
    </style>
    @include('admin._partials.leftWrapper')

    <div id="rightWrapper">
        {{--<div id="header"><a id="fullPage" href="#">|||</a></div>--}}
        <div id="contentWrapper">
            <h2>Create Customer</h2>

            <style>
                .longerfield {
                    width: 400px
                }

                .shorterfield {
                    width: 80px;
                }
            </style>
            @include('admin._partials.notifications')

            {!! Form::open(array('route' => 'admin.user.store')) !!}

            <div class="form-group">
                <label class="col-md-2 control-label">Username</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="username"
                           value="{{ old('username') }}">
                </div>
            </div>
            <div style="clear:both;height:1px"></div>


            <div class="form-group">
                <label class="col-md-2 control-label">Contact Name 1</label>
                <div class="col-md-4">
                    <input required  type="text" class="form-control" name="name2"
                           value="{{ old('name2') }}">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Contact Role 1</label>
                <div class="col-md-4">
                    <input required  type="text" class="form-control" name="name2_role"
                           value="{{ old('name2_role') }}">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Contact Name 2</label>
                <div class="col-md-4">
                    <input  type="text" class="form-control" name="name3"
                           value="{{ old('name3') }}">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Contact Role 2</label>
                <div class="col-md-4">
                    <input  type="text" class="form-control" name="name3_role"
                           value="{{ old('name3_role') }}">
                </div>
            </div>
<style>
    .reduce {width:150px}
</style>
            <div style="clear:both;height:1px"></div>
            <div class="form-group">
                <label class="col-md-2 control-label">Customer Type</label>
                <div class="col-md-10">
                    {!! Form::select('type', array('0' => 'Simon', '3' => 'Demelza', '2' => 'Target'),
                         null, array('class' => 'form-control reduce')) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Customer or Agent</label>
                <div class="col-md-10">
                    {!! Form::select('agent', array('0' => 'Customer', '1' => 'Agent'),
                         null, array('class' => 'form-control reduce')) !!}
                </div>
            </div>
            <div style="clear: both"></div>

            <div style="margin-top:10px" class="form-group">
                <label class="col-md-2 control-label">E-Mail Address</label>
                <div class="col-md-4">
                    <input  type="email" class="form-control" name="email"
                           value="{{ old('email') }}">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Company Name</label>
                <div class="col-md-4">
                    <input  type="text" class="form-control" name="companyName"
                           value="{{ old('companyName') }}">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Password</label>
                <div class="col-md-4">
                    <input  type="password" class="form-control" name="password">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Confirm Password</label>
                <div class="col-md-4">
                    <input  type="password" class="form-control"
                           name="password_confirmation">
                </div>
            </div>

            <div style="clear:both;height:1px"></div>

                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary">
                        Register
                    </button>
                </div>
            <br/><br/><br/>
            <div class="form-group">
                <label class="col-md-2 control-label">Address 1</label>
                <div class="col-md-4">
                    <input  type="text" class="form-control" name="address1"
                           value="{{ old('address1') }}">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Address 2</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="address2" value="{{ old('address2') }}">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Town/City</label>
                <div class="col-md-4">
                    <input  type="text" class="form-control" name="town"
                           value="{{ old('town') }}">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">County</label>
                <div class="col-md-4">
                    <input  type="text" class="form-control" name="county"
                           value="{{ old('county') }}">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Postcode</label>
                <div class="col-md-4">
                    <input  type="text" class="form-control" name="postcode"
                           value="{{ old('postcode') }}">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Telephone</label>
                <div class="col-md-4">
                    <input type="text" class="form-control"  name="phone" value="{{ old('phone') }}">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">VAT number</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="vat" value="{{ old('vat') }}">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Company Number</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" name="companyID" value="{{ old('companyID') }}">
                </div>
            </div>


            <div class="form-group">
                <label class="col-md-2 control-label">Notes</label>
                <div class="col-md-10">
                    <textarea rows="6" class="form-control" cols="150" name="notes">{{ old('notes') }}</textarea>

                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary">
                        Register
                    </button>
                </div>
            </div>


            {!! Form::close() !!}
        </div>
@endsection
