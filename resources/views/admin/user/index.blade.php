@extends('app')

@section('title', 'Customers')

@section('content')

    @include('admin._partials.leftWrapper')

    <div id="rightWrapper">
        {{--<div id="header"><a id="fullPage" href="#">|||</a></div>--}}
    <div id="contentWrapper">


        <h1>
            Customers &nbsp;<a href="{{ URL::route('admin.user.create') }}" class="btn btn-success"><i
                        class="icon-plus-sign"></i> Add new Customer</a>
        </h1>

        <hr>

        {!!   Notification::showAll() !!}

        {!! Form::open(array('action' => 'admin\UserController@type')) !!}

        <table>
            <tr>
                <td width="260"><strong>Select Customer Type</strong></td>

            </tr>
            <tr>
                <td><select id="form1_type" name="type" class="autosubmit">
                        <option value="">Please Select</option>
                        <option value="0">Simon</option>
                        <option value="1">Max</option>
                        <option value="2">Target</option>

                    </select></td>

            </tr>
        </table>


        <input type="submit" value="Submit">
        {!! Form::close() !!}
        <br/><br/>

        <table class="table table-striped">
            <thead>
            <tr>

                <th>#</th>
                <th>Username</th>
                <th>Company</th>
                <th>Name</th>
                <th>Logins</th>
                <th>E-mail</th>
                {{--<th>Confirmed</th>--}}
                <th width="120"><i class="icon-cog"></i></th>
            </tr>
            </thead>
            <tbody>

            @foreach ($users as $user)
                <tr>
                    <td>{{ $user->id }}</td>
                    <td><a href="{{ URL::route('admin.user.edit', $user->id) }}">{{ $user->username }}</a></td>
                    <td><a href="{{ URL::route('admin.user.edit', $user->id) }}">{{ $user->address->companyName }}</a></td>
                    <td><a href="{{ URL::route('admin.user.edit', $user->id) }}">{{ $user->name2 }}</a></td>
                    <td><a href="{{ URL::route('admin.user.edit', $user->id) }}">{{ $user->logins }}</a></td>
                    <td>{{ $user->email }}</td>
                    {{--<td>@if ($user->confirmed == 1) NO  @endif</td>--}}
                    <td>
                        <a href="{{ URL::route('admin.user.edit', $user->id) }}"
                           class="btn btn-success btn-mini pull-left">Edit</a>

                        {!! Form::open(array('route' => array('admin.user.destroy', $user->id), 'method' => 'delete', 'data-confirm' => 'Are you sure?')) !!}
                        <button type="submit" href="{{ URL::route('admin.user.destroy', $user->id) }}"
                                class="btn btn-danger btn-mini">Delete
                        </button>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <br/><br/><br/>

    </div>
@endsection


