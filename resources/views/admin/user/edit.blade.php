@extends('app')

@section('title', 'Customers')

@section('content')
    @include('admin._partials.leftWrapper')

    <div id="rightWrapper">
        {{--<div id="header"><a id="fullPage" href="#">|||</a></div>--}}
        <div id="contentWrapper">

            <h2>Edit Customer <span style="color:red">{!! $user->name !!} </span> </h2>

            @include('admin._partials.notifications')

            {!! Form::model($user, array('method' => 'put', 'route' => array('admin.user.update', $user->id), 'files' => true)) !!}
<style>
    .form-group {
        margin-bottom: 0;
    }
</style>
            <div class="form-group">
                <label class="col-md-2 control-label">Username</label>
                <div class="col-md-4">
                    {!! Form::text('username', null, array('class' => 'form-control','required')) !!}
                </div>
            </div>

            <div style="clear:both;height:1px"></div>

            <div class="form-group">
                <label class="col-md-2 control-label">Contact Name 1</label>
                <div class="col-md-4">
                    {!! Form::text('name2', isset($user->name2) ? $user->name2 : null, array( 'class'=>'form-control','required' )) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Contact Role 1</label>
                <div class="col-md-4">
                    {!! Form::text('name2_role', isset($user->name2_role) ? $user->name2_role : null, array( 'class'=>'form-control','required' )) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Contact Name 2</label>
                <div class="col-md-4">
                    {!! Form::text('name3', isset($user->name3) ? $user->name3 : null, array( 'class'=>'form-control' )) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Contact Role 2</label>
                <div class="col-md-4">
                    {!! Form::text('name3_role', isset($user->name3_role) ? $user->name3_role : null, array( 'class'=>'form-control' )) !!}
                </div>
            </div>
            <style>
                .reduce {width:150px}
            </style>

            <div class="form-group">
                <label class="col-md-2 control-label">Customer Type</label>
                <div class="col-md-10">
                    {!! Form::select('type', array('0' => 'Simon', '3' => 'Demelza', '2' => 'Target'),
                         null, array('class' => 'form-control reduce')) !!}
                </div>
            </div>


            <div class="form-group">
                <label class="col-md-2 control-label">Customer Authorised</label>
                <div class="col-md-10">
                    {!! Form::select('confirmed', array('0' => 'Yes', '1' => 'No'),
                         null, array('class' => 'form-control reduce')) !!}
                </div>
            </div>

            <div style="clear: both"></div>

            <div style="margin-top:20px" class="form-group">
                <label class="col-md-2 control-label">Email</label>
                <div class="col-md-4">
                    {!! Form::text('email', null, array('class' => 'form-control')) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Company Name</label>
                <div class="col-md-4">

                    {!! Form::text('companyName', isset($user->address->companyName) ? $user->address->companyName : null, array( 'class'=>'form-control', 'required' )) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Password</label>
                <div class="col-md-4">
                    <input type="password" class="form-control" name="password">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Confirm Password</label>
                <div class="col-md-4">
                    <input type="password" class="form-control" name="password_confirmation">
                </div>
            </div>


            <div class="clearfix"></div>
            <div class="form-actions">
                {!! Form::submit('Save', array('class' => 'btn btn-success btn-save btn-large')) !!}
                <a href="{{ URL::route('admin.user.index') }}" class="btn btn-large">Cancel</a>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Address 1</label>
                <div class="col-md-4">


                    {!! Form::text('address1', isset($user->address->address1) ? $user->address->address1 : null, array( 'class'=>'form-control' )) !!}

                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Address 2</label>
                <div class="col-md-4">

                    {!! Form::text('address2', isset($user->address->address2) ? $user->address->address2 : null, array( 'class'=>'form-control' )) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Town/City</label>
                <div class="col-md-4">


                    {!! Form::text('town', isset($user->address->town) ? $user->address->town : null, array( 'class'=>'form-control' )) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">County</label>
                <div class="col-md-4">

                    {!! Form::text('county', isset($user->address->county) ? $user->address->county : null, array( 'class'=>'form-control' )) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Postcode</label>
                <div class="col-md-4">

                    {!! Form::text('postcode', isset($user->address->postcode) ? $user->address->postcode : null, array( 'class'=>'form-control' )) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Telephone</label>
                <div class="col-md-4">


                    {!! Form::text('phone', isset($user->address->phone) ? $user->address->phone : null, array( 'class'=>'form-control' )) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">VAT number</label>
                <div class="col-md-4">

                    {!! Form::text('vat', isset($user->address->vat) ? $user->address->vat : null, array( 'class'=>'form-control' )) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Company Number</label>
                <div class="col-md-4">
                    {!! Form::text('companyID', isset($user->address->companyID) ? $user->address->companyID : null, array( 'class'=>'form-control' )) !!}
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Notes</label>
                <div class="col-md-10">
                    <textarea rows="6" class="form-control" cols="150" name="notes">{{ isset($user->notes) ? $user->notes : null }}</textarea>

                </div>
            </div>

            <div class="clearfix"></div>
            <div class="form-actions">
                {!! Form::submit('Save', array('class' => 'btn btn-success btn-save btn-large')) !!}
                <a href="{{ URL::route('admin.user.index') }}" class="btn btn-large">Cancel</a>
            </div>

            {!! Form::close() !!}
            <br/><br/><br/><br/>
        </div>
        </div>
        <br/><br/><br/><br/>
@endsection
