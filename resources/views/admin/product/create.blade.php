@extends('app')

@section('title', 'Products')

@section('content')
    @include('admin._partials.leftWrapper')

    <div id="rightWrapper">
        {{--<div id="header"><a id="fullPage" href="#">|||</a></div>--}}

        <h2>Create Product</h2>

        <style>
            .longerfield {
                width: 400px
            }

            .shorterfield {
                width: 80px;
            }
        </style>
        @include('admin._partials.notifications')

        {!! Form::open(array('route' => 'admin.product.store', 'files' => true)) !!}


        <div class="control-group">
            {!! Form::label('productName', 'Product Name') !!}
            <div class="controls">
                {!! Form::text('productName', null, array('class' => 'longerfield')) !!}
            </div>
        </div>

        <div class="form-group">
            {!!  Form::label('category', 'Category')  !!}
            <select name="category_id" id="category_id" required="required" >
                <option value="">Select Product Category...</option>
                @foreach($categories as $cat)
                    <option value="{{ $cat->id}}"> {{ $cat->category}} </option>
                @endforeach
            </select>
        </div>

        <div class="control-group">
            {!! Form::label('live', 'Live?') !!}
            <div style="width:90px" class="form-group">
                {!!Form::select('live', array('1' => 'Yes','0' => 'No'), null, array('class' => 'form-control')) !!}
            </div>
        </div>
        <div style="margin-left:-16px">
            <div class="col-md-4">
                <div class="control-group">
                    {!! Form::label('similar', 'Matching Item') !!}
                    <div  class="form-group">
                        {{--<select class="form-control" name="similar1" style="width: 100%">--}}
                            {{--<option value="">Select</option>--}}
                            {{--@foreach ($products as $p)--}}
                                {{--<option value="{{ $p->id }}">{{ $p->sku }} - {{ $p->productName }}</option>--}}
                            {{--@endforeach--}}
                        {{--</select>--}}
                        {!! Form::select('similar1', $productsSelect, null, ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="control-group">
                    {!! Form::label('similar', 'Matching Item 2') !!}
                    <div  class="form-group">
                        {{--<select class="form-control" name="similar2" style="width: 100%">--}}
                            {{--<option value="">Select</option>--}}
                            {{--@foreach ($products as $p)--}}
                                {{--<option value="{{ $p->id }}">{{ $p->sku }} - {{ $p->productName }}</option>--}}
                            {{--@endforeach--}}
                        {{--</select>--}}
                        {!! Form::select('similar2', $productsSelect, null, ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="control-group">
                    {!! Form::label('similar', 'Matching Item 3') !!}
                    <div  class="form-group">
                        {{--<select class="form-control" name="similar3" style="width: 100%">--}}
                            {{--<option value="">Select</option>--}}
                            {{--@foreach ($products as $p)--}}
                                {{--<option value="{{ $p->id }}">{{ $p->sku }} - {{ $p->productName }}</option>--}}
                            {{--@endforeach--}}
                        {{--</select>--}}
                        {!! Form::select('similar3', $productsSelect, null, ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="control-group">
            {!! Form::label('sku', 'SKU') !!}
            <div class="controls">
                {!! Form::text('sku', null, array('class' => 'shorterfield')) !!}
            </div>
        </div>
        <div class="control-group">
            {!! Form::label('productDesc', 'Product Description') !!}
            <div class="controls">
                {!! Form::textarea('productDesc') !!}
                <script>
                    var roxyFileman = '../../../fileman/index.html';

                    $(function () {
                        CKEDITOR.replace('productDesc', {
                            filebrowserBrowseUrl: roxyFileman,
                            height: 100,
                            width: 850,
                            filebrowserUploadUrl: roxyFileman,
                            filebrowserImageBrowseUrl: roxyFileman + '?type=image',
                            filebrowserImageUploadUrl: roxyFileman + '?type=image'
                        });
                    });
                </script>
            </div>
        </div>

        <div class="control-group">
            {!! Form::label('image', 'Image') !!}

            <div class="fileupload fileupload-new" data-provides="fileupload">
                <div class="fileupload-preview thumbnail" style="width: 200px; height: 150px;">
                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image">
                </div>
                <div>
                <span class="btn btn-file"><span class="fileupload-new">Select image</span><span
                            class="fileupload-exists">Change</span>{!! Form::file('image') !!}</span>
                    <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                </div>
            </div>
        </div>

        {{--<div class="control-group">--}}
            {{--{!! Form::label('imageAlt', 'Image Alt Text') !!}--}}
            {{--<div class="controls">--}}
                {{--{!! Form::text('imageAlt') !!}--}}
            {{--</div>--}}
        {{--</div>--}}

        <div class="form-actions">
            {!! Form::submit('Save', array('class' => 'btn btn-success btn-save btn-large')) !!}
            <a href="{{ URL::route('admin.product.index') }}" class="btn btn-large">Cancel</a>
        </div>

    {!! Form::close() !!}
        <br/><br/><br/><br/><br/>

@stop
