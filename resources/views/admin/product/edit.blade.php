@extends('app')

@section('title', 'Products')

@section('content')
    @include('admin._partials.leftWrapper')

    <div id="rightWrapper">
        {{--<div id="header"><a id="fullPage" href="#">|||</a></div>--}}

        <h2>Edit Product</h2>

        @include('admin._partials.notifications')

        {!! Form::model($product, array('method' => 'put', 'route' => array('admin.product.update', $product->id), 'files' => true)) !!}

        <div class="control-group">
            {!! Form::label('productName', 'Product Name') !!}
            <div class="controls">
                {!! Form::text('productName', null, array('class' => 'longerfield')) !!}
            </div>
        </div>

        {{--<div class="form-group">--}}
        {{--{{ Form::label('category_id', 'Category') }}--}}

        {{--{{Form::select('category_id', $categories, null, array('class' => 'form-control')) }}--}}

        {{--</div>--}}

        <div class="control-group">
            {!! Form::label('live', 'Live?') !!}
            <div style="width:70px" class="form-group">
                {!!Form::select('live', array('0' => 'No', '1' => 'Yes'), null, array('class' => 'form-control')) !!}
            </div>
        </div>
        <div style="margin-left:-16px">
            <div class="col-md-4">
                <div class="control-group">
                    {!! Form::label('similar', 'Matching Piece') !!}
                    <div  class="form-group">
                        {{--<select class="form-control" name="similar1" style="width: 100%">--}}
                            {{--<option value="">Select</option>--}}
                            {{--@foreach ($products as $p)--}}
                                {{--@if (Input::old('similar1') == $p->id)--}}
                                    {{--<option selected  value="{{ $p->id }}">{{ $p->sku }} - {{ $p->productName }} </option>--}}
                                {{--@else--}}
                                    {{--<option value="{{ $p->id }}">{{ $p->sku }} - {{ $p->productName }} </option>--}}
                                {{--@endif--}}
                            {{--@endforeach--}}
                        {{--</select>--}}
                        {!! Form::select('similar1', $productsSelect, null, ['class' => 'form-control']) !!}

                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="control-group">
                    {!! Form::label('similar', 'Matching Piece 2') !!}
                    <div  class="form-group">
                        {{--<select class="form-control" name="similar2" style="width: 100%">--}}
                            {{--<option value="">Select</option>--}}
                            {{--@foreach ($products as $p)--}}
                                {{--<option value="{{ $p->id }}">{{ $p->sku }} - {{ $p->productName }}</option>--}}
                            {{--@endforeach--}}
                        {{--</select>--}}
                        {!! Form::select('similar2', $productsSelect, null, ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="control-group">
                    {!! Form::label('similar', 'Matching Piece 3') !!}
                    <div  class="form-group">
                        {{--<select class="form-control" name="similar3" style="width: 100%">--}}
                            {{--<option value="">Select</option>--}}
                            {{--@foreach ($products as $p)--}}
                                {{--<option value="{{ $p->id }}">{{ $p->sku }} - {{ $p->productName }}</option>--}}
                            {{--@endforeach--}}
                        {{--</select>--}}
                        {!! Form::select('similar3', $productsSelect, null, ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="control-group">
            {!! Form::label('sku', 'SKU') !!}
            <div class="controls">
                {!! Form::text('sku', null, array('class' => 'shorterfield')) !!}
            </div>
        </div>

        <div class="form-group">
            {!!  Form::label('category', 'Category')  !!}

            {!! Form::select('category_id', $categories,
                 null, array('class' => 'longerfield'))  !!}


            {{--<select name="category_id" id="category_id" required="required" >--}}
            {{--<option value="">Select Product Category...</option>--}}
            {{--@foreach($categories as $cat)--}}
            {{--<option value="{{ $cat->id}}"> {{ $cat->category}} </option>--}}
            {{--@endforeach--}}
            {{--</select>--}}
        </div>


        <div class="control-group" style="float:left">
            {!! Form::label('image', 'Image') !!}

            <div class="fileupload fileupload-new" data-provides="fileupload">
                <div class="fileupload-preview thumbnail" style="width: 200px; height: 150px;">
                    <img src="{{ '/images/products/' . $product->image }}">
                </div>
                <div>
                <span class="btn btn-file"><span class="fileupload-new">Select image</span><span
                            class="fileupload-exists">Change</span>{!! Form::file('image') !!}</span>
                    <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                </div>
            </div>


        </div>

        <div style="float:left;margin-left:50px">

            {!! Form::label('image1', 'Extra Image 1') !!}

            <div class="fileupload fileupload-new" data-provides="fileupload">
                <div class="fileupload-preview thumbnail" style="width: 200px; height: 150px;">
                    <img src="{{ '/images/products/altimages/' . $product->image1 }}">
                </div>
                <div>
                <span class="btn btn-file"><span class="fileupload-new">Select image</span><span
                            class="fileupload-exists">Change</span>{!! Form::file('image1') !!}</span>
                    <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                </div>
            </div>


        </div>

        <div style="float:left;margin-left:10px">

            {!! Form::label('image2', 'Extra Image 2') !!}

            <div class="fileupload fileupload-new" data-provides="fileupload">
                <div class="fileupload-preview thumbnail" style="width: 200px; height: 150px;">
                    <img src="{{ '/images/products/altimages/' . $product->image2 }}">
                </div>
                <div>
                <span class="btn btn-file"><span class="fileupload-new">Select image</span><span
                            class="fileupload-exists">Change</span>{!! Form::file('image2') !!}</span>
                    <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                </div>
            </div>


        </div>

        <div style="float:left;margin-left:10px">

            {!! Form::label('image3', 'Extra Image 3') !!}

            <div class="fileupload fileupload-new" data-provides="fileupload">
                <div class="fileupload-preview thumbnail" style="width: 200px; height: 150px;">
                    <img src="{{ '/images/products/altimages/' . $product->image3 }}">
                </div>
                <div>
                <span class="btn btn-file"><span class="fileupload-new">Select image</span><span
                            class="fileupload-exists">Change</span>{!! Form::file('image3') !!}</span>
                    <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                </div>
            </div>


        </div>
        <div style="float:left;margin-left:10px">

            {!! Form::label('image4', 'Extra Image 4') !!}

            <div class="fileupload fileupload-new" data-provides="fileupload">
                <div class="fileupload-preview thumbnail" style="width: 200px; height: 150px;">
                    <img src="{{ '/images/products/altimages/' . $product->image4 }}">
                </div>
                <div>
                <span class="btn btn-file"><span class="fileupload-new">Select image</span><span
                            class="fileupload-exists">Change</span>{!! Form::file('image4') !!}</span>
                    <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                </div>
            </div>


        </div>
        <div style="clear:both"></div>

        <div class="control-group">
            {!! Form::label('imageAlt', 'Image Alt Text') !!}
            <div class="controls">
                {!! Form::text('imageAlt') !!}
            </div>
        </div>

        <div class="control-group">
            {!! Form::label('productDesc', 'Product Description') !!}
            <div class="controls">
                {!! Form::textarea('productDesc') !!}
                <script>
                    var roxyFileman = '../../../fileman/index.html';

                    $(function () {
                        CKEDITOR.replace('productDesc', {
                            filebrowserBrowseUrl: roxyFileman,
                            height: 100,
                            width: 850,
                            filebrowserUploadUrl: roxyFileman,
                            filebrowserImageBrowseUrl: roxyFileman + '?type=image',
                            filebrowserImageUploadUrl: roxyFileman + '?type=image'
                        });
                    });
                </script>
            </div>
        </div>


        <div class="form-actions">
            {!! Form::submit('Save', array('class' => 'btn btn-success btn-save btn-large')) !!}
            <a href="/admin/subproduct/{{ $product->id }}" class="btn btn-large">Cancel</a>
        </div>
        <br/><br/><br/><br/><br/>

    {!! Form::close() !!}

@stop
