@extends('app')

@section('title', 'Products')

@section('content')
	@include('admin._partials.leftWrapper')

	<div id="rightWrapper">
		{{--<div id="header"><a id="fullPage" href="#">|||</a></div>--}}

	<h1>
		Products <a href="/admin/product/create/" class="btn btn-success"><i class="icon-plus-sign"></i> Add new product</a>
		<a style="float:right" href="/admin/missing/" class="btn btn-success"><i class="icon-plus-sign"></i> Missing Image Rpt</a>
		<a style="float:right;margin-right:10px" href="/admin/missingdownload/" class="btn btn-success"><i class="icon-plus-sign"></i> Download Missing Image Rpt</a>
	</h1>

	<hr>

	{!!  Notification::showAll()  !!}

	<table class="table table-striped">
		<thead>
			<tr>
				<th>SKU</th>
				<th></th>
				<th>Product</th>

				<th><i class="icon-cog"></i></th>
			</tr>
		</thead>
		<style>table form {width:250px;margin:0;padding:0;display: inline}
			   table form input {width:100px;margin:0;padding:0}</style>
		<tbody>
		@foreach ($product as $c)
			@foreach ($c->products as $product)
				<tr>
					<td style="padding-top:25px;width:50px">{{ $product->sku }}</td>
					<td style="width:70px"><img width="50" height="50" src="{{ '/images/products/thumbnails/' . $product->image }}"></td>
					<td style="padding-top:25px;min-width:250px"><a href="{{ URL::route('admin.product.edit', $product->id) }}">{{ $product->productName }}</a></td>
					<td style="padding-top:25px;min-width:450px">

					@foreach ($product->subproduct as $sub)

						<a title="{{ $sub->stone->stone }}" href="/admin/subproduct/{{ $sub->id }}/edit">
							<img <?php if($sub->image == 'currently-awaiting-image.jpg') { echo 'style="-moz-box-shadow: 0 0 15px red;

-webkit-box-shadow: 0 0 15px red;
box-shadow: 0px 0px 15px red;"'; } ?>

								 src="/images/stone_icons/{{ $sub->stone->image }}?v=4" width="{{ $sub->stone->imageWidth }}" height="{{ $sub->stone->imageHeight }}"/></a>
					@endforeach

					</td>
					<td style="padding-top:25px;min-width:250px;">
						<a style="display: inline" href="{{ URL::route('admin.product.edit', $product->id) }}" class="btn btn-success btn-mini pull-left">Edit</a>

						{!! Form::open(array('route' => array('admin.product.destroy', $product->id), 'method' => 'delete', 'data-confirm' => 'Are you sure?')) !!}
							<button type="submit" href="{{ URL::route('admin.product.destroy', $product->id) }}" class="btn btn-danger btn-mini">Delete</button>
						{!! Form::close() !!}

						<a style="display: inline" href="/admin/subproduct/{{ $product->id }}" class="btn btn-success btn-mini pull-left">Product options</a>
					</td>
				</tr>
			@endforeach
		@endforeach
		</tbody>
	</table>

@stop
