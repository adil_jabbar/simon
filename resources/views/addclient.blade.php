@extends('layouts.master')

@section('seo')

    <meta name="description" content="Add Client">
    <title>Simon Alexander</title>

@endsection

@section('content')

    <div>


        <div class="container">

            <div class="row">

                <div class="col-sm-10 col-sm-offset-1">


                    <h2>Create Customer</h2>

                    <style>
                        .row {
                            margin-bottom: 10px;
                        }

                        .row label {
                            margin-top: 0px;
                        }
                    </style>
                    @include('admin._partials.notifications')
                    <br/><br/>
                    <form id="add-client" class="form-horizontal" action="/add-client" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                        <div class="row">

                            <label class="col-md-2 control-label">Username</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="username"
                                       value="{{ old('username') }}">
                            </div>

                        </div>

                        <div class="row">

                            <label class="col-md-2 control-label">Contact Name</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="name"
                                       value="{{ old('name') }}">
                            </div>

                            <label class="col-md-2 control-label">Contact Role</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="name_role"
                                       value="{{ old('name_role') }}">
                            </div>

                        </div>


                        <div class="row">

                            <label class="col-md-2 control-label">E-Mail Address</label>
                            <div class="col-md-4">
                                <input type="email" class="form-control" name="email"
                                       value="{{ old('email') }}">
                            </div>

                            <label class="col-md-2 control-label">Company Name</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="companyName"
                                       value="{{ old('companyName') }}">
                            </div>
                        </div>

                        <div class="row">

                            <label class="col-md-2 control-label">Password</label>
                            <div class="col-md-4">
                                <input type="password" class="form-control" name="password">
                            </div>
                            <label class="col-md-2 control-label">Confirm Password</label>
                            <div class="col-md-4">
                                <input type="password" class="form-control"
                                       name="password_confirmation">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">
                                    Add Customer
                                </button>
                            </div>

                        </div>
                        <div class="row">
                            <label class="col-md-2 control-label">Address 1</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="address1"
                                       value="{{ old('address1') }}">
                            </div>

                            <label class="col-md-2 control-label">Address 2</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="address2" value="{{ old('address2') }}">
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-md-2 control-label">Town/City</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="town"
                                       value="{{ old('town') }}">
                            </div>

                            <label class="col-md-2 control-label">County</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="county"
                                       value="{{ old('county') }}">
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-md-2 control-label">Postcode</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="postcode"
                                       value="{{ old('postcode') }}">
                            </div>


                            <label class="col-md-2 control-label">Telephone</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="phone" value="{{ old('phone') }}">
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-md-2 control-label">VAT number</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="vat" value="{{ old('vat') }}">
                            </div>

                            <label class="col-md-2 control-label">Company Number</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="companyID" value="{{ old('companyID') }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">
                                    Add Customer
                                </button>
                            </div>
                        </div>


                    {!! Form::close() !!}


                </div>


            </div>

        </div>

    </div>

@endsection

