

        <!DOCTYPE html>
<html>
<head>
    <title>Simon Alexander</title>

    <style>
        html, body {
            height: 100%;
            background-color: #324c71;
        }

        .outer-wrapper {
            background: url("assets/images/background.jpg") no-repeat center center;
            background-size: 100% auto;
            max-height: 1200px;
            height: 100%;
        }

        .inner-wrapper {
            display: table;
            width: 100%;
            height: 100%;
        }

        .header-wrapper {
            display: table-cell;
            text-align: center;
            vertical-align: middle;
            font-family: Verdana, Geneva, sans-serif;
            font-size: 16px;
            font-style: normal;
            font-weight: 500;
            line-height: 26px;
            color: white;
        }

        a {
            font-family: Verdana, Geneva, sans-serif;
            font-size: 24px;
            font-style: normal;
            font-variant: normal;
            font-weight: 500;
            line-height: 26px;
            color: white;
            text-transform: uppercase;
            text-decoration: none;

        }
    </style>
</head>
<body>


<div class="outer-wrapper">
    <div class="inner-wrapper">
        <div class="header-wrapper">
            <a href="auth/register">REGISTER</a> / <a
                    href="{{ URL::route('admin.manage.index') }}">LOGIN</a>
        </div>
    </div>
</div>
</body>
</html>

