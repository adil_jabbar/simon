<!-- app/views/company/edit.blade.php -->

<!DOCTYPE html>
<html>
<head>
	<title>Performance CMS</title>
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js"></script> 
    <script src="http://malsup.github.com/jquery.form.js"></script> 
</head>
<body>
<div class="container">

<nav class="navbar navbar-inverse">
	<div class="navbar-header">
		<a class="navbar-brand" href="{{ URL::to('admin') }}">Back to Admin</a>
	</div>
	<ul class="nav navbar-nav">
		<li><a href="{{ URL::to('admin/banners') }}">View All Bannerss</a></li>
		<li><a href="{{ URL::to('admin/banners/create') }}">Create a Banners</a></li>
	</ul>
</nav>

<h1>Upload image</h1>

<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all()) }}



                <form class="form-horizontal" id="myForm" enctype="multipart/form-data" method="post" action="{{ url('upload/image') }}" autocomplete="off">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input type="file" name="image" id="image" /> 
                    <input type="submit" value="Submit Comment" /> 
                </form>

<script> 
        // wait for the DOM to be loaded 
        $(document).ready(function() { 
            // bind 'myForm' and provide a simple callback function 
            $('#myForm').ajaxForm(function() { 
                alert("Thank you for your comment!"); 
            }); 
        }); 
    </script> 
</body>
</html>