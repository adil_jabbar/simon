@extends('layouts.master')

@section('content')

    {!!  Notification::showAll()  !!}

    <div class="container-fluid container-inset">

        <div class="row">

            <div id="prod-image" class="col-md-6 col-xs-12 col-sm-12">
                <div class="row">
                    <div id="stock-code">
                        <h1 id="sku">{{ $product->subproduct[0]->sku }}</h1>
                    </div>
                    @if(strlen(($product->image)) > 1)
                        <div class="product-image-container">
                            <img id="productimage" class="img-responsive"
                             src="/images/products/{{ $product->subproduct[0]->image }}"
                             alt="{{ $product->subproduct[0]->imageAlt }}" />

                        <div class="OverlayText rotate">Out of stock</div>
                    </div>
                    @endif
                </div>
                <div class="row">
                    <div id="extraimages">
                        <ul>
                            <div class="col-md-3">
                                <div style="text-align: center;display:inline-block;width:100%">
                                    @if (!empty($product->image1))
                                        <li class="extraimages">
                                            <figure><img
                                                        src="/images/products/altimages/{{ $product->image1 }}"
                                                        data-id="{{ $product->image1 }}"
                                                        width="100"
                                                        height="100"/>

                                            </figure>
                                        </li>
                                    @endif
                                </div>

                            </div>
                            <div class="col-md-3">
                                <div style="text-align: center;display:inline-block;width:100%">
                                    @if (!empty($product->image2))
                                        <li class="extraimages">
                                            <figure><img
                                                        src="/images/products/altimages/{{ $product->image2 }}"
                                                        data-id="{{ $product->image2 }}" width="100"
                                                        height="100"/>

                                            </figure>
                                        </li>
                                    @endif
                                </div>

                            </div>
                            <div class="col-md-3">
                                <div style="text-align: center;display:inline-block;width:100%">
                                    @if (!empty($product->image3))
                                        <li class="extraimages">
                                            <figure><img data-id="{{ $product->image3 }}"
                                                        src="/images/products/altimages/{{ $product->image3 }}"
                                                        width="100"
                                                        height="100"/>

                                            </figure>
                                        </li>
                                    @endif
                                </div>

                            </div>
                            <div class="col-md-3">
                                <div style="text-align: center;display:inline-block;width:100%">
                                    @if (!empty($product->image4))
                                        <li class="extraimages">
                                            <figure><img data-id="{{ $product->image4 }}"
                                                        src="/images/products/altimages/{{ $product->image4 }}"
                                                        width="100"
                                                        height="100"/>

                                            </figure>
                                        </li>
                                    @endif
                                </div>

                            </div>
                        </ul>
                    </div>

                </div>


            </div>
            <div id="prod-details" class="col-md-6">

                <h1 id="prodh1">{{ $product->productName }}</h1>

                <div id="prod-info-price">

                    @if(Auth::check())
                        @if (Auth::user()->prices == 2)
                            <div id="price">

                                &pound;{{ number_format(round($product->subproduct[0]->grossPrice * 2.8,0), 0, '.', ',') }}  <span class="rrp-small">(RRP)</span>

                            </div>
                        @endif
                        @if (Auth::user()->prices == 0)
                            <div id="price">

                                &pound;{{ $product->subproduct[0]->grossPrice  }}

                            </div>
                        @endif
                    @else
                        <a style="color:#f4cbc1" href="{{ url('/auth/login') }}">Login for prices</a>
                    @endif


                </div>
                <style>
                    .rrp-small {
                        font-size: 12px;
                        color: #CCC;
                    }
                </style>
                <div id="stones">
                    <div id="stones_label">
                        Stones:
                    </div>
                    <ul>
                        @foreach($product->subproduct as $s)
                            @if(strlen(trim($s->image)) > 1)
                                <li class="stones">
                                    <img class="stoneicons" src="/images/stone_icons/{{ $s->stone->image }}?ver=4" alt="{{ $s->image }}" data-id="{{ $s->id }}"/>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </div>
                <div class="clearfix"></div>
                <div>
                    <div id="purchase">
                        @if(Auth::check())



                            <h2 id="stone">{{ $product->subproduct[0]->stone->stone }}</h2>
                            {{--@if( Auth::user()->is('guest') )--}}
                            <form id="myForm" method="post" action="{{url('cart')}}" autocomplete="off">

                                <input type="hidden" id="product_id" name="product_id" value="{{ $product->subproduct[0]->id }}">
                                <input type="hidden" name="isEOL" value="0" />
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <br/>
                                <label>Quantity:</label>
                                <br/><br/>
                                <div>
                                    <div class="rgqty" style="width: 100%;">
                                        <h2 class="rg-text">R/Gold</h2>
                                        <div class="input-col">
                                            <input type="number" name="qty" id="qty" min="1" max="1" title="Qty" class="input-text qty">
                                        </div>
                                        <div class="stock-msg stock-msg-rg">
                                            <p>In stock</p>
                                        </div>
                                    </div>
                                    <div class="ygqty" style="width:100%;">
                                        <h2 class="yg-text">Y/Gold</h2>
                                        <div class="input-col">    
                                            <input type="number" name="qty-yg" id="qty-yg" min="1" title="Qty" class="input-text qty qty-yg">
                                        </div>
                                        <div class="stock-msg stock-msg-yg">
                                            <p>Pre-order<br/><span style="color:red;">Delivery 8-12 weeks</span></p>
                                        </div>
                                    </div>
                                </div>
                                <div style="clear:both;height:1px;"></div>
                                @if ($errors->any())
                                    <ul>
                                        <li>Please enter a quantity for rose gold or yellow gold.</li>
                                    </ul>
                                    <div style="clear:both;height:1px;"></div>
                                @endif
                                
                                <button type="submit" title="Add to Cart" class="action primary tocart"
                                        id="product-addtocart-button">
                                    <span>Add to Basket</span>
                                </button>
                            </form>

                            {{--@endif--}}



                        @endif
                    </div>
                    <div style="clear:both;height:20px;"></div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        @if (isset($p1) or isset($p2) or isset($p3))
                            <h2 style="text-align: center" class="matching-items">Matching Items</h2>
                        @endif
                    </div>
                    <div class="col-md-4">
                        <div style="text-align: center;display:inline-block;width:100%">
                            @if (isset($p1))
                                <a id="mi1" href="/product/{{ $p1->slug }}?sku={{ $p1->subproduct[0]->sku }}">
                                    <figure><img id="matchingitem1"
                                                 src="/images/products/{{ $p1->image }}"
                                                 width="100"
                                                 height="100"/>
                                        <figcaption id="fmi1" style="text-align: center">{{ $p1->sku }}</figcaption>
                                    </figure>
                                </a>
                            @endif
                        </div>

                    </div>
                    <div class="col-md-4">
                        <div style="text-align: center;display:inline-block;width:100%">
                            @if (isset($p2))
                                <a id="mi2" href="/product/{{ $p2->slug }}?sku={{ $p2->subproduct[0]->sku }}">
                                    <figure><img id="matchingitem2"
                                                 src="/images/products/{{ $p2->image }}"
                                                 width="100"
                                                 height="100"/>
                                        <figcaption id="fmi2" style="text-align: center">{{ $p2->sku }}</figcaption>
                                    </figure>
                                </a>
                            @endif


                        </div>
                    </div>
                    <div class="col-md-4">
                        <div style="text-align: center;display:inline-block;width:100%;">
                            @if (isset($p3))

                                <a id="mi3" href="/product/{{ $p3->slug }}?sku={{ $p3->subproduct[0]->sku }}">
                                    <figure><img id="matchingitem3"
                                                 src="/images/products/{{ $p3->image }}"
                                                 width="100"
                                                 height="100"/>
                                        <figcaption id="fmi3" style="text-align: center">{{ $p3->sku }}</figcaption>
                                    </figure>
                                </a>
                            @endif
                        </div>

                    </div>

                </div>


            </div>

        </div>
        <br/><br/>
        <div class="push2"></div>

    </div>
    <script>

        $(document).ready(function () {


            var params = [];

            var params2 = [];
            var params3 = [];
            var params4 = [];

            @foreach($product->subproduct as $s)


                    params[{{ $s->id }}] = ['{{ $s->image }}', '{{ $s->imageAlt }}', '{{ $s->grossPrice }}', '{{ $s->quantity }}', '{{ $s->sku }}', '{{ $s->id }}', '{{ $s->stone->stone }}', '{{ $s->stone_id }}', '{{ $s->eoline }}'];


            @endforeach
                    @if (isset($p1))
                    @foreach($p1->subproduct as $s)

                    params2[{{ $s->stone_id }}] = ['{{ $s->image }}', '{{ $s->sku }}', '{{ $p1->slug }}' ];

            @endforeach
                    @endif
                    @if (isset($p2))
                    @foreach($p2->subproduct as $s)

                    params3[{{ $s->stone_id }}] = ['{{ $s->image }}', '{{ $s->sku }}', '{{ $p2->slug }}' ];

            @endforeach
                    @endif
                    @if (isset($p3))
                    @foreach($p3->subproduct as $s)

                    params4[{{ $s->stone_id }}] = ['{{ $s->image }}', '{{ $s->sku }}',  '{{ $p3->slug }}'];


            @endforeach
            @endif

            function setImage($image) {
                //alert($image);
                $("#productimage").attr("src", "/images/products/" + $image);
                $("#price").text('£500');
            }

            function setData($id) {
                var img = params[$id][0];
                var imgupdated = img.replace('&amp;', '&');

                $("#productimage").attr("src", "/images/products/" + imgupdated);
                $("#productimage").attr("alt", params[$id][1]);

                @if (Auth::user()->prices == 2)

                    $("#price").html("£" + Math.round(params[$id][2] * 2.8, 0) + ' <span class="rrp-small">(RRP)</span>');
                @else
                    $("#price").html("£" + params[$id][2]);
                @endif
                $("#qty").attr("max", params[$id][3]);
                $("#sku").text(params[$id][4]);
                $("#product_id").val(params[$id][5]);
                $("#stone").text(params[$id][6]);


                // if end of line

                if( params[$id][8] == 1) {
                    if(params[$id][3] <= 3 & params[$id][3] > 0) {
                        $('.stock-msg.stock-msg-rg p').html('End of line (' + params[$id][3] + ' left)');

                    } else if(params[$id][3] <= 0) {
                        $('.OverlayText').fadeIn();
                        
                        $('#purchase .rgqty .qty').fadeOut('slow');
                        $('.stock-msg.stock-msg-rg p').html('End of line');
                    } else {
                        $('.stock-msg.stock-msg-rg p').html('In stock');
                    }

                    $("#qty-yg").fadeOut('slow');
                    $('.stock-msg.stock-msg-yg p').html('End of line');
                } else {

                    $("#qty-yg").fadeIn('slow');
                    $('.stock-msg.stock-msg-yg p').html('Pre-order<br/><span style="color:red;">Delivery 8-12 weeks</span>');

                    if (params[$id][3] == 1) {
                        $('.OverlayText').fadeOut();
                        
                        $('#purchase .rgqty .qty').fadeIn('slow');
                        $('.stock-msg.stock-msg-rg p').html('In stock (1 left)');

                    } else if (params[$id][3] > 1) {
                        $('#purchase .rgqty .qty').fadeIn('slow');
                        $('.OverlayText').fadeOut();

                        if(params[$id][3] <= 3) {
                            $('.stock-msg.stock-msg-rg p').html('In stock (' + params[$id][3] + ' left)');
                        } else {
                            $('.stock-msg.stock-msg-rg p').html('In stock');
                        }
                    } else {
                        $('.OverlayText').fadeIn();
                        
                        $('#purchase .rgqty .qty').fadeOut('slow');
                        $('.stock-msg.stock-msg-rg p').html('Out of stock');
                    }
                }

                if (params2[params[$id][7]]) {
                    $("#matchingitem1").attr("src", "/images/products/thumbnails/" + params2[params[$id][7]][0]).show();
                    $("#mi1").attr("href", "/product/" + params2[params[$id][7]][2] + "?sku=" + params2[params[$id][7]][1]);
                    $("#fmi1").show();
                } else {
                    $("#matchingitem1").hide();
                    $("#fmi1").hide();
                    $("#mi1").attr("href", "#");
                }
                if (params3[params[$id][7]]) {
                    $("#matchingitem2").attr("src", "/images/products/thumbnails/" + params3[params[$id][7]][0]).show();
                    $("#mi2").attr("href", "/product/" + params3[params[$id][7]][2] + "?sku=" + params3[params[$id][7]][1]);
                    $("#fmi2").show();
                } else {
                    $("#matchingitem2").hide();
                    $("#fmi2").hide();
                    $("#mi2").attr("href", "#");
                }
                if (params4[params[$id][7]]) {
                    $("#matchingitem3").attr("src", "/images/products/thumbnails/" + params4[params[$id][7]][0]).show();
                    $("#mi3").attr("href", "/product/" + params4[params[$id][7]][2] + "?sku=" + params4[params[$id][7]][1]);
                    $("#fmi3").show();
                } else {
                    $("#matchingitem3").hide();
                    $("#fmi3").hide();
                    $("#mi3").attr("href", "#");
                }

            }


            @if ($itemcode)
                setData({{ $itemcode }});
            @else
                setData(<?php echo $product->subproduct[0]->id; ?>);
            @endif
            $("div#stones ul li.stones").hover(function () {
                var id = $(this).find("img").attr("data-id");

                setData(id);
            });
            $("div#extraimages ul li.extraimages").hover(function () {
                var id = $(this).find("img").attr("data-id");
                $("#productimage").attr("src", "/images/products/altimages/" + id);
            });


            $("div#stones ul li").click(function () {
                var id = $(this).find("img").attr("data-id");
                $("div#stones ul li").unbind("mouseenter mouseleave");
                setData(id);
            });
            $("div.qty").click(function () {
                var id = $(this).find("img").attr("data-id");
                $("div#stones ul li").unbind("mouseenter mouseleave");
                setData(id);
            });
            $(".tocart").click(function () {

                $("div#stones ul li").bind("mouseenter mouseleave");

            });
        });

    </script>
    <script>
        setTimeout(function () {
            $(".alert-success").fadeOut().empty();
        }, 5000);
    </script>

@stop