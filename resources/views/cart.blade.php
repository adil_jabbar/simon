@extends('layouts.master')

@section('content')


    <div class="container-fluid container-inset wrapper">

        <div class="row">

            <div class="col-md-12 col-xs-12 col-sm-12">

                {!! Notification::container('myContainer')->all() !!}


                <div class="table-responsive">
                    @if(count($cart))
                        <table class="table">
                            <thead>
                            <tr>
                                <td class="col-xs-1">Item</td>
                                <td class="col-xs-4"><span class="mobilehide">Description</span>
                                    <span class="mobileshow">Code</span></td>
                                <td class="col-xs-2">Price</td>
                                <td class="col-xs-2" style="text-align:center;">Quantity</td>
                                <td class="col-xs-2">Total</td>
                                <td class="col-xs-1" style="text-align: center">Remove</td>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($cart as $item)
                                <tr>
                                    <td class="cart_product">

                                        <img width="80" height="80"
                                             src="/images/products/{{ $item->model->image }}"
                                             alt="{{ $item->model->imageAlt }}">
                                    </td>
                                    <td class="cart_description">
                                        <span class="mobilehide">{!! $item->name !!}</span>
                                        <span class="mobileshow">{{$item->model->sku}}</span>
                                    </td>

                                    <td class="cart_price">
                                        £{{$item->price}}
                                    </td>
                                    <td class="cart_quantity" style="text-align: center;">
                                        <div class="cart_quantity_button" style="display: inline-block;margin-right:20px;">
                                            R/Gold<br/>
                                            <?php
                                                if( isset( $item->options['qty-yg'] ) ) {
                                                    $theAmount = $item->qty - (int) $item->options['qty-yg'];
                                                } else {
                                                    $theAmount = $item->qty;
                                                }
                                            ?>
                                            <a class="cart_quantity_up"
                                               href="{{url("cartIncDec?rowId=$item->rowId&increment=1")}}">
                                                + </a>
                                            <input class="cart_quantity_input" type="number" name="quantity"
                                                   value="<?=$theAmount;?>" autocomplete="off" disabled min="1" size="2" style="width:30px;">
                                            <a class="cart_quantity_down"
                                               href="{{url("cartIncDec?rowId=$item->rowId&decrease=1")}}">
                                                - </a>
                                        </div>
                                        <?php

                                            if( $item->options->eoline !== 1 ) {
                                                ?>
                                                    <div class="cart_quantity_button" style="display: inline-block;">
                                                        Y/Gold<br/>
                                                        <?php
                                                            if( isset( $item->options['qty-rg'] ) ) {
                                                                $theAmount = $item->qty - (int) $item->options['qty-rg'];
                                                            } else {
                                                                if( isset( $item->options['qty-yg'] ) ) {
                                                                    $theAmount = $item->options['qty-yg'];
                                                                } else {
                                                                    $theAmount = 0;
                                                                }
                                                            }
                                                        ?>
                                                        <a class="cart_quantity_up"
                                                           href="{{url("cartIncDec?rowId=$item->rowId&increment=1&gold=yg")}}">
                                                            + </a>
                                                        <input class="cart_quantity_input" type="number" name="quantity"
                                                               value="<?=$theAmount;?>" autocomplete="off" disabled min="1" size="2" style="width:30px;">
                                                        <a class="cart_quantity_down"
                                                           href="{{url("cartIncDec?rowId=$item->rowId&decrease=1&gold=yg")}}">
                                                            - </a>
                                                    </div>
                                                <?php
                                            }
                                        ?>
                                    </td>
                                    <td class="cart_total">
                                        £{{$item->subtotal}}
                                    </td>
                                    <td class="cart_delete"><p>
                                            <a class="cart_quantity_delete"
                                               href="{{url("cartDelete?rowId=$item->rowId&delete=1")}}">X</a>
                                        </p>
                                    </td>
                                </tr>
                            @endforeach
                            @else
                                <p>You have no items in the shopping cart</p>
                            @endif
                            </tbody>
                        </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="row">



                    <div class="col-md-6 col-md-offset-6 col-xs-12 col-sm-12">

                        <div id="carttotal">

                            <div class="col-xs-8 col-md-5 col-md-offset-5 col-sm-4 col-sm-offset-6">Basket total:</div>
                            <div class="col-xs-4 col-md-2 col-sm-2"style="text-align: right"><span>£{{ number_format($subtotal, 2, '.', '') }}</span></div>

                            <div class="col-xs-8 col-md-5 col-md-offset-5 col-sm-4 col-sm-offset-6">Post & Packing:</div>
                            <div class="col-xs-4 col-md-2 col-sm-2"style="text-align: right"><span>{{ number_format($postamt, 2, '.', '') }}</span></div>

                            <div class="col-xs-8 col-md-5 col-md-offset-5 col-sm-4 col-sm-offset-6">VAT:</div>
                            <div class="col-xs-4 col-md-2 col-sm-2"style="text-align: right"><span>£{{   number_format($tax, 2, '.', '')   }}</span></div>

                            <div class="col-xs-8 col-md-5 col-md-offset-5 col-sm-4 col-sm-offset-6">Total:</div>
                            <div class="col-xs-4 col-md-2 col-sm-2"style="text-align: right"><span>£{{   number_format($total, 2, '.', '')  }}</span></div>

                        </div>


                        <div style="margin-top:10px;text-align: right"
                             class="col-xs-12 col-lg-7 col-lg-offset-5 col-md-8 col-md-offset-4 col-sm-6 col-sm-offset-6">
                            <a class="btn btn-default update" href="{{url('clear-cart')}}">Clear Cart</a>
                            <a class="btn btn-default green" href="{{url('checkout')}}">Check Out</a>
                        </div>

                    </div>

                </div>
            </div>
        </div>

        <div class="push"></div>
        <div class="push"></div>

    </div>



@stop