<!DOCTYPE html>
<html lang="en-US">
<head>
	<meta charset="utf-8">
</head>
<body>
<h2>Contact via website</h2>

<div>
	The following message was sent:<br/>
</div>
<div>{{ $detail }}</div>
<div><br/><br/>
	Message from:<br/>{{ $name}}<br/>
	Email entered: {{ $email}}<br/>
	Phone entered: {{ $phone}}<br/>
</div>
</body>
</html>