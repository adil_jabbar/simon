<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <style>
        .col1 {
            margin-right: 20px;
            width: 50%;

            padding: 5px;
        }

        .col2 {
            width: 50%;

            padding: 5px;
        }

        td {
            border: 1px solid #fff;
        }
    </style>
</head>
<body>
<h2>A user has recommend a business for Little Green Book</h2>


<table>
    <thead>
    </thead>
    <tbody>
    <tr>
        <td class="col1"><p>Business Name</p></td>
        <td class="col2"> {{ $businessname }} </td>
    </tr>
    <tr>
        <td class="col1"><p>Type of Business? </p></td>
        <td class="col2"> {{ $businesstype }} </td>
    </tr>
    <tr>
        <td class="col1"><p>Name and address of business </p></td>
        <td class="col2"> {{ $nameaddress }} </td>
    </tr>
    <tr>
        <td class="col1"><p>Business E-mail address </p></td>
        <td class="col2"> {{ $email }} </td>
    </tr>
    <tr>
        <td class="col1"><p>Postcode</p></td>
        <td class="col2"> {{ $postcode }} </td>
    </tr>
    <tr>
        <td class="col1"><p>Name of Contact</p></td>
        <td class="col2"> {{$contactname}} </td>
    </tr>
    <tr>
        <td class="col1"><p>Telephone Number</p></td>
        <td class="col2"> {{$telephone}} </td>
    </tr>
    <tr>
        <td class="col1"><p>Web address</p></td>
        <td class="col2"> {{ $url }} </td>
    </tr>
    <tr>
        <td class="col1"><p>Your Name</p></td>
        <td class="col2"> {{ $yourname}} </td>
    </tr>
    <tr>
        <td class="col1"><p>Your Email</p></td>
        <td class="col2"> {{ $youremail }} </td>
    </tr>
    <tr>
        <td class="col1"><p>Comments</p></td>
        <td class="col2"> {{ $comments }} </td>
    </tr>
    </tbody>
</table>

</body>
</html>