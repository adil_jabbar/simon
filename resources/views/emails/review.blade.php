<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Review via Little Green Book website</h2>

<div>
    The following review was sent:<br/>
</div>
<div>{{ $detail }}</div>
<div><br/><br/>
    Message from:<br/>{{ $name}}<br/>
    Email entered: {{ $email}}<br/>
    Phone entered: {{ $phone}}<br/>
    Business Name: {{ $business_name}}<br/>
</div>
</body>
</html>