<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <style>
        .col1
        {
            margin-right:20px;
            width:50%;

            padding:5px;
        }
        .col2
        {
            width:50%;

            padding:5px;
        }
        td {
            border:1px solid #fff;
        }
    </style>
</head>
<body>
<h2>A user has requested entry into the Little Green Book</h2>

<div>
    <table>
        <thead></thead>
        <tbody>
        <tr>
            <td class="col1">

                <p>How much do you want to spend per entry?
                    </p>

            </td>
            <td class="col2">
                {{ $amount }}
            </td>
        </tr>
        <tr>
            <td class="col1">

                    <p>Which book(s) do you want to go in?
                        </p>

            </td>
            <td class="col2">
                {{ $towns }}
            </td>
        </tr>

        <tr>
            <td class="col1">

                    <p>What category do you want to go under?
                        </p>

            </td>
            <td class="col2">
               {{ $category }}
            </td>
        </tr>

        <tr>
            <td class="col1">

                <p>How long have you been established?
                </p>

            </td>
            <td class="col2">
                {{ $established }}
            </td>
        </tr>

        <tr>
            <td class="col1">

                    <p>Name of business?
                        (including any trading style)</p>

            </td>
            <td class="col2">
                {{ $businessname }}
            </td>
        </tr>
        <tr>
            <td class="col1">

                    <p>Who is the name of main contact?</p>

            </td>
            <td class="col2">
                {{$contactname}}
            </td>
        </tr>
        <tr>
            <td class="col1">

                    <p>Landline Telephone Number</p>

            </td>
            <td class="col2">
                {{$landline}}
            </td>
        </tr>
        <tr>
            <td class="col1">

                    <p>Mobile Telephone Number</p>

            </td>
            <td class="col2">
                {{ $mobile }}
            </td>
        </tr>
        <tr>
            <td class="col1">

                    <p>Address</p>

            </td>
            <td class="col2">
               {{ $address}}
            </td>
        </tr>

        <tr>
            <td class="col1">

                    <p>Email</p>

            </td>
            <td class="col2">
                {{ $email }}
            </td>
        </tr>

        <tr>
            <td class="col1">

                    <p>Web address</p>

            </td>
            <td class="col2">
                {{ $url }}
            </td>
        </tr>
        <tr>
            <td class="col1">

                    <p>Insurance, Certification, Kitemark details</p>

            </td>
            <td class="col2">
                {{ $insurance }}
            </td>
        </tr>
        <tr>
            <td class="col1">
                <label for="other">
                    <p>Additional
                        information relevant to your business </p>
                </label>
            </td>
            <td class="col2">
                {{ $other }}
            </td>
        </tr>

        <tr>
            <td class="col1">
                <label for="offers">
                    <p>Any offers or discounts </p>
                </label>
            </td>
            <td class="col2">
                {{ $offers }}
            </td>
        </tr>
        <tr>
            <td class="col1">
                <label for="reference">
                    <p>3 to 4 recent clients
                        </p>
                </label>
            </td>
            <td class="col2">
               {{ $reference }}
            </td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>