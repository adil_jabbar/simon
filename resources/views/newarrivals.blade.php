@extends('layouts.master')

@section('content')




    <div class="container-fluid container-inset">


        <div class="wrapper">

            <div id="your-carousel-id" class="sky-carousel">
                <div class="sky-carousel-wrapper">
                    <ul class="sky-carousel-container">

                        {{--@foreach($newarrivals as $p)--}}
                        {{--@if ($p->category_id == 7)--}}

                        {{--@if (file_exists('images/products/'. $p->image ))--}}
                        {{--<li><a href="/product/{{ $p->slug }}"><img src="/images/products/{{ $p->image }}"--}}
                        {{--alt=""/></a></li>--}}
                        {{--@endif--}}
                        {{--@else--}}
                        {{--@foreach($p->subproduct as $s)--}}

                        {{--@if (file_exists('images/products/'. $p->subproduct->image ))--}}
                        {{--@if ($s->newarrival == 1)--}}
                        {{--<li><h6 id="sku">{{ $p->sku }}</h6>--}}
                        {{--<a href="/product/{{ $p->slug }}"><img class="carouselImg"--}}
                        {{--src="/images/products/{{ $s->image }}"--}}
                        {{--alt=""/></a></li>--}}
                        {{--@endif--}}
                        {{--@endforeach--}}
                        {{--@endif--}}


                        {{--@endforeach--}}


                        @foreach ($newarrivals as $key => $row)
                            <li><h6 id="sku">{{ $row['sku']  }}</h6>
                                <a href="/product/{{ $row['slug'] }}"><img class="carouselImg"
                                                                           src="/images/products/{{ $row['image'] }}"
                                                                           alt=""/></a></li>
                        @endforeach

                    </ul>
                </div>
            </div>
            <div class="push"></div>
        </div>

        <script> $(document).ready(function () {


                if ($(window).width() < 767) {
                    $w = 300;
                    $h = 300
                } else {
                    $w = 450;
                    $h = 450
                }
                $(function () {
                    $('#your-carousel-id').carousel({
                        itemWidth: $w, // The width of your images.
                        itemHeight: $h, // The height of your images.
                        navigationButtonsVisible: true,
                        distance: 30,
//                        startIndex:20,
                        selectByClick: false,
                        enableMouseWheel: false
                    });
                });

            });
        </script>


    </div>


@stop