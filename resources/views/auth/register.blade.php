@extends('applogin')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Register</div>

				<h1 id="register">Please enter your details below to register. Once your request has been authorised, we will send you an email to let you know you can login.</h1>

				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/register') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label class="col-md-4 control-label">Name</label>
							<div class="col-md-6">
								<input required aria-required="true" type="text" class="form-control" name="name" value="{{ old('name') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">E-Mail Address</label>
							<div class="col-md-6">
								<input required aria-required="true" type="email" class="form-control" name="email" value="{{ old('email') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Company Name</label>
							<div class="col-md-6">
								<input required aria-required="true" type="text" class="form-control" name="username" value="{{ old('username') }}">
							</div>
						</div>


						<div class="form-group">
							<label class="col-md-4 control-label">Password</label>
							<div class="col-md-6">
								<input required aria-required="true" type="password" class="form-control" name="password">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Confirm Password</label>
							<div class="col-md-6">
								<input required aria-required="true" type="password" class="form-control" name="password_confirmation">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Address 1</label>
							<div class="col-md-6">
								<input required aria-required="true" type="text" class="form-control" name="address1" value="{{ old('address1') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Address 2</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="address2" value="{{ old('address2') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Town/City</label>
							<div class="col-md-6">
								<input required aria-required="true" type="text" class="form-control" name="town" value="{{ old('town') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">County</label>
							<div class="col-md-6">
								<input required aria-required="true" type="text" class="form-control" name="county" value="{{ old('county') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Postcode</label>
							<div class="col-md-6">
								<input required aria-required="true" type="text" class="form-control" name="postcode" value="{{ old('postcode') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Telephone</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="phone" value="{{ old('phone') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">VAT number</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="vat" value="{{ old('vat') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Company Number</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="companyID" value="{{ old('companyID') }}">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Register
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
