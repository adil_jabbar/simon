@extends('applogin')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit <strong>{{ $user->username }}</strong> profile</div>


                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        {{--<form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/edit') }}">--}}

                            {!! Form::model($user, ['action' => 'ProfileController@update', 'class' => 'editprofile']) !!}

                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group">
                                <label class="col-md-4 control-label">Username</label>
                                <div class="col-md-6">

                                    {!! Form::text('username', null, ['class' => 'form-control']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Your Name</label>
                                <div class="col-md-6">

                                    {!! Form::text('name2', null, ['class' => 'form-control']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">E-Mail Address</label>
                                <div class="col-md-6">
                                    {!! Form::text('email', null, ['class' => 'form-control']) !!}
                                </div>
                            </div>

                            {{--<div class="form-group">--}}
                                {{--<label class="col-md-4 control-label">Company Name</label>--}}
                                {{--<div class="col-md-6">--}}
                                    {{--<input required aria-required="true" type="text" class="form-control" name="username" value="{{ old('username') }}">--}}
                                    {{--{!! Form::text('username', null, ['class'=>'form-control'])!!}--}}
                                {{--</div>--}}
                            {{--</div>--}}


                            <div class="form-group">
                                <label class="col-md-4 control-label">Password (optional)</label>
                                <div class="col-md-6">
                                    <input  type="password" class="form-control" name="password">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Confirm Password</label>
                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password_confirmation">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Address 1</label>
                                <div class="col-md-6">
                                    {!! Form::text('address[address1]', null, ['class' => 'form-control']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Address 2 (optional)</label>
                                <div class="col-md-6">
                                    {!! Form::text('address[address2]', null, ['class' => 'form-control']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Town/City</label>
                                <div class="col-md-6">
                                    {!! Form::text('address[town]', null, ['class' => 'form-control']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">County</label>
                                <div class="col-md-6">
                                    {!! Form::text('address[county]', null, ['class' => 'form-control']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Postcode</label>
                                <div class="col-md-6">
                                    {!! Form::text('address[postcode]', null, ['class' => 'form-control']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Telephone</label>
                                <div class="col-md-6">
                                    {!! Form::text('address[phone]', null, ['class' => 'form-control']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">VAT number (optional)</label>
                                <div class="col-md-6">
                                    {!! Form::text('address[vat]', null, ['class' => 'form-control']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Company Number (optional)</label>
                                <div class="col-md-6">
                                    {!! Form::text('address[companyID]', null, ['class' => 'form-control']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Update
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
