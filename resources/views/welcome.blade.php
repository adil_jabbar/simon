{{--<!DOCTYPE html>--}}
{{--<html>--}}
{{--<head>--}}
    {{--<title>Simon Alexander</title>--}}

    {{--<style>--}}
        {{--html {--}}
            {{--background: url('assets/images/background.jpg') no-repeat center center fixed;--}}
            {{---webkit-background-size: cover;--}}
            {{---moz-background-size: cover;--}}
            {{---o-background-size: cover;--}}
            {{--background-size: cover;--}}
        {{--}--}}
        {{--@media (min-width: 380px) {--}}
            {{--#page-wrap { width: 250px; margin: 90% auto; }--}}

        {{--}--}}

        {{--#page-wrap a {color:white;font-size:38px;}--}}
        {{--@media (min-width: 768px) {--}}
            {{--#page-wrap { width: 250px; margin: 70% auto; }--}}

        {{--}--}}
        {{--@media (min-width: 1024px) {--}}
            {{--#page-wrap {--}}
                {{--width: 250px;--}}
                {{--margin: 40% auto;--}}
            {{--}--}}
        {{--}--}}
        {{--@media (min-width: 1200px) {--}}
            {{--#page-wrap {--}}
                {{--width: 250px;--}}
                {{--margin: 25% auto;--}}
            {{--}--}}
        {{--}--}}


    {{--</style>--}}


{{--</head>--}}
{{--<body>--}}

{{--<div id="page-wrap">--}}
    {{--<a href="">Login / Register</a>--}}
{{--</div>--}}


{{--</body>--}}
{{--</html>--}}


<!DOCTYPE html>
<html>
<head>
<title>Simon Alexander</title>

<style>
    html, body {
        height: 100%;
        background-color:#324c71;
    }
    .outer-wrapper {
        background: url("assets/images/background.jpg") no-repeat center center;
        background-size: 100% auto;
        max-height: 1200px;
        height: 100%;
    }

    .inner-wrapper {
        display: table;
        width: 100%;
        height: 100%;
    }

    .header-wrapper {
        display: table-cell;
        text-align: center;
        vertical-align: middle;
        font-family: Verdana, Geneva, sans-serif;
        font-size: 16px;
        font-style: normal;
        font-weight: 500;
        line-height: 26px;
        color:white;
    }

    a {
        font-family: Verdana, Geneva, sans-serif;
        font-size: 24px;
        font-style: normal;
        font-variant: normal;
        font-weight: 500;
        line-height: 26px;
        color:white;
        text-transform: uppercase;
        text-decoration: none;

    }
</style>
</head>
<body>


<div class="outer-wrapper">
    <div class="inner-wrapper">
        <div class="header-wrapper">
            <a href="{{ url('/auth/register') }}">REGISTER</a> / <a href="{{ URL::route('admin.manage.index') }}">LOGIN</a>
        </div>
    </div>
</div>
</body>
</html>


