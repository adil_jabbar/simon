@extends('app')

@section('title', 'Products')

@section('content')
    <div id="rightWrapper">
        <div style = "text-align: center; font-family: sans-serif; font-weight: bold;" >
    You're connected! Please wait...
        </div>
</div>

    <script type="text/javascript">
        window.opener.location.reload(false);
        window.setTimeout('window.close()', 2000);
    </script>
@stop
