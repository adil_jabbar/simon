@extends('layouts.master')

@section('seo')

    <meta name="description" content="Contact Simon Alexander">
    <title>Contact Simon Alexander</title>

@endsection

@section('content')

    <div>


        <div class="container">

            <div class="row">

                <div class="col-sm-8 col-sm-offset-2">

                    <h1>Contact Us</h1>

                    <div class="row">

                        <div class="col-sm-4">

                            <h3>Office Hours</h3>

                            <p>Mon-Sat: 9.30am - 5.00pm<br>

                                </p>

                        </div>

                        <div class="col-sm-4">

                            <h3>Telephone</h3>

                            <p><strong>From the UK</strong><br>
                                01395 578373</p>

                            <p><strong>From abroad</strong><br>
                                +00441395 578373</p>

                        </div>

                        <div class="col-sm-4">

                            <h3>Postal Address</h3>

                            <p>Simon Alexander(UK) Ltd<br>

                                Banwells<br>
                                Old Fore Street<br>
                                Sidmouth<br>
                                Devon<br>
                                EX10 8LP<br>
                                UK</p>

                        </div>

                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2">

                            <h2 class="text-center">Email Us</h2>

                            <form id="enquiry-form" class="form-horizontal" action="" method="post">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                <input type="hidden" name="company" value="{{ config('api.company') }}" />
                                <div class="form-group">
                                    <label for="name" class="col-sm-3 control-label">Your Name</label>
                                    <div class="col-sm-9">
                                        <input type="text" required="required" class="form-control" id="name" name="name" placeholder="Your Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-sm-3 control-label">Email Address</label>
                                    <div class="col-sm-9">
                                        <input type="email" required="required" class="form-control" id="email" name="email" placeholder="email@domain.tld">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="message" class="col-sm-3 control-label">Your Message</label>
                                    <div class="col-sm-9">
                                        <textarea required="required" class="form-control" rows="3" id="comment" name="comment" placeholder="Your message"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9 text-right">
                                        <button type="submit" class="btn btn-primary">Send Enquiry</button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>

                </div>




            </div>

        </div>

    </div>

@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#enquiry-form').on('submit', function (e) {
                e.preventDefault();
                var form = $(this);
                $.ajax({
                            type: 'POST',
                            url: '/enquiry',
                            data: $(form).serialize()
                        })
                        .done(function (response) {
                            if (response.success) {
                                $(form).replaceWith('<div class="alert alert-success text-center">Thank you for your enquiry.</div>');
                                adjustAffixes();
                            } else {
                                $(form).before('<div class="alert alert-danger text-center">There was an error, please try again.</div>');
                                window.setTimeout(function () {
                                    $('.alert-danger').fadeOut();
                                }, 2000);
                            }
                        });
            });
        });
    </script>
@endsection