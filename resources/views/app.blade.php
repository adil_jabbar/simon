<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Laravel</title>

	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">
	{{--<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>--}}

	@include('admin._partials.assets')
	<script src="{{ URL::asset('ckeditor/ckeditor.js') }}"></script>
	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
	<link href="{{ asset('/assets/css/adminmain.css')}}" rel="stylesheet" type="text/css" />



	{{--<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->--}}
	{{--<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->--}}
	{{--<!--[if lt IE 9]>--}}
		{{--<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>--}}
		{{--<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>--}}
	{{--<![endif]-->--}}
</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">Simon Alexander</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				{{--<ul class="nav navbar-nav">--}}
					{{--<li><a href="{{ url('/') }}">Home</a></li>--}}
				{{--</ul>--}}

				<ul class="nav navbar-nav navbar-right">
					@if (Auth::guest())
						<li><a href="{{ url('/auth/login') }}">Login</a></li>
						<li><a href="{{ url('/auth/register') }}">Register</a></li>


					@else

						{{--<li class="dropdown">--}}
							{{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>--}}
							{{--<ul class="dropdown-menu" role="menu">--}}
								{{--<li><a href="{{ url('/auth/logout') }}">Logout</a></li>--}}
							{{--</ul>--}}
						{{--</li>--}}
						<li class="dropdown">

								<a href="{{ url('/auth/logout') }}">Logout</a>

						</li>
						<li class="dropdown">

							<a href="{{ url('/') }}">View Website</a>

						</li>
					@endif
				</ul>
			</div>
		</div>
	</nav>


	@yield('content')
		</div>
	</div>
	<!-- Scripts -->
	{{--<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>--}}
	{{--<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>--}}
	<script>

		$(function() {
			$("#fullPage").click(function() {
				$("#rightWrapper").toggleClass("full-page");
				$("#header").toggleClass("full-page");
			});
		})

		$(function() {
			$("#listView li").click(function () {
				if ( $("#listView li").hasClass("list-item-active") ) {
					$("#listView li").removeClass("list-item-active");
				}
				$(this).addClass("list-item-active");
			});
		});


	</script>
</body>
</html>
