@extends('layouts.master')

@section('content')


    <div class="container-fluid container-inset wrapper">


        <div class="row">

            <div class="col-md-12 col-xs-12 col-sm-12">
                <div>Delivery Address:</div><br/>
                <p>{{ $user->address->companyName }}<br/>
                {{ $user->address->address1 }}<br/>
                @if ($user->address->address2){{ $user->address->address2 }}<br/>@endif
                {{ $user->address->town }}<br/>{{ $user->address->county }}<br/>
                {{ $user->address->postcode }}</p>

                <p>Your order</p>
                <section id="cart_items">
                    <div class="container">

                        <div class="table-responsive cart_info">
                            @if(count($cart))
                                <table class="table table-condensed">
                                    <thead>
                                    <tr class="cart_menu">
                                        <td class="image">Item</td>
                                        <td class="description"></td>
                                        <td class="price">Price</td>
                                        <td class="quantity">Quantity</td>
                                        <td class="total">Total</td>
                                        <td></td>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($cart as $item)
                                        <tr>
                                            <td class="cart_product">
                                                <img width="50" height="50"
                                                     src="/images/products/{{ $item->model->image }}"
                                                     alt="{{ $item->model->imageAlt }}">
                                            </td>
                                            <td class="cart_description">
                                                <h4>{{$item->name}}</h4>
                                                {{--<p>Web ID: {{$item->id}}</p>--}}
                                            </td>
                                            <td class="cart_price">
                                                <p>£{{$item->price}}</p>
                                            </td>
                                            <td class="cart_quantity">
                                                <div class="cart_quantity_button">

                                                    {{$item->qty}}

                                                </div>
                                            </td>
                                            <td class="cart_total">
                                                <p class="cart_total_price">£{{$item->subtotal}}.00</p>
                                            </td>

                                        </tr>

                                    @endforeach
                                    <tr>
                                        <td></td>
                                        <td class="cart_total"><p>Total number of items &amp; price including VAT</p></td>
                                        <td></td>
                                        <td class="cart_total"><p><span>{{Cart::count()}}</span></p></td>
                                        <td class="cart_total"><p><span>£{{Cart::total()}}</span></p></td>
                                    </tr>
                                    @else
                                        <p>You have no items in the shopping cart</p>
                                    @endif
                                    </tbody>
                                </table>
                        </div>
                    </div>
                </section> <!--/#cart_items-->

                <section id="do_action">
                    <div class="container">

                        <div class="rowx">

                            <div class="col-sm-12">
                                <div id="complete_order_area" class="pull-right">

                                    <a class="btn btn-default update" href="{{url('clear-cart')}}">Cancel Order</a>
                                    <a class="btn btn-default green" href="{{url('complete')}}">Complete Order</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section><!--/#do_action-->


            </div>

            <div class="push"></div>
        </div>

    </div>


@stop