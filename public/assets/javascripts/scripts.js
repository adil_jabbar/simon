var currentDate = new Date();
var nextJuly;
if (currentDate.getMonth() < 8) {
    nextJuly = new Date(currentDate.getFullYear(), 6, 1);
} else {
    nextJuly = new Date(currentDate.getFullYear() + 1, 6, 1);
}

$(document).ready(function () {

    // Smooth scroll all anchor links + not bootstrap data toggles
    $('a[href*=#]:not([href=#])').not('a[data-toggle]').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - $('.navbar-fixed-top').outerHeight(true) - 20
                }, 1000);
                return false;
            }
        }
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var href = $(this).attr('href');
        $('html, body').animate({
            scrollTop: $(href).offset().top - $('.navbar-fixed-top').outerHeight(true) - 20
        }, 1000);
    });

    // Scroll watcher for shrunk header
    $(window).scroll(function() {
        if ($(this).scrollTop() > 60) {
            $('#header').addClass("shrunk");
        }
        else {
            $('#header').removeClass("shrunk");
        }
    });

    // Generic responsive slick carousel
    $('.slick').slick({
        slidesToShow: 3,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1
                }
            }
        ]

    });

    $('.home-page-feature-link').click(function() {
        var featureId = $(this).data('featureId');
        $('.home-page-feature-link').removeClass('active');
        $(this).addClass('active');
        $('.home-page-feature').fadeOut();
        $('#home-page-feature-'+featureId).fadeIn(400, function(){
            $('.slick', '#home-page-feature-'+featureId).slick('setPosition');
        });
    });


    // Arrival calendar datepicker
    $('#arrival-calendar').datepicker({
        minDate: +1,
        maxDate: "+2Y",
        defaultDate: nextJuly,
        altField: "#arrival-date",
        altFormat: "yy-mm-dd",
        dateFormat: "d MM, yy",
        beforeShowDay: function(date) {
            return [date.getDay() == 6];
        }
    });
    var arrivalDate;
    if (arrivalDate = $('#arrival-date').val()) $('#arrival-calendar').datepicker("setDate", new Date(arrivalDate));

    // Search options
    $('#toggle-more-options').click(function() {
        $('#more-options').slideToggle();
        var hide = 'Hide Options <span class="glyphicon glyphicon-th-list"></span>';
        var show = 'More Options <span class="glyphicon glyphicon-th-list"></span>';
        $(this).html(function(i, html){
            return html === hide ? show : hide;
        });
    });
    $('#more-options label').click(function() {
        $(this).toggleClass('checked');
        var input = $('input', this);
        if($(input).prop('checked')) {
            $(input).prop('checked', false);
        } else {
            $(input).prop('checked', true);
        }
        return false;
    });

    // Map column height fill
    $('#map-col').height($('#map-col').parent().height());


    // Property details image cycles
    var slideshows = $('.cycle-slideshow').on('cycle-next cycle-prev', function(e, opts) {
        // advance the other slideshow
        slideshows.not(this).cycle('goto', opts.currSlide);
    });

    $('.property-thumbs-cycle .cycle-slide').click(function(){
        var index = $('.property-thumbs-cycle:visible').data('cycle.API').getSlideIndex(this);
        slideshows.cycle('goto', index);
    });


    // Favourites functionality
    $('#favourites-properties > .container > .row').append(localStorage['favourites']);
    $('#favourites-number').html($('.favourite-box').length);

    var number = $('.favourite-box').length;
    if (number > 0) {
        $('#favourites').removeClass('empty');
    }

    $('#toggle-favourites').click(function() {
        $(this).toggleClass('glyphicon-menu-down');
        $(this).toggleClass('glyphicon-menu-up');
        $('#favourites-properties').slideToggle(400, function(){
            $('#favourites').toggleClass('closed');
        });
    });

    $('.add-to-favourites').click(function(e) {
        e.preventDefault();
        var slug = $(this).data('slug');
        var type = $(this).data('type');
        $.ajax({
            type: 'GET',
            url: '/api/'+type+'/favourite/'+slug
        })
        .done(function(response) {
            $('#favourites-properties > .container > .row').append(response);
            saveFavourites();
            $('#favourites-properties').slideDown();
            $('#toggle-favourites').addClass('glyphicon-menu-down').removeClass('glyphicon-menu-up');
            $('#favourites').removeClass('closed');
        });
    });

    $('#favourites').on('click', '.favourite-remove', function (e){
        e.preventDefault();
        $(this).closest('.favourite-box').remove();
        saveFavourites();
    });

    $("#quick-search").select2({
        placeholder: "Search",
        minimumInputLength: 2,
        dropdownCss: { 'width': '300px' }
    });
    $("#quick-search").on('change', function() {
        var url = $(this).val();
        window.location.href = url;
    });

    $('#enquiry-form').on('submit', function(e) {
        e.preventDefault();
        var form = $(this);

        $('input[name="date"]', form).val($('#arrive_date').val());
        $('input[name="duration"]', form).val($('#staying_for').val());
        $('input[name="property_id"]', form).val($('#property').val());

        $.ajax({
                type: 'POST',
                url: '/enquiry',
                data: $(form).serialize()
            })
            .done(function(response) {
                if (response.success) {
                    $(form).replaceWith('<div class="alert alert-success text-center">Thank you for your enquiry.</div>');
                    adjustAffixes();
                } else {
                    $(form).before('<div class="alert alert-danger text-center">There was an error, please try again.</div>');
                    window.setTimeout(function () {
                        $('.alert-danger').fadeOut();
                    }, 2000	);
                }
            });
    });

    $('#sort-order, #sort-direction').change(function (e) {
        $('#search-bar-form').submit();
    });

    // Make sure padding is correct for fixed header
    $('body').css('padding-top', $('#main-nav').outerHeight());

});

$(window).load(function() {

    // Generic affix container
    $('.affix-container').each(function() {
        var thebottom = ($( document ).height() - ($(this).closest('.container').position().top + $(this).closest('.container').outerHeight(true)));
        $(this).affix({
            offset: {
                top: ($(this).offset().top - 63),
                bottom: thebottom
            }
        });
    });

});

function openEnquiryForm() {
    $('#enquiry-form-strip').slideDown(400, function() {
        adjustAffixes();
    });
}

function saveFavourites() {
    var number = $('.favourite-box').length;
    $('#favourites-number').html(number);
    localStorage['favourites'] = $('#favourites-properties > .container > .row').html();
    if (number == 0) {
        $('#favourites').addClass('empty');
    } else {
        $('#favourites').removeClass('empty');
    }
}


function adjustAffixes() {
    $('.affix-container').each(function() {

        var thebottom = ($( document ).height() - ($(this).closest('.container').position().top + $(this).closest('.container').outerHeight(true)));

        $(this).data('bs.affix').options.offset.bottom = thebottom;
    });
}

var mapStyles = [
    {
        "featureType": "water",
        "stylers": [
            { "color": "#2DB1F0" }
        ]
    },
    {
        "featureType": "poi",
        "stylers": [
            { "visibility": "off" }
        ]
    },
    {
        "featureType": "landscape",
        "stylers": [
            { "color": "#f0f0f0" }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
            { "color": "#efc771" }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels",
        "stylers": [
            { "visibility": "off" }
        ]
    }
];
