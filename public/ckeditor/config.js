﻿/*
Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	config.forcePasteAsPlainText = true;
	config.disableNativeTableHandles = true;
	config.scayt_sLang = 'en_GB';
	config.disableNativeSpellChecker = false;
	config.scayt_autoStartup = true;
	config.toolbar = 'MyToolbar';
	config.toolbar_MyToolbar =
	[
		
		{ name: 'editing', items : [ 'Source', 'FontSize', 'TextColor','BGColor','Find','Replace','-','SelectAll','-','Scayt','-','Outdent','Indent','-','Copy','Paste' ,'-','SpellChecker','-','Bold','Italic','Strike','-','RemoveFormat','BulletedList', '-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock', 'Link','Unlink','Image'] },
		{ name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
		{ name: 'basicstyles', items : [ 'Bold','Italic','Strike','-','RemoveFormat' ] },

	];
    //config['filebrowserBrowseUrl'] = '/ckfinder/ckfinder.html';
    //config['filebrowserImageBrowseUrl'] = '/ckfinder/ckfinder.html?type=Images';
    //config['filebrowserUploadUrl'] = '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
    //config['filebrowserImageUploadUrl'] = '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';

    config.filebrowserBrowseUrl = '/kcfinder/browse.php?opener=ckeditor&type=files';
    config.filebrowserImageBrowseUrl = '/kcfinder/browse.php?opener=ckeditor&type=images';
    config.filebrowserFlashBrowseUrl = '/kcfinder/browse.php?opener=ckeditor&type=flash';
    config.filebrowserUploadUrl = '/kcfinder/upload.php?opener=ckeditor&type=files';
    config.filebrowserImageUploadUrl = '/kcfinder/upload.php?opener=ckeditor&type=images';
    config.filebrowserFlashUploadUrl = '/kcfinder/upload.php?opener=ckeditor&type=flash';



};


//CKEDITOR.replace( 'editor1',
//    {
//       filebrowserBrowseUrl : '/ckfinder/ckfinder.html',
//        filebrowserImageBrowseUrl : '/ckfinder/ckfinder.html?Type=Images',
//        filebrowserFlashBrowseUrl : '/ckfinder/ckfinder.html?Type=Flash',
//        filebrowserUploadUrl : '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
//        filebrowserImageUploadUrl : '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
//        filebrowserFlashUploadUrl : '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
//    });



//config.filebrowserUploadUrl = '../WA_iRite/editor/filemanager/connectors/php/upload.php';
	//config.filebrowserImageUploadUrl = '../WA_iRite/editor/filemanager/connectors/php/upload.php?Type=Image';
	//config.filebrowserImageBrowseUrl = '../WA_iRite/editor/filemanager/browser/default/browser.html?//Type=Image&Connector=http://localhost/hours4work.com//WA_iRite/editor/filemanager/connectors/php/connector.php';
	//config.filebrowserImageBrowseLinkUrl = 'http://localhost/hours4work.com/WA_iRite/editor/filemanager/browser/default/browser.html?//Connector=../WA_iRite/editor/filemanager/connectors/php/connector.php';
//config.toolbar_Full =
//[
//	{ name: 'document', items : [ 'Source','-','Save','NewPage','DocProps','Preview','Print','-','Templates' ] },
//	{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
//	{ name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
//	{ name: 'forms', items : [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 
//        'HiddenField' ] },
//	'/',
//	{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
//	{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv',
//	'-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
//	{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },
//	{ name: 'insert', items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ] },
//	'/',
//	{ name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
//	{ name: 'colors', items : [ 'TextColor','BGColor' ] },
//	{ name: 'tools', items : [ 'Maximize', 'ShowBlocks','-','About' ] }
//];
// 